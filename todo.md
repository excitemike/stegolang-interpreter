
# To Do

- Use single quoted strings. Single characters can be denoted by c'A'.
- backticks to allow more characters in identifier names. ``` `spaces and punctuation in name!` ```
- I don't think this usage of `where` clauses was described yet

  ```stegolang
  dec <CoordType> f T = ()
      where T = Point2D CoordType
  ```

- Additional data structures to include in type system or std lib. Some of these might be too esoteric to bother with for a long time.
  - Set and some special case known-size versions
    - special case imm Set
    - Fixed capacity Set
    - Set with fixed number of items. can replace, but not add/remove
  - Dict and some special case known-size versions
    - special case imm Dict
    - Fixed capacity Dict
    - Dict with fixed number of items. can replace, but not add/remove
  - multi-key Dict and known-size variations
  - multi-value Dict (multimap) and known-size variations
  - multi-key, multi-value, Dict and known-size variations
  - Bimap
  - immutable string type
  - mutable string type with fixed-capacity and fixed-size versions
  - List and special case known-size versions
    - imm List
    - fixed capacity
    - fixed length
  - Option (same as fixed-capacity-of-one list?)
    - mutable-in-place Option = List with fixed length of 1
  - Result
  - Named Tuple. Do some magic so that it is both a subtype of the corresponding structure and the corresponding tuple, so that it can be passed around, pattern matched, and destuctured as either.

      ```stegolang
      dec T = namedtuple
        name1 = type
        name2 = type
      let x = namedtuple
        name1 = value
        name2 = value
      ```

- document stegolang idioms like you see at <https://www.programming-idioms.org> (example: <https://www.programming-idioms.org/cheatsheet/Rust>)
- fix error message for cases like `print "x" + "*"` (resulting in a Nothing + a String)
- visibility modifiers on structs, typeclasses
- do a type erasure pass on AST
  - i.e. fields, indices become memory offsets
- parse to bytecode
- parse to llvm ir
- parse and JIT
- parse and AOT compile
- I think I'm doing some UnicodeSegmentation::graphemes where grapheme_indices would be preferable
- multiline input in repl
- english words for bitwise and comparison operations
- <https://docs.rs/tagged-box/0.1.1/tagged_box/>
- grammar: distinguish between function application to a list and list access/slicing `vec [0]`
  - requires parens to make it application?
  - use a get method
- grammar: slicing/splicing - read or assign with range notation in the brackets?

- grammar: module statement
- grammar: namespaces (redundant w module?)
- grammar: for_expr
- grammar: type declarations
- grammar: ADTs
- grammar: structs
- grammar: loop expression
- grammar: break/continue
- grammar: assignment
- grammar: partial function application - including with infix operators
- grammar: subscripting lists, slicing
- investigate using <https://hub.darcs.net/thielema/llvm-ffi>
- Word64->Double casting using FFI example <https://github.com/nh2/reinterpret-cast/blob/master/src/Data/ReinterpretCast/Internal/ImplFFI.hs>
- read <http://www.plover.net/~pscion/Inform%207%20for%20Programmers.pdf> and revise syntax based on it?
- maybe replace `:` in dictionary thing with `to`/`=`/`->`?
- break / breakWith expr
  - static analysis error if not within the non-else block of a for/while
  - runtime: set break flag in fiber context
  - evaluate expression and store for use in else cases
- update todos below to consider continue keyword
- WhileElse: `while` *expression* *block* `else` *block*
  - 1) evaluate condition expression
  - 2) if result is true to true
    - 2.1) evaluate body expression
    - 2.2) if break flag is set, catch (clear that flag) and evaluate to breakWith value
    - 2.3) otherwise, go to 1
  - 3) otherwise, resolve to else expression
- ForElse `for` (*name* | *pattern*) `in` *logical_or_expr* *block* `else` *block*
  - 1) evaluate range expression
  - 2) static analysis/runtime error if not iterable
  - 3) while there is a next element i
    - 3.1) assign name/destructure with pattern (static/runtime error if it can't match!)
    - 3.2) evaluate body block
    - 3.3) if break thrown, catch and resolve to breakWith expression
    - 3.4) otherwise, return to 3
  - 4) if no break happened in above loop, resolve to else expression
- pandoc for adding LaTeX?

## Interesting Rust crates to look into using

- [SmallVec](https://lib.rs/crates/smallvec) I actually make quite a lot of small temporary vectors. They could mostly stay on the stack.
- [SmartString](https://lib.rs/crates/smartstring) most identifiers are small strings and could avoid a heap allocation.
- [AnyHow](https://lib.rs/crates/anyhow) fancier error handling/reporting
- [cute](https://lib.rs/crates/cute) comprehensions in Rust via macro
- [strum](https://lib.rs/crates/strum) a number of handy macros for working with enums and converting to/from strings
- [criterion](https://lib.rs/crates/criterion) benchmarking, measuring optimizations, and being warned of changes in performance
- [scopeguard](https://lib.rs/crates/scopeguard) RAII to run a closure when it goes out of scope even if the code panics or early returns

## Bugs

- Errors in greater than give error messages about less than. example: `() > 0`
- Error messages that talk about `Nothing` should write it out instead of printing the empty string.

## Links

- Lexical analysis in Python <https://docs.python.org/3/reference/lexical_analysis.html#f-strings>
- PEP for python pattern matching: <https://www.python.org/dev/peps/pep-0622/#id42>
- Python full grammar: <https://docs.python.org/3/reference/grammar.html>
- haskell grammar: <https://www.haskell.org/onlinereport/haskell2010/haskellch10.html>
- Inform7 book: <http://inform7.com/book/WI_1_1.html>
- Implementation/Optimization
  - Book "The Implementation of Functional Programming Languages" saved at D:\read\The-Implementation-of-Functional-Programming-Languages-1987.pdf
  - Book "Implementing functional languages: a tutorial" saved at D:\read\Implementing-Functional-Languages-a-tutorial.pdf
  - <https://www.microsoft.com/en-us/research/wp-content/uploads/1992/04/spineless-tagless-gmachine.pdf>
  - <https://www.microsoft.com/en-us/research/wp-content/uploads/2016/07/icfp2003.pdf>

## ECS Ideas

- ECS in language
  - Entity -- `Entity . new` (or `new Entity`)
  - Component
    - declare: `property <propname> <valuetype>`
    - set default: `<propname> default = <valuetype>`
    - add/update for entity: `<propname> <entity> = <valuetype>`
    - get value for entity: `<propname> <entity>`
    - remove for entity: `deletePropertyFrom <propname> <entity>`
    - check whether set for entity: `hasProperty <propname> <entity>`
  - System -- some kind of query-esque things for running code over properties
    - `procProp <propname> <function taking entity and property value>`
    - `removePropertyWhere <property> <function taking entity and property value, returning bool>`

## Language Feature ideas

- `unreachable` keyword
- `$` operator from Haskell
- `ty` statement in grammar
- `alias` statement in grammar
- `dec` statement in grammar
- `tycl` statement in grammar
- `impl` statement in grammar
- `where` clause in grammar
- traits in grammar
- automatically deriving traits when possible
- automatically deriving non-required trait methods
- accessors are themselves a type. (Assign trait) Sort of a two-directional function. Add syntax for making them on your own? Maybe like

  ```stegolang
  acc some_property self =
    get imm self = self . _backing_field
    set mut self, value = (self._backing_field) = value
  ```

  **But don't make `get` and `set` into reserved identifiers**

- Drop trait - Call `drop` method when goes out of scope
- compiling is running - built-ins for emitting syntax tree, desugar, formatted code, C, bytecode, llvm ir, compile/linking with other ir/source, or all at once to make an .exe
- compiling is done while interpreting. that is, there are directives for spitting out a {an AST representation, C code, llvm ir, exe, dll, static library, reformatted stegolang} for a given thing (usually main)
- type guards / multiple dispatch / overloading based on function signature
- JIT compile while interpreting
- Foreign functions (C) somehow...
- async/await
- somehow: async Producer/Consumer pattern should be wicked simple in this language
- type inference
- functions are first-class objects
- algebraic data types
- operator overloading, new operators can be defined
- integer overflow detected?
- spread operator
- "defer" like from golang
- tables like in inform7
- string comprehension
- list comprehensions in parens instead of [] means lazy?
- rust's Result type and question mark suffix
- reserve '$' for rust-like macros
- hypothetical traits/typeclasses I'm likely to want built-in:

  |||
  --|--
  `Eq A`                | able to be on the left side of an `==` with an `A` on the right
  `Lt A`                | able to be on the left side of an `<` with an `A` on the right
  `Ord A`               | combination of `Eq A` and `Lt A`
  `Assign`              | can be on the left side of an assignment operator `=`
  `Repr`                | has a `repr` method
  `ToStr`               | has a `to_str` method
  `FromStr`             | has a `from_str` method
  `Enum A`              | has an `iter` method returning an `Iter A`
  `Iter A`              | has a `next` method
  `Apply ...`           | can be applied like a function. (`apply` method?)
  `Into A`              | has an `into` method that consumes the item<br>and returns an `A`
  `From A`              | has a `from` function
  `Map A`               | type has an `map` function that looks like<br>`dec <B> (Map A) (fn A = B) = Self B`
  `Number`              | usable with arithmetic operators
  `Then OkType ErrType` | has a `then` method
  `Forward A`           | if a method is not found in the type, call this one's `forward` method and try again on the result of that
- word alternatives for punctuation syntax

## Implementation Ideas

Links to reference for how some other interpreters did it

- full source
  - CPython source: <https://github.com/python/cpython/>
  - V8 source: <https://github.com/v8/v8>
  - wren: <https://github.com/wren-lang/wren/>
- NaN tagging
  - wren <https://github.com/wren-lang/wren/blob/main/src/vm/wren_value.h>
- array/list
  - v8 <https://github.com/v8/v8/blob/master/src/builtins/builtins-array.cc>
  - python <https://www.laurentluce.com/posts/python-list-implementation/>
- example of calling c functions from rust <https://github.com/alexcrichton/rust-ffi-examples/tree/master/rust-to-c>
- bytecode interpreting:
  - unroll the loop+switch. example: <https://github.com/pliniker/dispatchers/blob/master/src/unrollswitch.rs>

## IDE

real-time syntax check, excellent error messages
autoformatting
breakpoint, data debugging
TeX, markdown, images, supported in comments
accessibility
ability to re-JIT live
flutter-like (<https://flutter.dev/>)

### Source Control

???

### Package Manager

???
