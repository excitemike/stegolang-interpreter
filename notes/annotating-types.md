# Annotating Types

**Note:** <span style="color:red;background:white;border:1px solid red;padding:0.25em">**The following is all hypothetical!**</span>

In a perfect world, type inference would be enough, but in order to catch errors early and to help the compiler give useful error messages, it is usually a good idea to do some explicit annotating of types.

## `dec` Keyword

Type annotations most often come in the form of a separate statement beginning with the `dec` keyword. These associate an identifier with a type. In the simplest case, you can specify the type of a variable as in this example.

```stegolang
dec n = int
let n = 0

dec s = string
let s = 0 // this would produce an error!
```

`dec` supports destructuring, which is handy for mirroring a destructuring let, for instance.

```stegolang
dec (x, y) = (int, string)
let (x, y) = some_tuple
```

## Declaring Mutability

The `dec` keyword allows you to be explicit about the mutability of things by combining it with the `imm` or `mut` keywords.

```stegolang
dec x = imm int
let x = 0
x = 1 // produces an error because x is immutable
```

## Scope

Type annotations follow scoping rules identical to how the definition of that identifier would. If a `dec` appears inside a block, that particular type annotation will be checked inside that block.

This can be used to check assumptions about the outer scope. This code produces an error because the type expected in `x` is inconsistent.

```stegolang
let x = 0
fn increment =
    dec x = int // x required x to be an int in this scope
    x = x + 1   // uses float x from outer scope
x = 1.0 // assigning a float here
```

## Shadowing

You can make the compiler check a more specific type in one block by shadowing an outer type annotation.

```stegolang
alias Result = Ok int | Err int

// This dec says that x has to be a result.
dec x = Result
let x = Ok 0
do
    dec x = Result.Ok

    // this line would produce an error because x is now required to be Result.Ok
    //x = Result.Err 0
    println x

// this line would also produce an error because it keeps type inference from
// seeing x as specifically Result.Ok. By assigning the Err variant, it has to
// be the more-general Result type. This means that inside the `do` block above,
// the value held in `x` cannot be guaranteed to be the `Result.Ok` that we 
// declared as a requirement in that block.
//x = Result.Err 0
```

### Confusing Shadowing

Type annotations can get confusing when shadowing variables and other type annotations. You could think of each `dec` adding a type filter to uses of that identifier and one `dec` can shadow another `dec`. This can be avoided by making sure you shadow `dec`s when and only when you are shadowing the value. These examples describe the weird cases you can hit though.

```stegolang
dec x = int
let x = 0

// this is ok because we haven't shadowed the first dec yet
x = 1

// shadow the dec. x is now required to be a string in this scope
dec x = string

// this would be an error because even though it meets the current dec,
// the identifier holds an int
// x = "0" 

// this would be an error because even though the identifier holds an int,
// our dec says we must treat it as a string
// x = 1

// shadow outer variable. dec and let are now in sync
let x = "1"

// similarly, if you shadow the value without also shadowing the type, you
// can encounter strange things
let x = true

// this would be an error because annotation requires it to be a string now
// x = false

// but this would also be an error because that identifier holds a bool
// x = "false"

// brings the type annotation in sync again
dec x = bool
```

## Functions

Functions tend to benefit the most from type annotations.

```stegolang
dec sum_ints int int = int
fn sum_ints a b = a + b
```

In the example above, `sum_ints` operates on two `int`s, adding them together. WE can declare this to the compiler with `dec` in a pattern mirroring what the funciton definition does with `fn`. It goes `dec`, then the function name, then for each argument, the type of that argument (in this case `int` for both of them), then an `=`, then finally the function's return type (`int` in this case).

Note that this syntax doesn't work for a nullary (takes no arguments) function, because that would look the same how you annotate a simple variable created with `let`. You can correctly annotate a nullary function with code like `dec some_nullary_fn = (fn = int)`. This style of dec will be explained more later.

```stegolang
// notice that these are described the same way in a dec

dec from_let = int
let from_let = 0

dec from_fn = int
fn from_fn = 0

// It would be an error now to use from_fn. It is a nullary function, but
// annotated as an int

// you can instead annotate it this way
dec from_fn = (fn = int)
fn from_fn = 0
```

## `()`

`()` is an empty tuple. It is the "nothing" returned by code that has no meaningful result to produce.

```stegolang
dec no_return_val n = ()
fn no_return_val n = print n
```

## Specifying Function Types

You can specify functions as a type in your dec, too! The way to refer to the type is with the `fn` keyword, which in the `dec` context is interpreted to mean that you are describing a function type rather than creating a function. You should usually enclose these in parentheses for the sake of readability. The syntax is `fn`, followed by the type of each argument, followed by an `=`, followed by the return type. For instance, `(fn int int = int)` is the type for a function that takes two `int`s and returns an `int`.

```stegolang
// function that takes a function and applies it to an int,
// then applies it to that result, too
dec apply_twice (fn int = int) int = int
fn apply_twice f n = n . f . f

apply_twice println 10 // prints "10" twice
```

It turns out that this provides an alternative way to annotate functions.

```stegolang
dec increment = (fn int = int)
fn increment n = n + 1
```

## Where Clauses

Sometimes expressing types gets complicated and it is convenient to break up a long `dec`. One way to do this is to use `where` clauses. To do this, you can use placeholder name types in the `dec` statement, but then at the end, add the keyword `where` and a comma-delimited list of what those placeholders mean.

```stegolang
dec my_operation MyInput = MyResult
    where
        MyResult = Result int int,
        MyInput = Option int
fn my_operation x = ...
```

`ty` statements similarly support `where` clauses.

```stegolang
ty MyResult = ResultType
    where
        OkType = (fn string = ())
        ErrType = string
        ResultType = Result OkType ErrType
```

## Taking Ownership

Adding `consume` to a function parameter in a dec statement signifies a passing of ownership. Any value passed there is no longer safe to be used under the old identifier.

```stegolang
ty Wrapped mut A = Wrapped A

dec <A> wrap (consume A) = Wrapped A
fn wrap x = Wrapped.Wrapped x

dec <A, B> fmap (Wrapped A) (fn A=B)
fn fmap wrapped f =
    match wrapped,
        Wrapped.Wrapped x , x . f . Wrapped.Wrapped

let a = 0
let wa = wrap a

// this would produce an error about `a` already being consumed
// a = 1
```

## Generics

Sometimes when annotating the types in your functions, variable or types, it's helpful to use a placeholder types so that it can work with many different types. To do this, you use generics and type constraints.

After the `dec`, you can optionally include, enclosed in angle brackets (`<>`) a comma-delimited list of placeholder type names.

### Examples

Here is how you could annotate a function that builds a tuple out of whatever you pass to it, without limiting what those types are.

```stegolang
dec <A, B> cons imm A imm B = (imm A, imm B)
fn cons a b = (a, b)
```

A function that reverses the elements of a 2-tuple:

```stegolang
dec <A, B> flip_2_tuple (imm A, imm B) = (imm B, imm A)
fn flip_2_tuple (a, b) = (b, a)
```

A function that makes pairs, and a function that creates pairs of pairs.

```stegolang
ty Pair A = (A, A)

dec <A> make_pair (imm A, imm A) = Pair A
fn make_pair x1 x2 = Pair (x1, x2)

dec <A> make_pair_of_pairs (imm A, imm A, imm A, imm A) = Pair (Pair A)
fn make_pair_of_pairs x1 x2 = Pair (Pair ()) (Pair ())
```

`ty` statements can also be generic, by the way. They just don't need the angle brackets. For example here is a type that represents zero or two of whatever type is sent, but requires they both be of the same type.

```stegolang
ty ZeroOrTwo a = Zero | Two a a
```

## Requiring Traits

You can use a trait (like `Eq`) as the type requirement in a `where` clause.

```stegolang
// require that type argument `a` implement the `Eq a` trait
// though type inference would hopefully get that from the use of `==` anyway
dec <A> all_eq imm A imm A imm A = Bool
    where A = Eq A
fn all_eq a b c = (a == b) && (b == c)
```

You can combine type requirements with an `&`.

```stegolang
// require that the type be comparable and printable
dec <A, B> print_lower imm A imm B = Nothing
    where A = Lt B & ToStr
fn print_lower a b =
    (if a < b then a else b)
        . to_str . print
```

You can actually do this without Generic notation if you don't need a name for the type and the generic parameters should be inferred.

```stegolang
dec print_lower imm A imm B = Nothing
    where
        A = Lt B & ToStr,
fn print_lower a b =
    (if a < b then a else b)
        . to_str . print
```
