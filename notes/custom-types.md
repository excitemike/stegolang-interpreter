# Custom Types

**Note:** <span style="color:red;background:white;border:1px solid red;padding:0.25em">**The following is all hypothetical!**</span>

Creating custom types is done with the `ty` keyword. These statements begin with `ty`, a name, optional type arguments, and then an equals sign.

What follows the equals sign determines whether the type is an alias for a struct, an alias for a tuple, or a new ADT.

In all those cases, the type name in placed in scope for referring to later and can be used like a function, with any type arguments as it's arguments to construct a type object or act as a namespace for its accessor and constructor functions.

A type can function as both a constructor function and as a namespace.

## Enum Types

Using the `enum` keyword, you can create a type that consists of a number of specifically listed options.

```stegolang
// you can associate names with values like a namespace
enum ColorChoice =
    RED = "red"
    GREEN = "green"
    BLUE = "blue"
print (ColorChoice.RED)
    
// You can have values without names, for instance to document limited accepted values to a function
enum ColorChoice =
    "red"
    "green"
    "blue"
dec get_channel RgbColor ColorChoice = Byte

// You can have names without values, and values will be chosen for you
enum ColorChoice =
    RED
    GREEN
    BLUE
print (ColorChoice.RED)

// as long as all the provided values are integral, you can freely mix the three within one enum
enum ChannelNum =
    RED = 0
    GREEN
    2
    
// creates a new type that takes no space at runtime, but is still distinct from the unit type in the type system
enum Empty = ()
let x = Empty
println (sizeof x) // prints "0"
```

## Tuple-based Types

Using the `tup` keyword, you can create a new tuple-based type.

```stegolang
tup Pair A =
    A
    A
dec xy = Pair Float64 // this type annotation makes sure that xy ends up as a pair of floats
let xy = Pair 100 200
println (sizeof xy) // prints "16" because it is made of two 8-byte floats
println (xy.1) // prints "200"
```

This example also uses a type argument (`A`) and creates a constructor function,
`Pair` which accepts two arguments and creates instances of the `Pair` type,
causing an error if the two values are not of the same type.

## Structs

Using the `struct` keyword, you can create a struct-based type with named fields. Unlike tuple-based types, the fields of a struct are named and may be mutable.

```stegolang
struct Point2D =
    x = int
    y = int
```

Struct constructors are not normal functions and use a block of `property = value` statements.
If any fields are missing, an error message is produced.

```stegolang

// preferred style of struct construction
let p = Point2D
    x = 100
    y = 100
    
// if you want it all on one line, do it Haskell-style
let p3 = Point2D { x = 100 , y = 100 }

// they also offer pattern matching and destructuring using field names
match p =
    Point2D {x} = print x
    
let Point2D {x = x1, y = y1} = p;
```

## Union Type

Using the `ty` keyword, you can define a set of named subtypes that are possible values of the outer type. This is sometimes called a "tagged union".

```stegolang
// important standard library types are union types!
ty Result A B =
    tup Ok = A
    tup Err = B
    
ty Option A =
    tup Some = A
    enum None = ()

ty Mixed =
    tup TupleExample =
        string
        int
    struct StructExample =
        x = int
        y = int
    ty NestedExample =
        enum A = ()
        enum B = ()
        tup C = int
    enum EnumExample =
        1
        "one"
        '1'
    enum EmptyExample = ()

// don't actually do this
ty AllOnOneLineForSomeReason = tup TupleExample = string , int ,, struct StructExample = x = int , y = int ,, ty NestedExample = enum A = () ,, enum B = () ,, tup C = int ,, enum EnumExample = 1 , "one" , '1' ,, enum EmptyExample = () ,,
```

Unlike the other kinds of custom type, sum types are not usable as constructor functions. Instead use the constructor functions of the subtypes, which are found by treating it as a namespace.

```stegolang
let x = Result.Ok true
let y = Mixed.EnumExample.C 100
```

## More Examples

```stegolang
// making a partial function of an enum constructor
ty Contrived =
    tup Pair = int, int
    struct XY =
        x = int
        y = int
let y_intercept y = Contrived.Pair 0
let origin = y_intercept 0

// using an accessor method
ty Coord =
    struct TwoDee =
        x = int
        y = int
    struct ThreeDee =
        x = int
        y = int
        z = int
let p = Coord.TwoDee
    x = 0
    y = 0

dec foo Coord = int
fn foo p =
    match p,
        p3d @ Coord.ThreeDee, 
            // p3d is of type Coord.ThreeDee, so x, y, and z accessors are accessible
            print ("X: " + p3d.x + ", Y: " + p3d.y + ", Z: " + p3d.z)
        p2d @ Coord.TwoDee,
            // p2d is of type Coord.TwoDee, so only x, and y accessors are accessible
            print ("X: " + p2d.x + ", Y: " + p2d.y)
(Coord.ThreeDee {} 
        
```

The struct form creates accessor methods.

```stegolang
ty Point2D = { x: int, y: int }
let p = { x: 5, y: 5 }
(Point2D.x p) . println
```

Fully qualifying the accessor method on structs gets cumbersome, so the forward application operator (`.`) first tries to looks up the right hand side in the type of the left hand side.

```stegolang
ty Point2D = { x: int, y: int }
let x = 100
let p = { x: x, y: 5 }
x = -7
// even though x is in scope as a variable, the next line uses Point2D.x instead
p . x . println // prints "100"
```

## Anonymous Types

Anonymous types and instances of them can be created too! This is particularly useful in where clauses with one-off types.

Here's some examples.

```stegolang
dec rank_to_ordinal PlaceType = OrdinalType
where
    PlaceType = enum 1 , 2 , 3 , 4 , 5 , 6 , 7 , 8
    OrdinalType = enum "1st" , "2nd" , "3rd" , "4th" , "5th" , "6th" , "7th" , "8th"

fn rank_to_ordinal place =
    match place =
        1 = "1st"
        2 = "2nd"
        3 = "3rd"
        4 = "4th"
        5 = "5th"
        6 = "6th"
        7 = "7th"
        8 = "8th"
```
