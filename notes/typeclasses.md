# Typeclasses

**Note:** <span style="color:red;background:white;border:1px solid red;padding:0.25em">**The following is all hypothetical!**</span>

Typeclasses are a bit like interfaces or abstract base classes in object-oriented languages. They allow you to use values without knowing their exact type ahead of time by placing constraints on generic types.

## Create a Typeclass

In the following example, we create a `Point2D` typeclass using the `tycl`
keyword. This says that every `Point2D CoordType` has accessors named x and y.
`Point2D` is generic: `CoordType` could be any type of value.

```stegolang
tycl Point2D CoordType =
    dec x = CoordType
    dec y = CoordType
```

## Using a Typeclass

Using the typeclass from the above example and a `dec` statement with a `where` clause, we could create a function like this that accepts any type as the point,
so long as it is known to be a Point2D with the specified CoordType. This means it can use those
accessors despite not knowing an exact type.

```stegolang
dec dist_from_origin PointType = PointType
where
    CoordType = number
    PointType = Point2D CoordType
fn dist_from_origin p =
    sqrt ( p.x ^ 2 + p.y ^ 2)
where
    p = Point2D CoordType
    CoordType = number
```

## Methods

Typeclasses also allow the creation of methods -- functions associated with the
typeclass just like the accessors are. Here is the `dist_from_origin`
function from above as a method of `Point2D`.

```stegolang
tycl ReadonlyPoint2D CoordType =
    dec get x = CoordType
    dec get y = CoordType
    dec dist_from_origin imm Self = CoordType
where
    CoordType = number
```

### Self

You can see we used the special keyword `Self` above. Inside of `tycl`s and `impl`s , `Self` refers to the exact type implementing the method. In the following example, if the `rotated_ccw_around_origin` method is used on something with a more specific type than `ReadonlyPoint2D CoordType`, the return value will be that more specific type, too.

```stegolang
tycl ReadonlyPoint2D CoordType =
    readonly x = CoordType
    readonly y = CoordType
    rotated_ccw_around_origin imm Self = Self
```

### Methods as Accessors

Methods are just accessors for functions. Instead of `dec rotated_ccw_around_origin Self = Self`, it could have been specified with `dec readonly rotated_ccw_around_origin = fn imm Self = Self`.

### Method Implmentations

That new `ReadonlyPoint2D` definition says that `ReadonlyPoint2D`s have a `rotated_ccw_around_origin` method. Now we need to provide the implementation of that for our specific instance of that typeclass, namely `String2D`. We do that with the `impl` keyword. Here is what that could look like.

```stegolang
ty String2D = (string, string)

impl ReadonlyPoint2D string for String2D =
    // accessors are implemented with special get and set keywords
    get x = self.0
    get y = self.1

    // this implementation uses the accessors even though it's a tuple type
    fn rotated_ccw_around_origin, mut self =
        String2D ("-"+self.y) self.x
```

Now when you send a String2D to other code as a Point2D, that code need not be
aware of the String2D type or its implementations at all, and yet can still get
the correct behavior through the typeclass.

```stegolang
let s = String2D ("x value", "y value")

dec rot_and_print (Point2D ToStr) = ()
fn rot_and_print p =
    p2 = p.rotated_ccw_around_origin
    println ("( x = " + p.x + ", y = " + p.y + " )")

rot_and_print s // prints "( x = -y value, y = x value )"
```

## Default implementations

You can often anticipate what accessors and methods will need to do. You can provide default implementations of accessors and methods in the typeclass by placing them in the `tycl`.

```stegolang
// changed this to make properties writable, 
tycl Point2D CoordType =
    x = CoordType // declaring accessor. looks just like a struct field
    y = CoordType // declaring accessor. looks just like a struct field

    dec rotated_ccw_around_origin Self = Self
    fn imm self = (-self.y, self.x)
where
    CoordType is Negate // so that we can use unary `-` in the default implementation
```

When the types and names match up to make it possible, accessors will be automatically implemented for struct types.

```stegolang
ty PointStruct CoordType = {
    x = CoordType
    y = CoordType
}

// Point2D implementation is entirely automatic now
impl <CoordType> Point2D CoordType for PointStruct CoordType
```

## Acting like both a Tuple and a Struct

If you'd like to be able to use a type as either a tuple or a struct, the magic of `impl` makes it possible!

```stegolang
ty PointTuple = ( int, int )

tycl ReadonlyPoint2D CoordType =
    dec readonly x = CoordType
    dec readonly y = CoordType
    dec rotated_ccw_around_origin imm Self = Self

impl ReadonlyPoint2D for PointTuple =
    acc x =
        get imm self = self.0

let x = 100
let p = { x: x, y: 5 }
// even though x is in scope as a variable, the next line uses Point2D.x instead
p . x . println // prints "100"
```

## Multiple Dispatch

The `where` clause can also be applied to `fn` statements to give us a way to provide different implementations of the function for different types or typeclasses.

```stegolang
fn comment_on_argument a =
    "A is 100"
where
    a is 100

fn comment_on_type a =
    "A is floating point"
where
    a is not 100
    a is Float
    
fn comment_on_type a =
    "A is integral"
where
    a is not 100
    a is not Float
    a is Integral 
    
fn comment_on_type a =
    "A is a string"
where
    a is string
    
fn comment_on_type a =
    "I don't know what to say"
where
    a is not string
    a is not 100
    a is not Float
    a is not Integral
```

Where clauses can accept fairly complicated expressions involving function parameters, literals, types, traits, parentheses, and the operators `is`, `is not`, `and`, and `or`.

To sketch out the where clause grammar:

```none
*where_clause* ← *where_or_expr* | ( `(` *where_clause* `)` )
*where_or_expr* ← *where_and_expr* ( `or` *where_or_expr* ) <sup>*</sup> 
*where_and_expr* ← *where_is_expr* ( `and` *where_and_expr* ) <sup>*</sup> 
*where_is_expr* ← *type_expr* ( `is` | `is not` ) *type_expr*
*type_expr* ← ( `(` *type_expr* `)` ) | ( TYPEPARAM_OR_TYPE_OR_TRAIT_IN_SCOPE ( *type_expr* ) <sup>*</sup> ) | *literal* | NEW_TYPE_VARIABLE
```

It is an error when multiple implementations could apply to a specific function call.

Where clauses also let you define temporary type variables.

```stegolang
dec rot_and_print T = ()
where
    CoordType = ToStr
    T = Point2D CoordType
```

Is the same as

```stegolang
dec rot_and_print (Point2D ToStr) = ()
```

## Type Expressions

An `if` statement may also use type expressions just like those in where clauses, but using any identifier in scope instead of just function parameters.

```stegolang
dec check_division Numeric Numeric Numeric Float
fn check_division numerator denominator expected epsilon =
    let quotient = numerator / denominator
    if quotient is Float then
        abs (quotient - expected) <= epsilon
    else
        quotient == expected
```
