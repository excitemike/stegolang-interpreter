# Setup

## Install software

On Windows:

- install git if you don't have it: <https://git-scm.com/>
- install visual studio build tools if you don't have them: <https://visualstudio.microsoft.com/visual-cpp-build-tools/>
- if you don't already have rust, download and run rustup-init: <https://www.rust-lang.org/tools/install>
- See IDE section below

On Linux:

Tested via [Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/install-win10)

- install git and/or build tools like gcc if you don't have them

  ```bash
  sudo apt-get update
  sudo apt-get install git
  sudo apt-get install build-essential
  ```

- set user info in git, and to make git remember login info

  ```bash
  git config --global user.email "YOUR_NAME_HERE"
  git config --global user.email "YOUR_EMAIL_HERE"
  git config credential.helper store
  ```

- install rust via rustup

  ```bash
  curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
  ```

  I'm currently building with rustc version`1.73.0`. (Do `rustc --version` to check your version.)

- clone repo

  ```bash
  git clone https://bitbucket.org/excitemike/stegolang-interpreter.git
  ```

- build and run tests

  ```bash
  cd stegolang-interpreter
  cargo test
  ```

- to test Linux build on Windows through VS Code, also make sure you have wget

  ```bash
  sudo apt-get install wget ca-certificates
  ```

## Build LLVM

I could not find a windows binary anywhere that had what we need, so I built it myself:

1. download ninja executable <https://ninja-build.org/>
2. extract to `/ext/bin/ninja.exe`
3. download llvm 9.0.0 source (<https://releases.llvm.org/9.0.0/llvm-9.0.0.src.tar.xz>
4. extract to `/ext/llvm-9.0.0.src`
5. I didn't install cmake globally and instead did
   1. download cmake
   2. copy the `cmake-3.18.2-win64-x64` folder to `/ext/cmake-3.18.2-win64-x64` (edit buildllvm.bat if you are using a different version)
6. run `./scripts/buildllvm.bat` (takes *A WHILE*.)

## IDE

I used VS Code (<https://code.visualstudio.com/docs/setup/setup-overview>) with the following extensions:

- Required for build/debug/intellisense to work:
  - rust-analyzer <https://marketplace.visualstudio.com/items?itemName=matklad.rust-analyzer>
    - ~~now using the main Rust extension instead: <https://marketplace.visualstudio.com/items?itemName=rust-lang.rust>~~
  - CodeLLDB extension <https://marketplace.visualstudio.com/items?itemName=vadimcn.vscode-lldb>
    - ~~Old debug extension: C/C++ <https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools> -- launch config type will need to be `cppvsdbg`~~
- Just handy:
  - Markdown lint: <https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint>
  - Partial Diff: <https://marketplace.visualstudio.com/items?itemName=ryu1kn.partial-diff>
  - Prettier: <https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode>
  - Remote Development (for testing linux builds from within Windows) <https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack>
    - see also <https://code.visualstudio.com/docs/remote/wsl-tutorial>
    - on the Linux side, I also found it handy to set up a windows copy of the repo as a remote with `git add remote windows /mnt/<driveletter>/path/to/repo`
  - Todo Tree: <https://marketplace.visualstudio.com/items?itemName=Gruntfuggly.todo-tree>
  - Visual Studio Keymap: <https://marketplace.visualstudio.com/items?itemName=ms-vscode.vs-keybindings>

## Building

### VS Code

Build tasks and launch targets have been configured, so Ctrl+Shift+B/F5 should work.

### Command-line

You can then build with `cargo build` and run with `cargo run`. You can build and run test cases with `cargo test`.
