# Stegolang

This is a toy programming language I a working on as a hobby project! Some of it's goals are to be:

- Be statically-typed but require very little explicit typing
- Require less use of the shift key than most programming languages.
  This is largely due to RSI. Accomplished by the following:
  - The "`.`" operator reduces the need for parentheses. Similar to Haskell's "`&`" and "`$`".
  - Blocks of code are indentation rather than the curly braces of C-like languages. And where Python often uses "`:`" before its indented blocks, this language uses "`=`".
  - Strings can use "`'`" (single quotes) in place of "`"`".

    ```text
    'example' vs "example"
    ```

- Support typeclasses (like Rust's traits, Swift's interfaces, or a little like C#'s extension methods)
- Support Algebraic Data Types

## Quick Start

This is still quite far being ready for any kind of public release, but check out [SETUP.md](SETUP.md) for my notes about building from source. You can then test the REPL by just running the generated executable on the command line, or execute a file by sending the path to that file as a command-line argument.
