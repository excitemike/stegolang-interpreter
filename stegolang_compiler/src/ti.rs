//! type inference

use crate::ast::astnode::{AstNode, AstNodeKind};
use crate::ast::Operator;
use crate::ast::{DictExprElem, IfArm, SetExprElem};
use crate::error::{Error, Severity};
use crate::fntypes::fnfamily::FnFamily;
use crate::hlir::hlirnodekind::{HlirLetData, HlirNodeKind};
use crate::hlir::HlirNode;
use crate::loc::{self, err, MsgTuple};
use crate::pattern::StegoPattern;
use crate::sourceregion::SourceRegion;
use crate::ti::generalize::generalize;
use crate::ti::solve::solve_constraints;
use crate::ti::substitute::ApplySubstInPlace;
use crate::StringType;
use itertools::Itertools;
use std::borrow::Borrow;
use std::cell::RefCell;
use std::collections::{
    btree_map::Entry::{Occupied, Vacant},
    BTreeMap,
};
use std::fmt::Debug;
use std::rc::Rc;
use std::sync::atomic::{AtomicU32, Ordering};

pub(crate) mod arenas;
pub(crate) mod constraint;
mod context;
pub(crate) use context::*;
pub(crate) mod findmember;
pub(crate) mod freetypevars;
pub(crate) mod generalize;
mod hindleymilner;
pub(crate) mod implementationbuilder;
pub(crate) mod implschemeiter;
mod pattern;
mod scheme;
pub(crate) use scheme::*;
pub(crate) mod solve;
pub(crate) mod substitute;
use substitute::ApplySubst;
mod substitutions;
pub(crate) use substitutions::*;
pub(crate) mod traitbuilder;
pub(crate) mod traitstruct;
mod ty;
pub(crate) use ty::*;

use self::freetypevars::FreeTypeVars;
use self::traitstruct::TraitStructRef;
pub mod visitor;

/// errors accumulated during type inference
#[derive(Debug, PartialEq, Eq)]
#[allow(clippy::module_name_repetitions)]
pub struct TiError {
    pub(crate) msg_tuple: MsgTuple,
    pub(crate) source_region: SourceRegion,
}
impl TiError {
    pub(crate) fn new(msg_tuple: MsgTuple, source_region: SourceRegion) -> TiError {
        TiError {
            msg_tuple,
            source_region,
        }
    }
}

impl From<TiError> for Vec<Error> {
    fn from(val: TiError) -> Self {
        vec![Error {
            short_msg: val.msg_tuple.0.clone(),
            help: val.msg_tuple.1.clone(),
            severity: Severity::Error,
            source_region: val.source_region,
        }]
    }
}

impl Error {
    pub(crate) fn warning_from(ti_err: TiError) -> Self {
        Error {
            short_msg: ti_err.msg_tuple.0.clone(),
            help: ti_err.msg_tuple.1.clone(),
            severity: Severity::Warning,
            source_region: ti_err.source_region,
        }
    }
}

impl From<TiError> for Error {
    fn from(ti_err: TiError) -> Self {
        Error {
            short_msg: ti_err.msg_tuple.0.clone(),
            help: ti_err.msg_tuple.1.clone(),
            severity: Severity::Error,
            source_region: ti_err.source_region,
        }
    }
}

pub(crate) trait IntoTiResult<T> {
    fn into_tiresult(self, source_region: &SourceRegion) -> TiResult<T>;
}
impl<T> IntoTiResult<T> for Result<T, MsgTuple> {
    fn into_tiresult(self, source_region: &SourceRegion) -> TiResult<T> {
        self.map_err(|msg_tuple| TiError::new(msg_tuple, source_region.clone()))
    }
}

/// results of type inference operations
pub(crate) type TiResult<T> = Result<T, TiError>;

/// Get the next unique identifier for a type variable
pub(crate) fn make_unification_var_id() -> Result<UnificationVarId, MsgTuple> {
    static ID: AtomicU32 = AtomicU32::new(0);
    let id = ID.fetch_add(1, Ordering::Relaxed);
    let next_id = ID.load(Ordering::Relaxed);
    if next_id < id {
        Err(err::type_db_outofids())
    } else {
        Ok(id)
    }
}

/// Variant of a union type
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) struct Variant<'ctx> {
    pub(crate) name: StringType,
    pub(crate) variant_type: ITy<'ctx>,
}

impl<'ctx> FreeTypeVars<'ctx> for Variant<'ctx> {
    fn free_unification_variables(
        &self,
        exclude: &[UnificationVarId],
        collection: &mut Vec<UnificationVarId>,
    ) {
        self.variant_type
            .free_unification_variables(exclude, collection);
    }

    fn has_any_free_unification_variables(&self, exclude: &[UnificationVarId]) -> bool {
        self.variant_type
            .has_any_free_unification_variables(exclude)
    }

    fn has_free_type_var(&self, search_id: UnificationVarId) -> bool {
        self.variant_type.has_free_type_var(search_id)
    }

    fn get_type_parameters(
        &self,
        exclude: &[UnificationVarId],
        collection: &mut Vec<UnificationVarId>,
    ) {
        self.variant_type.get_type_parameters(exclude, collection);
    }

    fn has_any_type_parameters(&self, exclude: &[UnificationVarId]) -> bool {
        self.variant_type.has_any_type_parameters(exclude)
    }

    fn has_type_parameter(&self, search_id: UnificationVarId) -> bool {
        self.variant_type.has_type_parameter(search_id)
    }
}

/// Generic type - has some types that are deduced from how it is used
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub(crate) struct ImplScheme<'ctx> {
    pub(crate) ty_pars: Box<[(StringType, ITy<'ctx>)]>,
    /// placeholder for the type the scheme is implemented for
    pub(crate) self_var: ITy<'ctx>,
    pub(crate) implementation: ImplementationRef<'ctx>,
}
pub(crate) type ImplSchemeRef<'ctx> = &'ctx ImplScheme<'ctx>;
impl<'ctx> ImplScheme<'ctx> {
    pub(crate) fn debug_name(&self) -> String {
        loc::implementation_of_x_for_y(
            &self.implementation.implemented_trait.to_string(),
            &self.implementation.implemented_for.to_string(),
            &self.implementation.source_region.file_label,
            self.implementation.source_region.start_line_number,
            self.implementation.source_region.start_column_number,
        )
    }
    /// If this has type parameters, replace them with fresh unification variables.
    /// Also subs in Self
    pub(crate) fn instantiate(
        &self,
        ctx: &Context<'ctx>,
        source_region: &SourceRegion,
    ) -> TiResult<ImplementationRef<'ctx>> {
        let mut substitutions = Substitutions::new();
        // replace type parameter placeholders
        for (_, ty_par) in &*self.ty_pars {
            let replacement = ctx.mk_unification_var(source_region, "replacement for typar")?;
            substitutions.insert(ty_par.expect_type_parameter(source_region)?, replacement);
        }
        ctx.instantiate_constraints(&substitutions)?;
        let self_var = self
            .self_var
            .apply_subst(&substitutions, ctx)?
            .unwrap_or(self.self_var);

        // replace associated type placeholders
        // TODO: also check for any missing ones!
        for (name, ty) in &self.implementation.member_types {
            match self.implementation.implemented_trait.assoc_tys.get(name) {
                Some(placeholder) => {
                    let s = ctx.unify_eq(*placeholder, *ty, source_region)?;
                    substitutions = ctx.compose(s, substitutions)?;
                }
                // implementation is adding an associated type not mentioned in the trait itself.
                // TODO: this error should happen when building the imp
                None => {
                    return TiResult::Err(TiError::new(
                        loc::err::internal::assoc_type_in_implementation_but_not_trait(
                            &self.implementation.implemented_trait.trait_name,
                            name,
                        ),
                        source_region.clone(),
                    ))
                }
            }
        }
        let implemented_for = self
            .implementation
            .implemented_for
            .apply_subst(&substitutions, ctx)?
            .unwrap_or(self.implementation.implemented_for);
        // subs in the for type, finally replacing the self type
        {
            let s = ctx.unify_eq(self_var, implemented_for, source_region)?;
            substitutions = ctx.compose(s, substitutions)?;
        }

        let implemented_trait = self
            .implementation
            .implemented_trait
            .apply_subst(&substitutions, ctx)?
            .unwrap_or(self.implementation.implemented_trait);
        let member_types = self
            .implementation
            .member_types
            .apply_subst(&substitutions, ctx)?
            .unwrap_or_else(|| self.implementation.member_types.clone());
        let member_functions = self
            .implementation
            .member_functions
            .apply_subst(&substitutions, ctx)?
            .unwrap_or_else(|| self.implementation.member_functions.clone());
        let implementation = ctx.mk_implementation(Implementation {
            implemented_for,
            implemented_trait,
            member_types,
            member_functions,
            source_region: source_region.clone(),
        });
        // earlier substitution should have made sure that type placeholders don't leak
        // but I should check here to ensure it!
        // TODO: (even in constraints!)
        if cfg!(debug_assertions) {
            let mut found_ty_pars = Vec::new();
            implementation.get_type_parameters(&[], &mut found_ty_pars);
            if let Type::TypeParameter { uniq_id, .. } = *self.self_var {
                debug_assert!(!found_ty_pars.contains(&uniq_id));
            }
            for (_, x) in &*self.ty_pars {
                if let Type::TypeParameter { uniq_id, .. } = **x {
                    debug_assert!(!found_ty_pars.contains(&uniq_id));
                }
            }
        }
        Ok(implementation)
    }
}
impl<'ctx> std::fmt::Display for ImplScheme<'ctx> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.ty_pars.is_empty() {
            // TODO: use loc::implementation_of_x_for_y and make implementation_of_x_for_y_where
            write!(
                f,
                "(Implementation of {} for {})",
                &self.implementation.implemented_trait.to_string(),
                &self.implementation.implemented_for.to_string()
            )
        } else {
            write!(
                f,
                "(Implementation of {} for {} where",
                &self.implementation.implemented_trait.to_string(),
                &self.implementation.implemented_for.to_string()
            )?;
            for (name, _) in &*self.ty_pars {
                write!(f, " {name}")?;
            }
            std::fmt::Display::fmt(&')', f)
        }
    }
}
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub(crate) struct Implementation<'ctx> {
    pub(crate) implemented_for: ITy<'ctx>,
    pub(crate) implemented_trait: TraitStructRef<'ctx>,
    pub(crate) member_types: BTreeMap<StringType, ITy<'ctx>>,
    pub(crate) member_functions: BTreeMap<StringType, (ITy<'ctx>, Rc<FnFamily<'ctx>>)>,
    /// where the implementation scheme was defined
    pub(crate) source_region: SourceRegion,
}

impl<'a> std::hash::Hash for Implementation<'a> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.implemented_for.hash(state);
        self.implemented_trait.hash(state);
        // skipping these. the other fields should be enough
        //self.member_types.hash(state);
        //self.member_functions.hash(state);
        self.source_region.hash(state);
    }
}

pub(crate) type ImplementationRef<'ctx> = &'ctx Implementation<'ctx>;
impl<'ctx> Implementation<'ctx> {
    /// look up a member function
    pub(crate) fn find_member_function(
        &self,
        member_name: &StringType,
    ) -> Option<(ITy<'ctx>, Rc<FnFamily<'ctx>>)> {
        self.member_functions.get(member_name).cloned()
    }
}

impl<'ctx> std::fmt::Display for Implementation<'ctx> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "({})",
            loc::implementation_of_x_for_y(
                &self.implemented_trait.to_string(),
                &self.implemented_for.to_string(),
                &self.source_region.file_label,
                self.source_region.start_line_number,
                self.source_region.start_column_number
            )
        )
    }
}

impl<'ctx> FreeTypeVars<'ctx> for Implementation<'ctx> {
    // TODO: that should be a set
    fn free_unification_variables(
        &self,
        exclude: &[UnificationVarId],
        collection: &mut Vec<UnificationVarId>,
    ) {
        self.implemented_for
            .free_unification_variables(exclude, collection);
        for scheme in self.member_types.values() {
            scheme.free_unification_variables(exclude, collection);
        }
        for (scheme, _) in self.member_functions.values() {
            scheme.free_unification_variables(exclude, collection);
        }
    }

    fn has_any_free_unification_variables(&self, exclude: &[UnificationVarId]) -> bool {
        self.implemented_for
            .has_any_free_unification_variables(exclude)
            || self
                .member_types
                .values()
                .any(|scheme| scheme.has_any_free_unification_variables(exclude))
            || self
                .member_functions
                .values()
                .any(|(scheme, _)| scheme.has_any_free_unification_variables(exclude))
    }

    fn has_free_type_var(&self, search_id: UnificationVarId) -> bool {
        self.implemented_for.has_free_type_var(search_id)
            || self
                .member_types
                .values()
                .any(|scheme| scheme.has_free_type_var(search_id))
            || self
                .member_functions
                .values()
                .any(|(scheme, _)| scheme.has_free_type_var(search_id))
    }

    fn get_type_parameters(
        &self,
        exclude: &[UnificationVarId],
        collection: &mut Vec<UnificationVarId>,
    ) {
        self.implemented_for
            .get_type_parameters(exclude, collection);
        for scheme in self.member_types.values() {
            scheme.get_type_parameters(exclude, collection);
        }
        for (scheme, _) in self.member_functions.values() {
            scheme.get_type_parameters(exclude, collection);
        }
    }

    fn has_any_type_parameters(&self, exclude: &[UnificationVarId]) -> bool {
        self.implemented_for.has_any_type_parameters(exclude)
            || self
                .member_types
                .values()
                .any(|scheme| scheme.has_any_type_parameters(exclude))
            || self
                .member_functions
                .values()
                .any(|(scheme, _)| scheme.has_any_type_parameters(exclude))
    }

    fn has_type_parameter(&self, search_id: UnificationVarId) -> bool {
        self.implemented_for.has_type_parameter(search_id)
            || self
                .member_types
                .values()
                .any(|scheme| scheme.has_type_parameter(search_id))
            || self
                .member_functions
                .values()
                .any(|(scheme, _)| scheme.has_type_parameter(search_id))
    }
}
impl<'ctx> ApplySubst<'ctx> for ImplementationRef<'ctx> {
    fn apply_subst(
        &self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<Option<Self>> {
        if substitutions.is_empty() {
            return Ok(None);
        }

        let Implementation {
            implemented_for,
            implemented_trait,
            member_functions,
            member_types,
            source_region: impl_source_region,
        } = self;

        let implemented_for = implemented_for.apply_subst(substitutions, ctx)?;
        let implemented_trait = implemented_trait.apply_subst(substitutions, ctx)?;
        let member_functions = member_functions.apply_subst(substitutions, ctx)?;
        let member_types = member_types.apply_subst(substitutions, ctx)?;
        let any_changed = implemented_for.is_some()
            || implemented_trait.is_some()
            || member_functions.is_some()
            || member_types.is_some();

        if any_changed {
            Ok(Some(ctx.mk_implementation(Implementation {
                implemented_for: implemented_for.unwrap_or(self.implemented_for),
                implemented_trait: implemented_trait.unwrap_or(self.implemented_trait),
                member_functions: member_functions.unwrap_or_else(|| self.member_functions.clone()),
                member_types: member_types.unwrap_or_else(|| self.member_types.clone()),
                source_region: impl_source_region.clone(),
            })))
        } else {
            Ok(Some(*self))
        }
    }
}

/// data in intermediate stages of binary operator expression conversion
struct BinopStepReturn<'ctx> {
    substitutions: Substitutions<'ctx>,
    inferred_type: ITy<'ctx>,
    first: HlirNode<'ctx>,
    rest: Vec<(Operator, HlirNode<'ctx>)>,
}

impl<'ctx> Context<'ctx> {
    /// find, or create a unification variable for, the method type for an operator
    pub(crate) fn find_op_method_type(
        &self,
        implementing_type: ITy<'ctx>,
        rhs_type: ITy<'ctx>,
        op: Operator,
        source_region: &SourceRegion,
    ) -> TiResult<ITy<'ctx>> {
        let trait_name = op.get_trait_name();
        let scope = self.scopes.borrow();
        let ty = scope
            .get_global(&trait_name)
            .ok_or_else(|| loc::err::missing_trait_for_op(&trait_name))
            .into_tiresult(source_region)?;
        let trait_scheme = ty.expect_trait_scheme(source_region)?;
        let trait_type =
            trait_scheme.instantiate_with(self, std::slice::from_ref(&rhs_type), source_region)?;
        let method_name = op.get_method_name();
        let member;
        if let Some(implementation) =
            self.implementation_of_trait(trait_type, implementing_type, source_region)
        {
            member = implementation
                .member_functions
                .get(&method_name)
                .ok_or_else(|| {
                    TiError::new(
                        loc::err::no_member_for_type(&implementing_type.to_string(), &method_name),
                        source_region.clone(),
                    )
                })?
                .0;
        } else {
            let result_ty = self.mk_unification_var(source_region, "result of opmethod")?;
            member = self.mk_fn_type(Box::new([implementing_type, rhs_type]), result_ty);
            self.add_constraint(constraint::Constraint::Method {
                for_ty: implementing_type,
                method_ty: member,
                trait_: trait_type,
                name: method_name.clone(),
                source_region: source_region.clone().into(),
            });
        }
        Ok(member)
    }

    /// find the method for an operator
    pub(crate) fn find_op_method_for_eval(
        &self,
        implementing_type: ITy<'ctx>,
        rhs_type: ITy<'ctx>,
        op: Operator,
        source_region: &SourceRegion,
    ) -> TiResult<(ITy<'ctx>, Rc<FnFamily<'ctx>>)> {
        let trait_name = op.get_trait_name();
        let scope = self.scopes.borrow();
        let trait_type = scope
            .get_global(&trait_name)
            .ok_or_else(|| loc::err::missing_trait_for_op(&trait_name))
            .into_tiresult(source_region)?
            .expect_trait_scheme(source_region)?
            .instantiate_with(self, std::slice::from_ref(&rhs_type), source_region)?;
        let implementation =
            self.get_trait_implementation_for_eval(implementing_type, trait_type, source_region)?;
        let method_name = op.get_method_name();
        implementation
            .find_member_function(&method_name)
            .ok_or_else(|| {
                TiError::new(
                    err::missing_method_for_op(&method_name),
                    source_region.clone(),
                )
            })
    }

    /// start building an implementation for a trait
    pub(crate) fn implement(
        &self,
        implemented_trait: TraitStructRef<'ctx>,
        for_type: ITy<'ctx>,
        source_region: SourceRegion,
    ) -> implementationbuilder::ImplementationBuilder<'_, 'ctx> {
        implementationbuilder::ImplementationBuilder::new(
            self,
            implemented_trait,
            for_type,
            source_region,
        )
    }

    /// Pushes a new child scope, calls f, then pops that scope
    pub(crate) fn in_new_scope<F, R>(&self, f: F) -> R
    where
        F: FnOnce(&Context<'ctx>) -> R,
    {
        self.scopes.borrow_mut().push();
        let result = f(self);
        self.scopes.borrow_mut().pop();
        result
    }

    /// do type inference for function application
    #[allow(dead_code)] // TODO: use or remove
    fn infer_application(
        &self,
        func: ITy<'ctx>,
        arg: ITy<'ctx>,
        source_region: &SourceRegion,
    ) -> TiResult<(Substitutions<'ctx>, ITy<'ctx>)> {
        // intermediate type var for the (possibly partial) application of the function
        let result = self.mk_unification_var(source_region, "result of application")?;
        let s = self.unify_eq(
            self.mk_fn_type_from_iter([arg].into_iter(), result),
            func,
            source_region,
        )?;
        let result = result.apply_subst(&s, self)?.unwrap_or(result);
        Ok((s, result))
    }

    /// do type inference for function application on at least 2 args
    /// returns substitutions and the type of the result
    fn infer_application2(
        &self,
        func: ITy<'ctx>,
        arg0: ITy<'ctx>,
        arg1: ITy<'ctx>,
        source_region: &SourceRegion,
    ) -> TiResult<(Substitutions<'ctx>, ITy<'ctx>)> {
        // intermediate type var for the (possibly partial) application of the function
        let result_temp = self.mk_unification_var(source_region, "result of application2")?;
        let mut s = self.unify_eq(
            self.mk_fn_type_from_iter([arg0, arg1].into_iter(), result_temp),
            func,
            source_region,
        )?;
        let updated_result = result_temp.apply_subst(&s, self)?.unwrap_or(result_temp);
        s.remove_var(&result_temp, source_region)?;
        Ok((s, updated_result))
    }

    /// create a new, typed version of the syntax tree
    fn make_hlir<'a>(
        &'a self,
        node: &AstNode,
    ) -> Result<(Substitutions<'ctx>, HlirNode<'ctx>), TiError> {
        let (substitutions, mut typed_node) = match &node.kind {
            AstNodeKind::ApplyExpr { func, args } => self.make_hlir_apply_expr(func, args, node)?,
            AstNodeKind::AssignStmt { lhs, rhs } => self.make_hlir_assign(lhs, rhs, node)?,
            AstNodeKind::BitwiseNot(inner) => self.make_hlir_bitwise_not(inner, node)?,
            AstNodeKind::BinOpExpr { first, rest } => {
                self.make_hlir_binop_expr(first, rest, node)?
            }
            AstNodeKind::ComparisonOpExpr { first, rest } => {
                self.make_hlir_comparison_op(rest, first, node)?
            }
            AstNodeKind::DictExpr(v) => self.make_hlir_dict_expr(node, v)?,
            AstNodeKind::DoExpr(subnode) => self.make_hlir_do_expr(subnode, node)?,
            AstNodeKind::DotExpr { lhs, method_name } => {
                self.make_hlir_dot_expr(lhs, method_name, &node.source_region)?
            }
            AstNodeKind::FnStmt(crate::ast::FnStmt {
                body,
                name,
                params,
                placeholder_needed,
            }) => self.make_hlir_fn_stmt(name, params, node, body, *placeholder_needed)?,
            AstNodeKind::Identifier { name } => {
                let scope = self.scopes.borrow();
                let ty = scope.get(name).ok_or_else(|| {
                    TiError::new(
                        err::identifier_not_in_scope(name.as_ref()),
                        node.source_region.clone(),
                    )
                })?;
                if matches!(ty.inner(), Type::Type(_)) {
                    return Err(TiError::new(
                        err::type_used_as_a_value(&ty.to_string()),
                        node.source_region.clone(),
                    ));
                }
                let inferred_type = **ty.borrow();
                let (instantiation_subs, inferred_type) = inferred_type
                    .instantiate(self, &node.source_region)?
                    .unwrap_or((Substitutions::new(), inferred_type));
                let instantiation_subs = instantiation_subs.into_iter().collect();
                (
                    Substitutions::new(),
                    HlirNode::new(
                        inferred_type,
                        node.source_region.clone(),
                        HlirNodeKind::Identifier {
                            name: StringType::clone(name),
                            substitutions: instantiation_subs,
                        },
                    ),
                )
            }
            AstNodeKind::IfExpr {
                arms,
                catch_all_else,
            } => self.make_hlir_if_expr(node, arms, catch_all_else)?,
            AstNodeKind::InExpr {
                container,
                invert,
                item,
            } => {
                let (s1, updated_container) = self.make_hlir(container)?;
                let (s2, updated_item) = self.make_hlir(item)?;
                let substitutions = self.compose(s2, s1)?;
                (
                    substitutions,
                    HlirNode::new(
                        self.mk_bool(),
                        node.source_region.clone(),
                        HlirNodeKind::InExpr {
                            container: updated_container.into(),
                            invert: *invert,
                            item: updated_item.into(),
                        },
                    ),
                )
            }
            AstNodeKind::Lambda { body, params } => {
                let body_ref = (**body).borrow();
                self.make_hlir_lambda(params, &body_ref, &node.source_region, None)?
            }
            AstNodeKind::Let {
                pattern,
                value_expr,
            } => {
                let (s1, typed_node) = self.make_hlir(value_expr)?;
                self.apply_substitutions_in_current_scope(&s1)?;
                self.apply_subst_to_constraints(&s1)?;
                let (substitutions, typed_node) = generalize(typed_node, self)?;

                for (binding, ty) in pattern.destructure_type(typed_node.inferred_type)? {
                    self.scopes.borrow_mut().insert(binding, ty);
                }
                (
                    s1,
                    HlirNode {
                        inferred_type: self.mk_nothing(),
                        source_region: node.source_region.clone(),
                        kind: HlirNodeKind::Let(HlirLetData {
                            pattern: pattern.clone(),
                            value_expr: Box::new(typed_node),
                            substitutions,
                        }),
                    },
                )
            }
            AstNodeKind::LiteralValue(kind) => (
                Substitutions::new(),
                HlirNode {
                    inferred_type: self.type_from_literal_kind(kind),
                    source_region: node.source_region.clone(),
                    kind: kind.clone().into(),
                },
            ),
            AstNodeKind::LogicalNot(node) => {
                let (substitutions, node) = self.make_hlir(node)?;
                (
                    substitutions,
                    HlirNode {
                        inferred_type: self.mk_bool(),
                        source_region: node.source_region.clone(),
                        kind: HlirNodeKind::LogicalNot(node.into()),
                    },
                )
            }
            AstNodeKind::NamedExpr { name, value } => {
                let (s, typed_node) = self.make_hlir(value)?;
                self.scopes
                    .borrow_mut()
                    .insert(name, typed_node.inferred_type);
                (
                    s,
                    HlirNode {
                        inferred_type: typed_node.inferred_type,
                        source_region: node.source_region.clone(),
                        kind: HlirNodeKind::NamedExpr {
                            name: name.clone(),
                            value: Box::new(typed_node),
                        },
                    },
                )
            }
            AstNodeKind::ReturnStmt(inner) => match inner {
                None => (
                    Substitutions::new(),
                    HlirNode {
                        inferred_type: self.mk_nothing(),
                        source_region: node.source_region.clone(),
                        kind: HlirNodeKind::ReturnStmt(None),
                    },
                ),
                Some(inner_node) => {
                    let (subs, typed_node) = self.make_hlir(inner_node)?;
                    (
                        subs,
                        HlirNode {
                            inferred_type: self.mk_nothing(),
                            source_region: node.source_region.clone(),
                            kind: HlirNodeKind::ReturnStmt(Some(Box::new(typed_node))),
                        },
                    )
                }
            },
            AstNodeKind::SetExpr(v) => {
                let mut ty = self.mk_unification_var(&node.source_region, "set elem type")?;
                let mut substitutions = Substitutions::new();
                let mut values = Vec::with_capacity(v.len());
                for elem in v {
                    match elem {
                        SetExprElem::Spread(spread_node) => {
                            let (s0, typed_spread_node) = self.make_hlir(spread_node)?;
                            substitutions = self.compose(s0, substitutions)?;
                            if let Some(iter_ty) = typed_spread_node
                                .inferred_type
                                .iterable_type(self, &spread_node.source_region)?
                            {
                                let s1 = self.unify_eq(ty, iter_ty, &spread_node.source_region)?;
                                substitutions = self.compose(s1, substitutions)?;
                                if let Some(updated_ty) = ty.apply_subst(&substitutions, self)? {
                                    ty = updated_ty;
                                }
                            } else {
                                return Err(TiError::new(
                                    loc::err::spread_not_iterable(
                                        &typed_spread_node.inferred_type.to_string(),
                                    ),
                                    spread_node.source_region.clone(),
                                ));
                            }
                            values.push(SetExprElem::Spread(typed_spread_node));
                        }
                        SetExprElem::Value(elem_node) => {
                            let (s0, typed_elem_node) = self.make_hlir(elem_node)?;
                            substitutions = self.compose(s0, substitutions)?;
                            let s1 = self.unify_eq(
                                ty,
                                typed_elem_node.inferred_type,
                                &elem_node.source_region,
                            )?;
                            substitutions = self.compose(s1, substitutions)?;
                            if let Some(updated_ty) = ty.apply_subst(&substitutions, self)? {
                                ty = updated_ty;
                            }
                            values.push(SetExprElem::Value(typed_elem_node));
                        }
                    }
                }

                let inferred_type = self.mk_set(ty);
                let inferred_type = inferred_type
                    .apply_subst(&substitutions, self)?
                    .unwrap_or(inferred_type);
                let values = values.apply_subst(&substitutions, self)?.unwrap_or(values);

                (
                    substitutions,
                    HlirNode::new(
                        inferred_type,
                        node.source_region.clone(),
                        HlirNodeKind::SetExpr(values),
                    ),
                )
            }
            AstNodeKind::Statements { statements } => {
                let mut s = Substitutions::new();
                let mut t = self.mk_nothing();
                let mut typed_nodes = Vec::with_capacity(statements.len());
                for node in statements {
                    let (s1, typed_node) = self.make_hlir(node)?;
                    s = self.compose(s1, s)?;
                    typed_nodes.push(typed_node);
                }
                if let Some(typed_node) = typed_nodes.last() {
                    t = typed_node.inferred_type;
                }
                (
                    s,
                    HlirNode::new(
                        t,
                        node.source_region.clone(),
                        HlirNodeKind::Statements {
                            statements: typed_nodes,
                        },
                    ),
                )
            }
            AstNodeKind::Tuple(nodes) => {
                let mut s = Substitutions::new();
                let mut typed_nodes = Vec::with_capacity(nodes.len());
                let mut types = Vec::with_capacity(nodes.len());
                for node in nodes {
                    let (s1, typed_node) = self.make_hlir(node)?;
                    s = self.compose(s1, s)?;
                    types.push(typed_node.inferred_type);
                    typed_nodes.push(typed_node);
                }
                let t = self.mk_tuple(types);
                (
                    s,
                    HlirNode::new(
                        t,
                        node.source_region.clone(),
                        HlirNodeKind::Tuple(typed_nodes),
                    ),
                )
            }
            AstNodeKind::TypeOf(node) => {
                let (s, typed_node) = self.make_hlir(node)?;
                (
                    s,
                    HlirNode::new(
                        self.mk_string(),
                        node.source_region.clone(),
                        HlirNodeKind::TypeOf(Box::new(typed_node)),
                    ),
                )
            }
            AstNodeKind::UnaryMinus(node) => {
                let (s, typed_node) = self.make_hlir(node)?;
                (
                    s,
                    HlirNode::new(
                        typed_node.inferred_type,
                        node.source_region.clone(),
                        HlirNodeKind::UnaryMinus(Box::new(typed_node)),
                    ),
                )
            }
            AstNodeKind::UnaryPlus(node) => {
                let (s, typed_node) = self.make_hlir(node)?;
                (
                    s,
                    HlirNode::new(
                        typed_node.inferred_type,
                        node.source_region.clone(),
                        HlirNodeKind::UnaryPlus(Box::new(typed_node)),
                    ),
                )
            }
            AstNodeKind::UnitType => (
                Substitutions::new(),
                HlirNode::new(
                    self.mk_nothing(),
                    node.source_region.clone(),
                    HlirNodeKind::UnitType,
                ),
            ),
        };

        // if a nullary function, it gets evaluated right now
        if let Type::F(FnType { params, result }) = typed_node.inferred_type.inner() {
            if params.is_empty() {
                typed_node.inferred_type = *result;
            }
        }

        Ok((substitutions, typed_node))
    }

    fn make_hlir_fn_stmt(
        &self,
        name: &StringType,
        params: &Rc<RefCell<Vec<StegoPattern>>>,
        node: &AstNode,
        body: &Rc<RefCell<AstNode>>,
        placeholder_needed: bool,
    ) -> TiResult<(Substitutions<'ctx>, HlirNode<'ctx>)> {
        // error on shadowing with fn
        if matches!(
            self.scopes.borrow().lookup(name),
            crate::util::nestedscope::LookupResult::FoundInCurrentScope(_)
        ) {
            let params_ref = RefCell::borrow(&**params);
            let parameters_str: String = params_ref.iter().map(ToString::to_string).join(" ");
            return Err(TiError::new(
                loc::err::shadowing_fn(name.as_ref(), &parameters_str),
                node.source_region.clone(),
            )); // TODO: use location of just the name
        }

        // placeholder for self-reference
        let (substitutions, mut lambda) = self.in_new_scope(|ctx| {
            let placeholder =
                self.mk_unification_var(&node.source_region, "recursion placeholder")?;
            self.scopes.borrow_mut().insert(name, placeholder);

            // body is the same as lambda
            let (s1, mut lambda) = {
                let body_ref = (**body).borrow();
                let placeholder_name = if placeholder_needed {
                    Some(name.clone())
                } else {
                    None
                };
                self.make_hlir_lambda(params, &body_ref, &node.source_region, placeholder_name)?
            };

            // replace recursion placeholder
            let s2 = self.unify_eq(placeholder, lambda.inferred_type, &node.source_region)?;
            lambda.apply_subst_in_place(&s2, ctx)?;
            let substitutions = self.compose(s2, s1)?;

            Ok((substitutions, lambda))
        })?;

        lambda.apply_subst_in_place(&substitutions, self)?;
        self.apply_substitutions_in_current_scope(&substitutions)?;
        self.apply_subst_to_constraints(&substitutions)?;

        // from there it is like a `let`.
        let (substitutions, lambda) = generalize(lambda, self)?;
        self.scopes.borrow_mut().insert(name, lambda.inferred_type);

        let HlirNodeKind::Lambda {
            body,
            captures,
            params,
            placeholder_name: _,
        } = lambda.kind
        else {
            unreachable!();
        };

        let typed_node = HlirNode::<'ctx>::new(
            self.mk_nothing(),
            node.source_region.clone(),
            HlirNodeKind::FnStmt(crate::hlir::fnstmt::FnStmt {
                body,
                captures,
                fn_ty: lambda.inferred_type,
                name: name.clone(),
                params,
                placeholder_needed,
            }),
        );
        Ok((substitutions, typed_node))
    }

    fn make_hlir_do_expr(
        &self,
        subnode: &AstNode,
        node: &AstNode,
    ) -> Result<(Substitutions<'ctx>, HlirNode<'ctx>), TiError> {
        let (subst, subnode) = self.in_new_scope(|ctx| ctx.make_hlir(subnode))?;
        Ok((
            subst,
            HlirNode::new(
                subnode.inferred_type,
                node.source_region.clone(),
                HlirNodeKind::DoExpr(Box::new(subnode)),
            ),
        ))
    }

    fn make_hlir_binop_expr(
        &self,
        first: &AstNode,
        rest: &Vec<(Operator, AstNode)>,
        node: &AstNode,
    ) -> TiResult<(Substitutions<'ctx>, HlirNode<'ctx>)> {
        let (s0, first) = self.make_hlir(first)?;
        let mut data = BinopStepReturn::<'ctx> {
            substitutions: s0,
            inferred_type: first.inferred_type,
            first,
            rest: Vec::with_capacity(rest.len()),
        };
        for (op, rhs_node) in rest {
            data = self.make_hlir_binop_step(data, *op, rhs_node, &node.source_region)?;
        }
        Ok((
            data.substitutions,
            HlirNode::new(
                data.inferred_type,
                node.source_region.clone(),
                HlirNodeKind::BinOpExpr {
                    first: Box::new(data.first),
                    rest: data.rest,
                },
            ),
        ))
    }

    fn make_hlir_bitwise_not(
        &self,
        inner: &AstNode,
        node: &AstNode,
    ) -> TiResult<(Substitutions<'ctx>, HlirNode<'ctx>)> {
        let (s, typed_node) = self.make_hlir(inner)?;
        Ok((
            s,
            HlirNode::new(
                typed_node.inferred_type,
                node.source_region.clone(),
                HlirNodeKind::BitwiseNot(Box::new(typed_node)),
            ),
        ))
    }

    fn make_hlir_assign(
        &self,
        lhs: &AstNode,
        rhs: &AstNode,
        node: &AstNode,
    ) -> TiResult<(Substitutions<'ctx>, HlirNode<'ctx>)> {
        let (s0, lhs) = self.make_hlir(lhs)?;
        let (s1, rhs) = self.make_hlir(rhs)?;
        let lhs = Box::new(lhs);
        let rhs = Box::new(rhs);
        Ok((
            self.compose(s1, s0)?,
            HlirNode::new(
                self.mk_nothing(),
                node.source_region.clone(),
                HlirNodeKind::AssignStmt { lhs, rhs },
            ),
        ))
    }

    fn make_hlir_if_expr(
        &self,
        node: &AstNode,
        arms: &Vec<IfArm<AstNode>>,
        catch_all_else: &Option<Box<AstNode>>,
    ) -> TiResult<(Substitutions<'ctx>, HlirNode<'ctx>)> {
        let mut ty = self.mk_unification_var(&node.source_region, "result of if expr")?;
        let mut substitutions = Substitutions::new();
        let mut updated_arms = Vec::with_capacity(arms.len());
        let mut updated_catch_all_else = None;
        for IfArm { cond, consequent } in arms {
            let (s, updated_cond) = self.make_hlir(cond)?;
            ty = ty.apply_subst(&s, self)?.unwrap_or(ty);
            substitutions = self.compose(s, substitutions)?;
            let s = self.unify_eq(
                self.mk_bool(),
                updated_cond.inferred_type,
                &cond.source_region,
            )?;
            ty = ty.apply_subst(&s, self)?.unwrap_or(ty);
            substitutions = self.compose(s, substitutions)?;

            let (s, updated_consequent) = self.in_new_scope(|ctx| ctx.make_hlir(consequent))?;
            ty = ty.apply_subst(&s, self)?.unwrap_or(ty);
            substitutions = self.compose(s, substitutions)?;
            let s = self.unify_eq(
                ty,
                updated_consequent.inferred_type,
                &consequent.source_region,
            )?;
            ty = ty.apply_subst(&s, self)?.unwrap_or(ty);
            substitutions = self.compose(s, substitutions)?;

            updated_arms.push(IfArm {
                cond: updated_cond,
                consequent: updated_consequent,
            });
        }
        if let Some(arm) = catch_all_else {
            let (s, updated) = self.in_new_scope(|ctx| ctx.make_hlir(arm))?;
            ty = ty.apply_subst(&s, self)?.unwrap_or(ty);
            substitutions = self.compose(s, substitutions)?;
            let s = self.unify_eq(ty, updated.inferred_type, &arm.source_region)?;
            ty = ty.apply_subst(&s, self)?.unwrap_or(ty);
            substitutions = self.compose(s, substitutions)?;

            updated_catch_all_else = Some(Box::new(updated));
        } else {
            let s = self.unify_eq(ty, self.mk_nothing(), &node.source_region)?;
            ty = ty.apply_subst(&s, self)?.unwrap_or(ty);
            substitutions = self.compose(s, substitutions)?;
        }
        let typed_node = HlirNode::new(
            ty,
            node.source_region.clone(),
            HlirNodeKind::IfExpr {
                arms: updated_arms,
                catch_all_else: updated_catch_all_else,
            },
        );
        let typed_node = typed_node
            .apply_subst(&substitutions, self)?
            .unwrap_or(typed_node);
        Ok((substitutions, typed_node))
    }

    /// comparison op portion of `make_hlir`
    fn make_hlir_comparison_op<'a>(
        &'a self,
        rest: &Vec<(Operator, AstNode)>,
        first: &AstNode,
        node: &AstNode,
    ) -> TiResult<(Substitutions<'ctx>, HlirNode<'ctx>)> {
        let mut substitutions = Substitutions::<'ctx>::new();
        let mut updated_rest = Vec::with_capacity(rest.len());
        let (s, updated_first) = self.make_hlir(first)?;
        substitutions = self.compose(s, substitutions)?;
        let mut lhs_type = updated_first.inferred_type;
        for (op, rhs_node) in rest {
            let (s, updated_rhs_node) = self.make_hlir(rhs_node)?;
            lhs_type = lhs_type.apply_subst(&s, self)?.unwrap_or(lhs_type);
            substitutions = self.compose(s, substitutions)?;
            updated_rest.push((*op, updated_rhs_node));
        }
        let typed_node = HlirNode::new(
            self.mk_bool(),
            node.source_region.clone(),
            HlirNodeKind::ComparisonOpExpr {
                first: Box::new(updated_first),
                rest: updated_rest,
            },
        );
        let typed_node = typed_node
            .apply_subst(&substitutions, self)?
            .unwrap_or(typed_node);
        Ok((substitutions, typed_node))
    }

    /// dictexpr portion of `make_hlir`
    fn make_hlir_dict_expr<'a>(
        &'a self,
        node: &AstNode,
        v: &Vec<DictExprElem<AstNode>>,
    ) -> TiResult<(Substitutions<'ctx>, HlirNode<'ctx>)> {
        let key_type = self.mk_unification_var(&node.source_region, "dict expr key")?;
        let value_type = self.mk_unification_var(&node.source_region, "dict expr value")?;
        let mut substitutions = Substitutions::new();
        let mut kvs = Vec::with_capacity(v.len());
        for elem in v {
            match elem {
                crate::ast::DictExprElem::KeywordSpread(node) => {
                    let (s0, typed_node) = self.make_hlir(node)?;
                    substitutions = self.compose(s0, substitutions)?;
                    if let Some((found_key_type, found_value_type)) =
                        typed_node.inferred_type.get_dict_data()
                    {
                        substitutions = self.compose(
                            self.unify_eq(key_type, found_key_type, &node.source_region)?,
                            substitutions,
                        )?;
                        substitutions = self.compose(
                            self.unify_eq(value_type, found_value_type, &node.source_region)?,
                            substitutions,
                        )?;
                        kvs.push(DictExprElem::KeywordSpread(typed_node));
                    } else {
                        return Err(TiError::new(
                            loc::err::kwspread_not_a_dictionary(
                                &typed_node.inferred_type.to_string(),
                            ),
                            node.source_region.clone(),
                        ));
                    }
                }
                crate::ast::DictExprElem::KeyValue(key_node, value_node) => {
                    let (s0, typed_key_node) = self.make_hlir(key_node)?;
                    substitutions = self.compose(s0, substitutions)?;

                    let s1 = self.unify_eq(
                        key_type,
                        typed_key_node.inferred_type,
                        &key_node.source_region,
                    )?;
                    substitutions = self.compose(s1, substitutions)?;

                    let (s2, typed_value_node) = self.make_hlir(value_node)?;
                    substitutions = self.compose(s2, substitutions)?;

                    let s3 = self.unify_eq(
                        value_type,
                        typed_value_node.inferred_type,
                        &value_node.source_region,
                    )?;
                    substitutions = self.compose(s3, substitutions)?;
                    kvs.push(DictExprElem::KeyValue(typed_key_node, typed_value_node));
                }
            }
        }
        let inferred_type = self.mk_dict(key_type, value_type);
        let inferred_type = inferred_type
            .apply_subst(&substitutions, self)?
            .unwrap_or(inferred_type);
        let kvs = kvs.apply_subst(&substitutions, self)?.unwrap_or(kvs);
        Ok((
            substitutions,
            HlirNode::new(
                inferred_type,
                node.source_region.clone(),
                HlirNodeKind::DictExpr(kvs),
            ),
        ))
    }

    /// Apply Expr portion of `make_hlir`
    fn make_hlir_apply_expr<'a>(
        &'a self,
        func: &AstNode,
        args: &Vec<AstNode>,
        node: &AstNode,
    ) -> TiResult<(Substitutions<'ctx>, HlirNode<'ctx>)> {
        let (mut combined_subs, mut typed_func) = self.make_hlir(func)?;
        let (combined_subs, targs) = if args.is_empty() {
            (combined_subs, Vec::new())
        } else {
            // new scope so that the types inferred by one call site don't force other call sites to have the same types
            // in other words this is what makes it polymorphic
            self.in_new_scope(|ctx| {
                // get scope caught up before we infer args
                ctx.apply_substitutions_in_current_scope(&combined_subs)?;
                // convert each argument
                let mut targs: Vec<HlirNode> = Vec::with_capacity(args.len());
                for arg in args {
                    // infer next arg
                    let (new_subs, targ) = ctx.make_hlir(arg)?;

                    typed_func.apply_subst_in_place(&new_subs, ctx)?;
                    for targ in &mut targs {
                        targ.apply_subst_in_place(&new_subs, ctx)?;
                    }
                    ctx.apply_substitutions_in_current_scope(&new_subs)?;
                    combined_subs = ctx.compose(new_subs, combined_subs)?;

                    targs.push(targ);
                }
                Result::<_, TiError>::Ok((combined_subs, targs))
            })?
        };
        let result_var = self.mk_unification_var(&node.source_region, "result of apply")?;
        let arg_types = targs.iter().map(|targ: &HlirNode<'ctx>| targ.inferred_type);
        let built_type = self.mk_fn_type_from_iter(arg_types, result_var);
        let f_subs = self.unify_eq(built_type, typed_func.inferred_type, &node.source_region)?;
        let final_subs = self.compose(f_subs, combined_subs)?;
        let result_var = result_var
            .apply_subst(&final_subs, self)?
            .unwrap_or(result_var);
        let typed_func = typed_func
            .apply_subst(&final_subs, self)?
            .unwrap_or(typed_func);
        let targs = targs.apply_subst(&final_subs, self)?.unwrap_or(targs);
        Ok((
            final_subs,
            HlirNode::new(
                result_var,
                node.source_region.clone(),
                HlirNodeKind::ApplyExpr {
                    func: Box::new(typed_func),
                    args: targs,
                },
            ),
        ))
    }

    /// convert dot expression
    pub(crate) fn make_hlir_dot_expr(
        &self,
        lhs: &AstNode,
        method_name: &StringType,
        source_region: &SourceRegion,
    ) -> TiResult<(Substitutions<'ctx>, HlirNode<'ctx>)> {
        let (substitutions, lhs) = self.make_hlir(lhs)?;
        let method_ty = self.member(lhs.inferred_type, method_name, source_region)?;
        let result_ty = method_ty;
        Ok((
            substitutions,
            HlirNode {
                inferred_type: result_ty,
                source_region: source_region.clone(),
                kind: HlirNodeKind::DotExpr {
                    lhs: lhs.into(),
                    method_name: method_name.clone(),
                },
            },
        ))
    }

    /// convert the ast to a typed one
    pub(crate) fn make_hlir_from_ast(&self, node: &AstNode) -> Result<HlirNode<'ctx>, TiError> {
        let (substitutions, mut typed_node) = self.make_hlir(node)?;
        self.apply_subst_to_constraints(&substitutions)?;

        typed_node.apply_subst_in_place(&substitutions, self)?;

        let mut typed_node = solve_constraints(self, &typed_node)?.unwrap_or(typed_node);
        typed_node.finalize_types(self, &node.source_region)?;

        Ok(typed_node)
    }

    /// convert just a single binary operation
    fn make_hlir_binop_step(
        &self,
        init: BinopStepReturn<'ctx>,
        op: Operator,
        rhs: &AstNode,
        source_region: &SourceRegion,
    ) -> Result<BinopStepReturn<'ctx>, TiError> {
        // TODO: take a more careful look at when we need to do the substitutions in what
        let BinopStepReturn {
            substitutions: combined,
            inferred_type: left_type,
            mut first,
            mut rest,
        } = init;
        let (s, mut rhs) = self.make_hlir(rhs)?;
        let combined = self.compose(s, combined)?;

        first.apply_subst_in_place(&combined, self)?;
        let left_type = left_type.apply_subst(&combined, self)?.unwrap_or(left_type);
        for (_, node) in &mut rest {
            node.apply_subst_in_place(&combined, self)?;
        }

        // find the method type for the operation
        let method_type =
            self.find_op_method_type(left_type, rhs.inferred_type, op, source_region)?;

        // process function application
        let (s, return_type) =
            self.infer_application2(method_type, left_type, rhs.inferred_type, source_region)?;
        let combined = self.compose(s, combined)?;
        // update after unification
        for (_, node) in &mut rest {
            node.apply_subst_in_place(&combined, self)?;
        }
        rhs.apply_subst_in_place(&combined, self)?;

        rest.push((op, rhs));

        Ok(BinopStepReturn {
            substitutions: combined,
            inferred_type: return_type,
            first,
            rest,
        })
    }

    /// infer types for a lambda
    fn make_hlir_lambda(
        &self,
        params: &Rc<RefCell<Vec<StegoPattern>>>,
        body: &AstNode,
        source_region: &SourceRegion,
        placeholder_name: Option<StringType>,
    ) -> TiResult<(Substitutions<'ctx>, HlirNode<'ctx>)> {
        self.in_new_scope(|ctx: &Context<'ctx>| {
            let params_ref = (**params).borrow();
            let mut param_types = Vec::with_capacity(params_ref.len());

            for pat in params_ref.iter() {
                let name = pat.to_string(); // TODO: destructuring
                let placeholder =
                    ctx.mk_unification_var(&pat.source_region, format!("lambda parameter {name}"))?;
                let destructure_results = pat.destructure_type(placeholder)?;

                for (name, ty) in destructure_results {
                    param_types.push(ty);
                    ctx.scopes.borrow_mut().insert(name, ty);
                }
            }

            let captures = crate::parse::findcaptures::find_captures(self, body)?;

            let (substitutions, typed_body) = ctx.make_hlir(body)?;

            let fn_ty = self.mk_fn_type(param_types.into_boxed_slice(), typed_body.inferred_type);
            Ok((
                substitutions,
                HlirNode::new(
                    fn_ty,
                    source_region.clone(),
                    HlirNodeKind::Lambda {
                        body: Rc::new(RefCell::new(typed_body)),
                        captures,
                        params: Rc::clone(params),
                        placeholder_name,
                    },
                ),
            ))
        })
    }
}
