#![warn(clippy::all)]

// public modules TODO: probably fewer of these should be public?

pub mod ast;
pub mod interpreter;
pub mod parse;
pub mod repl;
pub mod stegoobject;

#[cfg(not(test))]
pub mod util;

// private modules

mod builtin;
mod codelines;
mod config;
mod controlflow;
mod env;
mod error;
pub use error::ErrorCode;
mod fntypes;
mod hlir;
mod intern;
mod lang;
mod loc;
mod pattern;
mod reporting;
mod sourceregion;
mod tests;
mod ti;
mod typedvalue;

#[cfg(test)]
mod util;

use interpreter::parse_and_evaluate_string;
use repl::Repl;
use util::output_stream::OutputStream;

// TODO: rename to indicate that it is interned
pub(crate) type StringType = string_cache::DefaultAtom;

/// start up a repl
pub fn run_repl() {
    Repl::run_new(&mut std::io::stdout(), &mut std::io::stderr())
        .unwrap_or_else(|(short_msg, help)| eprintln!("{short_msg}\n\n{help}"));
}

/// evaluate some stegolang and return result as a string
///
/// # Errors
///
/// If there are errors in parsing or evaluating the code, they will be printed
/// to `err_stream` and an error code returned
pub fn evaluate_string<'a, T>(
    code: T,
    out_stream: &'a mut dyn OutputStream,
    err_stream: &'a mut dyn OutputStream,
) -> Result<String, ErrorCode>
where
    T: Into<String> + Clone + std::marker::Send + 'static,
{
    let x = parse_and_evaluate_string(code, loc::default_file_path(), out_stream, err_stream)?;
    Ok(format!("{x:?}"))
}
