//! the code being compiled/analyzed/run, presliced into lines
use crate::{config, StringType};
use std::{cmp::min, ops::Range};

/// used by sourceregion to look up the original code
#[derive(Debug, PartialEq, Eq)]
pub struct CodeLines {
    file_label: StringType,
    code: String,
    lines: Vec<Range<usize>>,
}
impl PartialOrd for CodeLines {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}
impl Ord for CodeLines {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        use std::cmp::Ordering::{Equal, Greater, Less};
        match self.file_label.cmp(&other.file_label) {
            Less => Less,
            Greater => Greater,
            Equal => self.code.cmp(&other.code),
        }
    }
}

impl CodeLines {
    /// create
    pub fn new<T: Into<StringType>, U: Into<String>>(file_label: T, code: U) -> Self {
        let file_label = file_label.into();
        let code = code.into();
        let mut lines = Vec::with_capacity(config::LINES_PER_FILE_ESTIMATE);
        let mut pos = 0;
        for line in code.split('\n') {
            // split didn't include the newline
            let stride = line.len() + 1;
            let end = min(pos + stride, code.len());
            lines.push(pos..end);
            pos += stride;
        }
        CodeLines {
            file_label,
            code,
            lines,
        }
    }

    /// how many bytes of source code there are
    #[inline]
    pub fn len(&self) -> usize {
        self.code.len()
    }

    /// slice of whole code
    #[inline]
    pub fn get_by_byte<I: core::slice::SliceIndex<str>>(&self, i: I) -> Option<&I::Output> {
        self.code.get(i)
    }

    /// get a line of the code by 1-based line number
    pub fn get_line(&self, line_number: usize) -> Option<&str> {
        if line_number > 0 {
            match self.lines.get(line_number - 1) {
                Some(range) => Some(&self.code[range.clone()]),
                None => None,
            }
        } else {
            None
        }
    }

    /// name for the source code
    #[inline]
    pub fn get_file_name(&self) -> &str {
        &self.file_label
    }
}
