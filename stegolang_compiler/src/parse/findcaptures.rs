use crate::ast::astnode::AstNodeKind::{
    ApplyExpr, AssignStmt, BinOpExpr, BitwiseNot, ComparisonOpExpr, DictExpr, DoExpr, DotExpr,
    FnStmt, Identifier, IfExpr, InExpr, Lambda, Let, LiteralValue, LogicalNot, NamedExpr,
    ReturnStmt, SetExpr, Statements, Tuple, TypeOf, UnaryMinus, UnaryPlus, UnitType,
};
use crate::pattern::StegoPattern;
use crate::{
    ast::astnode::AstNode,
    loc,
    ti::{Context, TiError},
    StringType,
};
use std::{cell::RefCell, collections::BTreeSet};

type Captures = BTreeSet<StringType>;
type FindCapturesResult<T> = Result<T, TiError>;

/// walk the body of a function, looking for variables captured by the function
pub(crate) fn find_captures(ctx: &Context<'_>, node: &AstNode) -> FindCapturesResult<Captures> {
    let mut captures = Captures::new();
    let max_depth = ctx.scopes.borrow().get_current_depth();
    ctx.scopes.borrow_mut().push();
    let result = walk(ctx, node, max_depth, &mut captures);
    ctx.scopes.borrow_mut().pop();
    result?;
    Ok(captures)
}

/// Helper for `find_captures`. search statements for identifiers to capture.
///
/// `max_stack_depth` is the stack depth beyond which any found identifiers actually
/// refer to something local, so we would not be capturing them
///
/// Make sure the top-level call of it is surrounded by pushing and popping
/// the scope
fn walk(
    ctx: &Context<'_>,
    node: &AstNode,
    max_stack_depth: usize,
    found_captures: &mut Captures,
) -> FindCapturesResult<()> {
    let source_region = &node.source_region;

    match &node.kind {
        // nodes that can't capture
        LiteralValue(..) | ReturnStmt(None) | TypeOf(..) | UnitType => Ok(()),

        // nodes that are simple wrappers on other nodes
        BitwiseNot(node)
        | LogicalNot(node)
        | UnaryMinus(node)
        | UnaryPlus(node)
        | ReturnStmt(Some(node)) => walk(ctx, node, max_stack_depth, found_captures),

        // nodes that wrap a list of other nodes
        SetExpr(nodes) => walk_set_expr(nodes, ctx, max_stack_depth, found_captures),
        Statements {
            statements: nodes, ..
        }
        | Tuple(nodes) => {
            for node in nodes {
                walk(ctx, node, max_stack_depth, found_captures)?;
            }
            Ok(())
        }

        // walk subexpressions of apply expressions
        ApplyExpr { func, args, .. } => {
            walk(ctx, func, max_stack_depth, found_captures)?;
            for node in args {
                walk(ctx, node, max_stack_depth, found_captures)?;
            }
            Ok(())
        }

        // walk subexpressions of binary operators
        BinOpExpr { first, rest, .. } | ComparisonOpExpr { first, rest, .. } => {
            walk(ctx, first, max_stack_depth, found_captures)?;
            for (_, node) in rest {
                walk(ctx, node, max_stack_depth, found_captures)?;
            }
            Ok(())
        }

        // walk subexpressions of assignment
        AssignStmt { lhs, rhs, .. } => {
            walk(ctx, lhs, max_stack_depth, found_captures)?; // TODO: lhs should be a pattern and therefore have nothing to walk?
            walk(ctx, rhs, max_stack_depth, found_captures)
        }

        // walk subexpressions of dictionary expression
        DictExpr(v) => walk_dict_expr(v, ctx, max_stack_depth, found_captures),

        // walk each statement of a do
        DoExpr(node) => {
            ctx.scopes.borrow_mut().push();
            let result = walk(ctx, node, max_stack_depth, found_captures);
            ctx.scopes.borrow_mut().pop();
            result
        }

        //
        DotExpr {
            lhs,
            method_name: _,
        } => walk(ctx, lhs, max_stack_depth, found_captures),

        // internal fn stmt
        FnStmt(crate::ast::FnStmt {
            name, params, body, ..
        }) => walk_fn_stmt(
            params,
            ctx,
            name,
            source_region,
            body,
            max_stack_depth,
            found_captures,
        ),

        // identifiers capture if they refer to something from outside the function
        Identifier { name } => {
            walk_identifier(found_captures, name, ctx, source_region, max_stack_depth)
        }

        // conditions and statements in the arms of an if
        IfExpr {
            arms,
            catch_all_else,
            ..
        } => walk_if_expr(arms, ctx, max_stack_depth, found_captures, catch_all_else),

        InExpr {
            container,
            invert: _,
            item,
        } => {
            walk(ctx, container, max_stack_depth, found_captures)?;
            walk(ctx, item, max_stack_depth, found_captures)?;
            Ok(())
        }

        Lambda { params, body, .. } => {
            walk_lambda(ctx, params, body, max_stack_depth, found_captures)
        }

        Let {
            pattern,
            value_expr,
            ..
        } => walk_let(ctx, pattern, value_expr, max_stack_depth, found_captures),

        NamedExpr { name, value, .. } => {
            walk_named_expr(ctx, value, max_stack_depth, found_captures, name)
        }
    }
}

// part of `walk` above for let statements
fn walk_let(
    ctx: &Context<'_>,
    pattern: &StegoPattern,
    value_expr: &AstNode,
    max_stack_depth: usize,
    found_captures: &mut BTreeSet<string_cache::Atom<string_cache::EmptyStaticAtomSet>>,
) -> Result<(), TiError> {
    // TODO: instead of messing with scope and checking stack depths, this should just pass along a list of things to ignore
    let ty = ctx.mk_unification_var(&pattern.source_region, "temporary for walk_let")?;
    for (name, ty) in pattern.destructure_type(ty)? {
        ctx.scopes.borrow_mut().insert(name, ty);
    }
    walk(ctx, value_expr, max_stack_depth, found_captures)
}

// part of `walk` above for named expressions
fn walk_named_expr(
    ctx: &Context<'_>,
    value: &AstNode,
    max_stack_depth: usize,
    found_captures: &mut BTreeSet<string_cache::Atom<string_cache::EmptyStaticAtomSet>>,
    name: &string_cache::Atom<string_cache::EmptyStaticAtomSet>,
) -> Result<(), TiError> {
    walk(ctx, value, max_stack_depth, found_captures)?;
    ctx.scopes
        .borrow_mut()
        .insert(StringType::from(name), ctx.mk_nothing());
    Ok(())
}

// part of `walk` above for ifs
fn walk_if_expr(
    arms: &Vec<crate::ast::IfArm<AstNode>>,
    ctx: &Context<'_>,
    max_stack_depth: usize,
    found_captures: &mut BTreeSet<string_cache::Atom<string_cache::EmptyStaticAtomSet>>,
    catch_all_else: &Option<Box<AstNode>>,
) -> Result<(), TiError> {
    for arm in arms {
        walk(ctx, &arm.cond, max_stack_depth, found_captures)?;
        ctx.in_new_scope(|ctx| walk(ctx, &arm.consequent, max_stack_depth, found_captures))?;
    }
    if let Some(catch_all_else) = catch_all_else {
        ctx.in_new_scope(|ctx| walk(ctx, catch_all_else, max_stack_depth, found_captures))?;
    }
    Ok(())
}

// part of `walk` above for identifiers
fn walk_identifier(
    found_captures: &mut BTreeSet<string_cache::Atom<string_cache::EmptyStaticAtomSet>>,
    name: &string_cache::Atom<string_cache::EmptyStaticAtomSet>,
    ctx: &Context<'_>,
    source_region: &crate::sourceregion::SourceRegion,
    max_stack_depth: usize,
) -> Result<(), TiError> {
    if found_captures.contains(name) {
        return Ok(());
    }
    match ctx.scopes.borrow().stack_depth_of_definition(name) {
        None => {
            return Err(TiError::new(
                loc::err::identifier_not_in_scope(name.as_ref()),
                source_region.clone(),
            ))
        }
        Some(depth) => {
            if depth < max_stack_depth {
                match ctx.scopes.borrow().get_upvalue(name) {
                    Some(_) => {
                        // captured a value!
                        found_captures.insert(StringType::clone(name));
                        return Ok(());
                    }
                    None => {
                        return Err(TiError::new(
                            loc::err::identifier_not_in_scope(name.as_ref()),
                            source_region.clone(),
                        ))
                    }
                }
            }
        }
    }
    Ok(())
}

// part of `walk` above for dictionary expressions
fn walk_dict_expr(
    v: &Vec<crate::ast::DictExprElem<AstNode>>,
    ctx: &Context<'_>,
    max_stack_depth: usize,
    found_captures: &mut BTreeSet<string_cache::Atom<string_cache::EmptyStaticAtomSet>>,
) -> Result<(), TiError> {
    use crate::ast::DictExprElem::{KeyValue, KeywordSpread};
    for elem in v {
        match elem {
            KeywordSpread(node) => walk(ctx, node, max_stack_depth, found_captures)?,
            KeyValue(k, v) => {
                walk(ctx, k, max_stack_depth, found_captures)?;
                walk(ctx, v, max_stack_depth, found_captures)?;
            }
        }
    }
    Ok(())
}

// part of `walk` above for fn statements
fn walk_fn_stmt(
    params: &std::rc::Rc<RefCell<Vec<crate::pattern::StegoPattern>>>,
    ctx: &Context,
    name: &string_cache::Atom<string_cache::EmptyStaticAtomSet>,
    source_region: &crate::sourceregion::SourceRegion,
    body: &std::rc::Rc<RefCell<AstNode>>,
    max_stack_depth: usize,
    found_captures: &mut BTreeSet<string_cache::Atom<string_cache::EmptyStaticAtomSet>>,
) -> Result<(), TiError> {
    let param_vec = RefCell::borrow(params);
    ctx.scopes.borrow_mut().insert(
        StringType::from(name),
        ctx.mk_fn_var(param_vec.len(), source_region)?,
    );
    walk_lambda(ctx, params, body, max_stack_depth, found_captures)
}

// part of `walk` above for set expressions
fn walk_set_expr(
    nodes: &Vec<crate::ast::SetExprElem<AstNode>>,
    ctx: &Context<'_>,
    max_stack_depth: usize,
    found_captures: &mut BTreeSet<string_cache::Atom<string_cache::EmptyStaticAtomSet>>,
) -> Result<(), TiError> {
    for node in nodes {
        match node {
            crate::ast::SetExprElem::Spread(node) | crate::ast::SetExprElem::Value(node) => {
                walk(ctx, node, max_stack_depth, found_captures)?;
            }
        }
    }
    Ok(())
}

// part of `walk` above for lambdas
fn walk_lambda<'ctx>(
    ctx: &Context<'ctx>,
    params: &std::rc::Rc<RefCell<Vec<crate::pattern::StegoPattern>>>,
    body: &std::rc::Rc<RefCell<AstNode>>,
    max_stack_depth: usize,
    found_captures: &mut BTreeSet<string_cache::Atom<string_cache::EmptyStaticAtomSet>>,
) -> Result<(), TiError> {
    ctx.in_new_scope(|ctx: &Context<'ctx>| {
        let params_ref = (**params).borrow();
        for pat in params_ref.iter() {
            // TODO: instead of messing with scope and checking stack depths, this should just pass along a list of things to ignore
            let ty =
                ctx.mk_unification_var(&pat.source_region, "placeholder for lambda parameter")?;
            for (name, ty) in pat.destructure_type(ty)? {
                ctx.scopes.borrow_mut().insert(name, ty);
            }
        }
        let body_ref = (**body).borrow();
        walk(ctx, &body_ref, max_stack_depth, found_captures)
    })
}
