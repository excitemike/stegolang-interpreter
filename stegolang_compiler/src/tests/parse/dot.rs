use crate::{ast::astnode::AstNodeKind::*, match_or_panic, tests::parse::fst_stmt};

use super::test_file_error;


#[test]
fn parse_dot_1() {
    match_or_panic!(
        fst_stmt("10 . add 1"),
        ApplyExpr { func, ..} => func.kind,
        DotExpr { lhs, method_name} => &*method_name,
        "add" => lhs.kind,
        LiteralValue(_) => ()
    );
}
#[test]
fn parse_dot_2() {
    match_or_panic!(
        fst_stmt("10 .add 1"),
        ApplyExpr { func, ..} => func.kind,
        DotExpr { lhs, method_name} => &*method_name,
        "add" => lhs.kind,
        LiteralValue(_) => ()
    )
}

#[test]
fn parse_dot_3() {
    // `10.` is a float, so it actually is saying to apply a literal to identifier `add`
    match_or_panic!(
        fst_stmt("10. add 1"),
        ApplyExpr { func, args, ..} => func.kind,
        LiteralValue(_) => &args[..],
        [arg1, arg2] => (&arg1.kind, &arg2.kind),
        (Identifier{..}, LiteralValue(_)) => ()
    );
}

#[test]
fn parse_dot_4() {
    // looks like a number. make the user disambiguate
    test_file_error("10.add 1", 1, 4);
}

#[test]
fn parse_dot_5() {
    match_or_panic!(
        fst_stmt("(10).add 1"),
        ApplyExpr { func, ..} => func.kind,
        DotExpr { lhs, method_name} => &*method_name,
        "add" => lhs.kind,
        LiteralValue(_) => ()
    );
}