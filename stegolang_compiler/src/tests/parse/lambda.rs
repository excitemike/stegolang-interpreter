use crate::pattern::StegoPatternKind;

use super::*;

/// parse a simple lambda with no errors
#[test]
fn test_lambda_1() {
    match_or_panic! {
        parse("<test case>", r"\x=x+1"),
        Ok(_) => ()
    };
}
/// parse to a lambda node
#[test]
fn test_lambda_2() {
    match_or_panic! {
        &parse_stmts(r"\x=x+1")[..],
        [node, ..] => &node.kind,
        Lambda{..} => ()
    };
}
/// parse to a lambda node
#[test]
fn test_lambda_3() {
    match_or_panic! {
        &parse_stmts(r"\x=x+1")[..],
        [node, ..] => &node.kind,
        Lambda{
            body,
            params: patterns,
        } => {
            (&(**body).borrow().kind, &*(**patterns).borrow())
        },
        (AstNodeKind::Statements{statements}, patterns) => (&statements[..], &patterns[..]),
        ([AstNode{kind:AstNodeKind::BinOpExpr{..},..}],[pat]) => &pat.kind,
        StegoPatternKind::Capture(s) => &**s,
        "x" => ()
    };
}
