use itertools::Itertools;

use crate::tc;
use crate::ti::FnType;
use crate::{
    env::Env,
    match_or_panic,
    parse::parse_with_env,
    ti::{TiError, Type},
};
use crate::{typedvalue::OwnedValue::Int64, ErrorCode::*};
use std::borrow::Borrow;

fn with_new_env<F: FnOnce(&mut Env)>(f: F) {
    let out_stream = &mut std::io::stdout();
    let err_stream = &mut std::io::stderr();
    Env::enter(out_stream, err_stream, f).unwrap_or_else(
        |TiError {
             msg_tuple: (short_msg, help),
             ..
         }| {
            panic!(
                "Expected to have no errors. Got:\n  {}\n  {}",
                short_msg.borrow() as &str,
                help.borrow() as &str
            )
        },
    );
}

/// support function for the tests below
/// give it mock file contents and get back the final type of that code
#[track_caller]
fn test_file<'ctx, T>(env: &mut Env<'ctx>, code: T) -> Type<'ctx>
where
    T: Into<String> + Clone + std::marker::Send + 'static,
{
    let file_label = String::from("<test case>");
    let (maybe_ast, warnings) = parse_with_env(file_label, code, env)
        .unwrap_or_else(|errors| panic!("Expected to have no errors. Got {errors:#?}"));
    assert!(warnings.is_empty(), "Unexpected warnings {warnings:#?}");
    let ast = maybe_ast.expect("Missing result");

    env.ti_ctx
        .make_hlir_from_ast(&ast)
        .unwrap_or_else(|errors| panic!("Expected to have no errors. Got {errors:#?}"))
        .inferred_type
        .inner()
        .to_owned()
}

/// test the parser with empty input
#[test]
#[should_panic]
fn blank_file() {
    with_new_env(|env| {
        assert!(matches!(test_file(env, ""), Type::Nothing));
    });
}

#[test]
fn test_unit_type() {
    with_new_env(|env| {
        assert!(matches!(test_file(env, "nothing"), Type::Nothing));
        assert!(matches!(test_file(env, "let x = 0"), Type::Nothing));
        assert!(matches!(test_file(env, "fn foo x = x + 1"), Type::Nothing));
    });
}

#[test]
fn lambda() {
    #[allow(irrefutable_let_patterns)]
    with_new_env(|env| {
        match_or_panic! {
            test_file(env, r"\x=x"),
            Type::F(FnType {
                params,
                result
            }) => (params.iter().map(|x|x.inner()).collect_vec(), result),
            (v, result) => (v.as_slice(), result.inner()),
            ([Type::UnificationVar { uniq_id: a, .. }], Type::UnificationVar { uniq_id: b, .. }) => assert_eq!(a, b)
        }
    });
}

#[test]
fn test_bad_identifier() {
    with_new_env(|env| {
        let (opt_ast, warnings) = parse_with_env("<test case>", "(a,b,c,d,e,f,g)", env).unwrap();
        assert!(warnings.is_empty());
        let ast = opt_ast.unwrap();

        match_or_panic! {
            env.ti_ctx.make_hlir_from_ast(&ast),
            Err(_) => ()
        };
    });
}

tc! { generics_and_capturing r#"

fn double x = x + x
    
let first = do
    let a = 100
    fn f _ = double a
    f ()
    
let second = do
    let a = "100"
    fn f _ = double a
    f ()
    
println (first ())
println (second ())
"# stdout => "200\n100100\n"
}

tc! { typeinf_test_1 r#"
fn foo x =
    fn bar x = x + 1
    bar x
foo 1
"# => Int64(2)
}
tc! { typeinf_test_2 r#"
fn foo x =
    fn bar x = x + 1
    bar x
foo 'c'
"# err => TypeError
}

tc! { monomorphizing_with_capture r#"
fn foo x y =
    let z = x
    fn bar x =
        x + z // bar's return type depends on captured z, which depends on foo's arguments
    bar y z
foo 1 10
"# => Int64(11)
}

tc! { instantiation r#"
fn id x = x
fn double x = x + x
(id double) (id 2)
"# => Int64(4)
}

tc! { type_environment r#"
fn f x =
    fn f y = x
    f 123
f 456
"# => Int64(456)
}

// TODO: "fn x = x" recursion should be detected
