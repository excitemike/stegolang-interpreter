use crate::{tc, typedvalue::OwnedValue};

// test that different trait implementations are used in different
// instantiations of a polymorphic function
tc! { test_polymorphism_with_traits
r#"
fn add a b = a + b
println (add 1 2)
println (add "a" "b")
"# ok => OwnedValue::String(s) if s == "3\nab\n"
}
