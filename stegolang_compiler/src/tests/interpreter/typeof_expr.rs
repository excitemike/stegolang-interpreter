//! Test the typeof keyword
#![allow(irrefutable_let_patterns)]

use crate::{
    ast::{astnode::AstNodeKind, literal::Literal},
    match_or_panic,
    parse::parse,
    tc,
    typedvalue::OwnedValue,
};

const FILE_LABEL: &str = "<test case>";

/// test the parsing of typeof expressions
#[test]
fn parse_typeof() {
    match_or_panic! {
        parse(FILE_LABEL, "typeof 1234"),
        Ok((Some(node), _)) => node.kind,
        AstNodeKind::Statements{statements} => &statements[..],
        [stmt] => &stmt.kind,
        AstNodeKind::TypeOf(node) => &node.kind,
        AstNodeKind::LiteralValue(Literal::Int64(1234)) => ()
    }
}

tc! { test_eval_typeof_1
    // because it is a literal, the type should report as `1234`
    r#"typeof 1234"# => {
        OwnedValue::String(s) => &**s
    } => s if s == crate::config::INT64_TYPE_NAME
}

// only a literal is assigned to x, so its type should print as `1234`
tc! { test_eval_typeof_2 r#"
let x = 1234
typeof x
"# => {
        OwnedValue::String(s) => &**s
    } => s if s == crate::config::INT64_TYPE_NAME
}

// now math is done and assigned to x, so it can't be just a literal `1234`
tc! { test_eval_typeof_3 r#"
let x = 1234
x = x + 1
typeof x
"# => {
        OwnedValue::String(s) => &**s
    } => s if s == crate::config::INT64_TYPE_NAME
}

// again a literal
tc! { test_eval_typeof_4
    r#"typeof "some string""# => {
        OwnedValue::String(s) => &**s
    } => s if s == crate::config::STRING_TYPE_NAME
}

// calculated value is not a literal
tc! { test_eval_typeof_5 r#"
let x = "some" + " " + "string"
typeof x
"# => {
        OwnedValue::String(s) => &**s
    } => s if s == crate::config::STRING_TYPE_NAME
}

// type information passing through named_expr
tc! { test_eval_typeof_6 r#"
let y = x @ "some string"
typeof x
"# => {
        OwnedValue::String(s) => &**s
    } => s if s == crate::config::STRING_TYPE_NAME
}

// some nonsense
tc! { test_eval_typeof_7
    "typeof (let x = 0)" stderr => s if s.contains("<test>:1:9")
}

tc! { test_eval_typeof_8
    "let x = 1 < 2 ; typeof x" => {
        OwnedValue::String(s) => &**s
    } => s if s == crate::config::BOOLEAN
}
// When `foo`` is referenced inside of foo without any arguments, it is still generic
// so even though `foo 0` did use an instantiation of `foo`, that instance is
// still free and thus typeof should report it as a function scheme.
tc! { test_eval_typeof_9
"fn foo a = a ; typeof foo ;; foo 0" => OwnedValue::String(s) if s == "(Fn Int64 String)"
}
tc! { test_eval_typeof_10 "fn identity x = x ;; print (typeof (identity 0))" stdout => s if s == "Int64" }

// generic function used as value. Not enough information to monomorphise a value with, so typeof can't tell you the value's type
tc! { test_eval_typeof_11 r#"
fn add a b = a + b
typeof add
"# err => crate::ErrorCode::TypeError
}

// partially generic function used as value. Not enough information to monomorphise a value with, so typeof can't tell you the value's type
tc! { test_eval_typeof_12 r#"
fn add a b = a + b
typeof (add 1)
"# err => crate::ErrorCode::TypeError
}
tc! { test_eval_typeof_13 r#"
let x = 1 + 2
typeof x
"# => OwnedValue::String(s) if s == "Int64"
}

// the function gets instantiated with two Ints, so typeof should be able to see that
tc! { test_eval_typeof_14 r#"
fn add a b = typeof (a + b)
add 1 2
"# => {
    OwnedValue::String(s) => &**s
} => s if s == "Int64"
}
tc! { test_eval_typeof_15 r#"
typeof (1 + 10)
"# => {
    OwnedValue::String(s) => &**s
} => s if s == crate::config::INT64_TYPE_NAME
}
