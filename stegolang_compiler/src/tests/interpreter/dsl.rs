//! a macro-implemented dsl for writing tests
#![cfg(test)]
#![allow(unreachable_patterns, unused_macros)]

use crate::{
    error::ErrorCode, interpreter::parse_and_evaluate_string, typedvalue::OwnedValue,
    util::output_stream::OutputStream,
};

crate::tc! { test_test_dsl_1 r#""abc" * 2 * 2"# => _ }
crate::tc! { test_test_dsl_2 r#"!false"# => OwnedValue::Bool(true) }
crate::tc! { test_test_dsl_3 r#"(!false)"# => OwnedValue::Bool(true) }
crate::tc! { test_test_dsl_4 r#"(!false,)"#
=> {
    OwnedValue::Tuple(v) if v.len() == 1 => &v[..],
    [x, ..] =>  x as &OwnedValue
} => OwnedValue::Bool(true) }
crate::tc! { test_test_dsl_5 r#"99>"100""# err => ErrorCode::EvaluationError }

/// Defines a test function containing test cases.
///
/// To use it, make a tc! block, and inside of it place a function name,
/// followed by any number of test case definitions. See documentation of
/// macro `test_case` for details of what test cases look like.
///
/// You may provide multiple tests per function, but when one of them fails,
/// it can be difficult to tell which case it was.
///
/// # Examples
///
/// ```rust
/// use crate::tc;
///
/// tc! {
///     test_simple_addition {
///         "1 + 1" => Int64(x) if x < 3 => "One plus one should be less than three."
///         "1 + 1" => Int64(2) => "One plus one should be two."
///     }
/// }
/// ```
///
#[macro_export]
macro_rules! tc {
    (
        $name:ident
        $(#[$($test_attrs:ident),*])?
        $(setup {$($setup_code:tt)*})?
        $(
            $stegolang_code:literal
            $($mode:ident)?
            $( => {
                $(
                    $( $patterns:pat_param )|+ $( if $guards:expr )? => $outs:expr
                ),*
                $(,)*
            })?
            => $( $pattern_last:pat_param )|+ $( if $guard_last: expr )?
            $(=> $message:literal)?
            $(,)*
        )*
    ) => {
        #[test]
        $(#[$($test_attrs),*])?
        fn $name() {
            $($($setup_code)*)?
            $({
                let code:&str = $stegolang_code;
                let result = $crate::tests::interpreter::dsl::test_dsl_evaluate::<&str>(code, true, true);
                let match_value = $crate::tc_select!(result, $($mode)?);
                $crate::test_value!(
                    match_value
                    $( =>{
                        $(
                            $($patterns)|+ $(if $guards)? => $outs
                        ),*
                    })?
                    => $($pattern_last)|+ $(if $guard_last)?
                    $( => $message )?
                )
            })*
        }
    }
}

/// Helper for tc! macro. Not intended for direct use.
#[macro_export]
macro_rules! tc_select {
    ($three_tuple:ident, ) => {
        $crate::tc_select!($three_tuple, ok)
    };
    ($three_tuple:ident, ok) => {{
        match &$three_tuple.0 {
            Ok(x) => x,
            Err(x) => panic!(
                "\nCode did not complete successfully (error = \"{:#?}\")\nstderr:\n{}\n",
                x,
                &$three_tuple.2.unwrap()[..]
            ),
        }
    }};
    ($three_tuple:ident, err) => {{
        match $three_tuple.0 {
            Ok(x) => panic!(
                "Code unexpectedly completed successfully (result = \"{:#?}\")",
                x
            ),
            Err(x) => x,
        }
    }};
    ($three_tuple:ident, stdout) => {{
        if let Err(x) = &$three_tuple.0 {
            panic!(
                "\nCode did not complete successfully (error = \"{:#?}\")\nstderr:\n{}\n",
                x,
                &$three_tuple.2.unwrap()[..]
            )
        }
        &$three_tuple.1.unwrap()[..]
    }};
    ($three_tuple:ident, stderr) => {{
        &$three_tuple.2.unwrap()[..]
    }};
}

/// Run a test case. See examples for usage.
///
/// # Examples
///
/// ## Run code, expect a result
///
/// Code to run, followed by fat arrow, followed by a pattern to match.
/// This expects the code to run successfully and pattern matches against the
/// end result (a `StegoObject`). Panics if there is an error or the final pattern
/// doesn't match.
///
/// ```rust
/// use stegolang::tests::interpreter::test_case;
/// test_case!("1 + 1" => Int64(2))
/// ```
///
/// ## With custom message
///
/// Like the above, but if it panics due to a failed match, it will include
/// the provided message in the panic message.
///
/// ```rust
/// use stegolang::tests::interpreter::test_case;
/// test_case!("1 + 1" => Int64(2) => "One plus one should equal two.")
/// ```
///
/// ## With guards
///
/// Guards are permitted alongside the patterns. If the guard fails, the test case
/// will panic just like if the pattern does.
///
/// ```rust
/// use stegolang::tests::interpreter::test_case;
/// test_case!("1 + 1" => Int64(x) if x < 3 => "One plus one should be less than three.")
/// ```
///
/// ## Additional matching and transforming
///
/// You can also perform additional matching and transforming of the values
/// before the final pattern match. This is done with another arrow after the code
/// and curly braces containing comma-separated sets of
///     `<pattern> <optional guard> => <expression>`.
///
/// These are applied in order. If any of the patterns or guards fail, the test
/// case panics. If it succeeds the provided expression is matched with the next
/// set, or the final pattern.
///
/// ```rust
/// use stegolang::tests::interpreter::test_case;
/// test_case!(
///     r#"("abc" * 2,) * 2"# => {
///         Tuple(v) if v.len() == 2 => &v[..],
///         [x, ..] => &*(x.borrow()),
///         StegoString(s) => &**s
///         } => "abcabc"
///         => "Error multiplying tuple of multiplied string."
/// ```
///
/// ## Testing for Errors
///
/// To test that the code produces an error, add `err` before the first arrow.
///
/// ```rust
/// use stegolang::tests::interpreter::test_case;
/// test_case!{ r#"99>"100""# err => ErrorCode::EvaluationError }
/// ```
/// TODO: document stdout and stderr modes
///

/// Continue the work of `test_case` after evaluating the code
#[macro_export]
macro_rules! test_value {
    // expression, pattern to match and optional error message
    ($value:expr => $({} =>)? $( $pattern:pat_param )|+ $( if $guard: expr )? $(=> $message:literal)?) => {{
        match $value {
            $($pattern)|+ $(if $guard)? => (),
            x => $crate::test_value_panic!(x, stringify!($($pattern)|+ $(if $guard)?) $(, $message)?)
        }
    }};
    // code to run, block of pattern=>expression mappings, one final pattern to match, and optional error message
    (
        $value:expr => {
            $( $pattern0:pat_param )|+ $( if $guard0:expr )? => $out0:expr
            $(,
                $( $patterns:pat_param )|+ $( if $guards:expr )? => $outs:expr
            )*
        } => $( $pattern_last:pat_param )|+ $( if $guard_last: expr )?
        $(=> $message:literal)?
    ) => {{
        match $value {
            $($pattern0)|+ $(if $guard0)? => $crate::test_value!{
                $out0 => {$(
                    $($patterns)|+ $(if $guards)? => $outs
                ),*} =>
                $($pattern_last)|+ $(if $guard_last)?
                $(=> $message)?
            },
            x => $crate::test_value_panic!(x, stringify!($($pattern0)|+ $(if $guard0)?) $(, $message)?)
        }
    }}
}

/// Helper for `test_value`. Not intended to be used directly.
#[macro_export]
macro_rules! test_value_panic {
    ($value:expr, $pattern_string:expr, $custom_message:literal) => {{
        panic!(
            "{:#?} (Value {:#?} did not match pattern {:#?}.",
            $custom_message, $value, $pattern_string
        )
    }};
    ($value:expr, $pattern_string:expr) => {{
        panic!(
            "(Value {:#?} did not match pattern {:#?}.",
            $value, $pattern_string
        )
    }};
}

/// Parse and evaluate the code, optionally catching stdout and stderr.
///
/// # Panics
///
/// Panics if it evaluates successfully, but the end result can't be unwrapped.
pub(crate) fn test_dsl_evaluate<T>(
    code: T,
    catch_stdout: bool,
    catch_stderr: bool,
) -> (
    Result<OwnedValue, ErrorCode>,
    Option<String>,
    Option<String>,
)
where
    T: Into<String> + Clone + std::marker::Send + 'static,
{
    let mut out_stream: Box<dyn OutputStream> = if catch_stdout {
        Box::<Vec<u8>>::default()
    } else {
        Box::new(std::io::stdout())
    };
    let mut err_stream: Box<dyn OutputStream> = if catch_stderr {
        Box::<Vec<u8>>::default()
    } else {
        Box::new(std::io::stderr())
    };
    let result = parse_and_evaluate_string(code, "<test>", &mut *out_stream, &mut *err_stream);

    (result, out_stream.into_string(), err_stream.into_string())
}
