//! unit tests for the interpreter
#![cfg(test)]
#![allow(unreachable_patterns)]
use crate::error::ErrorCode::*;
use crate::tc;
use crate::typedvalue::OwnedValue;
use crate::typedvalue::OwnedValue::*;
use num::bigint::{BigInt, ToBigInt};
use rustc_hash::{FxHashMap, FxHashSet};
use std::fmt::Display;

mod builtin;
pub(crate) mod dsl;
mod lambda;
mod traits;
mod typeof_expr;

/// panic if `HashSet` doesn't have (the equivalent `StegoObject` of) all the specified things
#[track_caller]
fn assert_set_has<T>(hashset: &FxHashSet<OwnedValue>, things: &[T])
where
    OwnedValue: Match<T>,
    T: Display,
{
    for thing in things {
        assert!(
            hashset.iter().any(|set_elem| set_elem.matches(thing)),
            "Set was supposed to contain \"{thing}\""
        );
    }
}

/// panic if `HashSet` contains (the equivalent `StegoObject` of) any of the specified thing
#[track_caller]
fn assert_not_in_set<T>(hashset: &FxHashSet<OwnedValue>, things: &[T])
where
    OwnedValue: Match<T>,
    T: Display,
{
    for thing in things {
        for set_elem in hashset {
            assert!(
                !set_elem.matches(thing),
                "Set was NOT supposed to contain \"{thing}\""
            );
        }
    }
}
trait Match<T> {
    fn matches(&self, t: &T) -> bool;
}
impl Match<i64> for OwnedValue {
    fn matches(&self, t: &i64) -> bool {
        if let OwnedValue::Int64(i) = self {
            i == t
        } else {
            false
        }
    }
}

tc! { test_unit_type
    "" => OwnedValue::Nothing
    "()" => OwnedValue::Nothing
    "let x = () ; x" => OwnedValue::Nothing
    "nothing" => OwnedValue::Nothing
    "nothing == ()" => OwnedValue::Bool(true)
    "nothing == 0" err => TypeError
}

tc! { test_eval_simple_literals
    // booleans
    "true" => OwnedValue::Bool(true)
    "false" => OwnedValue::Bool(false)
    "(false)" => OwnedValue::Bool(false)

    // integers
    "1" => Int64(1)
    "9223372036854775807" => Int64(9_223_372_036_854_775_807)
    "1000000000000000000000000000000000000000" => BigInt(x) if x == &BigInt::parse_bytes(b"1000000000000000000000000000000000000000", 10).unwrap()

    // floats
    "1.2e34" => Float64(f) if (f - 1.2e34).abs() < f64::EPSILON
    "1.2e34" => Float64(f) if (f - 1.2e34).abs() < f64::EPSILON
    "-1E100" => Float64(f) if (f + 1E100).abs() < f64::EPSILON
    "0.0" => Float64(f) if (f - 0.0).abs() < f64::EPSILON

    // character literals
    "' '" => Char(' ')
    r"'\u{1f192}'" => Char('🆒')

    //string literals
    "\"\"" => OwnedValue::String(s) if s.is_empty()
    "\"123_ö̲🦌🦌🦌🦌🦌🦌🦌🦌🛷🕶🎅🏾8\"" => OwnedValue::String(s) if s == "123_ö̲🦌🦌🦌🦌🦌🦌🦌🦌🛷🕶🎅🏾8"
    "\"\\\"\"" => OwnedValue::String(s) if s == "\""
}

tc! { test_eval_unary_minus
    "-9223372036854775808" => BigInt(x) if x == &BigInt::parse_bytes(b"-9223372036854775808", 10).unwrap(),
    "-1000000000000000000000000000000000000000" => BigInt(x) if x == &BigInt::parse_bytes(b"-1000000000000000000000000000000000000000", 10).unwrap(),
    "-1" => Int64(-1),
    "-0x100" => Int64(-256),
    "-1.0" => Float64(f) if (f + 1.0).abs() < f64::EPSILON
}

tc! { test_eval_bitwise_not
    "~1" => Int64(-2),
    "-~1" => Int64(2),
    "~100" => Int64(-101),
    "~-101" => Int64(100),
    "~9223372036854775807" => Int64(-9_223_372_036_854_775_808),
    "~0xcc" => Int64(-0xcd),
    "~10000000000000000000" => BigInt(x) if x == &BigInt::parse_bytes(b"-10000000000000000001", 10).unwrap()
}

tc! { test_eval_logical_not
    "!true" => OwnedValue::Bool(false),
    "!false" => OwnedValue::Bool(true),
    "!!true" => OwnedValue::Bool(true),
    "!1" => OwnedValue::Bool(false),
    r#"!"something""# => OwnedValue::Bool(false),
    r#"!"""# => OwnedValue::Bool(true),
    r#"!' '"# => OwnedValue::Bool(false),
    r#"!(0.0)"# => OwnedValue::Bool(true),
    r#"!(1.0)"# => OwnedValue::Bool(false),
    r#"!10000000000000000000"# => OwnedValue::Bool(false)
}
tc! { test_eval_unary_plus
    "+1" => Int64(1),
    "++++0" => Int64(0),
    "++++123.456" => Float64(f) if (f - 123.456).abs() < f64::EPSILON,
    "+10000000000000000000" => BigInt(x) if x == &BigInt::parse_bytes(b"10000000000000000000", 10).unwrap()
}

tc! { test_eval_add_sub
    "1+1" => Int64(2),
    "100--50" => Int64(150),
    "11-7" => Int64(4),
    "3-0x70" => Int64(-109),
    "(1000 + 2000) - (5000 + 100 - 200 + 300) - 5" => Int64(-2205),
    "10000000000000000000 + 20000000000000000000" => BigInt(x) if x == &BigInt::parse_bytes(b"30000000000000000000", 10).unwrap()
}

tc! { test_eval_string_addition
    r#""abc" + "def""# => OwnedValue::String(s) if s == "abcdef",
    r#"1 + "abc""# => OwnedValue::String(s) if s == "1abc",
    r#""abc" + 123"# => OwnedValue::String(s) if s == "abc123",
    r#""abc " + ()"# => OwnedValue::String(s) if **s == format!("abc {}", crate::config::NOTHING_VALUE),
    r#""abc " + ("def", 1e2)"# => OwnedValue::String(s) if s == "abc (def,100)",
    r#"("abc",) + "def""# => OwnedValue::String(s) if s == r#"(abc,)def"#,
    "10000000000000000000 + \" is a big number\"" => OwnedValue::String(s) if s == "10000000000000000000 is a big number",
    "\"look at this big number: \" + 10000000000000000000" => OwnedValue::String(s) if s == "look at this big number: 10000000000000000000"
}

tc! { test_eval_equals
    r#"0 === 0"# err => ParseError,
    "1==1" => OwnedValue::Bool(true),
    "0==1" => OwnedValue::Bool(false),
    "-1==-1" => OwnedValue::Bool(true),
    "-1==1" => OwnedValue::Bool(false),
    "1==-1" => OwnedValue::Bool(false),
    "1.0==1" => OwnedValue::Bool(true),
    "1==1.0" => OwnedValue::Bool(true),
    "1==1.5" => OwnedValue::Bool(false),
    r#""x"=="x""# => OwnedValue::Bool(true),
    r"' '=='\x20'" => OwnedValue::Bool(true),
    r"' '=='\x1f'" => OwnedValue::Bool(false),
    "20000000000000000000 == 20000000000000000001" => OwnedValue::Bool(false),
    "20000000000000000000 == --20000000000000000000" => OwnedValue::Bool(true),
    "20000000000000000000 == -20000000000000000000" => OwnedValue::Bool(false),
    "20000000000000000000 == i64_to_bigint 20 * 1000 * 1000 * 1000 * 1000 * 1000 * 1000" => OwnedValue::Bool(true),
    "20000000000000000000 == i64_to_bigint 20 * 1000 * 1000 * 1000 * 1000 * 1000 * 1000 + 1" => OwnedValue::Bool(false),
    "20000000000000000000 / 10000000000000000000 - 2 == 0" => OwnedValue::Bool(true),
    "20000000000000000000 / 10000000000000000000 - 1 == 0" => OwnedValue::Bool(false),
    "20000000000000000000 / 10000000000000000000 - 1 == 20000000000000000000" => OwnedValue::Bool(false)
}
tc! { test_equals_type_error
    r#""x"=='x'"# err => TypeError,
}

tc! { test_eval_strictly_greater_than
    "1>1" => OwnedValue::Bool(false),
    "99>-100" => OwnedValue::Bool(true),
    "-99>-100" => OwnedValue::Bool(true),
    "99>100" => OwnedValue::Bool(false),
    r#"'a'>'b'"# => OwnedValue::Bool(false),
    r#"'z'>'a'"# => OwnedValue::Bool(true),
    r"' '>'\x20'" => OwnedValue::Bool(false),
    r"' '>'\x21'" => OwnedValue::Bool(false),
    r"' '>'\x1f'" => OwnedValue::Bool(true),
    r#""">"a""# => OwnedValue::Bool(false),
    r#""a">"""# => OwnedValue::Bool(true),
    r#""abc">"def""# => OwnedValue::Bool(false),
    r#""abc">"abc""# => OwnedValue::Bool(false),
    r#""abcd">"abc""# => OwnedValue::Bool(true),
    r#""abcde">"abcd""# => OwnedValue::Bool(true),
    r#""abcd">"abc">"ab">"a""# => OwnedValue::Bool(true),
    r#""">"abc">"ab">"a""# => OwnedValue::Bool(false),
    r#""abcd">"">"ab">"a""# => OwnedValue::Bool(false),
    r#""abcd">"abc">"ab">"""# => OwnedValue::Bool(true),
    "20000000000000000000 > 20000000000000000001" => OwnedValue::Bool(false),
    "20000000000000000000 > 20000000000000000000" => OwnedValue::Bool(false),
    "20000000000000000000 > 19999999999999999999" => OwnedValue::Bool(true),
    "1 > 20000000000000000000" => OwnedValue::Bool(false),
    "1 > -20000000000000000000" => OwnedValue::Bool(true),
    "20000000000000000000 > 1" => OwnedValue::Bool(true),
    "-20000000000000000000 > 1" => OwnedValue::Bool(false),
    "20000000000000000000 > 1 > -20000000000000000000" => OwnedValue::Bool(true),
    r#"99>"100""# err => TypeError
}

tc! { test_eval_greater_than_or_equal
    "1>=1" => OwnedValue::Bool(true),
    "99>=-100" => OwnedValue::Bool(true),
    "-99>=-100" => OwnedValue::Bool(true),
    "99>=100" => OwnedValue::Bool(false),
    r#"99>="100""# err => TypeError,
    r#"'a'>='b'"# => OwnedValue::Bool(false),
    r#"'z'>='a'"# => OwnedValue::Bool(true),
    r"' '>='\x20'" => OwnedValue::Bool(true),
    r"' '>='\x21'" => OwnedValue::Bool(false),
    r"' '>='\x1f'" => OwnedValue::Bool(true),
    r#""">="a""# => OwnedValue::Bool(false),
    r#""a">="""# => OwnedValue::Bool(true),
    r#""abc">="def""# => OwnedValue::Bool(false),
    r#""abc">="abc""# => OwnedValue::Bool(true),
    r#""abcd">="abc""# => OwnedValue::Bool(true),
    r#""abcde">="abcd""# => OwnedValue::Bool(true),
    r#""abcd">="abc">="ab">="a""# => OwnedValue::Bool(true),
    r#""">="abc">="ab">="a""# => OwnedValue::Bool(false),
    r#""abcd">="">="ab">="a""# => OwnedValue::Bool(false),
    r#""abcd">="abc">="ab">="""# => OwnedValue::Bool(true),
    "20000000000000000000 >= 20000000000000000001" => OwnedValue::Bool(false),
    "20000000000000000000 >= 20000000000000000000" => OwnedValue::Bool(true),
    "20000000000000000000 >= 19999999999999999999" => OwnedValue::Bool(true),
    "1 >= 20000000000000000000" => OwnedValue::Bool(false),
    "1 >= -20000000000000000000" => OwnedValue::Bool(true),
    "20000000000000000000 >= 1" => OwnedValue::Bool(true),
    "-20000000000000000000 >= 1" => OwnedValue::Bool(false),
    "20000000000000000000 >= 1 >= -20000000000000000000" => OwnedValue::Bool(true)
}

tc! { test_eval_in_operator
    "1 in {1}" => OwnedValue::Bool(true),
    "0 in {1}" => OwnedValue::Bool(false),
    "1 in {0}" => OwnedValue::Bool(false),
    "1 in {,}" => OwnedValue::Bool(false),
    "() in {()}" => OwnedValue::Bool(true),
    "0 in 0" err => EvaluationError,
    r#"'a' in "abc""# => OwnedValue::Bool(true),
    r#"'b' in "abc""# => OwnedValue::Bool(true),
    r#"'c' in "abc""# => OwnedValue::Bool(true),
    r#"'d' in "abc""# => OwnedValue::Bool(false),
    r#""a" in "abc""# => OwnedValue::Bool(true),
    r#""ab" in "abc""# => OwnedValue::Bool(true),
    r#""abc" in "abc""# => OwnedValue::Bool(true),
    r#""abcd" in "abc""# => OwnedValue::Bool(false),
    r#""x j" in "the quick brown fox jumped over the lazy dog""# => OwnedValue::Bool(true),
    r#""the quick brown fox jumped over the lazy dog" in "x j""# => OwnedValue::Bool(false),
    // TODO: test vectors and maps, too
}
tc! { test_eval_strictly_less_than
    "1<1" => OwnedValue::Bool(false),
    "99<-100" => OwnedValue::Bool(false),
    "-99<-100" => OwnedValue::Bool(false),
    "99<100" => OwnedValue::Bool(true),
    r#"99<"100""# err => TypeError,
    r#"'a'<'b'"# => OwnedValue::Bool(true),
    r#"'z'<'a'"# => OwnedValue::Bool(false),
    r"' '<'\x20'" => OwnedValue::Bool(false),
    r"' '<'\x21'" => OwnedValue::Bool(true),
    r#"""<"a""# => OwnedValue::Bool(true),
    r#""a"<"""# => OwnedValue::Bool(false),
    r#""abc"<"def""# => OwnedValue::Bool(true),
    r#""abc"<"abc""# => OwnedValue::Bool(false),
    r#""abcd"<"abc""# => OwnedValue::Bool(false),
    r#""abcd"<"abcde""# => OwnedValue::Bool(true),
    r#""a"<"ab"<"abc"<"abcd""# => OwnedValue::Bool(true),
    r#""a"<"ab"<"abc"<"""# => OwnedValue::Bool(false),
    r#""a"<"_"<"abc"<"abcd""# => OwnedValue::Bool(false),
    "20000000000000000000 < 20000000000000000001" => OwnedValue::Bool(true),
    "20000000000000000000 < 20000000000000000000" => OwnedValue::Bool(false),
    "20000000000000000000 < 19999999999999999999" => OwnedValue::Bool(false),
    "1 < 20000000000000000000" => OwnedValue::Bool(true),
    "1 < -20000000000000000000" => OwnedValue::Bool(false),
    "20000000000000000000 < 1" => OwnedValue::Bool(false),
    "-20000000000000000000 < 1" => OwnedValue::Bool(true),
    "-20000000000000000000 < 1 < 20000000000000000000" => OwnedValue::Bool(true),
}

tc! { test_eval_less_than_or_equal
    "1<=1" => OwnedValue::Bool(true),
    "99<=-100" => OwnedValue::Bool(false),
    "-99<=-100" => OwnedValue::Bool(false),
    "99<=100" => OwnedValue::Bool(true),
    "256<=0x100" => OwnedValue::Bool(true),
    "0x100<=256" => OwnedValue::Bool(true),
    r#"99<="100""# err => TypeError,
    r#"'a'<='b'"# => OwnedValue::Bool(true),
    r#"'z'<='a'"# => OwnedValue::Bool(false),
    r"' '<='\x20'" => OwnedValue::Bool(true),
    r"' '<='\x21'" => OwnedValue::Bool(true),
    r#"""<="a""# => OwnedValue::Bool(true),
    r#""a"<="""# => OwnedValue::Bool(false),
    r#""abc"<="def""# => OwnedValue::Bool(true),
    r#""abc"<="abc""# => OwnedValue::Bool(true),
    r#""abcd"<="abc""# => OwnedValue::Bool(false),
    r#""abcd"<="abcde""# => OwnedValue::Bool(true),
    r#""a"<="ab"<="abc"<="abcd""# => OwnedValue::Bool(true),
    r#""a"<="ab"<="abc"<="""# => OwnedValue::Bool(false),
    r#""a"<="_"<="abc"<="abcd""# => OwnedValue::Bool(false),
    "20000000000000000000 <= 20000000000000000001" => OwnedValue::Bool(true),
    "20000000000000000000 <= 20000000000000000000" => OwnedValue::Bool(true),
    "20000000000000000000 <= 19999999999999999999" => OwnedValue::Bool(false),
    "1 <= 20000000000000000000" => OwnedValue::Bool(true),
    "1 <= -20000000000000000000" => OwnedValue::Bool(false),
    "20000000000000000000 <= 1" => OwnedValue::Bool(false),
    "-20000000000000000000 <= 1" => OwnedValue::Bool(true),
    "-20000000000000000000 <= 1 < 20000000000000000000" => OwnedValue::Bool(true),
    "20000000000000000001 - 20000000000000000000 <= 1" => OwnedValue::Bool(true),
}

tc! { test_eval_not_equal
    "1!=1" => OwnedValue::Bool(false),
    "0!=1" => OwnedValue::Bool(true),
    "-1!=-1" => OwnedValue::Bool(false),
    "-1!=1" => OwnedValue::Bool(true),
    "1!=-1" => OwnedValue::Bool(true),
    "1.0!=1" => OwnedValue::Bool(false),
    "1!=1.0" => OwnedValue::Bool(false),
    "1!=1.5" => OwnedValue::Bool(true),
    r#""x"!='x'"# err => TypeError,
    r#""x"!="x""# => OwnedValue::Bool(false),
    r"' '!='\x20'" => OwnedValue::Bool(false),
    r"' '!='\x1f'" => OwnedValue::Bool(true),
    r"()!=7" err => TypeError,
    r"()!=()" => OwnedValue::Bool(false),
    r"(1,)!=(2,)" => OwnedValue::Bool(true),
    r"(1,2)!=(1,)" err => TypeError,
    r"(1,2)!=(1,2)" => OwnedValue::Bool(false),
    "20000000000000000000 != 20000000000000000001" => OwnedValue::Bool(true),
    "20000000000000000000 != 20000000000000000000" => OwnedValue::Bool(false),
    "20000000000000000000 != 19999999999999999999" => OwnedValue::Bool(true),
    "1 != 20000000000000000000" => OwnedValue::Bool(true),
    "1 != -20000000000000000000" => OwnedValue::Bool(true),
    "20000000000000000000 != 1" => OwnedValue::Bool(true),
    "-20000000000000000000 != 1" => OwnedValue::Bool(true),
    "-20000000000000000000 != 20000000000000000000" => OwnedValue::Bool(true),
    "20000000000000000001 - 20000000000000000000 != 1" => OwnedValue::Bool(false)
}

tc! { test_eval_not_in
    "1 not in {1,}" => OwnedValue::Bool(false),
    "0 not in {1,}" => OwnedValue::Bool(true),
    "1 not in {0,}" => OwnedValue::Bool(true),
    "1 not in {,}" => OwnedValue::Bool(true),
    "() not in {(),}" => OwnedValue::Bool(false),
    "0 not in 0" err => EvaluationError,
    r#"'a' not in "abc""# => OwnedValue::Bool(false),
    r#"'b' not in "abc""# => OwnedValue::Bool(false),
    r#"'c' not in "abc""# => OwnedValue::Bool(false),
    r#"'d' not in "abc""# => OwnedValue::Bool(true),
    r#""a" not in "abc""# => OwnedValue::Bool(false),
    r#""ab" not in "abc""# => OwnedValue::Bool(false),
    r#""abc" not in "abc""# => OwnedValue::Bool(false),
    r#""abcd" not in "abc""# => OwnedValue::Bool(true),
    r#""x j" not in "the quick brown fox jumped over the lazy dog""# => OwnedValue::Bool(false),
    r#""xx" not in "the quick brown fox jumped over the lazy dog""# => OwnedValue::Bool(true),
    r#""the quick brown fox jumped over the lazy dog" not in "x j""# => OwnedValue::Bool(true),
    // TODO: test vectors and maps, too
}

tc! { test_eval_divide_num
    "1/1" => Int64(1),
    "1/0" err => EvaluationError,
    "1/0.0" => Float64(x) if x.is_infinite()
}

// no trait implementations for these
tc! { test_eval_divide_type_errors
    r#"1/"abc""# err => TypeError,
    r#"""/10"# err => TypeError,
    r#"()/10"# err => TypeError
}
tc! { test_eval_divide_float
    r#"-1.1 / 0.1"# => Float64(x) if (x - -11.0).abs() < f64::EPSILON,
    r#"1 / -2.0 * 3 / 0.5 / 1e3"# => Float64(x) if (x - -3e-3).abs() < f64::EPSILON,
    r#"12 / 7 / (2.0 * 3) * 12"# => Float64(x) if (x - 2.0).abs() < f64::EPSILON
}
tc! { test_eval_divide_bigint
    r#"50000000000000000000 / -20000000000000000000"# => BigInt(x) if x == &(-2i64).to_bigint().unwrap(),
    r#"50000000000000000000 / 10"# => BigInt(x) if x == &(5_000_000_000_000_000_000_i64).to_bigint().unwrap(),
    r#"6 / 90000000000000000000"# => BigInt(x) if x == &(0i64).to_bigint().unwrap()
}

tc! { test_eval_remainder
    "1 % 1" => Int64(0),
    "1 % 0" err => EvaluationError,
    "1 % 0.0" => Float64(x) if x.is_nan(),
    "1e2 % (10,)" err => TypeError,
    r#""abc" % 10"# err => TypeError,
    r#"11.25 % 9.5"# => Float64(x) if (x - 1.75).abs() < f64::EPSILON,
    r#"-11.25 % 9.5"# => Float64(x) if (x - -1.75).abs() < f64::EPSILON,
    r#"-11.25 % -9.5"# => Float64(x) if (x - -1.75).abs() < f64::EPSILON,
    r#"11.25 % -9.5"# => Float64(x) if (x - 1.75).abs() < f64::EPSILON,
    r#"4.56 % 1.23 * 7 % 4"# => Float64(x) if (x - 2.09).abs() < 10.0 * f64::EPSILON,
    r#"50000000000000000000 % -20000000000000000000"# => BigInt(x) if x == &BigInt::parse_bytes(b"10000000000000000000", 10).unwrap(),
    r#"50000000000000000001 % 10"# => BigInt(x) if x == &BigInt::parse_bytes(b"1", 10).unwrap(),
    r#"6 % 90000000000000000000"# => BigInt(x) if x == &BigInt::parse_bytes(b"6", 10).unwrap()
}

tc! { test_eval_times
    "-1 * 1" => Int64(-1),
    "1e2 * 7" => Float64(x) if (x - 700.0).abs() < f64::EPSILON,
    "1.2 * 3.4" => Float64(x) if (x - 4.08).abs() < f64::EPSILON,
    "0xff * -0.01" => Float64(x) if (x - -2.55).abs() < 10.0 * f64::EPSILON,
    "0xff * -0.01" => Float64(x) if (x - -2.55).abs() < 10.0 * f64::EPSILON,
    "true * false" err => TypeError,
    "7.0 * false" err => TypeError,
    "true * 7" err => TypeError,
    r#"-10000000000000000000 * -10000000000000000000"# => BigInt(x) if x == &BigInt::parse_bytes(b"100000000000000000000000000000000000000", 10).unwrap(),
    r#"-10000000000000000000 * 2"# => BigInt(x) if x == &BigInt::parse_bytes(b"-20000000000000000000", 10).unwrap()
}

// TODO: replace tuple multiply tests with list multiply
// tc! { test_eval_tuple_multiply_1
//     "(0,) * 0" => OwnedValue::Nothing
//     "(0,) * 1" => { Tuple(v) => &v[..], [x] => &*x as &StegoObject } => Int64(0)
//     r#"("abc",11) * 1"# => {
//         Tuple(v) => unwrap_vec(&v),
//         v => &v[..]
//     } => [OwnedValue::String(s), Int64(11)] if &*s == "abc"
// }
// tc! { test_eval_tuple_multiply_2
//     r#"(1,-1) * 10"# => {
//         Tuple(v) if v.len() == 20 => &v.iter().map(|x|&*x as &StegoObject).collect_vec(),
//         v => &v[..]
//     } => [
//         Int64(1), Int64(-1),
//         Int64(1), Int64(-1),
//         Int64(1), Int64(-1),
//         Int64(1), Int64(-1),
//         Int64(1), Int64(-1),
//         Int64(1), Int64(-1),
//         Int64(1), Int64(-1),
//         Int64(1), Int64(-1),
//         Int64(1), Int64(-1),
//         Int64(1), Int64(-1) ]
// }
// tc! { test_eval_tuple_multiply_3
//     r#"(1,-1) * 0.5"# err => EvaluationError
// }
// tc! { test_eval_tuple_multiply_4
//     r#"((1,-1)) * -1"# => OwnedValue::Nothing
// }
// tc! { test_eval_tuple_multiply_5
//     r#"(((),) * 2) * 2"# => {
//         Tuple(v) => unwrap_vec(&v),
//         v => &v[..]
//     } => [OwnedValue::Nothing, OwnedValue::Nothing, OwnedValue::Nothing, OwnedValue::Nothing]
//     r#"3 * (0,0) * 4"# => {
//         Tuple(v) if v.len() == 24 => v
//     } => v if v.iter().all(|x| matches!(&*x as &StegoObject, Int64(0)))
//     r#"3 * () * 4"# => OwnedValue::Nothing
//     r#"20000000000000000000 * ()"# => OwnedValue::Nothing
//     r#"20000000000000000000 * ((),)"# err => EvaluationError
// }

tc! { test_eval_string_multiply
    r#""abc" * 0"# => OwnedValue::String(s) if s.is_empty()
    r#""abc" * 1"# => OwnedValue::String(s) if s == "abc"
    r#""abc" * 10"# => OwnedValue::String(s) if s == "abcabcabcabcabcabcabcabcabcabc"
    r#""abc" * 0.5"# err => TypeError
    r#""abc" * -1"# => OwnedValue::String(s) if s.is_empty()
    r#"("abc" * 2) * 2"# => OwnedValue::String(s) if s == "abcabcabcabc"
    r#"3 * "abc" * 4"# => OwnedValue::String(s) if s == "abcabcabcabcabcabcabcabcabcabcabcabc"
    r#"1000 * """# => OwnedValue::String(s) if s.is_empty()
    r#"1000 * """# => OwnedValue::String(s) if s.is_empty()
    // no plans to implement this for BigInt, even if it happens to be a small BigInt
    r#""abc" * (20000000000000000000 - 19999999999999999995)"# err => TypeError
}

tc! { test_eval_tuples
    r#"()"# => OwnedValue::Nothing
    r#"("abc",11)"# => {
        Tuple(v) => &v[..]
    } => [OwnedValue::String(s), Int64(11)] if s == "abc"
    r#"((), ())"# => {
        Tuple(v) =>  &v[..]
    } => [OwnedValue::Nothing, OwnedValue::Nothing]
    r#"("test",)"# => {
        Tuple(v) =>  &v[..]
    } => [OwnedValue::String(s)] if s == "test"
    r#"(1,2,3,4,)"# => {
        Tuple(v) =>  &v[..]
    } => [Int64(1), Int64(2), Int64(3), Int64(4)]
    r#"(((((1+1),)),))"# => {
        Tuple(v) => &v[..],
        [Tuple(v)] => &v[..]
    } => [Int64(2)]
}

tc! { test_eval_let
    "let x = 0" => OwnedValue::Nothing
    r#"let x = 0;x"# => Int64(0)
    r#"let foo = "foo";let times = 5;foo * times"# => OwnedValue::String(s) if s == "foofoofoofoofoo"
    "let x = 10; let y = 11; let z = 10*x+y;z" => Int64(111)
    r#"let x = 1 + (let x = 1) ; x"# err => ParseError
}

tc! { test_indentation
    r#"
let x = i64_to_bigint(20) * 1000 * 1000 * 1000 * 1000 * 1000 * 1000
x"# => BigInt(x) if x == &BigInt::parse_bytes(b"20000000000000000000", 10).unwrap()

r#"
1
 +
 10
 +
 100
 +
 1000"# => Int64(1111)

r#"
1
 /*comment*/+
 10"# => Int64(11)

 r#"
 1
   +
  10"# => Int64(11)
}

tc! { test_eval_named_expr
    r#"x@0"# => Int64(0)

    // missing parens
    r#"1 + x @ 7"# err => ParseError

    "1 + (x @ 7)" => Int64(8)
    "1 + (x @ 7) ; x - 3" => Int64(4)
    "1 + (x @ 10 + 100) ; x" => Int64(110)
    "x@1 ; x@(100+(x@(10 + x))) ; x==111" => OwnedValue::Bool(true)
    r#"somevar@20000000000000000000"# => BigInt(x) if x == &BigInt::parse_bytes(b"20000000000000000000", 10).unwrap()
}

tc! { test_eval_assignment
    r#"let x = (1,); x = (2,); x"# => {
        Tuple(v) => &v[..]
    } => [Int64(2)]
    "let some_var = 1; some_var = 10; some_var = 100; some_var" => Int64(100)
    "let x = 1; let y = 10; y = 100; x = y; x" => Int64(100)
}

tc! { test_dict_literals_1
    r#"{}"# => OwnedValue::Dict(v) if v.is_empty()
}

fn hashmap_get_string_keys(
    hashmap: &FxHashMap<OwnedValue, OwnedValue>,
    keys: &[&str],
) -> Box<[OwnedValue]> {
    let mut v: Vec<OwnedValue> = Vec::with_capacity(keys.len());
    for &key in keys {
        v.push(
            hashmap
                .get(&OwnedValue::String(key.into()))
                .unwrap()
                .clone(),
        );
    }
    v.into_boxed_slice()
}

tc! { test_dict_literals_2
    r#"{"some_key":"some_value"}"# => {
        Dict(hashmap) if hashmap.len()== 1 => &hashmap_get_string_keys(hashmap, &["some_key"])[..],
        [OwnedValue::String(s)] => &**s
    } => "some_value"
}

tc! { test_dict_literals_3 r#"
let first_dict = {"a":0,"b":1}
{"c":10, **first_dict}"# => {
        Dict(hashmap) if hashmap.len() == 3 => &hashmap_get_string_keys(hashmap,  &["a", "b", "c"])[..],
    } => [Int64(0), Int64(1), Int64(10)]
}

tc! { test_dict_literals_4 r#"let first_dict = {"a":0,"b":1, "c": 10}
let second_dict = {"b":100,"a":1000, "z":10000}
{"z":-1, "y": -2, **first_dict, **second_dict, "z":-100}"# => {
    Dict(hashmap) if hashmap.len() == 5 => &hashmap_get_string_keys(hashmap,  &["a", "b", "c", "y", "z"])[..],
} => [Int64(1000i64), Int64(100), Int64(10), Int64(-2), Int64(-100) ]
}

tc! { test_dict_literals_5
    r#"let first_dict = {"a":0,"b":1} ; {**first_dict}"# => {
        Dict(hashmap) if hashmap.len()== 2 => &hashmap_get_string_keys(hashmap, &["a", "b"])[..],
    } => [Int64(0), Int64(1)]
}

tc! { test_empty_set_literal r#"{,}"# => Set(v) if v.is_empty() }

tc! { test_set_literal_1
    r#"{0}"# => {
        Set(hashset) => assert_set_has(hashset, &[0])
    } => ()
    "{0,1,2}" => {
        Set(hashset) => {
            assert_set_has(hashset, &[0,1,2]);
            assert_not_in_set(hashset, &[-1,3]);
        }
    } => ()
    "let set0 = {0,1,2} ; {*set0,3,4}" => {
        Set(hashset) => {
            assert_set_has(hashset, &[0,1,2,3,4]);
            assert_not_in_set(hashset, &[-1,5]);
        }
    } => ()
    r#"let set0 = {0};{1,*set0}"# => {
        Set(hashset) => {
            assert_set_has(hashset, &[0,1]);
            assert_not_in_set(hashset, &[-1,2]);
        }
    } => ()
    "let set0 = {0,1} ; let set1 = {2,3} ; {1,*set0,*set0,*set1}" => {
        Set(hashset) => {
            assert_set_has(hashset, &[0,1,2,3]);
            assert_not_in_set(hashset, &[-1,4]);
        }
    } => ()
}

tc! { test_if0 r#"
fn foo n =
    if n == 0 then
        1
    else if n == 1 then
        10
    else if n == 2 then
        100
    else if n == 3 then
        1000
    else
        if n == 4 then
            10000
        else
            100000
(foo 0) + (foo 1) + (foo 2) + (foo 3) + (foo 4) + (foo 5)
"# => Int64(111_111)
}

tc! { test_if1 r#"
fn foo n =
    if n == 0 then 1 else if n == 1 then 10 else 100
(foo 0) + (foo 1) + (foo 2)
"# => Int64(111)
}

tc! { test_if2 r#"
let global = 0
fn foo control =
    if control == 1 then global = 100
foo 0
println global
foo 1
println global"# stdout => "0\n100\n"
}

tc! { test_if_scoping_1 r#"
let n = 999
fn foo n =
    if n == 0 then
        n = 10
        let n = 100
    n
println (foo 0)
println (foo 1)
println n
"# stdout => "10\n1\n999\n"
}

tc! { test_if_scoping_2 r#"
if false then let x = 1
x
"# err => TypeError
}

tc! { test_branch_types_must_match r#"
if 1 > 0 then
    'y'
else
    0
"# err => TypeError
}

tc! { test_dangling_else r#"
fn f n =
    if n > 0 then if n > 1 then 2
    else
        1
(f 0, f 1, f 2)"# err => TypeError
}

tc! { test_do r#"
let a = 10
let b = do
    do a = 11 ; a = -11
    let a = 12
    a
(a, b)"# => { 
    Tuple(v) => &v[..]
} => [Int64(-11), Int64(12)]
}

tc! { no_nullary r#"
fn nullary = nothing
"# err => ParseError
}

tc! { test_fn r#"
let count = 0
fn inc_count _ =
    count = count + 1
count"# => Int64(0)
}

// count to 3 in a silly way to test recursion
tc! { test_recursion r#"
fn count n =
    if n > 1 then
        count (n-1) + ", " + n
    else
        "1"
count 3"# => OwnedValue::String(s) if s == "1, 2, 3"
}

tc! { test_recursion2 r#"
fn collatz n =
    let steps = 0
    fn collatz_even n = 1 + collatz (n / 2)
    fn collatz_odd n = 1 + collatz_even (3 * n + 1)
    if n == 1 then
       steps
    else if n % 2 == 0 then
        steps + collatz_even n
    else
        steps + collatz_odd n
collatz 15"# => Int64(17)
// TODO: I used to be able to do `collatz 100` here without running out of stack. what happened?
}

tc! { test_recursion3 r#"
fn fib n =
    if n < 2 then
        1
    else
        fib (n-1) + fib (n-2)
""+(fib 0)+(fib 1)+(fib 2)+(fib 3)+(fib 4)+(fib 5)+(fib 6)
    "# => OwnedValue::String(s) if s == "11235813"
}

// a function should be able to "capture" itself
tc! { test_recursion4 r#"
fn main n =
    fn factorial n =
        if n < 2
            then 1
            else n * factorial (n-1)
    factorial n
main 5"# => Int64(120)
}

tc! { test_variable_capture1 r#"
let a = 1
fn f a =
    a + 1
f a"# => Int64(2)
}

tc! { test_mutate_captured_variable r#"
let a = 1
fn f_a _ =
    a = a + 1
f_a ()
a"# => Int64(2)
}

tc! { test_variable_capture3 r#"
let a = 1
let b = 0
fn fib _ =
    let c = a + b
    a = b
    b = c
    c
fib () // 1
fib () // 1
fib () // 2
fib () // 3
fib () // 5
fib () // 8
fib () // 13"# => OwnedValue::Int64(13)
}

tc! { test_variable_capture4 r#"
fn foo _ = x // x not defined yet to be captured
let x = 10
foo ()"# err => TypeError
}

tc! { test_capture5_alt r#"
let x = 100
fn foo n =
    print (typeof x)
    x = x + n
foo 3"# stdout => "Int64"
}

tc! { test_capture5 r#"
let x = 100
fn foo n =
    x = x + n
    x
foo 20
foo 3"# => Int64(123)
}

tc! { test_capture6 r#"
do
    let x = 100
    fn foo n =
        x = x + n
        x
    foo 20
    foo 3
"# => Int64(123)
}

tc! { test_capture7 r#"
let a = "global"
do
    // foo should capture the global `a` whose value is "global"
    fn foo suffix = a + suffix
    // shadow the global `a` with one local to the `do` block
    let a = "inner"
    // which is what bar should now capture
    fn bar suffix = a + suffix
    // test that we get what we expect
    println (foo "*")
    println (bar "*")
"# stdout => "global*\ninner*\n"
}

tc! { test_return_1 r#"
fn foo n =
    if n % 2 == 0 then
        return "even"
    return "o" + "d" + "d"
    "error"
foo 1 + ", " + foo 2"# => OwnedValue::String(s) if s == "odd, even"
}

tc! { test_return_2 "return 0" err => ParseError }

tc! { test_return_3 r#"
fn foo _ =
    return
foo ()
"# => OwnedValue::Nothing
}

// recursion using variable capture
tc! { test_recursion_and_capture r#"
let fib_a = 1
let fib_b = 1
let n = 0
fn fib =
    if n <= 0 then return fib_a
    let c = fib_a + fib_b
    fib_a = fib_b
    fib_b = c
    n = n-1
    fib
n = 5 ; fib // 8
n = 2 ; fib // 21
"# => Int64(21) => "Recursion in nullary function using variable capture to terminate."
}

tc! { test_precedence_1
r#"
fn foo n = n + 1
(
    3 * foo 2,
    foo 2 * 3,
    foo (2 * 3)
)
"# => {
    Tuple(v) => &v[..]
} => [Int64(9), Int64(9), Int64(7)] => "Function application should have higher precedence than addition."
}

// operator precedence unfortunately means that negative numbers often need to
// be in parentheses or they will instead be interpreted as subtractions
tc! { test_precedence_2
r#"
print -1
"# err => TypeError
r#"
print (-1)
"# stdout => "-1"
}

// typeof should be pretty low precedence
tc! { precedence_3
r"typeof \_=1" => OwnedValue::String(_)
}

// Semicolons can be handy for packing multiple statements on one line,
// but a single semicolon won't end a block opened on that line with them.
// Repeated semicolons will exit blocks, but readability suffers, so this is
// discouraged

tc! { test_semicolons_1

// works the way it looks like it does
r#"
fn identity x = x
print (identity "print test")
"# stdout => "print test"

// Looks like the identity function, but infinitely recurses.
// This is because the semicolon separates statements within the fn,
// rather than starting a new statement outside it.
// If we were to call bad_identity, it would recurse until we blow up the stack.
r#"
fn bad_identity x = x ; print (bad_identity "print test")
"# stdout => ""
}
tc! { test_semicolons_2
// however, repeating semicolons do exit the block
r#"
fn fixed x = x ; ; print (fixed "print test")
"# stdout => "print test"
}
tc! { test_semicolons_3
// however, repeating semicolons do exit the block
r#"
fn outer x = print "begin outer " ; println x ; fn inner x = print "begin inner " ; println x ; println "still in inner" ;; inner (x+1) ; print "back in outer " ; println x ;; println "outside of all"
outer 0
"# stdout => r#"outside of all
begin outer 0
begin inner 1
still in inner
back in outer 0
"#
}
tc! { test_semicolons_4
// examples of selecting how far to pop out
r#"let depth = 0

// not doubled -- stays 3 levels deep
do let depth = 1 ; do let depth = 2 ; do let depth = 3 ; println depth

// two semicolons -- pop up to 2 levels deep
do let depth = 1 ; do let depth = 2 ; do let depth = 3 ;; println depth

// three semicolons -- pop up two levels (to 1 level deep)
do let depth = 1 ; do let depth = 2 ; do let depth = 3 ;;; println depth

// four semicolons -- pop up three levels (now out of all the do blocks)
do let depth = 1 ; do let depth = 2 ; do let depth = 3 ;;;; println depth

// five semicolons -- no further out to go, but not an error
do let depth = 1 ; do let depth = 2 ; do let depth = 3 ;;;; println depth
"# stdout => "3\n2\n1\n0\n0\n"
}

tc! { test_polymorphism
r#"fn add a b = a + b
println (add 1 10)
println (add "a" "b")
"# stdout => "11\nab"
}

tc! { test_partial_function_1
r#"fn add a b = a + b
let partial = add "pre-"
partial "post"
"# ok => _
}

tc! { test_partial_function_2 r#"
fn add a b = a + b
let partial = add 1
partial 10
"# ok => OwnedValue::Int64(11)
}

tc! { test_partial_function_3 r#"
fn add a b = a + b
let partial = add "pre-"
partial "-post"
"# ok => OwnedValue::String(s) if s == "pre--post"
}

// partial functions are still polymorphic
tc! { test_partial_function_4 r#"
fn add a b = a + b
let partial = add 10
println (partial 0.5)
println (partial 10000000000000000000)
"# stdout => "10.5\n20000000000000000010\n"
}

tc! { test_fn_redef_error r#"
fn foo x = x
fn foo x = x
"# err => TypeError
}

tc! { test_fn_scoping r#"
do
    fn identity x = x
do
    fn identity x = x
"# ok => _
}

tc! { test_more_scoping r#"
let x = 0
do do do print x
         let x = 1
         print x
      print x
      let x = 2
      print x
   print x
   let x = 3
   print x
print x
let x = 4
print x
"# stdout => "01020304"
}

// TODO: test error when sending args to a nullary fn
