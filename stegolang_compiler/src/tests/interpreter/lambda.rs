use super::*;

tc! { lambda_1
    r"(\ _ = 1) ()" => Int64(1)
}
tc! { lambda_2 r"
let x = \_=1
x ()
" => Int64(1)
}
tc! { lambda_3 r"
let add_one = \ lamparam = lamparam+1
add_one 1
" => Int64(2)
}
tc! { lambda_5_alt r"
let typeof_wrapper = \x = typeof x
typeof_wrapper 0
" => OwnedValue::String(s) if s == "Int64"
}
tc! { lambda_5 r"
let typeof_wrapper = \x = typeof x
print (typeof_wrapper 0)
" stdout => "Int64"
}
tc! { lambda_6 r"
let typeof_wrapper = \x = typeof x
typeof_wrapper
" => OwnedValue::FnFamily(s) if s == "Lambda created at <test>:2:22"
}
tc! { lambda_7 r"
let pi = 3.14
let rad_to_deg = \rad=rad*180.0/pi
print (rad_to_deg 1.57)
" stdout => "90"
}

tc! { immediately_applied_lambda r#"
print ((\x = "prefix" + x) "suffix")
"# stdout => "prefixsuffix"
}
