//! test cases for builtin functions in the interpreter
#![cfg(test)]
use crate::{tests::interpreter::*, typedvalue::OwnedValue};
use regex::Regex;

// Test calling the built-in print() function
tc! { test_print_1
    "print 100" stdout => "100"
    "print true" stdout => "true"
    "print ()" stdout => s if s == crate::config::NOTHING_VALUE
    "print print" stdout => s if s.starts_with("(native fn print")
}
tc! { test_print_2
    "fn identity_fn x = x\nprint (typeof identity_fn)" stdout => s if s.starts_with("(Fn")

    "fn identity_fn x = x\nidentity_fn" ok => OwnedValue::FnFamily(..)
}

tc! { test_print_3 r#"
fn add_fn a b = a + b
fn test f n =
    n = f n
    typeof f
test (add_fn 1) 100
"# => OwnedValue::String(s) if Regex::new(r"\(Fn \S+ \S+\)").unwrap().is_match(s)
}

// Test calling the built-in println() function
tc! { test_println
    "println 100" stdout => "100\n"
    "println \"test\"" stdout => "test\n"
}

tc! { test_parse_int
    "parse_int" => {
        OwnedValue::FnFamily(debug_name) => &debug_name[..],
    } => "parse_int"

    // TODO: parse_int should not always make a BigInt
    // TODO: parse_int should produce a Result
    r#"parse_int "-1""# => OwnedValue::BigInt(x) if x == &BigInt::parse_bytes(b"-1", 10).unwrap()
    r#"parse_int "20000000000000000000""# => OwnedValue::BigInt(x) if x == &BigInt::parse_bytes(b"20000000000000000000", 10).unwrap()
    r#"parse_int """# => OwnedValue::Nothing
}

tc! { refer_to_builtins
    "BigInt" err => TypeError
    "Bool" err => TypeError
    "Char" err => TypeError
    "Dict" err => TypeError
    "Float64" err => TypeError
    "Int64" err => TypeError
    "Set" err => TypeError
    "Nothing" err => TypeError
    "String" err => TypeError
    "nothing" => _
    "parse_int" => _
    "print" => _
    "println" => _
}

tc! { trait_as_value
    "Add" err => TypeError
}
