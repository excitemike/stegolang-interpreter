//! unit tests for the parser
#![cfg(test)]
#![allow(irrefutable_let_patterns)]

use crate::ast::literal::Literal;
use crate::ast::{IfArm, Operator, SetExprElem};
use crate::match_or_panic;
use crate::pattern::StegoPatternKind;
use crate::{ast::astnode::AstNodeKind::*, ast::astnode::*, parse::parse, util::RcStr};
use num::{bigint::BigInt, traits::pow};

mod dot;
mod lambda;

// helper for pattern matching out the kind from an AstNode
macro_rules! match_kind {
    ($p:pat) => {
        AstNode { kind: $p, .. }
    };
}

/// support function for the tests below
/// give it mock file contents and get back a result of parsing it
#[track_caller]
fn test_file<T>(code: T) -> Option<AstNodeKind>
where
    T: Into<String> + Clone + std::marker::Send + 'static,
{
    let file_label = String::from("<test case>");
    match parse(file_label, code) {
        // successfully ran and found parse error
        Err(errors) => panic!("Expected to have no errors. Got {errors:#?}"),
        // successfully parsed
        Ok((result, v)) => {
            assert!(v.is_empty(), "Unexpected warnings {v:#?}");
            result.map(|x| x.kind)
        }
    }
}

/// assert that the code would produce a warning
#[track_caller]
fn test_file_warning<T>(code: T, line: usize, column: usize)
where
    T: Into<String> + Clone + std::marker::Send + 'static,
{
    let file_label = String::from("<test case>");

    match parse(file_label, code) {
        // successfully ran and found parse error
        Err(errors) => panic!("Expected to have no errors. Got {errors:#?}"),
        // successfully parsed
        Ok((_, v)) => {
            assert!(!v.is_empty(), "Expected warnings but had none {v:#?}");
            let loc = &v[0].source_region;
            assert_eq!(
                loc.start_line_number, line,
                "Wrong line number for warning!"
            );
            assert_eq!(
                loc.start_column_number, column,
                "Wrong column number for warning!"
            );
        }
    }
}

/// assert that the code would produce an error
#[track_caller]
fn test_file_error(code: &'static str, line: usize, column: usize) {
    let file_label = String::from("<test case>");

    match_or_panic! {
        parse(file_label, code),
        Err(errors) => &errors[..],
        [error, ..] => {
            assert!(
                matches!(error.severity, crate::error::Severity::Error),
                "parsing code produced error of wrong severity"
            );
            let loc = &error.source_region;
            assert_eq!(loc.start_line_number, line, "Wrong line number for error!");
            assert_eq!(
                loc.start_column_number, column,
                "Wrong column number for error!"
            );
        }
    }
}

/// test the parser with empty input
#[test]
fn blank_file() {
    assert!(test_file("").is_none());
}

/// test the parser with intentional compile error
#[test]
fn file_with_error() {
    test_file_error("!!!! : // colon not valid here", 1, 6);
}
/// helper for test cases below
/// parses the code to an AST then returns the first statement
#[track_caller]
fn fst_stmt<T>(code: T) -> AstNodeKind
where
    T: Into<String> + Clone + std::marker::Send + 'static,
{
    nth_stmt(0, code)
}

/// helper for test cases below
/// parses the code to an AST then returns the nth statement
#[track_caller]
fn nth_stmt<T>(n: usize, code: T) -> AstNodeKind
where
    T: Into<String> + Clone + std::marker::Send + 'static,
{
    if let Some(Statements { mut statements, .. }) = test_file(code) {
        return statements.swap_remove(n).kind;
    }
    panic!()
}
/// helper for test cases below
/// parses the code to an AST then returns the first statement
#[track_caller]
fn parse_stmts<T>(code: T) -> Vec<AstNode>
where
    T: Into<String> + Clone + std::marker::Send + 'static,
{
    if let Some(Statements { statements, .. }) = test_file(code) {
        return statements;
    }
    panic!()
}

/// helper for test cases below
/// parses the code to an AST then returns the second statement
#[track_caller]
fn snd_stmt<T>(code: T) -> AstNodeKind
where
    T: Into<String> + Clone + std::marker::Send + 'static,
{
    if let Some(Statements { mut statements, .. }) = test_file(code) {
        return statements.swap_remove(1).kind;
    }
    panic!()
}

/// test parsing of literals
#[test]
fn test_parse_literal() {
    #[track_caller]
    fn fst_stmt_strlitstr<T>(code: T) -> RcStr
    where
        T: Into<String> + Clone + std::marker::Send + 'static,
    {
        match_or_panic! {
            fst_stmt(code),
            LiteralValue(Literal::String(s)) => s
        }
    }
    fn floateq(a: f64, b: f64) -> bool {
        (a - b).abs() < f64::EPSILON
    }

    assert!(matches!(
        fst_stmt("true"),
        LiteralValue(Literal::Bool(true))
    ));
    assert!(matches!(
        fst_stmt(r"false"),
        LiteralValue(Literal::Bool(false))
    ));
    assert!(matches!(
        fst_stmt(r"' '"),
        LiteralValue(Literal::Character(' '))
    ));
    assert!(matches!(
        fst_stmt(r"'\u{1f468}'"),
        LiteralValue(Literal::Character('👨'))
    ));
    assert!(matches!(
        fst_stmt("\"trailing whitespace\" "),
        LiteralValue(Literal::String(..))
    ));

    assert_eq!(fst_stmt_strlitstr(r#""""#), "");
    assert_eq!(
        fst_stmt_strlitstr(r#""🦌🦌🦌🦌🦌🦌🦌🦌🛷🎅🏾""#),
        "🦌🦌🦌🦌🦌🦌🦌🦌🛷🎅🏾"
    );
    match_or_panic!(
        fst_stmt("\"ignore\tsome white\\\nspace\""),
        LiteralValue(Literal::String(s)) => assert_eq!(s, "ignore\tsome whitespace")
    );
    match_or_panic!(
        fst_stmt("\"ignore\tsome white\\nspace\""),
        LiteralValue(Literal::String(s)) => assert_eq!(s, "ignore\tsome white\nspace")
    );
    assert_eq!(fst_stmt_strlitstr("\"\u{1f468}\""), "👨");

    assert!(matches!(fst_stmt("0"), LiteralValue(Literal::Int64(0))));
    assert!(matches!(fst_stmt("0o10"), LiteralValue(Literal::Int64(8))));
    assert!(matches!(fst_stmt("0x10"), LiteralValue(Literal::Int64(16))));
    assert!(matches!(
        fst_stmt("0o0777777777777777777777"),
        LiteralValue(Literal::Int64(9_223_372_036_854_775_807))
    ));
    assert!(matches!(
        fst_stmt("0x7FFFFFFFFFFFFFFF"),
        LiteralValue(Literal::Int64(9_223_372_036_854_775_807))
    ));
    match_or_panic! {
        fst_stmt("1e3"),
        LiteralValue(Literal::Float(f)) => assert!(floateq(f, 1000.0))
    }
    match_or_panic! {
        fst_stmt("1.0"),
        LiteralValue(Literal::Float(f)) => assert!(floateq(f, 1.0))
    };
    match_or_panic! {
        fst_stmt("0.1"),
        LiteralValue(Literal::Float(f)) => assert!(floateq(f, 0.1))
    };
    match_or_panic! {
        fst_stmt("0.1e-10"),
        LiteralValue(Literal::Float(f)) => assert!(floateq(f, 0.1e-10))
    };
    match_or_panic! {
        fst_stmt("100E-10"),
        LiteralValue(Literal::Float(f)) => assert!(floateq(f, 100E-10))
    };
    match_or_panic! {
        fst_stmt("1.2e34"),
        LiteralValue(Literal::Float(f)) => assert!(floateq(f, 1.2e34))
    };
    match_or_panic! {
        fst_stmt("1_234_567"),
        LiteralValue(Literal::Int64(1_234_567)) => ()
    };
    match_or_panic! {
        fst_stmt("0xffff_ffff_ffff_ffff"),
        LiteralValue(Literal::BigInt(x)) => assert_eq!(x, BigInt::parse_bytes(b"18446744073709551615", 10).unwrap())
    };
}

/// test parsing very big numbers
#[test]
fn test_parse_bigint() {
    match_or_panic! {
        fst_stmt("0xFFFFFFFFFFFFFFFFFFFFFFFF"),
        LiteralValue(Literal::BigInt(x)) => assert_eq!(x, BigInt::parse_bytes(b"79228162514264337593543950335", 10).unwrap())
    };
    match_or_panic! {
        fst_stmt("0x7FFFFFFFFFFFFFFF"),
        LiteralValue(Literal::Int64(x)) => assert_eq!(x, 0x7FFF_FFFF_FFFF_FFFF)
    };
    match_or_panic! {
        fst_stmt("0x8000000000000000"),
        LiteralValue(Literal::BigInt(x)) => assert_eq!(x, BigInt::parse_bytes(b"9223372036854775808", 10).unwrap())
    };
}

/// test parsing of parenthesized expressions
#[test]
fn test_parse_paren() {
    #[track_caller]
    fn tuple_len<T>(code: T) -> usize
    where
        T: Into<String> + Clone + std::marker::Send + 'static,
    {
        if let Tuple(v) = fst_stmt(code) {
            v.len()
        } else {
            usize::MAX
        }
    }

    assert!(matches!(fst_stmt("()"), UnitType));
    assert!(matches!(fst_stmt("( )"), UnitType));
    assert!(matches!(fst_stmt("(1)"), LiteralValue(Literal::Int64(1))));
    assert_eq!(tuple_len("(1,)"), 1);
    assert_eq!(tuple_len("((()),)"), 1);
    assert_eq!(tuple_len("(((())),)"), 1);
    assert_eq!(tuple_len("(1,1)"), 2);
    assert_eq!(tuple_len("(1,1,)"), 2);
    assert_eq!(tuple_len("(1,1,1)"), 3);
    assert_eq!(tuple_len("(1,1,1,)"), 3);
    assert_eq!(tuple_len("('a','b','c','d','e','f','g')"), 7);
    assert_eq!(tuple_len("((),())"), 2);
    assert_eq!(tuple_len("((),(1),)"), 2);
    assert_eq!(tuple_len("((),(1),())"), 3);
    assert_eq!(tuple_len("((((1))),1,((())),)"), 3);
    assert_eq!(tuple_len("(((((1))),1,((())),))"), 3);
    assert_eq!(tuple_len("(1,2,\"some_string\")"), 3);
}

/// test parsing of factor expressions
#[test]
fn test_parse_factor() {
    match_or_panic! {
        fst_stmt("+1"),
        UnaryPlus(b) => b.kind,
        LiteralValue(Literal::Int64(1)) => ()
    };

    match_or_panic! {
        fst_stmt("-1"),
        UnaryMinus(b) => b.kind,
        LiteralValue(Literal::Int64(1)) => ()
    };

    match_or_panic! {
        fst_stmt("~1"),
        BitwiseNot(b) => b.kind,
        LiteralValue(Literal::Int64(1)) => ()
    };

    match_or_panic! {
        fst_stmt("++1"),
        UnaryPlus(b) => b.kind,
        UnaryPlus(b) => b.kind,
        LiteralValue(Literal::Int64(1)) => ()
    };

    match_or_panic! {
        fst_stmt("(+-(~()))"),
        UnaryPlus(b) => b.kind,
        UnaryMinus(b) => b.kind,
        BitwiseNot(b) => b.kind,
        UnitType => ()
    };
}

/// test parsing of logical not expressions
#[test]
fn test_parse_logical_not() {
    if let LogicalNot(b) = fst_stmt("!false") {
        assert!(matches!(b.kind, LiteralValue(Literal::Bool(false))));
    } else {
        panic!();
    }

    if let LogicalNot(b) = fst_stmt("!!false") {
        if let LogicalNot(b) = b.kind {
            assert!(matches!(b.kind, LiteralValue(Literal::Bool(false))));
        } else {
            panic!();
        }
    } else {
        panic!();
    }

    if let LogicalNot(b) = fst_stmt("!~false") {
        if let BitwiseNot(b) = b.kind {
            assert!(matches!(b.kind, LiteralValue(Literal::Bool(false))));
        } else {
            panic!();
        }
    } else {
        panic!();
    }
}

/// test parsing of *,/,% expressions
#[test]
fn test_parse_term() {
    match_or_panic! {
        nth_stmt(2, "let foo=2;let bar=3;foo * bar"),
        BinOpExpr{first, rest, ..} => (first.kind, rest),
        (Identifier {name, ..}, rest) => (&name[..], &rest[..]),
        ("foo", [(Operator::Times, match_kind!(Identifier{name, ..}))]) => &name[..],
        "bar" => ()
    };
    match_or_panic! {
        nth_stmt(2, "let foo=2;let bar=3;foo / bar"),
        BinOpExpr{first, rest, ..} => (first.kind, rest),
        (Identifier{name, ..}, rest) => (&name[..], &rest[..]),
        ("foo", [(Operator::Divide, match_kind!(Identifier{name, ..}))]) => &name[..],
        "bar" => ()
    };
    match_or_panic! {
        nth_stmt(2, "let foo=2;let bar=3;foo % bar"),
        BinOpExpr{first, rest, ..} => (first.kind, rest),
        (Identifier{name, ..}, rest) => (&name[..], &rest[..]),
        ("foo", [(Operator::Remainder, match_kind!(Identifier{name, ..}))]) => &name[..],
        "bar" => ()
    };
    match_or_panic! {
        fst_stmt("1%2/3*4"),
        BinOpExpr{first, rest, ..} => (first.kind, rest),
        (LiteralValue(Literal::Int64(1)), rest) => &rest[..],
        [
            (Operator::Remainder, match_kind!(LiteralValue(Literal::Int64(2)))),
            (Operator::Divide, match_kind!(LiteralValue(Literal::Int64(3)))),
            (Operator::Times, match_kind!(LiteralValue(Literal::Int64(4))))
        ] => ()
    };
}

/// test parsing of +,- expressions
#[test]
fn test_parse_addsub() {
    match_or_panic! {
        nth_stmt(2, "let foo=2;let bar=3;foo + bar"),
        BinOpExpr{first, rest, ..} => (first.kind, rest),
        (Identifier{name, ..}, rest) => (&name[..], &rest[..]),
        ("foo", [(Operator::Plus, match_kind!(Identifier{name, ..}))]) => &name[..],
        "bar" => ()
    };
    match_or_panic! {
        nth_stmt(2, "let foo=2;let bar=3;foo - bar"),
        BinOpExpr{first, rest, ..} => (first.kind, rest),
        (Identifier{name, ..}, rest) => (&name[..], &rest[..]),
        ("foo", [(Operator::Minus, match_kind!(Identifier{name, ..}))]) => &name[..],
        "bar" => ()
    };
    match_or_panic! {
        fst_stmt("1+2-3+4"),
        BinOpExpr{first, rest, ..} => (first.kind, rest),
        (LiteralValue(Literal::Int64(1)), rest) => &rest[..],
        [
            (Operator::Plus, match_kind!(LiteralValue(Literal::Int64(2)))),
            (Operator::Minus, match_kind!(LiteralValue(Literal::Int64(3)))),
            (Operator::Plus, match_kind!(LiteralValue(Literal::Int64(4))))
        ] => ()
    };
}

#[test]
fn test_parse_ops() {
    match_or_panic! {
        fst_stmt("1 / -2.0 * 3 / 0.5 / 1e3"),
        BinOpExpr{first, rest, ..} => (first.kind, rest),
        (LiteralValue(Literal::Int64(1)), rest) => &rest[..],
        [
            (Operator::Divide, match_kind!(UnaryMinus(two))),
            (Operator::Times, match_kind!(LiteralValue(Literal::Int64(3)))),
            (Operator::Divide, match_kind!(LiteralValue(Literal::Float(point_five)))),
            (Operator::Divide, match_kind!(LiteralValue(Literal::Float(one_thousand)))),
        ] => {
            assert!((point_five-0.5).abs() < f64::EPSILON);
            assert!((one_thousand-1000.0).abs() < f64::EPSILON);
            &**two
        },
        match_kind!(LiteralValue(Literal::Float(two))) => assert!((two-2.0).abs() < f64::EPSILON)
    };
}

/// test parsing of comparison operators
#[test]
fn test_parse_compare() {
    match_or_panic! {
        nth_stmt(2, "let foo=2;let bar=3;foo == bar"),
        ComparisonOpExpr{first, rest, ..} => (first.kind, rest),
        (Identifier{name, ..}, rest) => (&name[..], &rest[..]),
        ("foo", [(Operator::Equals, match_kind!(Identifier{name, ..}))]) => &name[..],
        "bar" => ()
    };
    match_or_panic! {
        nth_stmt(2, "let foo=2;let bar=3;foo != bar"),
        ComparisonOpExpr{first, rest, ..} => (first.kind, rest),
        (Identifier{name, ..}, rest) => (&name[..], &rest[..]),
        ("foo", [(Operator::NotEquals, match_kind!(Identifier{name, ..}))]) => &name[..],
        "bar" => ()
    };
    match_or_panic! {
        nth_stmt(2, "let foo=2;let bar=3;foo <= bar"),
        ComparisonOpExpr{first, rest, ..} => (first.kind, rest),
        (Identifier{name, ..}, rest) => (&name[..], &rest[..]),
        ("foo", [(Operator::LessThanOrEqualTo, match_kind!(Identifier{name, ..}))]) => &name[..],
        "bar" => ()
    };
    match_or_panic! {
        nth_stmt(2, "let foo=2;let bar=3;foo < bar"),
        ComparisonOpExpr{first, rest, ..} => (first.kind, rest),
        (Identifier{name, ..}, rest) => (&name[..], &rest[..]),
        ("foo", [(Operator::LessThan, match_kind!(Identifier{name, ..}))]) => &name[..],
        "bar" => ()
    };
    match_or_panic! {
        nth_stmt(2, "let foo=2;let bar=3;foo >= bar"),
        ComparisonOpExpr{first, rest, ..} => (first.kind, rest),
        (Identifier{name, ..}, rest) => (&name[..], &rest[..]),
        ("foo", [(Operator::GreaterThanOrEqualTo, match_kind!(Identifier{name, ..}))]) => &name[..],
        "bar" => ()
    };
    match_or_panic! {
        nth_stmt(2, "let foo=2;let bar=3;foo > bar"),
        ComparisonOpExpr{first, rest, ..} => (first.kind, rest),
        (Identifier{name, ..}, rest) => (&name[..], &rest[..]),
        ("foo", [(Operator::GreaterThan, match_kind!(Identifier{name, ..}))]) => &name[..],
        "bar" => ()
    };
    match_or_panic! {
        fst_stmt("1 not in {1,}"),
        InExpr{container, invert:true, item} => (container.kind, item.kind),
        (SetExpr(set), LiteralValue(Literal::Int64(1))) => &set[..],
        [SetExprElem::Value(match_kind!(LiteralValue(Literal::Int64(1))))] => ()
    };
    match_or_panic! {
        nth_stmt(2, "let foo=2;let bar=3;foo not in bar"),
        InExpr{container, invert:true, item} => (container.kind, item.kind),
        (Identifier{name:container_name, ..}, Identifier{name: item_name, ..}) => (container_name.as_ref(), item_name.as_ref()),
        ("bar", "foo") => ()
    };
    match_or_panic! {
        nth_stmt(2, "let foo=2;let bar=3;foo in bar"),
        InExpr{container, invert:false, item} => (container.kind, item.kind),
        (Identifier{name:container_name, ..}, Identifier{name: item_name, ..}) => (container_name.as_ref(), item_name.as_ref()),
        ("bar", "foo") => ()
    };

    match_or_panic! {
        fst_stmt("1*2 <= 3+4 != 5%6"),
        ComparisonOpExpr{first, rest, ..} => (first.kind, rest),
        (BinOpExpr{first, rest:rest1, ..}, rest2) => (first.kind, &rest1[..], &rest2[..]),
        (
            LiteralValue(Literal::Int64(1)),
            [(Operator::Times, match_kind!(LiteralValue(Literal::Int64(2))))],
            [
                (Operator::LessThanOrEqualTo, match_kind!(BinOpExpr{first: f1, rest:v1, ..})),
                (Operator::NotEquals, match_kind!(BinOpExpr{first: f2, rest:v2, ..}))
            ]
        ) => (&**f1, &v1[..], &**f2, &v2[..]),
        (
            match_kind!(LiteralValue(Literal::Int64(3))),
            [(Operator::Plus, match_kind!(LiteralValue(Literal::Int64(4))))],
            match_kind!(LiteralValue(Literal::Int64(5))),
            [(Operator::Remainder, match_kind!(LiteralValue(Literal::Int64(6))))]
        ) => ()
    };

    match_or_panic! {
        fst_stmt("-1==-1"),
        ComparisonOpExpr{first, rest, ..} => (first.kind, &rest[..]),
        (UnaryMinus(a), rest) => (*a, rest),
        (match_kind!(LiteralValue(Literal::Int64(1))), [(Operator::Equals, match_kind!(UnaryMinus(b)))]) => &b.kind,
        LiteralValue(Literal::Int64(1)) => ()
    };
}

/// test reading a "let"
#[test]
fn test_let_1() {
    match_or_panic! {
        fst_stmt(r#"let x = 0"#),
        Let {pattern, value_expr, ..} => (pattern.kind, value_expr.kind),
        (StegoPatternKind::Capture(name), AstNodeKind::LiteralValue(Literal::Int64(0))) => &name[..],
        "x" => ()
    };
}

#[test]
fn test_let_2() {
    // this is not a valid location for a `let`
    test_file_error("1 + (let x = 1)", 1, 6);
}

#[test]
fn test_let_3() {
    test_file_error("let let = 0", 1, 5);

    // TODO: test more complicated let patterns
}

/// test reading a named expression
#[test]
fn test_named_expr() {
    match_or_panic! {
        test_file(r#"(some_name@("some", "value")) < ("","")"#),
        Some(Statements { statements, .. }) => &statements[..],
        [node] => &node.kind,
        ComparisonOpExpr{first, ..} => &first.kind,
        NamedExpr {name, value} => (&name[..], &value.kind),
        ("some_name", Tuple(v)) => &v[..],
        [a, b] => (&a.kind, &b.kind),
        (LiteralValue(Literal::String(s1)), LiteralValue(Literal::String(s2))) => (&s1[..], &s2[..]),
        ("some", "value") => ()
    };
}

#[test]
fn test_named_expr_as_term() {
    test_file_error(r#"1 + x@7"#, 1, 6);

    match_or_panic! {
        test_file(r#"1 + (x@7)"#),
        Some(Statements { statements, .. }) => &statements[..],
        [node] => &node.kind,
        BinOpExpr{rest, ..} => &rest[..],
        [(Operator::Plus, named_expr_node), ..] => &named_expr_node.kind,
        NamedExpr{name, value} => (&name[..], &value.kind),
        ("x", LiteralValue(Literal::Int64(7))) => ()
    }
}

/// test indentation issues
#[test]
fn test_indentation() {
    test_file_warning(" () ", 1, 1);
    match_or_panic! {
        fst_stmt("1\n    +2"),
        BinOpExpr{first, rest, ..} => (first.kind, &rest[..]),
        (LiteralValue(Literal::Int64(1)), [(Operator::Plus, match_kind!(LiteralValue(Literal::Int64(2))))]) => ()
    };
    match_or_panic! {
        fst_stmt("1\n+2"),
        LiteralValue(Literal::Int64(1)) => ()
    };
    test_file_error(
        "(1\n\
        +2)",
        1,
        1,
    );
    test_file_warning("1\n\t+2", 2, 1);
}

/// test assign statement
#[test]
fn test_assignment() {
    match_or_panic! {
        snd_stmt("let some_var = 10;some_var = 11"),
        AssignStmt{lhs, rhs, ..} => (lhs.kind, rhs.kind),
        (Identifier{name,..}, LiteralValue(Literal::Int64(11))) => assert_eq!(&name, "some_var")
    };
    test_file_error("let some_var = 10;some_var = 100 = 1", 1, 34);
}

/// syntax errors
#[test]
fn test_syntax_error() {
    test_file_warning("1+\n\t2", 2, 1);
    test_file_error("let x = 1 let y = 10", 1, 11);
}

#[test]
fn test_if0() {
    match_or_panic! {
        fst_stmt("if 1 then \"one\""),
        IfExpr {arms, catch_all_else: None} => &arms[..],
        [IfArm{cond: match_kind!(LiteralValue(Literal::Int64(1))), consequent: match_kind!(Statements{statements,..})}] => &statements[..],
        [match_kind!(LiteralValue(Literal::String(s)))] => assert_eq!(s, "one")
    };
}

#[test]
fn test_if1() {
    match_or_panic!(
        snd_stmt("let x = 0 ; if x<0 then \"a\" else \"b\""),
        IfExpr {arms, catch_all_else: Some(else_stmts)} => (&arms[..], else_stmts.kind),
        (
            [IfArm{cond: match_kind!(ComparisonOpExpr {first: cond_first,rest:cond_rest,..}), consequent: match_kind!(Statements{statements:arm0_stmts,..})}],
            Statements{statements:ref else_stmts,..},
        ) => (&cond_first.kind, &cond_rest[..], &arm0_stmts[..], &else_stmts[..]),
        (
            Identifier {name,..},
            [(Operator::LessThan, match_kind!(LiteralValue(Literal::Int64(0))))],
            [match_kind!(LiteralValue(Literal::String(a)))],
            [match_kind!(LiteralValue(Literal::String(b)))]
        ) => (&name[..], &a[..], &b[..]),
        (
            "x",
            "a",
            "b"
        ) => ()
    );
}

#[test]
fn test_if2() {
    match_or_panic!(
        nth_stmt(2, "let a = false;let b = 100;if a then 0;1 else if b then 2;3"),
        IfExpr {arms, catch_all_else: None} => &arms[..],
        [
            IfArm{
                cond: match_kind!(Identifier {name:name0,..}),
                consequent: match_kind!(Statements{statements:arm0_stmts,..})
            },
            IfArm{
                cond: match_kind!(Identifier {name:name1,..}),
                consequent: match_kind!(Statements{statements:arm1_stmts,..})
            },
        ] => (&name0[..], &arm0_stmts[..], &name1[..], &arm1_stmts[..]),
        (
            "a",
            [zero, one],
            "b",
            [two, three]
        ) => (&zero.kind, &one.kind, &two.kind, &three.kind),
        (LiteralValue(Literal::Int64(0)), LiteralValue(Literal::Int64(1)), LiteralValue(Literal::Int64(2)), LiteralValue(Literal::Int64(3))) => ()
    );
}

#[test]
fn test_if3() {
    match_or_panic!(
        &parse_stmts("let a = false;let b = 100;(if a then 0;1 else if b then 2);3")[..],
        [
            ..,
            match_kind!(IfExpr {arms, catch_all_else: None}),
            match_kind!(LiteralValue(Literal::Int64(3)))
        ] => &arms[..],
        [
            IfArm{cond: cond0, consequent: consequent0},
            IfArm{cond: cond1, consequent: consequent1},
        ] => (&cond0.kind, &consequent0.kind, &cond1.kind, &consequent1.kind),
        (
            Identifier {name:name0,..},
            Statements{statements:arm0_stmts,..},
            Identifier {name:name1,..},
            Statements{statements:arm1_stmts,..}
        ) => (&name0[..], &arm0_stmts[..], &name1[..], &arm1_stmts[..]),
        (
            "a",
            [zero, one],
            "b",
            [two]
        ) => (&zero.kind, &one.kind, &two.kind),
        (LiteralValue(Literal::Int64(0)), LiteralValue(Literal::Int64(1)), LiteralValue(Literal::Int64(2))) => ()
    );
}

#[test]
fn test_if4() {
    let code = "\
    let a = false;\
    if a then\
    \n    0\
    \n    1\
    \nelse\
    \n    2\
    \n    3";
    match_or_panic!(
        &parse_stmts(code)[..],
        [_, node] => &node.kind,
        IfExpr {arms, catch_all_else: Some(else_stmts)} => (&arms[..], else_stmts as &AstNode),
        (
            [IfArm{cond: match_kind!(Identifier {name:name0,..}), consequent: match_kind!(Statements{statements:arm0_stmts,..})}],
            match_kind!(Statements{statements:else_stmts, ..}),
        ) => (&name0[..], &arm0_stmts[..], &else_stmts[..]),
        (
            "a",
            [match_kind!(LiteralValue(Literal::Int64(0))), match_kind!(LiteralValue(Literal::Int64(1)))],
            [match_kind!(LiteralValue(Literal::Int64(2))), match_kind!(LiteralValue(Literal::Int64(3)))]
        ) => ()
    );
}

#[test]
fn test_if5() {
    let code = "\
    let a = false;\
    if a then\
    \n    0\
    \n    1\
    \nelse\
    \n    2\
    \n    3";
    match_or_panic!(
        &parse_stmts(code)[..],
        [_, match_kind!(IfExpr {arms, catch_all_else: Some(else_stmts)})] => (&arms[..], &**else_stmts),
        (
            [IfArm{cond: match_kind!(Identifier {name:name0,..}), consequent: match_kind!(Statements{statements:arm0_stmts,..})}],
            match_kind!(Statements{statements:else_stmts, ..}),
        ) => (&name0[..], &arm0_stmts[..], &else_stmts[..]),
        (
            "a",
            [match_kind!(LiteralValue(Literal::Int64(0))), match_kind!(LiteralValue(Literal::Int64(1)))],
            [match_kind!(LiteralValue(Literal::Int64(2))), match_kind!(LiteralValue(Literal::Int64(3)))]
        ) => ()
    );
}

#[test]
fn test_if6() {
    let code = [
        "let a = 5",
        "if a == 0 then",
        "    1",
        "else if a == 1 then",
        "    10",
        "else if a == 2 then",
        "    100",
        "else if a == 3 then",
        "    1000",
        "else",
        "    if a == 4 then",
        "        10000",
        "    else",
        "        0",
    ]
    .join("\n");

    if let [match_kind!(Let { .. }), match_kind!(IfExpr {
        arms,
        catch_all_else: Some(else_stmts),
    })] = &parse_stmts(code)[..]
    {
        assert_eq!(arms.len(), 5);
        for (arm, i) in arms.iter().zip(0..5) {
            match_or_panic! {
                arm,
                IfArm {
                    cond: match_kind!(ComparisonOpExpr{first, rest, ..}),
                    consequent: match_kind!(Statements{statements,..})
                } => (&**first, &rest[..], &statements[..]),
                (
                    match_kind!(Identifier{name,..}),
                    [(Operator::Equals, match_kind!(LiteralValue(Literal::Int64(n0))))],
                    [match_kind!(LiteralValue(Literal::Int64(n1)))]
                ) => {
                    assert_eq!(name, "a");
                    assert_eq!(*n0, i);
                    assert_eq!(*n1, pow(10, i as usize));
                }
            };
        }

        match_or_panic! {
            &**else_stmts,
            match_kind!(Statements {statements, ..}) => &statements[..],
            [match_kind!(LiteralValue(Literal::Int64(0)))] => {}
        };
    } else {
        panic!()
    }
}

#[test]
fn test_if7() {
    let code = "let a = 0\nif a == 0 then a = 100\na";
    match_or_panic! {
        &parse_stmts(code)[..],
        [
            match_kind!(Let {..}),
            match_kind!(IfExpr {arms, catch_all_else: None}),
            match_kind!(Identifier{name,..}),
        ] => (&arms[..], &name[..]),
        (
            [IfArm{cond: match_kind!(ComparisonOpExpr{first, rest, ..}), consequent: match_kind!(Statements{statements,..})}],
            "a"
        ) => (&**first, &rest[..], &statements[..]),
        (
            match_kind!(Identifier{name,..}),
            [(Operator::Equals, match_kind!(LiteralValue(Literal::Int64(0))))],
            [match_kind!(AssignStmt{lhs,rhs,..})]
        ) => (&name[..], &**lhs, &**rhs),
        ("a", match_kind!(Identifier{name,..}), match_kind!(LiteralValue(Literal::Int64(100)))) => assert_eq!(name, "a")
    }
}

#[test]
fn test_function_application() {
    let code = "let some_arg=0;fn some_fn x = x;;some_fn some_arg";
    match_or_panic! {
        &parse_stmts(code)[..],
        [.., node] => &node.kind,
        ApplyExpr{func, args, ..} => (&func.kind, &args[..]),
        (
            Identifier{name: name0,..},
            [match_kind!(Identifier{name: name1,..})]
        ) => {
            assert_eq!(name0, "some_fn");
            assert_eq!(name1, "some_arg");
        }
    }
    let code =
        "let some_arg0=10;let some_arg1=2;fn some_fn a b = a + b;;some_fn some_arg0 some_arg1";
    match_or_panic! {
        &parse_stmts(code)[..],
        [.., node] => &node.kind,
        ApplyExpr{func, args, ..} => (&func.kind, &args[..]),
        (
            Identifier{name: name0,..},
            [id0, id1]
        ) => (&id0.kind, &id1.kind),
        (
            Identifier{name: name1,..},
            Identifier{name: name2,..}
        ) => {
            assert_eq!(name0, "some_fn");
            assert_eq!(name1, "some_arg0");
            assert_eq!(name2, "some_arg1");
        }
    }
}

#[test]
fn test_do() {
    let code = "\
    let a = 10\
    \ndo\
    \n    do a = 11 ; a = -11\
    \n    let a = 12\
    \n    a\
    \na";
    match_or_panic! {
        &parse_stmts(code)[..],
        [let_node, do_node, id_node] => (&let_node.kind, &do_node.kind, &id_node.kind),
        (
            Let {..},
            DoExpr(statements),
            Identifier{..}
         ) => &statements.kind,
        Statements{statements,..} => &statements[..],
        [do_node, let_node, id_node] => (&do_node.kind, &let_node.kind, &id_node.kind),
        (
            DoExpr(statements),
            Let {..},
            Identifier{..}
         ) => &statements.kind,
        Statements{statements,..} => &statements[..],
        [assign_0, assign_1] => (&assign_0.kind, &assign_1.kind),
        (AssignStmt{..}, AssignStmt{..}) => ()
    }
}

#[test]
fn test_fn0() {
    let code = "fn is_pos x = x > 0";
    match_or_panic! {
        &parse_stmts(code)[..],
        [node] => &node.kind,
        crate::ast::astnode::AstNodeKind::FnStmt(crate::ast::FnStmt{name, params, body, ..}) => (&name[..], params.borrow(), body.borrow()),
        ("is_pos", params, body) => (&params[..], &body.kind),
        ([cap], Statements{statements}) => (&cap.kind, &statements[..]),
        (StegoPatternKind::Capture(param_name), [comp_expr]) => (&param_name[..], &comp_expr.kind),
        ("x", ComparisonOpExpr{first, rest, ..}) => (&first.kind, &rest[..]),
        (Identifier{name,..}, [(Operator::GreaterThan, zero)]) => (&name[..], &zero.kind),
        ("x", LiteralValue(Literal::Int64(0))) => ()
    }
}

#[test]
fn test_fn1() {
    let code = "fn is_pos x =\n    x > 0";
    match_or_panic! {
        &parse_stmts(code)[..],
        [node] => &node.kind,
        crate::ast::astnode::AstNodeKind::FnStmt(crate::ast::FnStmt{name, params, body, ..}) => (&name[..], params.borrow(), body.borrow()),
        (a, params, body) => (a, &params[..], &body.kind),
        (a, b, Statements{statements}) => (a, b, &statements[..]),
        ("is_pos", [cap], [comp_expr]) => (&cap.kind, &comp_expr.kind),
        (StegoPatternKind::Capture(param_name), ComparisonOpExpr{first, rest, ..}) => (&param_name[..], &first.kind, &rest[..]),
        ("x", Identifier{name,..}, [(Operator::GreaterThan, zero)]) => (&name[..], &zero.kind),
        ("x", LiteralValue(Literal::Int64(0))) => ()
    }
}

#[test]
fn test_fn4() {
    let code = "fn avg4 a b c d = (a + b + c + d) / 4.0";
    match_or_panic! {
        &parse_stmts(code)[..],
        [node] => &node.kind,
        crate::ast::astnode::AstNodeKind::FnStmt(crate::ast::FnStmt{name, params, body, ..}) => (&name[..], params.borrow(), body.borrow()),
        (a, params, body) => (a, &params[..], &body.kind),
        (a, b, Statements{statements}) => (a, b, &statements[..]),
        (
            "avg4",
            [cap_a, cap_b, cap_c, cap_d],
            [binop]
        ) => (
            &cap_a.kind,
            &cap_b.kind,
            &cap_c.kind,
            &cap_d.kind,
            &binop.kind
        ),
        (StegoPatternKind::Capture(a), StegoPatternKind::Capture(b), StegoPatternKind::Capture(c), StegoPatternKind::Capture(d), BinOpExpr{first,rest,..}) => (&a[..],
            &b[..],
            &c[..],
            &d[..],
            &first.kind,
            &rest[..]),
        (
            "a",
            "b",
            "c",
            "d",
            BinOpExpr{first,rest,..},
            [(Operator::Divide, denom)]
        ) => (&first.kind, &rest[..], &denom.kind),
        (
            Identifier{name:a,..},
            [(Operator::Plus, b),
            (Operator::Plus, c),
            (Operator::Plus, d)],
            LiteralValue(Literal::Float(f))
        ) => {
            assert!((f-4.0).abs() < f64::EPSILON);
            (a, &b.kind, &c.kind,& d.kind)
        },
        (
            a,
            Identifier{name:b,..},
            Identifier{name:c,..},
            Identifier{name:d,..}
        ) => (
            &a[..],
            &b[..],
            &c[..],
            &d[..]
        ),
        ("a", "b", "c", "d") => ()
    }
}

#[test]
fn test_fn_shadow_warning() {
    let code = r#"
fn foo x = x
do
    fn foo x = x"#;

    // allowed because scope is different
    match_or_panic! {
        parse("<test case>", code),
        Ok((_, warnings)) => &warnings[..],
        [] => ()
    }
}

#[test]
fn test_dict_literal_1() {
    let code = r#"{}"#;
    match_or_panic! {
        &parse_stmts(code)[..],
        [node] => &node.kind,
        DictExpr(v) => &v[..],
        [] => ()
    }
}

#[test]
fn test_dict_literal_2() {
    use crate::ast::DictExprElem::*;
    let code = r#"{"a":1,"b":2}"#;
    match_or_panic! {
        &parse_stmts(code)[..],
        [node] => &node.kind,
        DictExpr(v) => &v[..],
        [KeyValue(k1, v1), KeyValue(k2, v2)] => (&k1.kind, &v1.kind, &k2.kind, &v2.kind),
        (
            LiteralValue(Literal::String(k1)),
            LiteralValue(Literal::Int64(1)),
            LiteralValue(Literal::String(k2)),
            LiteralValue(Literal::Int64(2))
        ) => (&k1[..], &k2[..]),
        ("a", "b") => ()
    }
}

#[test]
fn test_dict_literal_3() {
    use crate::ast::DictExprElem::*;
    let code = r#"let kw={} ; {**kw}"#;
    match_or_panic! {
        &parse_stmts(code)[..],
        [_, node] => &node.kind,
        DictExpr(v) => &v[..],
        [KeywordSpread(node)] => &node.kind,
        Identifier {name, ..} => assert_eq!(name, "kw")
    }
}

#[test]
fn test_set_literal() {
    let code = "let set0 = {,}; {*set0,1,2}";
    match_or_panic! {
        &parse_stmts(code)[..],
        [_, node] => &node.kind,
        SetExpr(v) => &v[..],
        [SetExprElem::Spread(spread), SetExprElem::Value(lit1), SetExprElem::Value(lit2)] => (&spread.kind, &lit1.kind, &lit2.kind),
        (Identifier {name, ..}, LiteralValue(Literal::Int64(1)), LiteralValue(Literal::Int64(2))) => assert_eq!(name.as_ref(), "set0")
    }
}
#[test]
fn test_parse_identifier() {
    let code = "let abc0 = 0; (abc0,1)";
    match_or_panic! {
        &parse_stmts(code)[..],
        [_, node] => &node.kind,
        Tuple(v) => &v[..],
        [id, literal] => (&id.kind, &literal.kind),
        (Identifier {name, ..} , LiteralValue(Literal::Int64(1))) => assert_eq!(name, "abc0")
    }
}

#[test]
fn test_variable_capture() {
    let code = "\
    \nlet a = 1\
    \nlet b = 0\
    \nfn fib _ =\
    \n    let c = a + b\
    \n    a = b\
    \n    b = c\
    \n    c";
    match_or_panic! {
        &parse_stmts(code)[..],
        [s0, s1, s2] => (&s0.kind, &s1.kind, &s2.kind),
        (
            Let{..},
            Let{..},
            crate::ast::astnode::AstNodeKind::FnStmt(crate::ast::FnStmt{
                name,
                body,
                ..
            })
         ) => (&**name, &(body.borrow()).kind),
         ("fib", Statements { statements }) => &statements[..],
        [s0, s1, s2, s3] => (&s0.kind, &s1.kind, &s2.kind, &s3.kind),
        (
            Let{pattern: capture_c, value_expr: a_plus_b, ..},
            AssignStmt{lhs: a2, rhs: b2,..},
            AssignStmt{lhs: b3, rhs: c2,..},
            Identifier{name: c3,..}
        ) => (
            &capture_c.kind,
            &a_plus_b.kind,
            &a2.kind,
            &b2.kind,
            &b3.kind,
            &c2.kind,
            &**c3
        ),
        (
            StegoPatternKind::Capture(c1),
            BinOpExpr{first: a1, rest: plus_b, ..},
            Identifier{name: a2,..},
            Identifier{name: b2,..},
            Identifier{name: b3,..},
            Identifier{name: c2,..},
            "c",
        ) => (
            &**c1,
            &a1.kind,
            &plus_b[..],
            &**a2,
            &**b2,
            &**b3,
            &**c2,
        ),
        ("c", Identifier{name: id0,..}, [(Operator::Plus, match_kind!(Identifier{name: id1, ..}))], "a", "b", "b", "c") => (&**id0, &**id1),
        ("a", "b") => ()
    }
}

#[test]
fn test_parse_return() {
    let code = "fn f x = if x then return 0";
    match_or_panic! {
        &parse_stmts(code)[..],
        [node] => &node.kind,
        crate::ast::astnode::AstNodeKind::FnStmt(crate::ast::FnStmt{body,..}) => &(body.borrow()).kind,
        Statements{statements,..} => &statements[..],
        [node] => &node.kind,
        IfExpr {arms, catch_all_else: None} => &arms[..],
        [IfArm {consequent, ..}] => &consequent.kind,
        Statements{statements,..} => &statements[..],
        [node] => &node.kind,
        ReturnStmt(Some(retval)) => &retval.kind,
        LiteralValue(Literal::Int64(0)) => ()
    }
}

#[test]
fn test_binary_op() {
    let code = "let x = 1 + 1 ; x";
    match_or_panic! {
        &parse_stmts(code)[..],
        [node, ..] => &node.kind,
        Let {pattern, value_expr} => (&pattern.kind, &value_expr.kind),
        (StegoPatternKind::Capture(s), BinOpExpr{first, rest}) => (&**s, &first.kind, &rest[..]),
        ("x", LiteralValue(Literal::Int64(1)), [(Operator::Plus, match_kind!(LiteralValue(Literal::Int64(1))))]) => ()
    }
}

#[test]
fn test_comments_in_block() {
    let code = r#"
do
    // say hi
    print "hello"
"#;
    match_or_panic! {
        &parse_stmts(code)[..],
        [do_node] => &do_node.kind,
        DoExpr(body) => &body.kind,
        Statements{statements} => &statements[..],
        [node] => &node.kind,
        ApplyExpr{func, args, ..} => (&func.kind, &args[..]),
        (Identifier{name,..}, [arg_node]) => {
            assert_eq!(name, "print");
            &arg_node.kind
        },
        LiteralValue(Literal::String(s)) => assert_eq!(s, "hello")
    }
}

#[test]
fn test_comments_in_block_2() {
    let code = r#"
fn foo x =
    // say hi
    print "hello "
    println x
foo 123
"#;
    parse_stmts(code);
}

#[test]
fn test_comments_in_block_3() {
    parse_stmts(
        r#"
let foo = \x=
    // say hi
    print "hello "
    println x
foo 123
"#,
    );
    parse_stmts(
        r#"
let foo = \x= // say hi
    print "hello "
    println x
foo 123"#,
    );
    parse_stmts(
        r#"
do
    /**/println "hello"
        println "hello""#,
    );
    parse_stmts(
        r#"
\x=/*leading comments are weird maybe there should be a warning*/println "hello"
                                                                 println "hello""#,
    );
}

#[test]
fn test_comments_in_block_4() {
    parse_stmts(
        r#"
do          print 'a'
            print 'b'
            print 'c'
fn foo x =/*leading comments stink*/print 'd'
                                    print "ef"
foo 123
fn bar x = print 'g'
           print 'h'
           print x
bar 'i'
do do do print 'j'
      print 'k'
   print 'l'
println ""
(\x= /*...*/ print "hello "
/*...*/      print x) "world"
println ""
let foo = \x=
    // say hi
    print "hello "
    println x
foo 123
"#,
    );
}
