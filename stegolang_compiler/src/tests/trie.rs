use stegolang_proc_macros::trie;

use crate::config::KEYWORDS;
#[derive(Copy, Clone)]
#[repr(u8)]
enum Symbol {
    FnType = 0,
    And,
    Async,
    Await,
    Break,
    Continue,
    Dec,
    Do,
    Else,
    False,
    Fn,
    For,
    Get,
    If,
    Imm,
    In,
    Inf,
    Is,
    Let,
    Match,
    Matching,
    Mod,
    Mut,
    Namespace,
    NaN,
    Not,
    NotIn,
    Or,
    Otherwise,
    Readonly,
    Return,
    Set,
    Struct,
    Then,
    True,
    Ty,
    TyCl,
    TypeOf,
    Where,
    While,
    Xor,
    Empty,
}
use Symbol::*;
fn convert(s: &str) -> Option<Symbol> {
    trie!(s,
        => Empty,
        Fn => FnType,
        a {
            nd => And,
            sync => Async,
            wait => Await,
        },
        break => Break,
        continue => Continue,
        d {
            ec => Dec,
            o => Do,
        },
        else => Else,
        f {
            alse => False,
            n => Fn,
            or => For
        },
        get => Get,
        i {
            f => If,
            mm => Imm,
            n {
                => In,
                f => Inf,
            },
            s => Is,
        },
        let => Let,
        m {
            atch {
                => Match,
                ing => Matching,
            },
            od {
                => Mod,
            },
            ut => Mut,
        },
        n {
            a {
                mespace => Namespace,
                n => NaN,
            },
            ot {
                => Not,
                in => NotIn
            }
        },
        o {
            r => Or,
            therwise => Otherwise,
        },
        re {
            adonly => Readonly,
            turn => Return,
        },
        s {
            et => Set,
            truct => Struct,
        },
        t {
            hen => Then,
            rue => True,
            y {
                => Ty,
                cl => TyCl,
                peof => TypeOf,
            }
        },
        wh {
            ere => Where,
            ile => While,
        },
        xor => Xor
    )
}

#[test]
fn macro_test() {
    for (i, kw) in KEYWORDS.iter().enumerate() {
        // skip the multiword "keywords" for this
        if kw.contains(' ') {
            continue;
        }
        let got = convert(kw);
        assert!(got.is_some(), "Failed to parse keyword {kw} into symbol");
        let got = got.unwrap();
        assert_eq!(
            i,
            (got as u8).into(),
            "Expected keyword \"{kw}\" to parse to {i}. Got {}",
            got as u8
        )
    }
}
