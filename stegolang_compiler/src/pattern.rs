use std::fmt::Display;

use crate::{ast::literal::Literal, config, sourceregion::SourceRegion, StringType};

/// describes how to destructure/match a value
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) struct StegoPattern {
    pub(crate) kind: StegoPatternKind,
    pub(crate) source_region: SourceRegion,
}

impl Display for StegoPattern {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.kind.fmt(f)
    }
}

/// describes how to destructure
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) enum StegoPatternKind {
    Capture(StringType),
    Literal(Literal),
    Or(Vec<StegoPatternKind>),
    Named(StringType, Box<StegoPattern>),
}

impl Display for StegoPatternKind {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            StegoPatternKind::Capture(name) => name.fmt(f),
            StegoPatternKind::Literal(x) => x.fmt(f),
            StegoPatternKind::Or(v) => {
                for (i, pat) in v.iter().enumerate() {
                    if i != 0 {
                        " | ".fmt(f)?;
                    }
                    pat.fmt(f)?;
                }
                Ok(())
            }
            StegoPatternKind::Named(name, pat) => {
                name.fmt(f)?;
                write!(f, " {} ", config::NAMED_PATTERN_OP)?;
                pat.fmt(f)
            }
        }
    }
}
