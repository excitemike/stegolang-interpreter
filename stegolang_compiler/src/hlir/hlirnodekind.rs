use super::HlirNode;
use crate::{
    ast::{literal::Literal, DictExprElem, IfArm, Operator, SetExprElem},
    config,
    pattern::StegoPattern,
    ti::{ITy, Substitutions, UnificationVarId},
    StringType,
};
use std::{cell::RefCell, collections::BTreeSet, rc::Rc};

/// data specific to the kind of node
#[derive(Clone, PartialEq, Eq, PartialOrd, Ord)]
pub(crate) enum HlirNodeKind<'ctx> {
    ApplyExpr {
        func: Box<HlirNode<'ctx>>,
        args: Vec<HlirNode<'ctx>>,
    },
    AssignStmt {
        lhs: Box<HlirNode<'ctx>>,
        rhs: Box<HlirNode<'ctx>>,
    },
    BitwiseNot(Box<HlirNode<'ctx>>),
    BinOpExpr {
        first: Box<HlirNode<'ctx>>,
        rest: Vec<(Operator, HlirNode<'ctx>)>,
    },
    ComparisonOpExpr {
        first: Box<HlirNode<'ctx>>,
        rest: Vec<(Operator, HlirNode<'ctx>)>,
    },
    DictExpr(Vec<DictExprElem<HlirNode<'ctx>>>),
    DoExpr(Box<HlirNode<'ctx>>),
    DotExpr {
        lhs: Box<HlirNode<'ctx>>,
        method_name: StringType,
    },
    FnStmt(super::fnstmt::FnStmt<'ctx>),
    Identifier {
        name: StringType,
        substitutions: Box<[(UnificationVarId, ITy<'ctx>)]>,
    },
    IfExpr {
        arms: Vec<IfArm<HlirNode<'ctx>>>,
        catch_all_else: Option<Box<HlirNode<'ctx>>>,
    },
    InExpr {
        container: Box<HlirNode<'ctx>>,
        invert: bool,
        item: Box<HlirNode<'ctx>>,
    },
    Lambda {
        body: Rc<RefCell<HlirNode<'ctx>>>,
        captures: BTreeSet<StringType>,
        params: Rc<RefCell<Vec<StegoPattern>>>,
        placeholder_name: Option<StringType>,
    },
    Let(HlirLetData<'ctx>),
    LiteralValue(Literal),
    LogicalNot(Box<HlirNode<'ctx>>),
    NamedExpr {
        name: StringType,
        value: Box<HlirNode<'ctx>>,
    },
    ReturnStmt(Option<Box<HlirNode<'ctx>>>),
    SetExpr(Vec<SetExprElem<HlirNode<'ctx>>>),
    Statements {
        statements: Vec<HlirNode<'ctx>>,
    },
    Tuple(Vec<HlirNode<'ctx>>),
    TypeOf(Box<HlirNode<'ctx>>),
    UnaryMinus(Box<HlirNode<'ctx>>),
    UnaryPlus(Box<HlirNode<'ctx>>),
    UnitType,
}

impl<'ctx> std::fmt::Debug for HlirNodeKind<'ctx> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            HlirNodeKind::ApplyExpr { .. } => write!(f, "ApplyExpr"),
            HlirNodeKind::AssignStmt { .. } => write!(f, "AssignStmt"),
            HlirNodeKind::BitwiseNot(..) => write!(f, "BitwiseNot"),
            HlirNodeKind::BinOpExpr { .. } => write!(f, "BinOpExpr"),
            HlirNodeKind::ComparisonOpExpr { .. } => write!(f, "ComparisonOpExpr"),
            HlirNodeKind::DictExpr(_) => write!(f, "DictExpr"),
            HlirNodeKind::DoExpr(_) => write!(f, "DoExpr"),
            HlirNodeKind::DotExpr { .. } => write!(f, "DotExpr"),
            HlirNodeKind::FnStmt(_) => write!(f, "FnStmt"),
            HlirNodeKind::Identifier { .. } => write!(f, "Identifier"),
            HlirNodeKind::IfExpr { .. } => write!(f, "IfExpr"),
            HlirNodeKind::InExpr { .. } => write!(f, "InExpr"),
            HlirNodeKind::Lambda { .. } => write!(f, "Lambda"),
            HlirNodeKind::Let { .. } => write!(f, "Let"),
            HlirNodeKind::LiteralValue(_) => write!(f, "LiteralValue"),
            HlirNodeKind::LogicalNot(_) => write!(f, "LogicalNot"),
            HlirNodeKind::NamedExpr { .. } => write!(f, "NamedExpr"),
            HlirNodeKind::ReturnStmt(_) => write!(f, "ReturnStmt"),
            HlirNodeKind::SetExpr(_) => write!(f, "SetExpr"),
            HlirNodeKind::Statements { .. } => write!(f, "Statements"),
            HlirNodeKind::Tuple(_) => write!(f, "Tuple"),
            HlirNodeKind::TypeOf(_) => write!(f, "TypeOf"),
            HlirNodeKind::UnaryMinus(_) => write!(f, "UnaryMinus"),
            HlirNodeKind::UnaryPlus(_) => write!(f, "UnaryPlus"),
            HlirNodeKind::UnitType => write!(f, "{}", config::NOTHING_VALUE),
        }
    }
}

impl<'ctx> From<Literal> for HlirNodeKind<'ctx> {
    fn from(l: Literal) -> Self {
        HlirNodeKind::LiteralValue(l)
    }
}

/// data for the "Let" `HlirNodeKind` variant
#[derive(Clone)]
pub(crate) struct HlirLetData<'a> {
    pub(crate) pattern: Box<StegoPattern>,
    pub(crate) value_expr: Box<HlirNode<'a>>,
    /// the substitutions used when generalizing the value
    pub(crate) substitutions: Substitutions<'a>,
}

impl<'a> PartialEq for HlirLetData<'a> {
    fn eq(&self, other: &Self) -> bool {
        (self.pattern == other.pattern) && (self.value_expr == other.value_expr)
    }
}
impl<'a> Eq for HlirLetData<'a> {}
impl<'a> PartialOrd for HlirLetData<'a> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}
impl<'a> Ord for HlirLetData<'a> {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        match self.pattern.cmp(&other.pattern) {
            core::cmp::Ordering::Equal => {}
            ord => return ord,
        }
        self.value_expr.cmp(&other.value_expr)
    }
}
impl<'a> std::hash::Hash for HlirNodeKind<'a> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        // doing just the disciminant instead of the whole tree
        // this would be bad if we were using HlirNodeKind as a
        // hashmap key, but we don't do that
        core::mem::discriminant(self).hash(state);
    }
}
