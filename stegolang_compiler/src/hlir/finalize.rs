use itertools::Itertools;

use crate::sourceregion::SourceRegion;
use crate::ti::traitstruct::{TraitScheme, TraitStruct};
use crate::ti::visitor::Visitor;
use crate::ti::{Context, FnType, ITy, Scheme, TiError, TiResult, Type, UnificationVarId, Variant};
use crate::{loc, StringType};

use super::hlirnodekind::{HlirLetData, HlirNodeKind};
use super::HlirNode;

pub(super) struct TypeFinalizer<'finalizer, 'ctx> {
    ctx: &'finalizer Context<'ctx>,
    source_region: &'finalizer SourceRegion,
    ignore_stack: Vec<Box<[UnificationVarId]>>,
}

impl<'finalizer, 'ctx> TypeFinalizer<'finalizer, 'ctx> {
    pub(super) fn new(
        ctx: &'finalizer Context<'ctx>,
        source_region: &'finalizer SourceRegion,
    ) -> Self {
        TypeFinalizer {
            ctx,
            source_region,
            ignore_stack: Vec::default(),
        }
    }

    /// check whether a var has already been ignored
    fn is_ignored(&self, ty: ITy<'ctx>) -> bool {
        for ignores in self.ignore_stack.iter().rev() {
            if let Type::TypeParameter { uniq_id, .. } | Type::UnificationVar { uniq_id, .. } =
                ty.inner()
            {
                if ignores.contains(uniq_id) {
                    return true;
                }
            }
        }
        false
    }

    /// remove the last set of types to ignore
    fn pop_ignore(&mut self) {
        self.ignore_stack.pop();
    }

    /// add a new set of types to ignore
    fn push_ignore(&mut self, ignore_list: &[UnificationVarId]) {
        self.ignore_stack
            .push(ignore_list.to_owned().into_boxed_slice());
    }

    /// helper for `visit_type`
    fn visit_dict(&mut self, key: ITy<'ctx>, value: ITy<'ctx>) -> TiResult<Option<ITy<'ctx>>> {
        let updated_key = self.visit_type(key)?;
        let updated_value = self.visit_type(value)?;
        if updated_key.is_none() && updated_value.is_none() {
            Ok(None)
        } else {
            Ok(Some(self.ctx.mk_dict(
                updated_key.unwrap_or(key),
                updated_value.unwrap_or(value),
            )))
        }
    }

    /// helper for `visit_type`
    fn visit_fn(&mut self, params: &[ITy<'ctx>], result: ITy<'ctx>) -> TiResult<Option<ITy<'ctx>>> {
        let updated_params = self.visit_slice(params)?;
        let updated_result = self.visit_type(result)?;
        if updated_params.is_none() && updated_result.is_none() {
            return Ok(None);
        }
        let updated_params = updated_params.map_or_else(
            || params.to_owned().into_boxed_slice(),
            Vec::into_boxed_slice,
        );
        let updated_result = updated_result.unwrap_or(result);
        Ok(Some(self.ctx.mk_fn_type(updated_params, updated_result)))
    }

    /// helper for `visit_type`
    fn visit_scheme(
        &mut self,
        type_params: &[UnificationVarId],
        inner: ITy<'ctx>,
    ) -> TiResult<Option<ITy<'ctx>>> {
        let ignore_list = type_params.iter().copied().collect_vec();
        while_ignoring(self, &ignore_list, |f| f.visit_type(inner))
    }

    /// helper for `visit_type`
    fn visit_set(&mut self, element_ty: ITy<'ctx>) -> TiResult<Option<ITy<'ctx>>> {
        Ok(self.visit_type(element_ty)?.map(|ty| self.ctx.mk_set(ty)))
    }

    /// visit each element of a slice
    fn visit_slice(&mut self, slice: &[ITy<'ctx>]) -> TiResult<Option<Vec<ITy<'ctx>>>> {
        let mut iter = slice.iter().enumerate();
        let mut result;
        // don't clone until we hit one that changes
        while let Some((i, x)) = iter.next() {
            if let Some(x) = self.visit_type(*x)? {
                // found one. copy previous ones into new Vec
                result = slice.iter().take(i).copied().collect_vec();
                // add the current one
                result.push(x);
                // continue for the rest of iter
                for (_, x) in iter {
                    result.push(self.visit_type(*x)?.unwrap_or(*x));
                }
                return Ok(Some(result));
            }
        }
        Ok(None)
    }

    /// helper for `visit_type`
    fn visit_tuple(&mut self, element_tys: &[ITy<'ctx>]) -> TiResult<Option<ITy<'ctx>>> {
        Ok(self
            .visit_slice(element_tys)?
            .map(|element_tys| self.ctx.mk_tuple(element_tys)))
    }

    /// helper for `visit_type`
    fn visit_typetype(&mut self, inner: ITy<'ctx>) -> TiResult<Option<ITy<'ctx>>> {
        Ok(self
            .visit_type(inner)?
            .map(|inner| self.ctx.mk_typetype(inner)))
    }

    /// helper for `visit_type`
    fn visit_union(&mut self, variants: &[ITy<'ctx>]) -> TiResult<Option<ITy<'ctx>>> {
        Ok(self
            .visit_slice(variants)?
            .map(|updated_variants| self.ctx.mk_union(updated_variants)))
    }

    /// helper for `visit_type`
    #[allow(clippy::unnecessary_wraps)]
    fn visit_unification_var(
        &mut self,
        _ty: ITy<'ctx>,
        source_region: &SourceRegion,
    ) -> TiResult<Option<ITy<'ctx>>> {
        self.ctx.warn(
            loc::err::defaulting_unresolved_type(),
            source_region.clone(),
        );
        Ok(None)
    }
    /// helper for `visit_type`
    fn visit_vector(&mut self, element_ty: ITy<'ctx>) -> TiResult<Option<ITy<'ctx>>> {
        Ok(self
            .visit_type(element_ty)?
            .map(|element_type| self.ctx.mk_vector(element_type)))
    }
    /// helper for `visit_type`
    fn visit_variant(
        &mut self,
        name: &StringType,
        variant_type: ITy<'ctx>,
    ) -> TiResult<Option<ITy<'ctx>>> {
        Ok(self
            .visit_type(variant_type)?
            .map(|variant_type| self.ctx.mk_variant(name, variant_type)))
    }
}

/// perform some finalizing while ignoring some type vars
fn while_ignoring<'finalizer, 'ctx, F, R>(
    finalizer: &mut TypeFinalizer<'finalizer, 'ctx>,
    ignore_list: &[UnificationVarId],
    f: F,
) -> R
where
    F: FnOnce(&mut TypeFinalizer<'finalizer, 'ctx>) -> R,
{
    if ignore_list.is_empty() {
        return f(finalizer);
    }
    finalizer.push_ignore(ignore_list);
    let result = f(finalizer);
    finalizer.pop_ignore();
    result
}

impl<'finalizer, 'ctx> Visitor<'ctx> for TypeFinalizer<'finalizer, 'ctx> {
    fn visit_pattern(&mut self, _pat: &crate::pattern::StegoPattern) -> TiResult<bool> {
        Ok(false)
    }

    fn visit_type(&mut self, ty: ITy<'ctx>) -> TiResult<Option<ITy<'ctx>>> {
        if self.is_ignored(ty) {
            return Ok(None);
        }
        match ty.inner() {
            Type::BigInt
            | Type::Bool
            | Type::Char
            | Type::Float64
            | Type::Int64
            | Type::Nothing
            | Type::String => Ok(None),
            Type::Dict { key, value } => self.visit_dict(*key, *value),
            Type::ElementOf(_, _) => todo!(),
            Type::F(FnType { params, result }) => self.visit_fn(params, *result),
            Type::Implementation(_) => todo!(),
            Type::Set(ty) => self.visit_set(*ty),
            Type::Struct(_) => todo!(),
            Type::TraitScheme(TraitScheme { trait_name, .. })
            | Type::TraitStruct(TraitStruct { trait_name, .. }) => {
                let err_message = loc::err::trait_used_as_value(trait_name);
                Err(TiError::new(err_message, self.source_region.clone()))
            }
            Type::Tuple(v) => self.visit_tuple(v),
            Type::Type(inner) => self.visit_typetype(*inner),
            Type::TypeParameter {
                uniq_id: _,
                source_region: _,
            } => unreachable!(),
            Type::TypeScheme(Scheme {
                ty_pars,
                ty,
                source_region: _,
            }) => self.visit_scheme(ty_pars, *ty),
            Type::Union(variants) => self.visit_union(variants),
            Type::UnificationVar {
                uniq_id: _,
                source_region,
                debug_note: _,
            } => self.visit_unification_var(ty, source_region),
            Type::Vector(inner) => self.visit_vector(*inner),
            Type::Variant(Variant { name, variant_type }) => {
                self.visit_variant(name, *variant_type)
            }
        }
    }

    /// visit each node of the tree
    fn visit(&mut self, node: &mut HlirNode<'ctx>) -> TiResult<bool> {
        match &node.kind {
            // fn statements always produce nothing, so it's already
            // monomorphized. It's when we pull the fn back out of the
            // environment that we instantiate and have to be able to resolve
            // things
            HlirNodeKind::FnStmt(_) => Ok(true),
            // Typeof does not have to finalize it's argument!
            HlirNodeKind::TypeOf(_) => Ok(false),
            // let expressions generalize and therefore should not finalize the
            // bound expression value
            HlirNodeKind::Let(HlirLetData {
                pattern,
                value_expr: _,
                substitutions: _,
            }) => self.visit_pattern(pattern),
            _ => self.default_visit(node),
        }
    }
}
