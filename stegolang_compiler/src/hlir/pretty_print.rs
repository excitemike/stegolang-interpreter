use itertools::Itertools;

use crate::{
    ast::{DictExprElem, IfArm, Operator, SetExprElem},
    hlir::{fnstmt::FnStmt, hlirnodekind::HlirLetData},
    pattern::StegoPattern,
    ti::ITy,
};

use super::HlirNode;

const INDENT_SIZE: usize = 4;

/// helper for pretty printing `HlirNode` trees
pub(super) fn pretty_print(
    f: &mut std::fmt::Formatter<'_>,
    node: &HlirNode<'_>,
) -> std::fmt::Result {
    pretty_print_(f, node, 0)
}

/// helper for pretty printing `HlirNode` trees
fn pretty_print_(
    f: &mut std::fmt::Formatter<'_>,
    node: &HlirNode<'_>,
    indent_level: usize,
) -> std::fmt::Result {
    use super::hlirnodekind::HlirNodeKind;
    let HlirNode {
        inferred_type,
        source_region: _,
        kind,
    } = node;
    match kind {
        HlirNodeKind::ApplyExpr { func, args } => {
            pretty_print_apply(f, indent_level, *inferred_type, func, args)
        }
        HlirNodeKind::AssignStmt { lhs, rhs } => {
            pretty_print_assign(f, indent_level, *inferred_type, lhs, rhs)
        }
        HlirNodeKind::BitwiseNot(value) => {
            pretty_print_unary(f, indent_level, *inferred_type, "BitwiseNot", value)
        }
        HlirNodeKind::BinOpExpr { first, rest } => {
            pretty_print_binop(f, indent_level, *inferred_type, first, rest)
        }
        HlirNodeKind::ComparisonOpExpr { first, rest } => {
            pretty_print_compop(f, indent_level, *inferred_type, first, rest)
        }
        HlirNodeKind::DictExpr(elems) => {
            pretty_print_dict_expr(f, indent_level, *inferred_type, elems)
        }
        HlirNodeKind::DoExpr(inner) => {
            pretty_print_unary(f, indent_level, *inferred_type, "Do", inner)
        }
        HlirNodeKind::DotExpr { lhs, method_name } => {
            pretty_print_dot(f, indent_level, *inferred_type, lhs, method_name)
        }
        HlirNodeKind::FnStmt(fn_stmt) => {
            pretty_print_fn_stmt(f, indent_level, *inferred_type, fn_stmt)
        }
        HlirNodeKind::Identifier {
            name,
            substitutions: _,
        } => pretty_print_identifier(f, indent_level, *inferred_type, name),
        HlirNodeKind::IfExpr {
            arms,
            catch_all_else,
        } => pretty_print_if(f, indent_level, *inferred_type, arms, catch_all_else),
        HlirNodeKind::InExpr {
            container,
            invert,
            item,
        } => pretty_print_in(f, indent_level, *inferred_type, container, *invert, item),
        HlirNodeKind::Lambda {
            body,
            captures: _,
            params,
            placeholder_name: _,
        } => {
            let body = body.borrow();
            let params = params.borrow();
            pretty_print_lambda(f, indent_level, *inferred_type, &body, &params)
        }
        HlirNodeKind::Let(HlirLetData {
            pattern,
            value_expr,
            substitutions: _,
        }) => pretty_print_let(f, indent_level, *inferred_type, pattern, value_expr),
        HlirNodeKind::LiteralValue(literal) => {
            pretty_print_literal(f, indent_level, *inferred_type, literal)
        }
        HlirNodeKind::LogicalNot(value) => {
            pretty_print_unary(f, indent_level, *inferred_type, "LogicalNot", value)
        }
        HlirNodeKind::NamedExpr { name, value } => {
            pretty_print_named(f, indent_level, *inferred_type, name, value)
        }
        HlirNodeKind::ReturnStmt(value) => {
            pretty_print_return(f, indent_level, *inferred_type, value)
        }
        HlirNodeKind::SetExpr(elems) => {
            pretty_print_set_expr(f, indent_level, *inferred_type, elems)
        }
        HlirNodeKind::Statements { statements } => {
            pretty_print_statements(f, indent_level, *inferred_type, statements)
        }
        HlirNodeKind::Tuple(elems) => pretty_print_tuple(f, indent_level, *inferred_type, elems),
        HlirNodeKind::TypeOf(value) => {
            pretty_print_unary(f, indent_level, *inferred_type, "TypeOf", value)
        }
        HlirNodeKind::UnaryMinus(value) => {
            pretty_print_unary(f, indent_level, *inferred_type, "UnaryMinus", value)
        }
        HlirNodeKind::UnaryPlus(value) => {
            pretty_print_unary(f, indent_level, *inferred_type, "UnaryPlus", value)
        }
        HlirNodeKind::UnitType => writeln!(f, "{:>indent_level$}()", ""),
    }
}

fn pretty_print_apply(
    f: &mut std::fmt::Formatter<'_>,
    indent_level: usize,
    ty: ITy<'_>,
    func: &HlirNode<'_>,
    args: &[HlirNode<'_>],
) -> std::fmt::Result {
    writeln!(f, "{:>indent_level$}Apply {{", "")?;
    {
        let indent_level = indent_level + INDENT_SIZE;
        pretty_print_(f, func, indent_level)?;
        for (i, node) in args.iter().enumerate() {
            writeln!(f, "{:>indent_level$}arg{i}:", "")?;
            let indent_level = indent_level + INDENT_SIZE;
            pretty_print_(f, node, indent_level)?;
        }
    }
    writeln!(f, "{:>indent_level$}}} : {ty}", "")
}

fn pretty_print_assign(
    f: &mut std::fmt::Formatter<'_>,
    indent_level: usize,
    ty: ITy<'_>,
    lhs: &HlirNode<'_>,
    rhs: &HlirNode<'_>,
) -> std::fmt::Result {
    writeln!(f, "{:>indent_level$}Assign {{", "")?;
    {
        let indent_level = indent_level + INDENT_SIZE;
        pretty_print_(f, lhs, indent_level)?;
    }
    writeln!(f, "{:>indent_level$}}} = {{", "")?;
    {
        let indent_level = indent_level + INDENT_SIZE;
        pretty_print_(f, rhs, indent_level)?;
    }
    writeln!(f, "{:>indent_level$}}} : {ty}", "")
}

fn pretty_print_binop(
    f: &mut std::fmt::Formatter<'_>,
    indent_level: usize,
    ty: ITy<'_>,
    first: &HlirNode<'_>,
    rest: &[(Operator, HlirNode<'_>)],
) -> Result<(), std::fmt::Error> {
    writeln!(f, "{:>indent_level$}BinOp {{", "")?;
    {
        let indent_level = indent_level + INDENT_SIZE;
        pretty_print_(f, first, indent_level)?;
        for (op, rhs) in rest {
            writeln!(f, "{:>indent_level$}{op:?}", "")?;
            pretty_print_(f, rhs, indent_level)?;
        }
    }
    writeln!(f, "{:>indent_level$}}} : {ty}", "")
}

fn pretty_print_compop(
    f: &mut std::fmt::Formatter<'_>,
    indent_level: usize,
    ty: ITy<'_>,
    first: &HlirNode<'_>,
    rest: &[(Operator, HlirNode<'_>)],
) -> Result<(), std::fmt::Error> {
    writeln!(f, "{:>indent_level$}CompOp {{", "")?;
    {
        let indent_level = indent_level + INDENT_SIZE;
        pretty_print_(f, first, indent_level)?;
        for (op, rhs) in rest {
            writeln!(f, "{:>indent_level$}{op:?}", "")?;
            pretty_print_(f, rhs, indent_level)?;
        }
    }
    writeln!(f, "{:>indent_level$}}} : {ty}", "")
}

fn pretty_print_dict_expr(
    f: &mut std::fmt::Formatter<'_>,
    indent_level: usize,
    ty: ITy<'_>,
    elems: &[DictExprElem<HlirNode<'_>>],
) -> Result<(), std::fmt::Error> {
    writeln!(f, "{:>indent_level$}Dict {{", "")?;
    {
        let indent_level = indent_level + INDENT_SIZE;
        for elem in elems {
            match elem {
                DictExprElem::KeywordSpread(node) => {
                    writeln!(f, "{:>indent_level$}KwSpread {{", "")?;
                    {
                        let indent_level = indent_level + INDENT_SIZE;
                        pretty_print_(f, node, indent_level)?;
                    }
                    writeln!(f, "{:>indent_level$}}}", "")?;
                }
                DictExprElem::KeyValue(k, v) => {
                    pretty_print_(f, k, indent_level)?;
                    {
                        let indent_level = indent_level + INDENT_SIZE;
                        writeln!(f, "{:>indent_level$}=>", "")?;
                        pretty_print_(f, v, indent_level)?;
                    }
                }
            }
        }
    }
    writeln!(f, "{:>indent_level$}}} : {ty}", "")
}

fn pretty_print_dot(
    f: &mut std::fmt::Formatter<'_>,
    indent_level: usize,
    ty: ITy<'_>,
    lhs: &HlirNode<'_>,
    method_name: &str,
) -> std::fmt::Result {
    writeln!(f, "{:>indent_level$}Dot {{", "")?;
    {
        let indent_level = indent_level + INDENT_SIZE;
        pretty_print_(f, lhs, indent_level)?;
        writeln!(f, "{:>indent_level$}.{method_name}", "")?;
    }
    writeln!(f, "{:>indent_level$}}} : {ty}", "")
}
fn pretty_print_fn_stmt(
    f: &mut std::fmt::Formatter<'_>,
    indent_level: usize,
    _ty: ITy<'_>,
    fn_stmt: &super::fnstmt::FnStmt<'_>,
) -> Result<(), std::fmt::Error> {
    let FnStmt {
        body,
        captures: _,
        fn_ty,
        name,
        params,
        placeholder_needed: _,
    } = fn_stmt;
    writeln!(f, "{:>indent_level$}Fn {name} {{", "")?;
    {
        let indent_level = indent_level + INDENT_SIZE;
        writeln!(f, "{:>indent_level$}: {fn_ty}", "")?;
        writeln!(
            f,
            "{:>indent_level$}({})",
            "",
            params.borrow().iter().join(", ")
        )?;
        pretty_print_(f, &body.borrow(), indent_level)?;
    }
    writeln!(f, "{:>indent_level$}}}", "")
}

fn pretty_print_identifier(
    f: &mut std::fmt::Formatter<'_>,
    indent_level: usize,
    ty: ITy<'_>,
    name: &str,
) -> Result<(), std::fmt::Error> {
    writeln!(f, "{:>indent_level$}Identifier {name} : {ty}", "")
}
fn pretty_print_if(
    f: &mut std::fmt::Formatter<'_>,
    indent_level: usize,
    ty: ITy<'_>,
    arms: &[IfArm<HlirNode<'_>>],
    catch_all_else: &Option<Box<HlirNode<'_>>>,
) -> Result<(), std::fmt::Error> {
    for (i, arm) in arms.iter().enumerate() {
        writeln!(
            f,
            "{:>indent_level$}{}",
            "",
            if i == 0 { "if:" } else { "else if:" }
        )?;
        {
            let indent_level = indent_level + INDENT_SIZE;
            pretty_print_(f, &arm.cond, indent_level)?;
        }
        writeln!(f, "{:>indent_level$}then:", "")?;
        {
            let indent_level = indent_level + INDENT_SIZE;
            pretty_print_(f, &arm.consequent, indent_level)?;
        }
    }
    if let Some(final_else) = catch_all_else {
        writeln!(f, "{:>indent_level$}else:", "")?;
        {
            let indent_level = indent_level + INDENT_SIZE;
            pretty_print_(f, final_else, indent_level)?;
        }
    }
    writeln!(f, "{:>indent_level$}}} : {ty}", "")
}

fn pretty_print_in(
    f: &mut std::fmt::Formatter<'_>,
    indent_level: usize,
    ty: ITy<'_>,
    container: &HlirNode,
    invert: bool,
    item: &HlirNode<'_>,
) -> Result<(), std::fmt::Error> {
    writeln!(f, "{:>indent_level$}InExpr {{", "")?;
    {
        let indent_level = indent_level + INDENT_SIZE;
        pretty_print_(f, container, indent_level)?;
    }
    writeln!(
        f,
        "{:>indent_level$}{}",
        "",
        if invert {
            "does not contain"
        } else {
            "contains"
        }
    )?;
    {
        let indent_level = indent_level + INDENT_SIZE;
        pretty_print_(f, item, indent_level)?;
    }
    writeln!(f, "{:>indent_level$}}} : {ty}", "")
}

fn pretty_print_lambda(
    f: &mut std::fmt::Formatter<'_>,
    indent_level: usize,
    ty: ITy<'_>,
    body: &HlirNode<'_>,
    params: &[StegoPattern],
) -> Result<(), std::fmt::Error> {
    writeln!(f, "{:>indent_level$}Lambda {{", "")?;
    {
        let indent_level = indent_level + INDENT_SIZE;
        writeln!(f, "{:>indent_level$} {}", "", params.iter().join(", "))?;
        pretty_print_(f, body, indent_level)?;
    }
    writeln!(f, "{:>indent_level$}}} : {ty}", "")
}

fn pretty_print_let(
    f: &mut std::fmt::Formatter<'_>,
    indent_level: usize,
    ty: ITy<'_>,
    pattern: &StegoPattern,
    value: &HlirNode<'_>,
) -> Result<(), std::fmt::Error> {
    writeln!(f, "{:>indent_level$}Let {pattern} = {{", "")?;
    {
        let indent_level = indent_level + INDENT_SIZE;
        pretty_print_(f, value, indent_level)?;
    }
    writeln!(f, "{:>indent_level$}}} : {ty}", "")
}

fn pretty_print_literal(
    f: &mut std::fmt::Formatter<'_>,
    indent_level: usize,
    ty: ITy<'_>,
    literal: &crate::ast::literal::Literal,
) -> Result<(), std::fmt::Error> {
    writeln!(f, "{:>indent_level$}{literal} : {ty}", "")
}

fn pretty_print_named(
    f: &mut std::fmt::Formatter<'_>,
    indent_level: usize,
    ty: ITy<'_>,
    name: &str,
    value: &HlirNode<'_>,
) -> std::fmt::Result {
    writeln!(f, "{:>indent_level$}{name} = {{", "")?;
    {
        let indent_level = indent_level + INDENT_SIZE;
        pretty_print_(f, value, indent_level)?;
    }
    writeln!(f, "{:>indent_level$}}}: {ty}", "")
}

fn pretty_print_return(
    f: &mut std::fmt::Formatter<'_>,
    indent_level: usize,
    ty: ITy<'_>,
    value: &Option<Box<HlirNode<'_>>>,
) -> std::fmt::Result {
    if let Some(value) = value {
        pretty_print_unary(f, indent_level, ty, "Return", value)
    } else {
        writeln!(f, "{:>indent_level$}Return Nothing : {ty}", "")
    }
}

fn pretty_print_set_expr(
    f: &mut std::fmt::Formatter<'_>,
    indent_level: usize,
    ty: ITy<'_>,
    elems: &[SetExprElem<HlirNode<'_>>],
) -> Result<(), std::fmt::Error> {
    writeln!(f, "{:>indent_level$}Set {{", "")?;
    {
        let indent_level = indent_level + INDENT_SIZE;
        for elem in elems {
            match elem {
                SetExprElem::Spread(node) => {
                    writeln!(f, "{:>indent_level$}Spread {{", "")?;
                    {
                        let indent_level = indent_level + INDENT_SIZE;
                        pretty_print_(f, node, indent_level)?;
                    }
                    writeln!(f, "{:>indent_level$}}}", "")?;
                }
                SetExprElem::Value(v) => {
                    pretty_print_(f, v, indent_level)?;
                }
            }
        }
    }
    writeln!(f, "{:>indent_level$}}} : {ty}", "")
}

fn pretty_print_statements(
    f: &mut std::fmt::Formatter<'_>,
    indent_level: usize,
    ty: ITy<'_>,
    statements: &[HlirNode<'_>],
) -> std::fmt::Result {
    writeln!(f, "{:>indent_level$}Statements {{", "")?;
    {
        let indent_level = indent_level + INDENT_SIZE;
        for node in statements {
            pretty_print_(f, node, indent_level)?;
        }
    }
    writeln!(f, "{:>indent_level$}}} : {ty}", "")
}

fn pretty_print_tuple(
    f: &mut std::fmt::Formatter<'_>,
    indent_level: usize,
    ty: ITy<'_>,
    elems: &[HlirNode<'_>],
) -> std::fmt::Result {
    writeln!(f, "{:>indent_level$}Tuple {{", "")?;
    {
        let indent_level = indent_level + INDENT_SIZE;
        for node in elems {
            pretty_print_(f, node, indent_level)?;
        }
    }
    writeln!(f, "{:>indent_level$}}} : {ty}", "")
}

fn pretty_print_unary(
    f: &mut std::fmt::Formatter<'_>,
    indent_level: usize,
    ty: ITy<'_>,
    label: &str,
    value: &HlirNode<'_>,
) -> std::fmt::Result {
    writeln!(f, "{:>indent_level$}{label} {{", "")?;
    {
        let indent_level = indent_level + INDENT_SIZE;
        pretty_print_(f, value, indent_level)?;
    }
    writeln!(f, "{:>indent_level$}}}: {ty}", "")
}
