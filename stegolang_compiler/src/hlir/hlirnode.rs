use crate::{
    sourceregion::SourceRegion,
    ti::{self, visitor::Visitor, Context, ITy, TiResult},
};

use super::{finalize::TypeFinalizer, hlirnodekind::HlirNodeKind, pretty_print::pretty_print};

/// high level intermediate representation
#[derive(Clone, PartialEq, Eq, PartialOrd, Ord)]
pub(crate) struct HlirNode<'ctx> {
    pub inferred_type: ti::ITy<'ctx>,
    pub source_region: SourceRegion,
    pub kind: HlirNodeKind<'ctx>,
}

impl<'ctx> std::fmt::Debug for HlirNode<'ctx> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if f.alternate() {
            pretty_print(f, self)
        } else {
            write!(f, "(HlirNode {:?} : {})", self.kind, self.inferred_type)
        }
    }
}

impl<'a> std::hash::Hash for HlirNode<'a> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.inferred_type.hash(state);
        self.source_region.hash(state);
        self.kind.hash(state);
    }
}

impl<'ctx> HlirNode<'ctx> {
    #[inline]
    pub(crate) fn get_source_region(&self) -> &SourceRegion {
        &self.source_region
    }

    /// create a typed ast node
    pub(crate) fn new(
        inferred_type: ITy<'ctx>,
        source_region: SourceRegion,
        kind: HlirNodeKind<'ctx>,
    ) -> Self {
        Self {
            inferred_type,
            source_region,
            kind,
        }
    }

    /// make sure all types are fully resolved so that we can be ready to interpret or generate code from this typed syntax tree
    pub(crate) fn finalize_types(
        &mut self,
        ctx: &Context<'ctx>,
        source_region: &SourceRegion,
    ) -> TiResult<()> {
        let mut finalizer = TypeFinalizer::new(ctx, source_region);
        finalizer.visit(self)?;
        Ok(())
    }
}
