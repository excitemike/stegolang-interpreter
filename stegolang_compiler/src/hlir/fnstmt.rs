use super::HlirNode;
use crate::{pattern::StegoPattern, ti::ITy, StringType};
use std::{cell::RefCell, collections::BTreeSet, rc::Rc};

/// data for fn statements
/// If you modify this struct, make sure you update the `PartialOrd` and Ord implementations
#[derive(Clone, Debug)]
pub(crate) struct FnStmt<'ctx> {
    pub(crate) body: Rc<RefCell<HlirNode<'ctx>>>,
    pub(crate) captures: BTreeSet<StringType>,
    pub(crate) fn_ty: ITy<'ctx>,
    pub(crate) name: StringType,
    pub(crate) params: Rc<RefCell<Vec<StegoPattern>>>,
    /// whether a placeholder needs to inserted into context to make recursion possible
    pub(crate) placeholder_needed: bool,
}

impl<'ctx> PartialEq for FnStmt<'ctx> {
    fn eq(&self, other: &Self) -> bool {
        // intentionally ignoring substitutions
        self.body == other.body
            && self.captures == other.captures
            && self.fn_ty == other.fn_ty
            && self.name == other.name
            && self.params == other.params
            && self.placeholder_needed == other.placeholder_needed
    }
}

impl<'ctx> Eq for FnStmt<'ctx> {}

impl<'ctx> PartialOrd for FnStmt<'ctx> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl<'ctx> Ord for FnStmt<'ctx> {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        match self.body.cmp(&other.body) {
            core::cmp::Ordering::Equal => {}
            ord => return ord,
        }
        match self.captures.cmp(&other.captures) {
            core::cmp::Ordering::Equal => {}
            ord => return ord,
        }
        match self.name.cmp(&other.name) {
            core::cmp::Ordering::Equal => {}
            ord => return ord,
        }
        match self.params.cmp(&other.params) {
            core::cmp::Ordering::Equal => {}
            ord => return ord,
        }
        self.placeholder_needed.cmp(&other.placeholder_needed)
        // intentionally ignoring substitutions
    }
}
