use crate::config;
use crate::config::LANGUAGE_NAME;
use std::borrow::Cow;
use std::fmt::Display;

type CowStr = Cow<'static, str>;
pub(crate) type MsgTuple = (CowStr, CowStr);

const WRAP_LIMIT: u32 = 80;

macro_rules! wrap {
    ($($arg:tt)*) => {{
        let res = format!($($arg)*);
        crate::util::wrap_text(&res, crate::loc::WRAP_LIMIT)
    }}
}

macro_rules! def_err_msg {
    (
        $( # [ $($attrs:tt)* ] )*
        $name:ident
        $( ( $( $param_names:ident : $param_types:ty ),* $(,)? ) )?
        $short_msg:literal
        $help_fmt:literal
        $($(,)? $help_args:expr)*
        $(,)?
    ) => {
        def_err_msg!{
            $( # [ $($attrs)* ] )*
            $name
            ( $( $( $param_names : $param_types ),* )? )
            ( $short_msg )
            $help_fmt
            $($help_args)*
        }
    };
    (
        $( # [ $($attrs:tt)* ] )*
        $name:ident
        ( $( $param_names:ident : $param_types:ty ),* $(,)? )
        ( $short_msg:expr )
        $help_fmt:literal
        $($(,)? $help_args:expr)*
        $(,)?
    ) => {
        $( # [ $($attrs)* ] )*
        pub fn $name (
            $( $param_names : $param_types ),*
        ) -> ($crate::loc::CowStr, $crate::loc::CowStr) {
            (
                $short_msg .into(),
                wrap!(
                    $help_fmt,
                    $($help_args),*
                ) .into()
            )
        }
    }
}

/// macro making it a little nicer to fill in our expected_* error messages
macro_rules! fmt_or_else {
    ( $input:expr ; $fmt:literal ; $elseexpr:expr ) => {{
        let result: $crate::loc::CowStr = match $input {
            Some(s) => format!($fmt, s).into(),
            None => $elseexpr.into(),
        };
        result
    }};
}

/// header printed when the REPL starts up
pub fn repl_header() -> String {
    format!(
        "{} interactive prompt. Enter \"{}\" to end.",
        LANGUAGE_NAME,
        config::REPL_EXIT_COMMAND
    )
}

/// used when displaying errors in I/O code
pub fn io_error_label() -> &'static str {
    "internal i/o error"
}

/// printed to prompt you to type in the repl
pub fn repl_prompt() -> &'static str {
    "stegolang>"
}

/// compiler label for errors
pub fn severity_error() -> &'static str {
    "error"
}

/// compiler label for errors
pub fn severity_warning() -> &'static str {
    "warning"
}

/// header printed ahead of errors
pub fn error_header() -> &'static str {
    "\n---- ERROR ---------------------------------------------------------------------"
}

/// header printed ahead of warnings
pub fn warning_header() -> &'static str {
    "\n---- WARNING -------------------------------------------------------------------"
}

/// file path to display in error messages if no file path is given
pub fn default_file_path() -> &'static str {
    "<code>"
}

/// how the repl says goodbye when you exit
pub fn goodbye() -> &'static str {
    "see you later!"
}

pub fn type_panic() -> &'static str {
    "Native function received incorrect types"
}

/// couldn't write to `err_stream`.
pub fn unable_to_write() -> &'static str {
    "Unable to write to output stream!"
}

pub fn implementation_of_x_for_y(
    trait_name: &str,
    type_name: &str,
    file_label: &str,
    line_number: usize,
    column_number: usize,
) -> String {
    format!("(Implementation of trait \"{trait_name}\" for \"{type_name}\" defined at {file_label}:{line_number}:{column_number})")
}

pub fn dict_type_name(key: &str, value: &str) -> String {
    format!("({} {} {})", config::DICT_TYPE_CONSTRUCTOR_NAME, key, value)
}

pub fn set_type_name(type_name: &str) -> String {
    format!("({} {})", config::SET_TYPE_CONSTRUCTOR_NAME, type_name)
}

pub fn vec_type_name(type_name: &str) -> String {
    format!("({} {})", config::VEC_TYPE_CONSTRUCTOR_NAME, type_name)
}

pub(crate) fn lambda_created_at(file: &str, line: usize, column: usize) -> String {
    format!(
        "{} created at {}:{}:{}",
        config::LAMBDA_NAME,
        file,
        line,
        column
    )
}

/// error messages
pub mod err {
    use super::{config, CowStr, MsgTuple, LANGUAGE_NAME};
    use lazy_static::lazy_static;

    const NUMBER_EXAMPLES: &str = "Some examples of numbers I know how to read:\
        \n  floating point: 1.2e-34\
        \n  decimal:        1234\
        \n                  1_234_567\
        \n  octal:          0o777\
        \n  hexadecimal:    0x7e57c0de\
        \n                  0xffff_ffff_ffff_ffff";

    const CHARACTER_EXAMPLES: &str = "Some examples of character literals I know how to read:\
        \n  'a'\
        \n  ' '\
        \n  '£'\
        \n  '\\\\'\
        \n  '\\''\
        \n  '\\x7e'     (hexadecimal 00-7f)\
        \n  '\\u{1f468}' (one to six hexadecimal digits)\
        \n  '🎅'         (multibyte characters are supported, though your terminal might not\
        \n                render them correctly)";

    lazy_static! {
        static ref INTERNAL_BUG_NOTICE: String = format!(
            "This is a bug within {} itself, not something you did wrong!\n\n",
            LANGUAGE_NAME
        );
    }

    // TODO: curly examples

    // TODO: alphabetize these

    /// short message for when the limit on reported errors is reached
    pub fn error_limit_reached() -> String {
        format!(
            "Reach error reporting limit of {}. Suppressing further errors.",
            crate::config::MAX_ERRORS_TO_DISPLAY
        )
    }

    /// short message for when the limit on reported warnings is reached
    pub fn warning_limit_reached() -> String {
        format!(
            "Reach warning reporting limit of {}. Suppressing further warnings.",
            crate::config::MAX_WARNINGS_TO_DISPLAY
        )
    }

    pub fn tab_character() -> (&'static str, &'static str) {
        (
            "Tab character encountered!",
            "Replace tab characters with spaces",
        )
    }

    pub fn unterminated_comment() -> (&'static str, String) {
        (
            "Reached end of input without closing comment!",
            wrap!(
                "A block comment was begun with \"{}\", but the code ended before it was closed with a \"{}\"",
                crate::config::BLOCK_COMMENT_START,
                crate::config::BLOCK_COMMENT_END
            )
        )
    }

    pub fn unterminated_paren() -> MsgTuple {
        (
            format!(
                "Reached end of parenthesized expression without closing \"{}\"!",
                crate::config::CLOSE_PAREN
            )
            .into(),
            wrap!(
                "An \"{0}\" was used in a parenthesized expression without a matching \"{1}\". \
                If there is a \"{1}\" for it on another line, check the indentation.",
                crate::config::OPEN_PAREN,
                crate::config::CLOSE_PAREN
            )
            .into(),
        )
    }

    pub fn unterminated_curly() -> MsgTuple {
        (
            format!(
                "Reached end of curly brace expression without closing \"{}\"!",
                crate::config::CLOSE_CURLY
            )
            .into(),
            wrap!(
                "An \"{0}\" was used in a curly brace expression without a matching \"{1}\". \
                If there is a \"{1}\" for it on another line, check the indentation.",
                crate::config::OPEN_CURLY,
                crate::config::CLOSE_CURLY
            )
            .into(),
        )
    }

    pub fn invalid_character_escape(found: &str) -> (&'static str, String) {
        (
            "Invalid character escape!",
            wrap!(
                "Something was wrong with this character escape sequence.\n\
                What I saw was \"{found}\"\n\n{CHARACTER_EXAMPLES}",
            ),
        )
    }

    pub fn empty_character_literal() -> (&'static str, String) {
        (
            "Empty character literal!",
            wrap!(
                "It looks like you started a character literal here, but there were no \
                characters between the single-quotes ({}). If you instead intended the empty \
                string, try using double quotes ({}).\n\n{}",
                crate::config::CHARACTER_BEGIN,
                crate::config::STRING_BEGIN,
                CHARACTER_EXAMPLES
            ),
        )
    }

    pub fn char_literal_line_break() -> (&'static str, String) {
        (
            "Line break in character literal!",
            wrap!(
                "Line breaks are not allowed inside character literals.\n\n{}",
                CHARACTER_EXAMPLES
            ),
        )
    }

    pub fn fn_pattern_expected(token: Option<&str>) -> (String, String) {
        const EXAMPLE: &str = "\
            If you really would like a function that needs no arguments that you can call \
            for its side effects, try making it accept the unit type.\
            \nEXAMPLE:\
            \n    let count = 0\
            \n    fn foo () =\
            \n        count = count + 1\
            \n    foo ()\
            \n    foo ()\
            n    print count // \"2\"";
        (
            String::from("Pattern expected!"),
            if let Some(token) = token {
                wrap!(
                    "To prevent some confusing situations, {} requires {} statements to have at least one argument. \
                    So after a \"{}\", there should be a variable name or a pattern to destructure the \
                    first argument into.\n\
                    Instead I saw {:?}.\n\n{EXAMPLE}",
                    crate::config::LANGUAGE_NAME,
                    crate::config::FN_KEYWORD,
                    crate::config::FN_KEYWORD,
                    token
                )
            } else {
                wrap!(
                    "To prevent some confusing situations, {} requires {} statements to have at least one argument. \
                    So after a \"{}\", there should be a variable name or a pattern to destructure the \
                    first argument into.\n\n{EXAMPLE}",
                    crate::config::LANGUAGE_NAME,
                    crate::config::FN_KEYWORD,
                    crate::config::FN_KEYWORD
                )
            },
        )
    }

    pub fn lambda_pattern_expected(token: Option<&str>) -> (String, String) {
        const EXAMPLE: &str = "\
            If you really would like a function that needs no arguments that you can call \
            for its side effects, try making it accept the unit type.\
            \nEXAMPLE:\
            \n    fn twice f =\
            \n        f ()\
            \n        f ()\
            \n    twice (\\ () = println \"hello\") // prints \"hello\" twice";
        (
            String::from("Pattern expected!"),
            if let Some(token) = token {
                wrap!(
                    "To prevent some confusing situations, {} requires {} expressions to have at least one argument. \
                    So after a \"{}\", there should be a variable name or a pattern to destructure the \
                    first argument into.\n\
                    Instead I saw {:?}.\n\n{EXAMPLE}",
                    crate::config::LANGUAGE_NAME,
                    crate::config::LAMBDA_NAME,
                    crate::config::LAMBDA_BEGIN,
                    token
                )
            } else {
                wrap!(
                    "To prevent some confusing situations, {} requires {} expressions to have at least one argument. \
                    So after a \"{}\", there should be a variable name or a pattern to destructure the \
                    first argument into.\n\n{EXAMPLE}",
                    crate::config::LANGUAGE_NAME,
                    crate::config::LAMBDA_NAME,
                    crate::config::LAMBDA_BEGIN
                )
            },
        )
    }

    pub fn let_pattern_expected(token: Option<&str>) -> (String, String) {
        (
            String::from("Pattern expected!"),
            if let Some(token) = token {
                wrap!(
                    "After a \"{}\", there should be a variable name or a pattern to destructure the \
                    expression into.\n\
                    Instead I saw {:?}.",
                    crate::config::LET,
                    token
                )
            } else {
                wrap!(
                    "After a \"{}\", there should be a variable name or a pattern to destructure the \
                    expression into.",
                    crate::config::LET
                )
            },
        )
    }

    pub fn fn_name_expected(token: Option<&str>) -> (String, String) {
        (
            String::from("Name expected!"),
            if let Some(token) = token {
                wrap!(
                    "After a \"{}\", I expected a name for the function being defined.\n\
                    Instead I saw {:?}.",
                    crate::config::FN_KEYWORD,
                    token
                )
            } else {
                wrap!(
                    "After a \"{}\", I expected a name for the function being defined.",
                    crate::config::FN_KEYWORD,
                )
            },
        )
    }

    pub fn let_eq_expected(token: Option<&str>) -> (String, String) {
        (
            format!("\"{}\" expected!", crate::config::LET_EQ),
            if let Some(token) = token {
                wrap!(
                    "There should be a \"{}\" between the name and the value in a {} expression.\n\
                    Instead I saw {:?}.",
                    crate::config::LET_EQ,
                    crate::config::LET,
                    token
                )
            } else {
                wrap!(
                    "There should be a \"{}\" between the name and the value in a {} expression.",
                    crate::config::LET_EQ,
                    crate::config::LET
                )
            },
        )
    }

    pub fn fn_eq_expected(token: Option<&str>) -> (String, String) {
        (
            format!("\"{}\" expected!", crate::config::LET_EQ),
            if let Some(token) = token {
                wrap!(
                    "There should be a \"{}\" between the name and the function body in a {} \
                    expression.\n\
                    Instead I saw {:?}.",
                    crate::config::FN_EQ,
                    crate::config::FN_KEYWORD,
                    token
                )
            } else {
                wrap!(
                    "There should be a \"{}\" between the name and the function body in a {} \
                    expression.",
                    crate::config::LET_EQ,
                    crate::config::FN_KEYWORD
                )
            },
        )
    }

    pub fn at_pattern_subpattern_expected(token: Option<&str>) -> (String, String) {
        (
            String::from("Pattern expected!"),
            if let Some(token) = token {
                wrap!(
                    "It looks like you are using a \"{}\" pattern, but that needs a pattern for the right-hand-side!\n\
                    Instead I saw {:?}.",
                    crate::config::NAMED_PATTERN_OP,
                    token
                )
            } else {
                wrap!(
                    "It looks like you are using a \"{}\" pattern, but that needs a pattern for the right-hand-side!",
                    crate::config::NAMED_PATTERN_OP
                )
            },
        )
    }

    pub fn pattern_after_pipe_expected(token: Option<&str>) -> (String, String) {
        (
            String::from("Pattern expected!"),
            if let Some(token) = token {
                wrap!(
                    "It looks like you are using \"{}\" to combine patterns, \
                    but the pattern on the right-hand-side is missing!\n\
                    Instead I saw {:?}.",
                    crate::config::PATTERN_SEPARATOR,
                    token
                )
            } else {
                wrap!(
                    "It looks like you are using \"{}\" to combine patterns, \
                    but the pattern on the right-hand-side is missing!",
                    crate::config::PATTERN_SEPARATOR
                )
            },
        )
    }

    pub fn expected_expression_for_paren(token: Option<&str>) -> (String, String) {
        (
            String::from("Expression expected!"),
            if let Some(token) = token {
                wrap!(
                    "After the \"{}\" here I expected an expression or a \"{}\".\n\
                    Instead I saw {:?}.",
                    crate::config::OPEN_PAREN,
                    crate::config::CLOSE_PAREN,
                    token
                )
            } else {
                wrap!(
                    "After the \"{}\" here I expected an expression or a \"{}\".",
                    crate::config::OPEN_PAREN,
                    crate::config::CLOSE_PAREN,
                )
            },
        )
    }

    pub fn expected_anything_for_tuple(token: Option<&str>) -> (String, String) {
        (
            wrap!(
                "Expected \"{}\", \"{}\" or an expression!",
                crate::config::CLOSE_PAREN,
                crate::config::SEQUENCE_DELIM
            ),
            if let Some(token) = token {
                wrap!(
                    "This appears to be a tuple, so I expected \"{}\", \"{}\", or an expression.\n\
                    Instead I saw {:?}.",
                    crate::config::CLOSE_PAREN,
                    crate::config::SEQUENCE_DELIM,
                    token
                )
            } else {
                wrap!(
                    "This appears to be a tuple, so I expected \"{}\", \"{}\", or an expression.",
                    crate::config::CLOSE_PAREN,
                    crate::config::SEQUENCE_DELIM,
                )
            },
        )
    }

    pub fn expected_comma_for_tuple(token: Option<&str>) -> (String, String) {
        (
            wrap!(
                "Expected \"{}\" or \"{}\"!",
                crate::config::CLOSE_PAREN,
                crate::config::SEQUENCE_DELIM
            ),
            if let Some(token) = token {
                wrap!(
                    "I saw a \"{}\" followed by an expression, so I expected either an \"{}\" to end \
                    the parenthesized expression, or a \"{}\" between items in a tuple.\n\
                    Instead I saw {:?}.",
                    crate::config::OPEN_PAREN,
                    crate::config::CLOSE_PAREN,
                    crate::config::SEQUENCE_DELIM,
                    token
                )
            } else {
                wrap!(
                    "I saw a \"{}\" followed by an expression, so I expected either an \"{}\" to end \
                    the parenthesized expression, or a \"{}\" between items in a tuple.",
                    crate::config::OPEN_PAREN,
                    crate::config::CLOSE_PAREN,
                    crate::config::SEQUENCE_DELIM,
                )
            },
        )
    }

    pub fn expected_unary_subexpr(token: Option<&str>) -> (String, String) {
        (
            String::from(r"Expression expected!"),
            if let Some(token) = token {
                wrap!(
                    "I saw a unary operator, so I expected it to be followed by an expression.\n\
                    Instead I saw {:?}.",
                    token
                )
            } else {
                wrap!("I saw a unary operator, so I expected it to be followed by an expression.",)
            },
        )
    }

    pub fn expected_rhs(token: Option<&str>) -> (String, String) {
        (
            String::from(r"Right-hand side expected!"),
            if let Some(token) = token {
                wrap!(
                    "I saw a binary operator, so I expected it to be followed by an expression.\n\
                    Instead I saw {:?}.",
                    token
                )
            } else {
                wrap!("I saw a binary operator, so I expected it to be followed by an expression.",)
            },
        )
    }

    pub fn parse_int_error(base: u32) -> (String, String) {
        (
            wrap!("Error parsing base-{} number!", base),
            wrap!(
                "I was trying to parse a base-{0} integer here, but it doesn't appear to be a valid \
                base-{0} number.\n\n{1}",
                base,
                NUMBER_EXAMPLES
            ),
        )
    }

    pub fn from_parse_bigint_error(e: &num::bigint::ParseBigIntError) -> (&'static str, String) {
        (
            "Error parsing integer!",
            wrap!(
                "I was trying to parse an integer, but ran into the following internal error:\n\
                {}",
                e
            ),
        )
    }

    pub fn from_parse_float_error(e: &std::num::ParseFloatError) -> (&'static str, String) {
        (
            "Error parsing float!",
            wrap!(
                "I was trying to parse a floating-point number, but ran into the following internal \
                error:\n\
                {}",
                e
            ),
        )
    }

    def_err_msg! {
        identifier_not_in_scope
        (name: &str)
        "Identifier not found in this scope!"
        "I ran into the identifier \"{}\", \
        but I don't know one by that name in the current scope.\n\n\
        Check that it is spelled correctly (capitalization matters!). If it's \n\
        meant to come from another module, make sure that module is imported.",
        name
    }

    pub fn bad_to_i64(value: &str) -> MsgTuple {
        (
            "Can not convert to integer!".into(),
            wrap!(
                "I needed to convert \"{}\" to an integer here, but I don't know how to do that.",
                value
            )
            .into(),
        )
    }

    pub fn bad_to_f64(value: &str) -> MsgTuple {
        (
            "Can not convert to float!".into(),
            wrap!(
                "I needed to convert \"{}\" to a float here, but I don't know how to do that.",
                value
            )
            .into(),
        )
    }

    def_err_msg! {
        div_by_zero
        "Division by zero!"
        "This code tried to divide by zero, which is not defined for \
        integers! You should probably check that the divisor is non-zero \
        before dividing, but if you really do want to get back a \
        non-finite value, use floats instead and you will get back \
        \"inf\", \"-inf\", or \"nan\"."
    }

    def_err_msg! {
        remainder_by_zero
        "Remainder by zero!"
        "This code would give the remainder after dividing by zero, but \
        division by zero is not defined for integers! You should \
        probably check that the divisor is non zero before using the \
        \"{}\" operator, but if you really do want to get back a \"nan\", \
        use floats instead.",
        crate::config::REMAINDER_OPERATOR
    }

    pub fn named_expr_value_expected(token: Option<&str>) -> (String, String) {
        (
            String::from("Expression expected!"),
            if let Some(token) = token {
                wrap!(
                    "There should be an expression after the \"{}\" in a named expression.\n\
                    Instead I saw {:?}.",
                    crate::config::NAMED_EXPR_OP,
                    token
                )
            } else {
                wrap!(
                    "There should be an expression after the \"{}\" in a named expression expression.",
                    crate::config::NAMED_EXPR_OP
                )
            },
        )
    }

    pub fn unexpected_indent() -> MsgTuple {
        (
            "Unexpected indent!".into(),
            wrap!(
                "This appears to be a new statement in the current block, but \
                it is indented too far for that.",
            )
            .into(),
        )
    }

    pub fn expected_character_literal_end(token: Option<&str>) -> (String, String) {
        (
            format!("Expected \"{}\"!", crate::config::CHARACTER_END),
            if let Some(token) = token {
                wrap!(
                    "I was expecting a character literal to end here with a ({}).\n\
                    Instead I saw {:?}.",
                    crate::config::CHARACTER_END,
                    token
                )
            } else {
                wrap!(
                    "I was expecting a character literal to end here with a ({}).",
                    crate::config::CHARACTER_END
                )
            },
        )
    }

    pub fn string_multiply_too_big() -> MsgTuple {
        (
            "String multiply too big!".into(),
            wrap!("String multiply failed because the string length would be too large").into(),
        )
    }

    pub fn bad_assign_target() -> MsgTuple {
        (
            "Can not assign to this expression".into(),
            wrap!(
                "The left-hand side of an assignment must be something that can \
                be changed. That means a variable (like \"x\"), a destructuring \
                pattern (like \"(a,b)\"), or indexing expression (like \"v[0]\").",
            )
            .into(),
        )
    }

    pub fn expected_expression_for_assignment(token: Option<&str>) -> MsgTuple {
        (
            "Expression expected!".into(),
            if let Some(token) = token {
                wrap!(
                    "After the \"{}\" here I expected an expression for the value to assign.\n\
                    Instead I saw {:?}.",
                    crate::config::ASSIGNMENT_OPERATOR,
                    token
                )
            } else {
                wrap!(
                    "After the \"{}\" here I expected an expression for the value to assign.",
                    crate::config::ASSIGNMENT_OPERATOR
                )
            }
            .into(),
        )
    }

    pub fn expected_end_of_empty_set_literal(token: Option<&str>) -> MsgTuple {
        (
            "Expected end of set literal!".into(),
            if let Some(token) = token {
                wrap!(
                    "After the \"{0}\" here, I expected a \"{1}\" to end the empty set literal. \
                    Instead I saw {2:?}.\n\n\
                    If you didn't intend an empty set literal, try removing the \"{0}\".",
                    crate::config::SEQUENCE_DELIM,
                    crate::config::CLOSE_CURLY,
                    token
                )
            } else {
                wrap!(
                    "After the \"{0}\" here, I expected a \"{1}\" to end the empty set literal. \
                    If you didn't intend an empty set literal, try removing the \"{0}\".",
                    crate::config::SEQUENCE_DELIM,
                    crate::config::CLOSE_CURLY
                )
            }
            .into(),
        )
    }

    pub fn expected_expression_for_dict_or_set_literal(token: Option<&str>) -> MsgTuple {
        (
            "Expression expected!".into(),
            if let Some(token) = token {
                wrap!(
                    "I saw a \"{0}\" so I expected a \"{1}\", a \"{2}\", or an expression.\n\
                    Instead I saw {3:?}.",
                    crate::config::OPEN_CURLY,
                    crate::config::CLOSE_CURLY,
                    crate::config::SEQUENCE_DELIM,
                    token
                )
            } else {
                wrap!(
                    "I saw a \"{0}\" so I expected a \"{1}\", a \"{2}\", or an expression.",
                    crate::config::OPEN_CURLY,
                    crate::config::CLOSE_CURLY,
                    crate::config::SEQUENCE_DELIM,
                )
            }
            .into(),
        )
    }

    pub fn expected_key_expr_for_dict(token: Option<&str>) -> (&'static str, String) {
        (
            "Expression expected!",
            if let Some(token) = token {
                wrap!(
                    "I was reading a dictionary literal, so I expected an expression for the key in a \
                    key-value pair.\n\
                    Instead I saw {:?}.",
                    token
                )
            } else {
                wrap!(
                    "I was reading a dictionary literal, so I expected an expression for the key in a \
                    key-value pair."
                )
            },
        )
    }

    pub fn expected_value_expr_for_dict(token: Option<&str>) -> (&'static str, String) {
        (
            "Expression expected!",
            if let Some(token) = token {
                wrap!(
                    "I saw a \"{}\" so I expected an expression for the value in this key-value pair.\n\
                    Instead I saw {:?}.",
                    crate::config::KEY_VALUE_SEPARATOR,
                    token
                )
            } else {
                wrap!(
                    "I saw a \"{}\" so I expected an expression for the value in this key-value pair.",
                    crate::config::KEY_VALUE_SEPARATOR
                )
            },
        )
    }

    pub fn expected_expr_for_set(token: Option<&str>) -> (&'static str, String) {
        (
            "Expression expected!",
            if let Some(token) = token {
                wrap!(
                    "It looked like I was parsing a set literal, so I expected an expression here.\n\
                    Instead I saw {:?}.",
                    token
                )
            } else {
                wrap!(
                    "It looked like I was parsing a set literal, so I expected an expression here."
                )
            },
        )
    }

    pub fn expected_keyvalue_separator(token: Option<&str>) -> (String, String) {
        (
            format!("Expected \"{}\"!", crate::config::KEY_VALUE_SEPARATOR),
            if let Some(token) = token {
                wrap!(
                    "I was reading a dictionary literal or dictionary comprehension, and after \
                    reading an expression for a key, I expected \"{}\" followed by an expression. \
                    Instead I saw {:?}.",
                    crate::config::KEY_VALUE_SEPARATOR,
                    token
                )
            } else {
                wrap!(
                    "I was reading a dictionary literal or dictionary comprehension, and after \
                    reading an expression for a key, I expected \"{}\" followed by an expression.",
                    crate::config::KEY_VALUE_SEPARATOR
                )
            },
        )
    }

    pub fn expected_kwspread_expr(token: Option<&str>) -> (&'static str, String) {
        (
            "Expression expected!",
            if let Some(token) = token {
                wrap!(
                    "I saw \"{}\", so I expected an expression to follow.\n\
                    Instead I saw {:?}.",
                    crate::config::KWSPREAD_OPERATOR,
                    token
                )
            } else {
                wrap!(
                    "I saw \"{}\", so I expected an expression to follow.",
                    crate::config::KWSPREAD_OPERATOR
                )
            },
        )
    }

    pub fn expected_spread_expr(token: Option<&str>) -> (&'static str, String) {
        (
            "Expression expected!",
            if let Some(token) = token {
                wrap!(
                    "I saw \"{}\", so I expected an expression to follow.\n\
                    Instead I saw {:?}.",
                    crate::config::SPREAD_OPERATOR,
                    token
                )
            } else {
                wrap!(
                    "I saw \"{}\", so I expected an expression to follow.",
                    crate::config::SPREAD_OPERATOR
                )
            },
        )
    }

    pub fn expected_condition_expr(token: Option<&str>) -> (&'static str, String) {
        (
            "Expression expected!",
            if let Some(token) = token {
                wrap!(
                    "I saw \"{}\", so I expected an expression to follow for the condition.\n\
                    Instead I saw {:?}.",
                    crate::config::IF,
                    token
                )
            } else {
                wrap!(
                    "I saw \"{}\", so I expected an expression to follow for the condition.",
                    crate::config::IF
                )
            },
        )
    }

    pub fn expected_then(token: Option<&str>) -> (String, String) {
        (
            format!("Expected \"{}\"!", crate::config::THEN),
            if let Some(token) = token {
                wrap!(
                    "I just read an \"{}\" and an expression for the condition, so I expected to see \"{}\".\n\
                    Instead I saw {:?}.",
                    crate::config::IF,
                    crate::config::THEN,
                    token
                )
            } else {
                wrap!(
                    "I just read an \"{}\" and an expression for the condition, so I expected to see \"{}\".",
                    crate::config::IF,
                    crate::config::THEN
                )
            },
        )
    }

    pub fn expected_consequent(token: Option<&str>) -> (&'static str, String) {
        (
            "Statement(s) expected!",
            if let Some(token) = token {
                wrap!(
                    "After the \"{}\" in an \"{}\" I expected to see a block of statements.\n\
                    Instead I saw {:?}.",
                    crate::config::THEN,
                    crate::config::IF,
                    token
                )
            } else {
                wrap!(
                    "After the \"{}\" in an \"{}\" I expected to see a block of statements.",
                    crate::config::THEN,
                    crate::config::IF
                )
            },
        )
    }

    pub fn expected_fn_body(token: Option<&str>) -> (&'static str, String) {
        (
            "Statement(s) expected!",
            if let Some(token) = token {
                wrap!(
                    "After the \"{}\" in a \"{}\" or a lambda expression, I \
                    expected to see a block of statements.\n\
                    Instead I saw {:?}.",
                    crate::config::FN_EQ,
                    crate::config::FN_KEYWORD,
                    token
                )
            } else {
                wrap!(
                    "After the \"{}\" in a \"{}\" I expected to see a block of statements.",
                    crate::config::FN_EQ,
                    crate::config::FN_KEYWORD
                )
            },
        )
    }

    pub fn expected_expr_for_typeof(token: Option<&str>) -> (&'static str, String) {
        (
            "Expression expected!",
            if let Some(token) = token {
                wrap!(
                    "I saw \"typeof\", so I expected it to be followed by an expression whose type I could report.\nInstead I saw {:?}.",
                    token
                )
            } else {
                wrap!("I saw \"typeof\", so I expected it to be followed by an expression whose type I could report.")
            },
        )
    }

    pub fn no_implicit_decl<T>(name: T) -> (String, String)
    where
        T: AsRef<str>,
    {
        let name_ref = name.as_ref();
        (
            wrap!("No value `{}` in scope", name_ref),
            wrap!(
                "Could not resolve the name `{}`. \
                Try checking for spelling errors (capitalization counts!), a missing `{}`, whether \
                a module needs to be imported, or if you need a `{}` statement",
                name_ref,
                crate::config::LET,
                crate::config::USE
            ),
        )
    }

    pub(crate) fn internal_error_can_not_assign(target: &str) -> (String, String) {
        (
            wrap!("Internal Error: can not assign to {:?}", target),
            String::new(),
        )
    }

    def_err_msg! {
        type_mismatch_assign
        (target_type:&str, value_type:&str)
        "Type mismatch"
        "The assignment here is trying to set something of type \"{}\" to \
        a value of type \"{}\". {} is statically typed, so types must match for \
        an assignment."
        value_type
        target_type
        LANGUAGE_NAME
    }

    // TODO: desperately need a way to add additional context information to this
    // for instance, an if without an else will say you tried to use "Nothing"
    // referring to the absent else block, which is very confusing
    def_err_msg! {
        type_mismatch
        (target_type:&str, value_type:&str)
        "Type mismatch"
        "It looks like you tried to use something of type \"{1}\" where type \
        \"{0}\" was expected."
        target_type
        value_type
    }

    pub fn not_hashable<T>(type_name: T) -> (String, String)
    where
        T: AsRef<str>,
    {
        let type_name_ref = type_name.as_ref();
        (
            wrap!("Type \"{}\" not hashable!", type_name_ref),
            wrap!(
                "It looks like you tried to use a \"{}\" as a dictionary key or in a set. \
                Unfortunately, that type is not hashable.",
                type_name_ref
            ),
        )
    }

    pub fn expected_dictionary_for_kwspread<T>(type_name: T) -> (String, String)
    where
        T: AsRef<str>,
    {
        let type_name_ref = type_name.as_ref();
        (
            wrap!(
                "Expected dictionary for \"{}\" operator!",
                crate::config::KWSPREAD_OPERATOR
            ),
            wrap!(
                "It looks like you tried to use a \"{0}\" to spread a dictionary, but the type I \
                saw was \"{1}\".",
                crate::config::KWSPREAD_OPERATOR,
                type_name_ref
            ),
        )
    }

    pub fn empty_do_block() -> (String, String) {
        (
            wrap!("Empty \"{}\" block!", crate::config::DO),
            wrap!(
                "This \"{}\" block contains no statements. Is there an indentation mistake?",
                crate::config::DO
            ),
        )
    }

    pub fn too_many_args<T, U, V>(name: Option<T>, req_args: U, found_args: V) -> MsgTuple
    where
        T: std::fmt::Display,
        U: std::fmt::Display,
        V: std::fmt::Display,
    {
        (
            "Too many arguments!".into(),
            if let Some(name) = name {
                wrap!(
                    "Function \"{}\" requires {} arguments, but {} were given.",
                    name,
                    req_args,
                    found_args
                )
            } else {
                wrap!(
                    "This function requires {} arguments, but {} were given.",
                    req_args,
                    found_args
                )
            }
            .into(),
        )
    }

    pub fn expected_end_of_statement(token: Option<&str>) -> (String, String) {
        (
            "Expected end of statement".to_owned(),
            if let Some(token) = token {
                wrap!(
                    "This statement seemed complete, but I found \"{}\" following it. \
                    You may have forgotten to start a new line or use a \"{}\" here, or perhaps \
                    this line is indented too far.",
                    token,
                    crate::config::STATEMENT_SEPARATOR
                )
            } else {
                wrap!(
                    "This statement seemed complete, but I found something following it. \
                    You may have forgotten to start a new line or use a \"{}\" here, or perhaps \
                    this line is indented too far.",
                    crate::config::STATEMENT_SEPARATOR
                )
            },
        )
    }

    pub fn unneccessary_statement_separator() -> (String, String) {
        (
            wrap!("Unneccessary \"{}\"!", crate::config::STATEMENT_SEPARATOR),
            wrap!(
                "A \"{}\" is normally used to separate statements, but this one does nothing and can \
                be removed.",
                crate::config::STATEMENT_SEPARATOR
            ),
        )
    }

    pub fn char_literal_unexpected_end_of_input() -> (&'static str, String) {
        (
            "Unexpected end of input!",
            wrap!(
                "A character literal was begun with ({}), but I reached end of input before it was complete.\n\n\
                {}",
                crate::config::CHARACTER_BEGIN,
                CHARACTER_EXAMPLES
            ),
        )
    }

    pub fn string_literal_unexpected_end_of_input(_token: Option<&str>) -> (&'static str, String) {
        (
            "Unexpected end of input!",
            wrap!(
                "A string literal was begun with ({}), but I reached end of input before it was complete.\n\n\
                {}",
                crate::config::STRING_BEGIN,
                CHARACTER_EXAMPLES
            ),
        )
    }

    pub fn invalid_digit(base: u32, c: char) -> (String, String) {
        (
            wrap!("Error parsing base-{} number!", base),
            wrap!(
                "I was trying to parse a base-{0} digit here, but I encountered \"{1}\", which isn't \
                a valid base-{0} digit.\n\n{2}",
                base,
                c,
                NUMBER_EXAMPLES
            ),
        )
    }
    
    pub fn invalid_digit_fractional(base: u32, c: char) -> (String, String) {
        (
            wrap!("Error parsing base-{} number!", base),
            wrap!(
                "I was trying to parse a base-{0} digit here, but I encountered \
                \"{1}\", which isn't a valid base-{0} digit.\
                \n\nIf you instead meant to call a method on a number literal, \
                try adding a space to disambiguate it from a number. For instance, \
                instead of \"10.eq\", you could write \"10 .eq\". \n\n{2}",
                base,
                c,
                NUMBER_EXAMPLES
            ),
        )
    }

    pub fn missing_exponent() -> (&'static str, String) {
        (
            "Missing digits for exponent!",
            wrap!(
                "It looks as though this was intended to be a floating-point literal, but that \
                the digits for the exponent are missing.\n\n{}",
                NUMBER_EXAMPLES
            ),
        )
    }

    pub fn return_outside_function() -> (String, String) {
        (
            wrap!("`{}` used outside function!", crate::config::RETURN),
            wrap!(
                "`{}` is only meaningful inside a function. Try placing this line inside a \
                function or double-checking indentation",
                crate::config::RETURN
            ),
        )
    }

    pub fn arguments_refuted(fn_family_name: &str) -> MsgTuple {
        (
            "No implementation found for arguments".into(),
            wrap!(
                "I attempted to apply the function called (\"{}\") using pattern \
                matching to select an implementation, but the parameter patterns \
                in every implementation refuted at least one of the arguments.\n\n\
                It's good practice idea to always have a catch-all implementation \
                so that this can't happen.",
                fn_family_name
            )
            .into(),
        )
    }

    def_err_msg! {
        cant_find_implementation
        "Can't find implementation"
        "Something went wrong internally to {0} while I was attempting to \
        look up the correct implementation of this function based on \
        pattern matching its parameters with the supplied arguments.\n\n\
        It's probably a bug in {0} itself, not something you did wrong!",
        LANGUAGE_NAME
    }

    def_err_msg! {
        type_db_outofids
        "Type DB problem"
        "While tracking the types in this program, I found there were \
        an incredible number of them to track!\n\n\
        This may be a bug in {0} itself, or you might be doing \
        something very complicated with my type system that I am not \
        ready to handle."
        LANGUAGE_NAME
    }
    def_err_msg! {
        unreachable_fn_impl
        "Unreachable fn implementation"
        "This implementation of the function is unreachable due to previous \
        implementations."
    }
    def_err_msg! {
        unexpected_at
        (_x:Option<&str>)
        "Unexpected character"
        "I did not expect to see \"{0}\" here. If you meant to make a named \
        expression, make sure the \"{0}\" is preceded by a valid pattern."
        crate::config::NAMED_PATTERN_OP
    }
    def_err_msg! {
        not_a_type
        (name:&str, ty:&str)
        "Expected a type"
        "I was trying to look up the type named \"{}\" but what I found by that \
        name was not a type. It was \"{}\""
        name
        ty
    }
    def_err_msg! {
        missing_trait_for_op
        (trait_name:&str)
        "Could not find trait"
        "{}\
        I can see you were trying to use an operator here, but I was unable \
        to find the matching built-in trait \"{}\". Without the trait, I don't \
        know how this operator works with these types."
        *INTERNAL_BUG_NOTICE
        trait_name
    }
    def_err_msg! {
        missing_method_for_op
        (method_name:&str)
        "Could not find method"
        "{}\
        I can see you were trying to use an operator here, but I was unable \
        to find the matching method \"{}\". Without that method, I don't \
        know how this operator works with these types."
        *INTERNAL_BUG_NOTICE
        method_name
    }
    def_err_msg! {
        no_member_for_type
        (type_name:&str, member_name:&str)
        "No member for type"
        "It looks like you were trying to access \"{1}\" in the type \
        \"{0}\", but I couldn't find any such member of that type. \
        If the member is meant to come from a trait, make sure the matching \
        trait is implemented for that type and that the trait is in scope."
        type_name
        member_name
    }
    pub fn lambda_eq_expected(token: Option<&str>) -> (CowStr, CowStr) {
        let what_i_saw = if let Some(token) = token {
            format!(" Instead I saw \"{token}\".")
        } else {
            String::new()
        };
        (
            format!("\"{}\" expected!", crate::config::LAMBDA_SEP).into(),
            wrap!(
                "I saw a \"{}\", so I expected patterns for lambda parameters \
                or a \"{}\" to follow.{}",
                crate::config::LAMBDA_BEGIN,
                crate::config::LAMBDA_SEP,
                &what_i_saw
            )
            .into(),
        )
    }
    def_err_msg! {
        shadowing_fn
        (fname:&str, parameters:&str)
        (format!("Shadowing with \"{}\"", crate::config::FN_KEYWORD))
        "The function created here shadows an existing identifier \"{0}\" in \
        the same scope. I'm considering this an error because this isn't how \
        \"{1}\" is normally used, so I think it is probably a mistake.\n\
        If you really do intend to shadow the existing identifier, you can \
        avoid this error by using a \"{2}\" and a \"{3}\" instead of \"{1}\" \
        like so:\
        \n    {2} {0} {4} {5}{6} {7} ..."
        fname
        crate::config::FN_KEYWORD
        crate::config::LET
        crate::config::LAMBDA_NAME
        crate::config::FN_EQ
        crate::config::LAMBDA_BEGIN
        parameters
        crate::config::LAMBDA_SEP
    }
    def_err_msg! {
        lambda_sep_expected
        (token: Option<&str>)
        (format!("\"{}\" expected!", crate::config::LAMBDA_SEP))
        "There should be a \"{}\" after the \"{}\" and parameters and before the body in a {} \
        expression.{}"
        crate::config::LAMBDA_SEP
        crate::config::LAMBDA_BEGIN
        crate::config::LAMBDA_NAME
        fmt_or_else!(token ; "\nInstead I saw \"{}\"" ; "")
    }
    def_err_msg! {
        let_expr_expected
        (token: Option<&str>)
        "Expression expected!"
        "There should be an expression after the \"{0}\" in a {0} expression.{1}"
        crate::config::LET
        fmt_or_else!(token ; "\nInstead I saw \"{}\"" ; "")
    }

    def_err_msg! {
        not_statement
        (token: Option<&str>)
        "Not a statement"
        "At this point in the program I was expecting either the end of input \
        or a new statement. I saw \"{0}\", but no statements start with that.\n\n\
        You might be missing an identifier or keyword, this line might not be \
        indented far enough, or maybe this \"{0}\" wasn't supposed to be here at all."
        token.unwrap_or("")
    }

    pub(crate) fn ambiguous_member_lookup<I: Iterator>(
        member_name: &str,
        type_name: &str,
        mut candidates: I,
    ) -> MsgTuple
    where
        I::Item: std::fmt::Display,
    {
        ("Ambiguous member lookup".into(), {
            let mut s = String::new();
            s.push_str(&wrap!(
                "This code requires me to look up a member named \"{}\" in \
                    the type \"{}\", but I found multiple possibilities for that.\n\
                    Here are the ones I found:\n",
                member_name,
                type_name
            ));
            for i in 0..4 {
                if let Some(candidate) = candidates.next() {
                    s.push_str(&wrap!(
                        "    Option #{} comes from \"{}\"\n",
                        i + 1,
                        candidate
                    ));
                } else {
                    break;
                }
            }
            if candidates.next().is_some() {
                s.push_str(&wrap!(
                    "    ...and maybe there were more, but I stopped looking there.\n"
                ));
            }
            // TODO: support the `as` keyword so that this is accurate
            s.push_str(&wrap!(
                "You could try specifying the trait in which to find the member \
                by using the \"as\" keyword.\n\
                Example:\
                \nfn add1 x =\
                \n    (x as Add Int64 Int64).add 1"
            ));
            s.into()
        })
    }

    // TODO: this would be much more useful if it named wher those
    // implementations were defined
    pub(crate) fn overlapping_implementations_of_trait<I: Iterator>(
        trait_name: &str,
        type_name: &str,
        mut candidates: I,
    ) -> MsgTuple
    where
        I::Item: std::fmt::Display,
    {
        ("Overlapping implementations of trait".into(), {
            let mut s = String::new();
            s.push_str(&wrap!(
                "I needed to look up the implementation of the trait \"{}\" for \
                    the type \"{}\", but I found multiple possibilities.\n\
                    Here are the ones I found:\n",
                trait_name,
                type_name
            ));
            for i in 0..4 {
                if let Some(candidate) = candidates.next() {
                    s.push_str(&wrap!("    Option #{}: \"{}\"\n", i + 1, candidate));
                } else {
                    break;
                }
            }
            if candidates.next().is_some() {
                s.push_str(&wrap!(
                    "    ...and maybe there were more, but I stopped looking there.\n"
                ));
            }
            s.into()
        })
    }

    def_err_msg! {
        missing_method
        (method_name:&str, trait_name:&str)
        "Could not find method"
        "I saw an implementation of the method \"{}\" in the trait \"{}\", but \
        that trait does not seem to have that method."
        method_name
        trait_name
    }

    def_err_msg! {
        expected_a_trait
        (got:&str)
        "Trait expected"
        "It looks like you were trying to implement a trait here, so I expected \
        this to be a trait. Instead I saw \"{}\"."
        got
    }
    def_err_msg! {
        expected_a_type
        (got:&str)
        "Type expected"
        "I expected to find a type here. Instead I saw \"{}\""
        got
    }
    def_err_msg! {
        too_few_type_parameters
        (expected:usize, got:usize)
        "Too few type arguments"
        "The trait you are using here needs {} type arguments, but only {} were \
        provided."
        expected
        got
    }
    def_err_msg! {
        too_many_type_parameters
        (expected:usize, got:usize)
        "Too many type arguments"
        "The trait you are using here needs only {} type arguments, but {} were \
        provided."
        expected
        got
    }

    def_err_msg! {
        not_function
        (ty:&str)
        "Applying non-function"
        "It appears you are trying to use this as a function and apply it to \
        some arguments, but it seems to be a \"{}\", not a function. Perhaps you \
        are missing an operator or inended to have a new line (or semicolon) after this."
        ty
    }

    def_err_msg! {
        #[allow(dead_code)] // TODO: use or delete
        not_a_tuple
        (nontuple:&str)
        "Not a tuple"
        "While examining the types here, it seems that a \"{}\" was used where \
        a tuple is needed."
        nontuple
    }

    def_err_msg! {
        #[allow(dead_code)] // TODO: use or delete
        bad_tuple_index
        (index:usize, max_index:usize)
        "Bad tuple index"
        "It looks like this program is trying to use element {} on a tuple that \
        isn't large enough for that. This one has values for indices up to {}."
        index
        max_index
    }

    def_err_msg! {
        kwspread_not_a_dictionary
        (found_type:&str)
        "Not a dictionary"
        "I saw the keyword spread operator (\"{}\") so i needed the next thing \
        to be a dictionary. Instead I saw a \"{}\"."
        config::KWSPREAD_OPERATOR
        found_type
    }

    def_err_msg! {
        spread_not_iterable
        (found_type:&str)
        "Spread operator used with non-iterable item"
        "I saw the spread operator (\"{}\") so I needed the next thing \
        to be iterable. Instead I saw a \"{}\"."
        config::SPREAD_OPERATOR
        found_type
    }

    def_err_msg! {
        element_of_noniterable
        (found_type:&str)
        "Type not iterable"
        "While figuring out the types in your program, I found it was trying to \
        use elements of this, which is of type \"{}\", as though it were \
        iterable, but that type doesn't appear to be iterable."
        found_type
    }

    def_err_msg! {
        no_implementation_found
        (trait_name:&str, for_type:&str)
        "No implementation found"
        "No implementation found of trait \"{}\" for type \"{}\"."
        trait_name
        for_type
    }

    def_err_msg! {
        #[allow(dead_code)] // TODO: use or remove
        unresolved_type
        "Unresolved type"
        "In order to compile this code, I need to determine a type for every \
        expression. It turns out that many types are possible in this case, so \
        I don't know what to do!\n\
        This error usually arises when there are unused function arguments or other \
        unused values. To fix it, try removing the unused item, using it, or \
        annotating its type."
    }

    def_err_msg! {
        defaulting_unresolved_type
        "Unresolved type. Defaulting to Nothing"
        "In order to compile this code, I need to determine a type for every \
        expression. It turns out that many types are possible in this case. I am \
        arbitrarily choosing the Nothing type.\n\
        This warning sometimes arises when there are unused function arguments \
        or when attempting to use a polymorphic trait, struct, or function without \
        providing enough type parameters."
    }

    def_err_msg! {
        member_of_unresolved_type
        (type_name: &str, member_name: &str)
        "Unresolved type."
        r#"In order to compile this code, I need to determine a type for every \
        expression. In this case, I see that you are attempting to look up \
        "{member_name}" in a type I couldn't resolve. ({type_name})\n\
        This warning sometimes arises when there are unused function arguments \
        or when attempting to use a polymorphic trait, struct, or function without \
        providing enough type parameters."#
    }

    def_err_msg! {
        trait_used_as_value
        (trait_name: &str)
        "Expected a value"
        "I expected a value here, but I found a trait (\"{}\") instead. Traits \
        cannot be used as values."
        trait_name
    }

    def_err_msg! {
        missing_associated_type
        (trait_name: &str, for_name: &str, assoc_ty_name: &str)
        "Missing Associated Type"
        "This implementation of {trait_name} for {for_name} is missing the \
        associated type {assoc_ty_name}. When implementing a trait, all methods \
        and associated types in the trait must be defined in the implementation. \n\
        Something like: \
        \n    tr SomeTrait\
        \n        type SomeAssocTy // must be defined in the implementation!\
        \n
        \n    impl SomeTrait for Int64\
        \n        type SomeAssocTy = Bool // this is necessary!"
    }

    def_err_msg! {
        missing_type_parameter
        (type_name: &str, type_debug: &str)
        "Type Parameter(s) Needed"
        "When I looked up \"{type_name}\" here I found a type \"{type_debug}\" \
        that requires you use type parameters with it.\n\
        As an example, when you use the Option type, you need to specify what \
        it optionally holds.\
        \n    Bad:\
        \n        impl MyTrait for Option\
        \n            ...\
        \n    Good:\
        \n         impl MyTrait for Option Int64\
        \n             ...\
        \n    Or even:\
        \n         impl MyTrait T for Option T where T \
        \n             ..."
    }

    def_err_msg! {
        unable_to_unify_types
        (a: &str, b: &str)
        "Unable to Unify Types"
        "While inferring types here, I was unable to determine whether this type:\
        \n    {a}\
        \nwas the same as this type:\
        \n    {b}\
        \n\n\
        In order to compile, I need to be determine a type for every value. \
        This sometimes occurs because a parameter, variable, or return type is \
        going unused and is usually most easily fixed by adding type annotations."
        // TODO: example
    }

    def_err_msg! {
        type_used_as_a_value
        (ty: &str)
        "Type Used as a Value"
        "I expected a value here. Instead I found the type {ty}."
    }

    def_err_msg! {
        member_name_expected_after_dot
        (got: Option<&str>)
        "Expected member name after a `.`."
        "The `.` operator is used to access methods, so it should be followed \
        by an identifier that I can look up.{}"
        {
            if let Some(got) = got {
                format!(" Instead I saw \"{got}\".")
            } else {
                String::new()
            }
        }
    }

    /// error that aren't supposed to ever surface to the person programming in stegolang
    pub mod internal {
        use super::INTERNAL_BUG_NOTICE;
        use crate::config::LANGUAGE_NAME;
        use std::fmt::Debug;

        def_err_msg! {
            assoc_type_in_implementation_but_not_trait
            (trait_name:&str, name:&str)
            "Unrecognized associated type"
            "{}\
            The implementation of trait \"{}\" being instantiated here has an \
            associated type \"{}\". However, that associated type does not \
            appear in the trait definition."
            *INTERNAL_BUG_NOTICE
            trait_name
            name
        }

        def_err_msg! {
            unhandled_case
            (case:&impl Debug, file:&str, line:u32, column:u32)
            "Unhandled case"
            "{}\
            I wasn't prepared for the \"{}\" case at location \"{}:{}:{}\"."
            *INTERNAL_BUG_NOTICE
            ,&{let s = format!("{case:?}"); if s.len()<25 { s } else { format!("{s:.22}...")}}
            file
            line
            column
        }

        def_err_msg! {
            cant_monomorphize
            (type_name:&str, file:&str, line:usize, column:usize)
            "Unable to monomorphize"
            "In order to compile this code, I need a monomorphic type here, \
            but some of the types involved are still unresolved. This probably \
            means there is a bug in {0} itself, and I should have produced \
            an error earlier than this.\n\n\
            The polymorphic type is \"{1}\"\n\
            at location \"{2}:{3}:{4}\""
            LANGUAGE_NAME
            type_name
            file
            line
            column
        }

        def_err_msg! {
            op_should_return_bool
            (op:&str, got:&str)
            "Operator should return bool"
            "{}\
            I was trying to evaluate the \"{}\" operator here, but the \
            implementation I found returned \"{}\" instead of a boolean value."
            *INTERNAL_BUG_NOTICE
            op
            got
        }

        def_err_msg! {
            type_error_eval_and_ti_disagree
            (eval:&str, ti:&str)
            "Internal type error"
            "{}\
            As I was evaluating this code, I found that the evaluation result \
            and the type I had earlier inferred are not in agreement. This \
            probably means there is a compiler bug!\n\
            Evaluation produced \"{}\" and earlier type inference had produced \"{}\"."
            *INTERNAL_BUG_NOTICE
            eval
            ti
        }

        def_err_msg! {
            expected_unif_var
            (ty:&str)
            "Internal type error"
            "{}\
            During type inference, I tried to eliminate a type inference \
            variable here, but it wasn't a type variable. It was \"{}\"."
            *INTERNAL_BUG_NOTICE
            ty
        }

        def_err_msg! {
            native_fn_received_wrong_types
            "Native function received incorrect types"
            "{}\
            I found a call to a native function here, but the types that were \
            sent to it were rejected. That probably means I made a mistake during \
            type inference"
            *INTERNAL_BUG_NOTICE
        }

        def_err_msg! {
            native_fn_received_incorrect_types
            "Native function received incorrect types"
            "{}\
            Incorrect types have been sent to a native fn! That means I made \
            some mistake during type inference!"
            *INTERNAL_BUG_NOTICE
        }

        def_err_msg! {
            unifying_type_param
            "Attempt to Unify Type Parameter"
            "{}\
            Please forgive the jargon below, as this message is mostly intended \
            for the developers of {LANGUAGE_NAME}.\n\
            During type inference, I encountered an unreplaced type parameter \
            while attempting to unify two types. This probably means that I \
            skipped instantiating a type scheme somewhere!"
            *INTERNAL_BUG_NOTICE
        }

        def_err_msg! {
            expected_a_type_parameter
            (got:&str)
            "Type parameter expected"
            "I expected to find a type parameter here. Instead I saw \"{got}\"."
        }
    }
}

/// Turn an integer type to an ordinal string. Like "1st" "2nd"
struct Ordinal(usize);
impl Display for Ordinal {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let ones = self.0 % 10usize;
        let tens = (self.0 / 10usize) % 10usize;
        self.0.fmt(f)?;
        match (tens, ones) {
            (0 | 2..=9, 1) => "st".fmt(f),
            (0 | 2..=9, 2) => "nd".fmt(f),
            (0 | 2..=9, 3) => "rd".fmt(f),
            (_, _) => "th".fmt(f),
        }
    }
}

#[test]
fn ordinal_test() {
    let f = |n: usize| format!("{}", Ordinal(n));
    assert_eq!(f(0), "0th");
    assert_eq!(f(1), "1st");
    assert_eq!(f(2), "2nd");
    assert_eq!(f(3), "3rd");
    assert_eq!(f(4), "4th");
    assert_eq!(f(9), "9th");
    assert_eq!(f(10), "10th");
    assert_eq!(f(11), "11th");
    assert_eq!(f(12), "12th");
    assert_eq!(f(13), "13th");
    assert_eq!(f(14), "14th");
    assert_eq!(f(20), "20th");
    assert_eq!(f(21), "21st");
    assert_eq!(f(101), "101st");
    assert_eq!(f(111), "111th");
    assert_eq!(f(111), "111th");
    assert_eq!(f(102), "102nd");
    assert_eq!(f(112), "112th");
    assert_eq!(f(103), "103rd");
    assert_eq!(f(113), "113th");
    assert_eq!(f(104), "104th");
    assert_eq!(f(114), "114th");
    assert_eq!(f(144), "144th");
    assert_eq!(f(211), "211th");
    assert_eq!(f(222), "222nd");
    assert_eq!(f(1_234_561), "1234561st");
}
