use crate::{
    config,
    fntypes::fnfamily::FnFamily,
    loc,
    sourceregion::SourceRegion,
    stegoobject::{FnImpl, StegoObject},
    ti::{
        substitute::ApplySubst,
        traitstruct::{TraitSchemeRef, TraitStructRef},
        Context, ITy, Substitutions, TiError, TiResult,
    },
    util::RcVal,
    StringType,
};
use num::BigInt;
use rustc_hash::{FxHashMap, FxHashSet};
use std::{
    cell::RefCell,
    hash::{Hash, Hasher},
    rc::Rc,
};

/// what can be found in a scope
#[derive(Debug, PartialEq, Eq, Hash)]
pub(crate) struct TypedValue<'ctx> {
    pub(crate) ty: ITy<'ctx>,
    pub(crate) kind: TypedValueKind<'ctx>,
}
impl<'ctx> Clone for TypedValue<'ctx> {
    fn clone(&self) -> Self {
        Self {
            ty: self.ty,
            kind: self.kind.clone(),
        }
    }
}
impl<'ctx> std::fmt::Display for TypedValue<'ctx> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "({}:{})", self.kind, self.ty)
    }
}

/// values that can be found in scope
#[derive(Debug, PartialEq, Eq)]
pub(crate) enum TypedValueKind<'ctx> {
    Nothing,
    PlaceHolder,
    Set(FxHashSet<TypedValue<'ctx>>),
    StegoFn {
        debug_name: StringType,
        fn_impl: FnImpl<'ctx>,
    },
    FnFam(Rc<FnFamily<'ctx>>),
    TraitScheme(TraitSchemeRef<'ctx>),
    TraitStruct(TraitStructRef<'ctx>),
    Type(ITy<'ctx>),
    Val(Rc<StegoObject<'ctx>>),
}

impl<'ctx> Hash for TypedValueKind<'ctx> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        std::mem::discriminant(self).hash(state);
        match self {
            TypedValueKind::Nothing => (),
            TypedValueKind::PlaceHolder
            | TypedValueKind::Set(_)
            | TypedValueKind::TraitScheme(_)
            | TypedValueKind::TraitStruct(_) => unreachable!(),
            TypedValueKind::StegoFn {
                debug_name,
                fn_impl,
            } => {
                debug_name.hash(state);
                fn_impl.hash(state);
            }
            TypedValueKind::FnFam(fn_fam) => fn_fam.hash(state),
            TypedValueKind::Type(x) => x.hash(state),
            TypedValueKind::Val(x) => x.hash(state),
        }
    }
}

impl<'ctx> Clone for TypedValueKind<'ctx> {
    fn clone(&self) -> Self {
        match self {
            Self::Nothing => Self::Nothing,
            Self::PlaceHolder => Self::PlaceHolder,
            Self::Set(x) => Self::Set(x.clone()),
            Self::StegoFn {
                debug_name,
                fn_impl,
            } => Self::StegoFn {
                debug_name: debug_name.clone(),
                fn_impl: fn_impl.clone(),
            },
            Self::FnFam(fn_fam) => Self::FnFam(Rc::clone(fn_fam)),
            Self::TraitScheme(arg0) => Self::TraitScheme(arg0),
            Self::TraitStruct(arg0) => Self::TraitStruct(arg0),
            Self::Type(arg0) => Self::Type(*arg0),
            Self::Val(arg0) => Self::Val(Rc::clone(arg0)),
        }
    }
}
impl<'ctx> std::fmt::Display for TypedValueKind<'ctx> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Nothing => config::NOTHING_VALUE.fmt(f),
            Self::PlaceHolder => unreachable!(),
            Self::Set(hashset) => {
                '{'.fmt(f)?;
                for (i, x) in hashset.iter().enumerate() {
                    if i != 0 {
                        ", ".fmt(f)?;
                    }
                    x.fmt(f)?;
                }
                if hashset.is_empty() {
                    ','.fmt(f)?;
                }
                '}'.fmt(f)
            }
            Self::StegoFn {
                fn_impl,
                debug_name,
            } => write!(f, "({debug_name} {fn_impl})"),
            Self::FnFam(fn_fam) => write!(f, "{fn_fam:?}"),
            Self::TraitScheme(x) => x.fmt(f),
            Self::TraitStruct(x) => x.fmt(f),
            Self::Type(x) => x.fmt(f),
            Self::Val(x) => x.fmt(f),
        }
    }
}

impl<'ctx> TypedValue<'ctx> {
    /// create a typed boolean
    pub(crate) fn bool(ctx: &Context<'ctx>, val: bool) -> TypedValue<'ctx> {
        TypedValue {
            ty: ctx.mk_bool(),
            kind: TypedValueKind::Val(StegoObject::from(val).into()),
        }
    }

    /// hack while I get things running again
    /// TODO: remove
    pub(crate) fn expect_stegoobject(&self) -> RcVal<'ctx> {
        match &self.kind {
            TypedValueKind::Val(obj) => Rc::clone(obj),
            TypedValueKind::Nothing => Rc::new(StegoObject::UnitType),
            _ => panic!("expected TypedValue to contain a StegoObject"),
        }
    }

    /// create a typed value for a function family
    pub(crate) fn fn_fam(ty: ITy<'ctx>, fn_fam: Rc<FnFamily<'ctx>>) -> TypedValue<'ctx> {
        TypedValue {
            ty,
            kind: TypedValueKind::FnFam(fn_fam),
        }
    }

    /// create a typed value for a function
    pub(crate) fn func(
        ty: ITy<'ctx>,
        debug_name: StringType,
        fn_impl: FnImpl<'ctx>,
    ) -> TypedValue<'ctx> {
        TypedValue {
            ty,
            kind: TypedValueKind::StegoFn {
                debug_name,
                fn_impl,
            },
        }
    }

    /// Generalize a type into a polytype - all the free type variables
    /// could be different in the next instantiation.
    ///
    /// # EXAMPLE
    ///
    /// ```no_run
    /// let identity = \x = x  // let expressions generalize
    /// println (identity 1)   // instantiated here so that we can use it with an int
    /// println (identity "a") // instantiated here so that we can use it with a string
    /// ```
    pub(crate) fn generalize(
        &mut self,
        substitutions: &Substitutions<'ctx>,
        generalized_type: ITy<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<()> {
        self.ty = generalized_type;
        if let Some(kind) = self.kind.generalize(substitutions, ctx)? {
            self.kind = kind;
        }
        Ok(())
    }

    /// replace type parameters with types as specified in `substitutions`
    /// returns `Ok(Some(TypeRef))` if a replacement was made, `Ok(None)` if no
    /// replacement was necessary, and `Err(...)` if something goes wrong
    pub(crate) fn instantiate(
        &self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
        source_region: &SourceRegion,
    ) -> TiResult<Option<TypedValue<'ctx>>> {
        match self.ty.instantiate_with(substitutions, ctx)? {
            None => Ok(None),
            Some(ty) => match &self.kind {
                TypedValueKind::TraitScheme(scheme) => {
                    let tr = scheme.instantiate_exactly(substitutions, ctx)?;
                    let ty = ctx.mk_trait(tr);
                    Ok(Some(TypedValue {
                        ty,
                        kind: TypedValueKind::TraitStruct(tr),
                    }))
                }
                TypedValueKind::Type(x) => match x.instantiate(ctx, source_region)? {
                    Some((_, ty)) => Ok(Some(TypedValue {
                        ty,
                        kind: TypedValueKind::Type(ty),
                    })),
                    None => Ok(None),
                },
                _ => {
                    let kind = self
                        .kind
                        .apply_subst(substitutions, ctx)?
                        .unwrap_or_else(|| self.kind.clone());
                    Ok(Some(TypedValue { ty, kind }))
                }
            },
        }
    }

    /// whether you can create a hash from this value
    pub(crate) fn is_hashable(&self) -> bool {
        match &self.kind {
            TypedValueKind::Val(obj) => obj.is_hashable(),
            TypedValueKind::Nothing => true,
            _ => false,
        }
    }

    /// whether you can iterate over this value
    pub(crate) fn is_iterable(&self) -> bool {
        match &self.kind {
            TypedValueKind::Nothing
            | TypedValueKind::StegoFn { .. }
            | TypedValueKind::TraitScheme(_)
            | TypedValueKind::TraitStruct(_)
            | TypedValueKind::Type(_)
            | TypedValueKind::FnFam(_) => false,
            TypedValueKind::PlaceHolder => todo!(),
            TypedValueKind::Set(_) => true,
            TypedValueKind::Val(x) => x.is_iterable(),
        }
    }

    /// whether the value coerces to true
    pub(crate) fn is_truthy(&self) -> bool {
        match &self.kind {
            TypedValueKind::Val(obj) => obj.is_truthy(),
            _ => false,
        }
    }

    /// get an iterator over the object's values
    ///
    /// # PANICS
    ///
    /// Panics if the object is not iterable
    ///
    pub(crate) fn iter(&self) -> TypedValueIterator<'_, 'ctx> {
        match &self.kind {
            TypedValueKind::Val(obj) if obj.is_iterable() => todo!(),
            TypedValueKind::Set(hashset) => TypedValueIterator::HashSetIter(hashset.iter()),
            _ => panic!("type not iterable: {}", self.ty),
        }
    }

    /// toggle between true and false
    pub(crate) fn logical_not(&self, ctx: &Context<'ctx>) -> Option<TypedValue<'ctx>> {
        match self {
            TypedValue {
                kind: TypedValueKind::Val(obj),
                ..
            } => match &**obj {
                StegoObject::False => Some(TypedValue {
                    ty: ctx.mk_bool(),
                    kind: TypedValueKind::Val(StegoObject::from(true).into()),
                }),
                StegoObject::True => Some(TypedValue {
                    ty: ctx.mk_bool(),
                    kind: TypedValueKind::Val(StegoObject::from(false).into()),
                }),
                _ => None,
            },
            _ => None,
        }
    }

    /// access members of the value
    pub(crate) fn member(
        &self,
        name: &StringType,
        ctx: &Context<'ctx>,
        source_region: &SourceRegion,
    ) -> TiResult<TypedValue<'ctx>> {
        // in the case of structs, look directly in the value first, then at traits
        // TODO: implement structs and early return in their case here
        match self.kind {
            TypedValueKind::Nothing
            | TypedValueKind::Set(_)
            | TypedValueKind::StegoFn { .. }
            | TypedValueKind::FnFam(_)
            | TypedValueKind::Type(_)
            | TypedValueKind::Val(_) => (),
            TypedValueKind::PlaceHolder => unreachable!(),
            TypedValueKind::TraitScheme(_) => todo!(),
            TypedValueKind::TraitStruct(_) => todo!(),
        }
        ctx.get_method_for_eval(self.ty, name, source_region)
            .and_then(|value| {
                value.ok_or_else(|| {
                    TiError::new(
                        loc::err::no_member_for_type(&self.to_string(), name),
                        source_region.clone(),
                    )
                })
            })
    }

    /// create a typed value
    pub(crate) fn new(ty: ITy<'ctx>, kind: TypedValueKind<'ctx>) -> TypedValue<'ctx> {
        TypedValue { ty, kind }
    }

    /// create a typed value for Nothing
    pub(crate) fn nothing(ctx: &Context<'ctx>) -> TypedValue<'ctx> {
        TypedValue {
            ty: ctx.mk_nothing(),
            kind: TypedValueKind::Nothing,
        }
    }

    /// create a typed value referring to a Scheme
    pub(crate) fn typetype(ctx: &Context<'ctx>, ty: ITy<'ctx>) -> TypedValue<'ctx> {
        let ty = ctx.mk_typetype(ty);
        TypedValue {
            ty,
            kind: TypedValueKind::Type(ty),
        }
    }

    /// break dependence on the environment
    pub(crate) fn to_owned(&self) -> OwnedValue {
        self.kind.to_owned()
    }

    /// create typed value for a tuple
    pub(crate) fn tuple(ctx: &mut Context<'ctx>, v: Vec<TypedValue<'ctx>>) -> TypedValue<'ctx> {
        let mut types = Vec::with_capacity(v.len());
        let mut values = Vec::with_capacity(v.len());
        for TypedValue { ty, kind } in v {
            types.push(ty);
            match kind {
                TypedValueKind::Val(obj) => values.push(obj),
                TypedValueKind::Nothing => values.push(StegoObject::UnitType.into()),
                _ => todo!("handle error"),
            }
        }
        TypedValue {
            ty: ctx.mk_tuple(types),
            kind: TypedValueKind::Val(StegoObject::Tuple(values).into()),
        }
    }

    /// construct with a Val variant kind
    pub(crate) fn val(ty: ITy<'ctx>, obj: Rc<StegoObject<'ctx>>) -> TypedValue<'ctx> {
        TypedValue {
            ty,
            kind: TypedValueKind::Val(obj),
        }
    }
}

impl<'ctx> TypedValueKind<'ctx> {
    /// Generalize a type into a polytype - all the free type variables
    /// could be different in the next instantiation.
    pub(crate) fn generalize(
        &self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<Option<Self>> {
        match self {
            TypedValueKind::StegoFn {
                debug_name,
                fn_impl,
            } => match &fn_impl.code {
                crate::fntypes::fncode::FnCode::Statements(stmts) => {
                    if let Some(updated) = stmts.borrow().apply_subst(substitutions, ctx)? {
                        let code = crate::fntypes::fncode::FnCode::Statements(Rc::new(
                            RefCell::new(updated),
                        ));
                        let fn_impl = FnImpl {
                            pattern_set: Rc::clone(&fn_impl.pattern_set),
                            captures: fn_impl.captures.clone(),
                            code,
                        };
                        let kind = TypedValueKind::StegoFn {
                            debug_name: debug_name.clone(),
                            fn_impl,
                        };
                        Ok(Some(kind))
                    } else {
                        Ok(None)
                    }
                }
                crate::fntypes::fncode::FnCode::Native(_) => Ok(None),
            },
            TypedValueKind::FnFam(fn_fam) => {
                if let Some(fn_fam) = fn_fam.apply_subst(substitutions, ctx)? {
                    Ok(Some(TypedValueKind::FnFam(fn_fam)))
                } else {
                    Ok(None)
                }
            }
            TypedValueKind::Nothing
            | TypedValueKind::PlaceHolder
            | TypedValueKind::Set(_)
            | TypedValueKind::TraitScheme(_)
            | TypedValueKind::TraitStruct(_)
            | TypedValueKind::Type(_)
            | TypedValueKind::Val(_) => Ok(None),
        }
    }

    /// break dependence on the environment
    pub(crate) fn to_owned(&self) -> OwnedValue {
        match self {
            TypedValueKind::Nothing => OwnedValue::Nothing,
            TypedValueKind::PlaceHolder => OwnedValue::Placeholder,
            TypedValueKind::Set(set) => {
                OwnedValue::Set(set.iter().map(&TypedValue::to_owned).collect())
            }
            TypedValueKind::StegoFn {
                debug_name,
                fn_impl: _,
            } => OwnedValue::FnFamily(debug_name.to_string()),
            TypedValueKind::FnFam(fn_fam) => {
                let FnFamily { debug_name, .. } = &**fn_fam;
                OwnedValue::FnFamily(debug_name.to_string())
            }
            TypedValueKind::TraitScheme(x) => OwnedValue::Trait(x.to_string()),
            TypedValueKind::TraitStruct(x) => OwnedValue::Trait(x.to_string()),
            TypedValueKind::Type(_) => OwnedValue::Type,
            TypedValueKind::Val(x) => (**x).to_owned(),
        }
    }
}

/// iterator returned by `StegoObject::iter`
pub(crate) enum TypedValueIterator<'a, 'ctx> {
    HashSetIter(std::collections::hash_set::Iter<'a, TypedValue<'ctx>>),
}

impl<'ctx> Iterator for TypedValueIterator<'_, 'ctx> {
    type Item = TypedValue<'ctx>;

    fn next(&mut self) -> Option<Self::Item> {
        match self {
            Self::HashSetIter(i) => i.next().cloned(),
        }
    }
}

/// fully owned version of `TypedValueKind`
#[derive(Debug, Clone)]
pub enum OwnedValue {
    Nothing,
    Placeholder,
    Type,
    Bool(bool),
    Int64(i64),
    Float64(f64),
    BigInt(BigInt),
    Char(char),
    String(String),
    Trait(String),
    Tuple(Vec<OwnedValue>),
    FnFamily(String),
    Dict(FxHashMap<OwnedValue, OwnedValue>),
    Set(FxHashSet<OwnedValue>),
}

impl Hash for OwnedValue {
    fn hash<H: Hasher>(&self, state: &mut H) {
        match self {
            OwnedValue::Nothing | OwnedValue::Placeholder | OwnedValue::Type => (),
            OwnedValue::Bool(x) => x.hash(state),
            OwnedValue::Int64(x) => x.hash(state),
            OwnedValue::Float64(x) => (*x).to_bits().hash(state),
            OwnedValue::BigInt(x) => x.hash(state),
            OwnedValue::Char(x) => x.hash(state),
            OwnedValue::String(x) | OwnedValue::FnFamily(x) | OwnedValue::Trait(x) => x.hash(state),
            OwnedValue::Tuple(x) => x.hash(state),
            OwnedValue::Dict(_) | OwnedValue::Set(_) => unreachable!("not hashable"),
        }
    }
}

impl PartialEq for OwnedValue {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Bool(l0), Self::Bool(r0)) => l0 == r0,
            (Self::Int64(l0), Self::Int64(r0)) => l0 == r0,
            (Self::Float64(l0), Self::Float64(r0)) => l0 == r0,
            (Self::BigInt(l0), Self::BigInt(r0)) => l0 == r0,
            (Self::Char(l0), Self::Char(r0)) => l0 == r0,
            (Self::String(l0), Self::String(r0)) | (Self::FnFamily(l0), Self::FnFamily(r0)) => {
                l0 == r0
            }
            (Self::Tuple(l0), Self::Tuple(r0)) => l0 == r0,
            (Self::Dict(l0), Self::Dict(r0)) => l0 == r0,
            (Self::Set(l0), Self::Set(r0)) => l0 == r0,
            _ => core::mem::discriminant(self) == core::mem::discriminant(other),
        }
    }
}

impl Eq for OwnedValue {}

impl std::fmt::Display for OwnedValue {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            OwnedValue::Nothing => "Nothing".fmt(f),
            OwnedValue::Placeholder => "Placeholder".fmt(f),
            OwnedValue::Type => "Type".fmt(f),
            OwnedValue::Bool(x) => x.fmt(f),
            OwnedValue::Int64(x) => x.fmt(f),
            OwnedValue::Float64(x) => x.fmt(f),
            OwnedValue::BigInt(x) => x.fmt(f),
            OwnedValue::Char(x) => x.fmt(f),
            OwnedValue::String(x) => write!(f, "(String {x})"),
            OwnedValue::Trait(x) => write!(f, "(Trait {x})"),
            OwnedValue::Tuple(x) => {
                '('.fmt(f)?;
                let mut i = x.iter();
                if let Some(v) = i.next() {
                    v.fmt(f)?;
                    for v in i {
                        write!(f, ",{v}")?;
                    }
                }
                ')'.fmt(f)
            }
            OwnedValue::FnFamily(x) => write!(f, "(FnFamily {x})"),
            OwnedValue::Dict(map) => {
                '{'.fmt(f)?;
                let mut i = map.iter();
                if let Some((k, v)) = i.next() {
                    k.fmt(f)?;
                    ':'.fmt(f)?;
                    v.fmt(f)?;
                    for (k, v) in i {
                        ','.fmt(f)?;
                        k.fmt(f)?;
                        ':'.fmt(f)?;
                        v.fmt(f)?;
                    }
                }
                '}'.fmt(f)
            }
            OwnedValue::Set(members) => {
                '{'.fmt(f)?;
                let mut i = members.iter();
                if let Some(v) = i.next() {
                    v.fmt(f)?;
                    for v in i {
                        ','.fmt(f)?;
                        v.fmt(f)?;
                    }
                }
                '}'.fmt(f)
            }
        }
    }
}
