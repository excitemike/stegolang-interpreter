//! error type used frequently throughout this crate
use crate::controlflow::err;
use crate::sourceregion::SourceRegion;
use crate::ti::TiError;
use crate::typedvalue::TypedValue;
use crate::{controlflow::ControlFlow, reporting::ReportableError};
use std::borrow::Cow;

/// Types of error the interpreter can report
#[derive(Clone, Copy, Debug)]
#[allow(clippy::module_name_repetitions)]
pub enum ErrorCode {
    ParseError = 0,
    TypeError = 1,
    // TODO: codegen error
    EvaluationError = 2,
    InitializationError = 3,
}

/// result from evaluating
pub(crate) type EvalResult<'ctx> = Result<TypedValue<'ctx>, ControlFlow<'ctx>>;

impl<'ctx> From<TiError> for EvalResult<'ctx> {
    fn from(
        TiError {
            msg_tuple,
            source_region,
        }: TiError,
    ) -> Self {
        err(msg_tuple, source_region)
    }
}

/// error/warning distinction
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Severity {
    Error,
    Warning,
}

/// information about what went wrong and where
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Error {
    pub short_msg: Cow<'static, str>,
    pub help: Cow<'static, str>,
    pub source_region: SourceRegion,
    pub severity: Severity,
}

impl ReportableError for Error {
    /// location at which the error occurred
    #[inline]
    fn loc(&self) -> &SourceRegion {
        &self.source_region
    }
    /// short message to describe the error
    #[inline]
    fn short_msg(&self) -> &str {
        &self.short_msg
    }
    /// longer explanation of the error and how it might be fixed
    #[inline]
    fn help(&self) -> &str {
        &self.help
    }
    /// error severity
    #[inline]
    fn severity(&self) -> Severity {
        self.severity
    }
}
