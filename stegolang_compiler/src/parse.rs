use crate::{
    ast::astnode::AstNode,
    codelines::CodeLines,
    env::Env,
    error::{Error, Severity},
    lang::AdvanceResult,
    loc::MsgTuple,
    sourceregion::SourceRegion,
    ti::TiError,
    StringType,
};
use std::{borrow::Cow, sync::Arc};
use unicode_segmentation::UnicodeSegmentation;

pub(crate) mod findcaptures;

/// build a syntax tree from a program as a string
/// Ok case contains a tuple of the parsed syntax tree (or None) and a Vec of errors
#[cfg(test)]
pub(crate) fn parse<S1, S2>(
    file_label: S1,
    code: S2,
) -> Result<(Option<AstNode>, Vec<Error>), Vec<Error>>
where
    S1: Into<StringType>,
    S2: Into<String>,
{
    Env::enter(&mut std::io::stdout(), &mut std::io::stderr(), |env| {
        parse_with_env(file_label, code, env)
    })
    .unwrap_or_else(|e| Err(e.into()))
}

/// parse using the provided environment
pub(crate) fn parse_with_env<S1, S2>(
    file_label: S1,
    code: S2,
    env: &mut Env,
) -> Result<(Option<AstNode>, Vec<Error>), Vec<Error>>
where
    S1: Into<StringType>,
    S2: Into<String>,
{
    let mut p = Parser::new(file_label, code, env);
    p.skip_bom();
    p.skip_shebang();
    let parsed = p.read_program()?;

    let Parser { warnings, .. } = p;
    Ok((parsed, warnings))
}

/// State of the parser
pub struct Parser<'envborrow, 'env>
where
    'env: 'envborrow,
{
    /// The code being parsed. Held onto for the sake of error messages.
    pub(crate) code: Arc<CodeLines>,

    /// label of the file being parsed
    pub(crate) file_label: StringType,

    /// Warnings we've accumulated as we parse.
    warnings: Vec<Error>,

    /// Current location in the source code.
    pub(crate) line_number: usize,

    /// Current location in the source code.
    pub(crate) column_number: usize,

    /// Current location in the source code.
    pub(crate) byte: usize,

    /// Used to prevent warning about tabs multiple times.
    pub(crate) did_tab_warning: bool,

    /// Track indentation level as we parse.
    indentation_stack: Vec<usize>,

    /// Parser currently needs to know whether it is in a function to know
    /// whether a return statement is allowed.
    /// TODO: That should probably be part of a separate pass over the AST?
    pub(crate) is_inside_function: bool,

    /// environment into which to insert type information
    pub(super) env: &'envborrow mut Env<'env>,
}

impl<'envborrow, 'ctx> Parser<'envborrow, 'ctx> {
    /// rewind to a backup point made with `make_backup_point`
    pub(crate) fn backup(&mut self, to: (usize, usize, usize)) {
        self.byte = to.0;
        self.line_number = to.1;
        self.column_number = to.2;
    }

    /// Begin tracking a region. The start location is set when you call
    /// `begin_source_region`, and the end location is set when you call
    /// the returned closure.
    pub(crate) fn begin_source_region(&self) -> impl FnOnce(&Parser) -> SourceRegion {
        let start_line_number = self.line_number;
        let start_column_number = self.column_number;
        move |parser: &Parser| SourceRegion {
            start_line_number,
            start_column_number,
            end_line_number: parser.get_line_number(),
            end_column_number: parser.get_column_number(),
            file_label: parser.file_label.clone(),
            code: Some(parser.get_code()),
        }
    }

    /// helper for detecting the end of a bracketted expression like parens or
    /// quotes
    pub(crate) fn bracketted_expr_end<F>(
        &mut self,
        close_str: &str,
        error_msg_func: F,
        start_line_number: usize,
        start_column_number: usize,
    ) -> Result<bool, Vec<Error>>
    where
        F: FnOnce() -> MsgTuple,
    {
        match self.skip_comments_and_whitespace()? {
            AdvanceResult::Continue => Ok(self.skip_exact(close_str)),
            AdvanceResult::NewStatement if self.skip_exact(close_str) => Ok(true),
            _ => {
                let (short_msg, help) = error_msg_func();
                self.error(short_msg, help, start_line_number, start_column_number)
            }
        }
    }

    /// whether we've reached the end of input
    pub(crate) fn eof(&self) -> bool {
        self.byte >= self.code.len()
    }

    /// create error rseult
    pub(crate) fn error<T, S1, S2>(
        &self,
        short_msg: S1,
        help: S2,
        start_line_number: usize,
        start_column_number: usize,
    ) -> Result<T, Vec<Error>>
    where
        S1: Into<Cow<'static, str>>,
        S2: Into<Cow<'static, str>>,
    {
        Err(vec![Error {
            short_msg: short_msg.into(),
            help: help.into(),
            source_region: self.make_source_region(start_line_number, start_column_number),
            severity: Severity::Error,
        }])
    }

    /// always fails - use to create an error for a missing item
    pub(crate) fn expected_something<T, S1, S2>(
        &mut self,
        err_msg_fn: fn(Option<&str>) -> (S1, S2),
    ) -> Result<T, Vec<Error>>
    where
        S1: Into<Cow<'static, str>>,
        S2: Into<Cow<'static, str>>,
    {
        let token = self
            .code
            .get_by_byte(self.byte..)
            .map(|s| s.split_word_bounds().next());
        if let Some(Some(token_str)) = token {
            let (short_msg, help) = err_msg_fn(Some(token_str));
            self.error(short_msg, help, self.line_number, self.column_number)
        } else {
            let (short_msg, help) = err_msg_fn(None);
            self.error(short_msg, help, self.line_number, self.column_number)
        }
    }

    /// access to `CodeLines`
    pub(crate) fn get_code(&self) -> Arc<CodeLines> {
        Arc::clone(&self.code)
    }

    /// access to code
    pub(crate) fn get_code_by_byte<I: core::slice::SliceIndex<str>>(
        &self,
        i: I,
    ) -> Option<&I::Output> {
        self.code.get_by_byte(i)
    }

    /// one-based column number of where parsing progress is currently at
    #[must_use]
    pub fn get_column_number(&self) -> usize {
        self.column_number
    }

    /// one-based line number of where parsing progress is currently at
    #[must_use]
    pub fn get_line_number(&self) -> usize {
        self.line_number
    }

    /// check current indentation level
    pub(crate) fn get_indentation_level(&self) -> &usize {
        self.indentation_stack.last().unwrap_or(&1)
    }

    /// Pushes a new child scope, calls f, then pops that scope
    pub(crate) fn in_new_scope<F, R>(&mut self, f: F) -> R
    where
        F: FnOnce(&mut Parser<'envborrow, 'ctx>) -> R,
    {
        self.env.scope.push();
        let result = f(self);
        self.env.scope.pop();
        result
    }

    /// return a token that can be used to rewind the parser position
    pub(crate) fn make_backup_point(&self) -> (usize, usize, usize) {
        (self.byte, self.line_number, self.column_number)
    }

    /// Create a `SourceRegion` instance. `begin_source_region` is usually more convenient
    pub(crate) fn make_source_region(
        &self,
        start_line_number: usize,
        start_column_number: usize,
    ) -> SourceRegion {
        SourceRegion {
            start_line_number,
            start_column_number,
            end_line_number: self.get_line_number(),
            end_column_number: self.get_column_number(),
            file_label: self.file_label.clone(),
            code: Some(self.get_code()),
        }
    }

    /// move forward in the code without watching for indentation along the way
    pub(crate) fn move_forward(&mut self, lines: usize, columns: usize, bytes: usize) {
        self.byte += bytes;
        self.line_number += lines;
        self.column_number = if lines > 0 {
            1 + columns
        } else {
            self.column_number + columns
        };
    }

    /// create new parser
    fn new<S1, S2>(file_label: S1, code: S2, env: &'envborrow mut Env<'ctx>) -> Self
    where
        S1: Into<StringType>,
        S2: Into<String>,
    {
        let file_label = file_label.into();
        let code = Arc::new(CodeLines::new(file_label.clone(), code.into()));
        Parser {
            file_label,
            code,
            warnings: Vec::new(),
            line_number: 1,
            column_number: 1,
            byte: 0,
            did_tab_warning: false,
            indentation_stack: Vec::new(),
            is_inside_function: false,
            env,
        }
    }

    /// peek ahead at the next grapheme
    pub(crate) fn peek_grapheme(&mut self) -> Option<&str> {
        self.code
            .get_by_byte(self.byte..)
            .and_then(|s| s.graphemes(true).next())
    }

    /// peek at an upcoming word
    pub(crate) fn peek_word(&mut self) -> &str {
        let word = self
            .code
            .get_by_byte(self.byte..)
            .map(|s| s.split_word_bounds().next());
        if let Some(Some(word)) = word {
            word
        } else {
            ""
        }
    }

    /// read a single character
    pub(crate) fn read_char(&mut self) -> Option<char> {
        self.code
            .get_by_byte(self.byte..)
            .and_then(|s| s.chars().next())
            .map(|c| {
                self.move_forward(0, 1, c.len_utf8());
                c
            })
    }

    /// record a warning that was spotted
    pub(crate) fn record_warning<S1, S2>(
        &mut self,
        short_msg: S1,
        help: S2,
        source_region: SourceRegion,
    ) where
        S1: Into<Cow<'static, str>>,
        S2: Into<Cow<'static, str>>,
    {
        self.warnings.push(Error {
            short_msg: short_msg.into(),
            help: help.into(),
            source_region,
            severity: Severity::Warning,
        });
    }

    /// Skips whitespace, then, if the column is > current indentation level,
    /// pushes the new indent level.
    /// Calls the provided fn sending whether it did.
    pub(crate) fn try_push_indentation_level<F1, MakeSourceRegionFn, R>(
        &mut self,
        f: F1,
        make_source_region: MakeSourceRegionFn,
    ) -> Result<R, Vec<Error>>
    where
        F1: FnOnce(&mut Parser<'envborrow, 'ctx>, bool, MakeSourceRegionFn) -> R,
        MakeSourceRegionFn: FnOnce(&Parser<'envborrow, 'ctx>) -> SourceRegion,
    {
        match self.skip_comments_and_whitespace()? {
            AdvanceResult::Continue => {
                self.indentation_stack.push(self.column_number);
                let r = f(self, true, make_source_region);
                self.indentation_stack.pop();
                Ok(r)
            }
            _ => Ok(f(self, false, make_source_region)),
        }
    }
}

/// Adding parser context to errors
pub(crate) trait IntoParseResult<T> {
    /// convert error message to error object with source context
    fn into_parse_result(self, source_region: &SourceRegion) -> Result<T, Vec<Error>>;
}

impl<T, S1, S2> IntoParseResult<T> for (S1, S2)
where
    S1: Into<Cow<'static, str>>,
    S2: Into<Cow<'static, str>>,
{
    fn into_parse_result(self, source_region: &SourceRegion) -> Result<T, Vec<Error>> {
        Err(vec![Error {
            short_msg: self.0.into(),
            help: self.1.into(),
            source_region: source_region.clone(),
            severity: Severity::Error,
        }])
    }
}

impl<T, S1, S2> IntoParseResult<T> for Result<T, (S1, S2)>
where
    S1: Into<Cow<'static, str>>,
    S2: Into<Cow<'static, str>>,
{
    fn into_parse_result(self, source_region: &SourceRegion) -> Result<T, Vec<Error>> {
        match self {
            Ok(x) => Ok(x),
            Err(message_pair) => Err(vec![Error {
                short_msg: message_pair.0.into(),
                help: message_pair.1.into(),
                source_region: source_region.clone(),
                severity: Severity::Error,
            }]),
        }
    }
}

impl<T> IntoParseResult<T> for Result<T, TiError> {
    fn into_parse_result(self, source_region: &SourceRegion) -> Result<T, Vec<Error>> {
        match self {
            Ok(x) => Ok(x),
            Err(TiError { msg_tuple, .. }) => Err(vec![Error {
                short_msg: msg_tuple.0,
                help: msg_tuple.1,
                source_region: source_region.clone(),
                severity: Severity::Error,
            }]),
        }
    }
}
