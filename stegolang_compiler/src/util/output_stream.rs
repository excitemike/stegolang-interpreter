/// used to allow functions to take any of stdout, stderr, or Vec<u8>
pub trait OutputStream {
    /// get as a `std::io::Write`
    fn as_write(&mut self) -> &mut dyn std::io::Write;
    /// extract the saved string, if a Vec<u8>
    fn into_string(self: Box<Self>) -> Option<String>;
}

impl OutputStream for std::io::Stdout {
    fn as_write(&mut self) -> &mut dyn std::io::Write {
        self
    }
    fn into_string(self: Box<Self>) -> Option<String> {
        None
    }
}

impl OutputStream for std::io::Stderr {
    fn as_write(&mut self) -> &mut dyn std::io::Write {
        self
    }
    fn into_string(self: Box<Self>) -> Option<String> {
        None
    }
}

impl OutputStream for Vec<u8> {
    fn as_write(&mut self) -> &mut dyn std::io::Write {
        self
    }
    fn into_string(self: Box<Self>) -> Option<String> {
        String::from_utf8(*self).ok()
    }
}
