//! map interface to a vector of keyvalue pairs

use std::{
    borrow::Borrow,
    hash::{Hash, Hasher},
    iter::FromIterator,
    mem::replace,
    ops::Index,
};

use itertools::Itertools;
pub struct SmallMap<K, V> {
    v: Vec<(K, V)>,
}

impl<K, V> Clone for SmallMap<K, V>
where
    K: Clone,
    V: Clone,
{
    fn clone(&self) -> Self {
        Self { v: self.v.clone() }
    }
}

impl<K: Ord, V: Ord> PartialOrd for SmallMap<K, V> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl<K: Ord, V: Ord> Ord for SmallMap<K, V> {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        let cmp = |a: &&(K, V), b: &&(K, V)| Ord::cmp(a, b);
        let sorted_a = self.v.iter().sorted_by(&cmp);
        let sorted_b = other.iter().sorted_by(cmp);
        sorted_a.cmp(sorted_b)
    }
}

impl<K: Ord, V: Ord> PartialEq for SmallMap<K, V> {
    fn eq(&self, other: &Self) -> bool {
        let cmp = |a: &&(K, V), b: &&(K, V)| Ord::cmp(a, b);
        let sorted_a = self.v.iter().sorted_by(cmp);
        let sorted_b = other.iter().sorted_by(cmp);
        for eob in sorted_a.zip_longest(sorted_b) {
            match eob {
                itertools::EitherOrBoth::Both(a, b) if a == b => {}
                _ => return false,
            }
        }
        true
    }
}

impl<K: Ord, V: Ord> Eq for SmallMap<K, V> {}

#[allow(dead_code)]
impl<K, V> SmallMap<K, V> {
    /// create a new, empty `SmallMap`
    ///
    /// # Examples
    ///
    /// ```
    /// use stegolanglib::util::SmallMap;
    /// let mut map: SmallMap<&str, i32> = SmallMap::new();
    /// ```
    #[must_use]
    pub fn new() -> Self {
        SmallMap { v: Vec::new() }
    }
    /// create a new `SmallMap` that can hold at least the specified
    /// initial capacity without reallocating
    ///
    /// # Examples
    ///
    /// ```
    /// use stegolanglib::util::SmallMap;
    /// let mut map: SmallMap<&str, i32> = SmallMap::with_capacity(16);
    /// ```
    #[must_use]
    pub fn with_capacity(capacity: usize) -> Self {
        SmallMap {
            v: Vec::with_capacity(capacity),
        }
    }

    /// Returns the number of elements the map can hold without reallocating.
    ///
    /// This number is a lower bound; it might be able to hold
    /// more, but is guaranteed to be able to hold at least this many.
    ///
    /// # Examples
    ///
    /// ```
    /// use stegolanglib::util::SmallMap;
    /// let mut map: SmallMap<&str, i32> = SmallMap::with_capacity(100);
    /// assert!(map.capacity() >= 100);
    /// ```
    #[must_use]
    pub fn capacity(&self) -> usize {
        self.v.capacity()
    }
    /// iterate over (key, value) pairs
    /// The iterator element type is `(&'a K, &'a V)`.
    ///
    /// # Examples
    ///
    /// ```
    /// use stegolanglib::util::SmallMap;
    /// let map = SmallMap::from([
    ///     ("a", 1),
    ///     ("b", 2),
    ///     ("c", 3),
    /// ]);
    /// for (key, val) in map.iter() {
    ///     println!("key: {key} val: {val}");
    /// }
    /// ```
    pub fn iter(&self) -> std::slice::Iter<(K, V)> {
        self.v.iter()
    }

    /// iterate over (key, value) pairs
    /// with mutable references to the values.
    /// The iterator element type is `(&'a K, &'a mut V)`.
    ///
    /// # Examples
    ///
    /// ```
    /// use stegolanglib::util::SmallMap;
    /// let mut map = SmallMap::from([
    ///     ("a", 1),
    ///     ("b", 2),
    ///     ("c", 3),
    /// ]);
    ///
    /// // Update all values
    /// for (_, val) in map.iter_mut() {
    ///     *val *= 2;
    /// }
    ///
    /// for (key, val) in &map {
    ///     println!("key: {key} val: {val}");
    /// }
    /// ```
    pub fn iter_mut(&mut self) -> std::slice::IterMut<(K, V)> {
        self.v.iter_mut()
    }

    /// empty the map
    ///
    /// # Examples
    ///
    /// ```
    /// use stegolanglib::util::SmallMap;
    /// let mut a = SmallMap::new();
    /// a.insert(1, "a");
    /// assert!(!a.is_empty());
    /// a.clear();
    /// assert!(a.is_empty());
    /// ```
    pub fn clear(&mut self) {
        self.v.clear();
    }

    /// return a reference to the value associated with the key (or None)
    ///
    /// The supplied key may be any borrowed form of the map's key type
    ///
    /// # Examples
    ///
    /// ```
    /// use stegolanglib::util::SmallMap;
    /// let mut map = SmallMap::new();
    /// map.insert(1, "a");
    /// assert_eq!(map.get(&1), Some(&"a"));
    /// assert_eq!(map.get(&2), None);
    /// ```
    pub fn get<Q: ?Sized>(&self, key: &Q) -> Option<&V>
    where
        K: Borrow<Q> + Eq,
        Q: Eq,
    {
        self.v
            .iter()
            .find(|x| {
                let x = Borrow::<Q>::borrow(&x.0);
                x == key
            })
            .map(|x| &x.1)
    }

    /// Returns the key-value pair corresponding to the supplied key.
    ///
    /// The supplied key may be any borrowed form of the map's key type
    ///
    /// # Examples
    ///
    /// ```
    /// use stegolanglib::util::SmallMap;
    /// let mut map = SmallMap::new();
    /// map.insert(1, "a");
    /// assert_eq!(map.get_key_value(&1), Some((&1, &"a")));
    /// assert_eq!(map.get_key_value(&2), None);
    pub fn get_key_value<Q: ?Sized>(&self, key: &Q) -> Option<(&K, &V)>
    where
        K: Borrow<Q> + Eq,
        Q: Eq,
    {
        self.v
            .iter()
            .find(|x| {
                let x = Borrow::<Q>::borrow(&x.0);
                x == key
            })
            .map(|x| (&x.0, &x.1))
    }

    /// Returns `true` if the map contains a value for the specified key.
    ///
    /// The key may be any borrowed form of the map's key type
    ///
    /// # Examples
    ///
    /// ```
    /// use stegolanglib::util::SmallMap;
    /// let mut map = SmallMap::new();
    /// map.insert(1, "a");
    /// assert_eq!(map.contains_key(&1), true);
    /// assert_eq!(map.contains_key(&2), false);
    /// ```
    pub fn contains_key<Q: ?Sized>(&self, key: &Q) -> bool
    where
        K: Borrow<Q> + Eq,
        Q: Eq,
    {
        self.get(key).is_some()
    }

    /// Returns a mutable reference to the value corresponding to the key.
    ///
    /// The key may be any borrowed form of the map's key type
    ///
    /// # Examples
    ///
    /// ```
    /// use stegolanglib::util::SmallMap;
    /// let mut map = SmallMap::new();
    /// map.insert(1, "a");
    /// if let Some(x) = map.get_mut(&1) {
    ///     *x = "b";
    /// }
    /// assert_eq!(map[&1], "b");
    /// ```
    pub fn get_mut<Q: ?Sized>(&mut self, key: &Q) -> Option<&mut V>
    where
        K: Borrow<Q> + Eq,
        Q: Eq,
    {
        self.v
            .iter_mut()
            .find(|x| {
                let x = Borrow::<Q>::borrow(&x.0);
                x == key
            })
            .map(|x| &mut x.1)
    }

    /// Inserts a key-value pair into the map.
    ///
    /// If the map did not have this key present, `None` is returned.
    ///
    /// If the map did have this key present, the value is updated, and the old
    /// value is returned. The key is not updated, though; this matters for
    /// types that can be `==` without being identical.
    ///
    /// # Examples
    ///
    /// ```
    /// use stegolanglib::util::SmallMap;
    /// let mut map = SmallMap::new();
    /// assert_eq!(map.insert(37, "a"), None);
    /// assert_eq!(map.is_empty(), false);
    ///
    /// map.insert(37, "b");
    /// assert_eq!(map.insert(37, "c"), Some("b"));
    /// assert_eq!(map[&37], "c");
    /// ```
    pub fn insert(&mut self, key: K, value: V) -> Option<V>
    where
        K: Eq,
    {
        if let Some((_, old_v)) = self.v.iter_mut().find(|x| x.0 == key) {
            Some(replace(old_v, value))
        } else {
            self.v.push((key, value));
            None
        }
    }

    /// Removes a key from the map, returning the value at the key if the key
    /// was previously in the map.
    ///
    /// The key may be any borrowed form of the map's key type
    ///
    /// # Examples
    ///
    /// ```
    /// use stegolanglib::util::SmallMap;
    /// let mut map = SmallMap::new();
    /// map.insert(1, "a");
    /// assert_eq!(map.remove(&1), Some("a"));
    /// assert_eq!(map.remove(&1), None);
    /// ```
    pub fn remove<Q: ?Sized>(&mut self, key: &Q) -> Option<V>
    where
        K: Borrow<Q> + Eq,
        Q: Eq,
    {
        match self.v.iter().position(|x| {
            let x = Borrow::<Q>::borrow(&x.0);
            x == key
        }) {
            Some(i) => Some(self.v.remove(i).1),
            None => None,
        }
    }

    /// Retains only the elements specified by the predicate.
    ///
    /// In other words, remove all pairs `(k, v)` for which `f(&k, &mut v)` returns `false`.
    ///
    /// # Examples
    ///
    /// ```
    /// use stegolanglib::util::SmallMap;
    /// let mut map: SmallMap<i32, i32> = (0..8).map(|x| (x, x*10)).collect();
    /// map.retain(|&k, _| k % 2 == 0);
    /// assert_eq!(map.len(), 4);
    /// ```
    pub fn retain<F>(&mut self, mut f: F)
    where
        K: Eq,
        F: FnMut(&K, &V) -> bool,
    {
        self.v.retain(|(k, v)| f(k, v));
    }

    /// get an iterator over the keys of the map
    ///
    /// # Examples
    ///
    /// ```
    /// use stegolanglib::util::SmallMap;
    ///
    /// let map = SmallMap::from([
    ///     ("a", 1),
    ///     ("b", 2),
    ///     ("c", 3),
    /// ]);
    ///
    /// for key in map.keys() {
    ///     println!("{key}");
    /// }
    /// ```
    pub fn keys(&self) -> impl Iterator<Item = &K> {
        self.v.iter().map(|x| &x.0)
    }

    /// Creates a consuming iterator visiting all the keys
    /// The map cannot be used after calling this.
    /// The iterator element type is `K`.
    ///
    /// # Examples
    ///
    /// ```
    /// use stegolanglib::util::SmallMap;
    ///
    /// let map = SmallMap::from([
    ///     ("a", 1),
    ///     ("b", 2),
    ///     ("c", 3),
    /// ]);
    ///
    /// for key in map.into_keys() {
    ///     println!("{key}");
    /// }
    /// ```
    pub fn into_keys(self) -> impl Iterator<Item = K> {
        self.v.into_iter().map(|x| x.0)
    }

    /// get an iterator over the values of the map
    ///
    /// # Examples
    ///
    /// ```
    /// use stegolanglib::util::SmallMap;
    ///
    /// let map = SmallMap::from([
    ///     ("a", 1),
    ///     ("b", 2),
    ///     ("c", 3),
    /// ]);
    ///
    /// for val in map.values() {
    ///     println!("{val}");
    /// }
    /// ```
    pub fn values(&self) -> impl Iterator<Item = &V> {
        self.v.iter().map(|x| &x.1)
    }

    /// get a mutable iterator over the values of the map
    ///
    /// # Examples
    ///
    /// ```
    /// use stegolanglib::util::SmallMap;
    ///
    /// let mut map = SmallMap::from([
    ///     ("a", 1),
    ///     ("b", 2),
    ///     ("c", 3),
    /// ]);
    ///
    /// for val in map.values_mut() {
    ///     *val = *val + 10;
    /// }
    ///
    /// for val in map.values_mut() {
    ///     println!("{val}");
    /// }
    /// ```
    pub fn values_mut(&mut self) -> impl Iterator<Item = &mut V> {
        self.v.iter_mut().map(|x| &mut x.1)
    }

    /// Creates a consuming iterator visiting all the values
    /// The map cannot be used after calling this.
    /// The iterator element type is `V`.
    ///
    /// # Examples
    ///
    /// ```
    /// use stegolanglib::util::SmallMap;
    ///
    /// let map = SmallMap::from([
    ///     ("a", 1),
    ///     ("b", 2),
    ///     ("c", 3),
    /// ]);
    ///
    /// let mut vec: Vec<i32> = map.into_values().collect();
    /// // The `IntoValues` iterator produces values in arbitrary order, so
    /// // the values must be sorted to test them against a sorted array.
    /// vec.sort_unstable();
    /// assert_eq!(vec, [1, 2, 3]);
    /// ```
    pub fn into_values(self) -> impl Iterator<Item = V> {
        self.v.into_iter().map(|x| x.1)
    }
    /// Returns the number of elements in the map.
    #[must_use]
    pub fn len(&self) -> usize {
        self.v.len()
    }
    /// Returns `true` if the map contains no elements.
    #[must_use]
    pub fn is_empty(&self) -> bool {
        self.v.is_empty()
    }
}

impl<K, V> Default for SmallMap<K, V>
where
    K: Default,
    V: Default,
{
    fn default() -> Self {
        Self { v: Vec::default() }
    }
}

impl<K, V> IntoIterator for SmallMap<K, V> {
    type Item = (K, V);
    type IntoIter = std::vec::IntoIter<(K, V)>;

    /// Creates a consuming iterator, that is, one that moves each key-value
    /// pair out of the map. The map cannot be used after
    /// calling this.
    ///
    /// # Examples
    ///
    /// ```
    /// use stegolanglib::util::SmallMap;
    ///
    /// let map = SmallMap::from([
    ///     ("a", 1),
    ///     ("b", 2),
    ///     ("c", 3),
    /// ]);
    ///
    /// // Not possible with .iter()
    /// let vec: Vec<(&str, i32)> = map.into_iter().collect();
    /// ```
    fn into_iter(self) -> std::vec::IntoIter<(K, V)> {
        self.v.into_iter()
    }
}
impl<K: Ord, V> FromIterator<(K, V)> for SmallMap<K, V> {
    /// # Examples
    ///
    /// ```
    /// use stegolanglib::util::SmallMap;
    /// use std::iter::FromIterator;
    /// let data = [
    ///     ("a", 1),
    ///     ("b", 2),
    ///     ("c", 3),
    /// ];
    /// let map = SmallMap::from_iter(IntoIterator::into_iter(data));
    /// ```
    fn from_iter<T: IntoIterator<Item = (K, V)>>(iter: T) -> SmallMap<K, V> {
        SmallMap {
            v: iter.into_iter().collect(),
        }
    }
}
impl<K: Hash, V: Hash> Hash for SmallMap<K, V> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.v.hash(state);
    }
}
impl<K: core::fmt::Debug, V: core::fmt::Debug> core::fmt::Debug for SmallMap<K, V> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.debug_map()
            .entries(self.iter().map(|(k, v)| (k, v)))
            .finish()
    }
}

impl<K: Clone, V: Clone, const N: usize> From<[(K, V); N]> for SmallMap<K, V> {
    /// Converts a `[(K, V); N]` into a `SmallMap<(K, V)>`.
    /// # Examples
    ///
    /// ```
    /// use stegolanglib::util::SmallMap;
    ///
    /// let map1 = SmallMap::from([(1, 2), (3, 4)]);
    /// let map2: SmallMap<_, _> = [(1, 2), (3, 4)].into();
    /// let mut map3: SmallMap<_, _> = Default::default();
    /// map3.insert(1, 2);
    /// map3.insert(3, 4);
    /// assert_eq!(map1, map2);
    /// assert_eq!(map2, map3);
    /// ```
    fn from(arr: [(K, V); N]) -> Self {
        SmallMap { v: arr.to_vec() }
    }
}

impl<K: Clone, V: Clone> From<&[(K, V)]> for SmallMap<K, V> {
    fn from(arr: &[(K, V)]) -> Self {
        SmallMap { v: arr.to_vec() }
    }
}

impl<'a, K, V> IntoIterator for &'a SmallMap<K, V> {
    type Item = &'a (K, V);

    type IntoIter = std::slice::Iter<'a, (K, V)>;

    /// # Examples
    ///
    /// ```
    /// use stegolanglib::util::SmallMap;
    ///
    /// let map = SmallMap::from([
    ///     ("a", 1),
    ///     ("b", 2),
    ///     ("c", 3),
    /// ]);
    /// let map_ref: &SmallMap<_,_> = &map;
    /// for (key, val) in map.into_iter() {
    ///     println!("key: {key} val: {val}");
    /// }
    /// ```
    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl<'a, K, V> IntoIterator for &'a mut SmallMap<K, V> {
    type Item = &'a mut (K, V);

    type IntoIter = std::slice::IterMut<'a, (K, V)>;

    /// # Examples
    ///
    /// ```
    /// use stegolanglib::util::SmallMap;
    ///
    /// let mut map = SmallMap::from([
    ///     ("a", 1),
    ///     ("b", 2),
    ///     ("c", 3),
    /// ]);
    /// let map_ref: &mut SmallMap<_,_> = &mut map;
    /// for (_, val) in map_ref.into_iter() {
    ///     *val = *val + 10;
    /// }
    /// for (key, val) in map_ref.into_iter() {
    ///     println!("key: {key} val: {val}");
    /// }
    /// ```
    fn into_iter(self) -> Self::IntoIter {
        self.iter_mut()
    }
}

impl<K, Q: ?Sized, V> Index<&Q> for SmallMap<K, V>
where
    K: Eq + Borrow<Q>,
    Q: Eq,
{
    type Output = V;

    /// Returns a reference to the value corresponding to the supplied key.
    ///
    /// # Panics
    ///
    /// Panics if the key is not present in the `SmallMap`.
    #[inline]
    fn index(&self, key: &Q) -> &V {
        self.get(key).expect("no entry found for key")
    }
}
