//! useful code that's not easily categorized into one of the other modules

use crate::{config::TAB_STOP, stegoobject::StegoObject};
use core::ops::Deref;
use std::{borrow::Cow, fmt::Display, hash::Hash, rc::Rc, sync::Arc};
use unicode_segmentation::UnicodeSegmentation;

pub(crate) mod nestedscope;
pub(crate) use nestedscope::NestedScope;
pub mod output_stream;

mod sharedref;
pub(crate) use sharedref::SharedRef;

mod smallmap;
pub use smallmap::SmallMap;

mod wrap;
pub(crate) use wrap::wrap_text;

/// sort of like a shortcut for
/// ```
/// if let $pat = $match_expr {
///     $code
///     if let $pat = $match_expr {
///         $code
///         etc.
///     } else { panic!(...) }
/// } else { panic!(...) }
/// ```
///
/// Example:
///
/// ```rust
/// use stegolanglib::match_or_panic;
///
/// let v = vec![0,1,2];
/// match_or_panic! {
///     v.get(1),
///     Some(x) => assert_eq!(*x, 1)
/// }
/// ```
#[macro_export]
macro_rules! match_or_panic {
    ($match_expr:expr) => {{$match_expr}};
    ($match_expr:expr, $pat:pat => $code:expr) => {{
        let match_value = ($match_expr);
        if let $pat = match_value {
            $code
        } else {
            panic!("value {:#?} did not match pattern {:#?}", match_value, stringify!($pat))
        }
    }};
    ($match_expr:expr, $pat:pat => $code:expr, $($pats:pat => $codes:expr),*) => {{
        let match_value = ($match_expr);
        if let $pat = match_value {
            match_or_panic! {
                $code,
                $($pats => $codes),*
            }
        } else {
            panic!("value {:#?} did not match pattern {:#?}", match_value, stringify!($pat))
        }
    }};
}

/// find the width in columns for the given string
#[inline]
pub(crate) fn measure_width_in_columns(s: &str, start_column: usize) -> usize {
    assert!(start_column > 0);
    let mut column = start_column;
    for g in UnicodeSegmentation::graphemes(s, true) {
        if g == "\t" {
            column += TAB_STOP - ((column - 1) % TAB_STOP);
        } else {
            column += 1;
        }
    }
    column - start_column
}

/// write! but panicking with our error message if the write fails
#[macro_export]
macro_rules! writeunwrap {
    ($dst:expr, $($arg:tt)*) => {
        write!($dst, $($arg)*)
            .unwrap_or_else(|_| panic!("{}", $crate::loc::unable_to_write()))
    }
}

// unit tests

/// test `measure_width_in_columns`
#[test]
#[allow(clippy::eq_op)]
fn test_measure_width_in_columns() {
    assert_eq!(
        measure_width_in_columns("Here comes Santa Claus: 💡🦌🦌🦌🦌🦌🦌🦌🦌🛷🎅🏾", 1),
        35
    );
    assert_eq!(measure_width_in_columns("👨‍👩‍👧‍👧", 1), 1);
    assert_eq!(measure_width_in_columns("🤦🏼‍♂️", 1), 1);
    assert_eq!("ö̲", "\u{6f}\u{308}\u{332}");
    assert_eq!(measure_width_in_columns("ö̲", 1), 1);
    assert_eq!(measure_width_in_columns("\u{6f}\u{308}\u{332}", 1), 1);
    assert_eq!(measure_width_in_columns("The 🎲 is cast", 1), 13);
    assert_eq!(
        measure_width_in_columns("🦁s and 🐯s and 🐻s, oh my! 😱", 1),
        26
    );
    assert_eq!(measure_width_in_columns("\tx", 1), 5);
    assert_eq!(measure_width_in_columns("x\tx", 1), 5);
    assert_eq!(measure_width_in_columns("xx\tx", 1), 5);
    assert_eq!(measure_width_in_columns("xxx\tx", 1), 5);
    assert_eq!(measure_width_in_columns("xxxx\tx", 1), 9);
    assert_eq!(measure_width_in_columns("👈🏻\t☝\t👉🏽", 1), 9);
    assert_eq!(measure_width_in_columns("o\u{308}\u{304}", 1), 1);
    assert_eq!(measure_width_in_columns("\u{f6}\u{304}", 1), 1);
    assert_eq!(measure_width_in_columns("\u{22b}", 1), 1);
    assert_eq!(measure_width_in_columns("🙈🙉🙊", 1), 3);
}

#[test]
fn test_match_or_panic1() {
    let v = [0, 1, 2];
    match_or_panic! {
        v.get(1),
        Some(x) => *x,
        1 => ()
    }
}

#[test]
#[should_panic]
fn test_match_or_panic2() {
    let v = [0, 1, 2];
    match_or_panic! {
        v.get(7),
        Some(x) => *x,
        1 => ()
    }
}

#[test]
fn test_match_or_panic3() {
    let vv = vec![vec![0, 1], vec![2, 3]];
    match_or_panic! {
        vv.get(1),
        Some(v) => v.get(1),
        Some(x) => *x,
        3 => ()
    };
}

/// wrapper for Arc<str>
/// TODO: should probably use smartstring
#[derive(Debug, Hash, Eq, Ord, PartialEq, PartialOrd)]
pub struct RcStr {
    x: Arc<str>,
}

impl Clone for RcStr {
    fn clone(&self) -> Self {
        RcStr {
            x: Arc::clone(&self.x),
        }
    }
}

impl Deref for RcStr {
    type Target = str;
    fn deref(&self) -> &str {
        &self.x
    }
}

impl std::borrow::Borrow<str> for RcStr {
    fn borrow(&self) -> &str {
        self
    }
}

impl From<&str> for RcStr {
    #[inline]
    fn from(x: &str) -> RcStr {
        RcStr { x: Arc::from(x) }
    }
}

impl From<Arc<str>> for RcStr {
    #[inline]
    fn from(x: Arc<str>) -> RcStr {
        RcStr { x }
    }
}

impl From<&Arc<str>> for RcStr {
    #[inline]
    fn from(x: &Arc<str>) -> RcStr {
        RcStr { x: Arc::clone(x) }
    }
}

impl From<&RcStr> for RcStr {
    #[inline]
    fn from(x: &RcStr) -> RcStr {
        RcStr::clone(x)
    }
}

impl From<String> for RcStr {
    #[inline]
    fn from(x: String) -> RcStr {
        RcStr {
            x: Arc::from(x.as_ref()),
        }
    }
}

impl From<RcStr> for SharedRef<str> {
    fn from(rcstr: RcStr) -> SharedRef<str> {
        SharedRef::Arc(rcstr.x)
    }
}
impl From<&RcStr> for SharedRef<str> {
    fn from(rcstr: &RcStr) -> SharedRef<str> {
        SharedRef::Arc(Arc::clone(&rcstr.x))
    }
}

impl From<RcStr> for Cow<'static, str> {
    fn from(rcstr: RcStr) -> Cow<'static, str> {
        Cow::Owned(rcstr.to_string())
    }
}
impl From<&RcStr> for Cow<'static, str> {
    fn from(rcstr: &RcStr) -> Cow<'static, str> {
        Cow::Owned(rcstr.to_string())
    }
}

impl From<SharedRef<str>> for RcStr {
    fn from(shared_ref: SharedRef<str>) -> RcStr {
        match shared_ref {
            SharedRef::Static(s) => s.into(),
            SharedRef::Arc(s) => s.into(),
            SharedRef::Owned(s) => String::clone(&s).into(),
        }
    }
}

impl AsRef<str> for RcStr {
    #[inline]
    fn as_ref(&self) -> &str {
        &self.x
    }
}

impl PartialEq<str> for RcStr {
    #[inline]
    fn eq(&self, other: &str) -> bool {
        &*self.x == other
    }
}

impl PartialEq<&str> for RcStr {
    #[inline]
    fn eq(&self, other: &&str) -> bool {
        &*self.x == *other
    }
}

impl PartialEq<String> for RcStr {
    #[inline]
    fn eq(&self, other: &String) -> bool {
        &*self.x == other
    }
}

impl Display for RcStr {
    #[inline]
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.x)
    }
}

impl Default for RcStr {
    #[inline]
    fn default() -> Self {
        "".into()
    }
}

/// for reference counting `StegoObjects`
/// TODO: deprecate name
pub(crate) type RcVal<'ctx> = Rc<StegoObject<'ctx>>;

/// adapt an `RcVal` as a Display with repr format
pub struct RcValRepr<'ctx> {
    x: RcVal<'ctx>,
}
impl<'ctx> Display for RcValRepr<'ctx> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.x.repr().fmt(f)
    }
}
