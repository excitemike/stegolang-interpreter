//! data structure for tracking identifiers in nesting contexts
use crate::loc::{self, MsgTuple};
use std::{
    borrow::Borrow,
    cell::RefCell,
    collections::{BTreeMap, BTreeSet},
    fmt::Debug,
    rc::Rc,
};

/// store identifiers for fast lookup by name
///
#[derive(Clone, Debug)]
pub(crate) struct NestedScope<K, V> {
    /// identifiers in each context
    stack_levels: Vec<BTreeSet<K>>,
    /// for each identifier name, stack of values
    map: BTreeMap<K, Vec<Rc<RefCell<V>>>>,
}

/// result of NestedScope.lookup
///
#[derive(Debug)]
pub(crate) enum LookupResult<T> {
    NotFound,
    FoundInOuterScope(T),
    FoundInCurrentScope(T),
}

impl<K: Ord, V> NestedScope<K, V> {
    /// create new
    ///
    pub fn new() -> Self {
        NestedScope {
            stack_levels: vec![BTreeSet::new()],
            map: BTreeMap::new(),
        }
    }

    /// begin a nested context
    ///
    pub fn push(&mut self) {
        self.stack_levels.push(BTreeSet::new());
    }

    /// end a nested context
    ///
    pub fn pop(&mut self) {
        debug_assert!(self.stack_levels.len() > 1);

        // pop off that stack level
        let stack_level = self.stack_levels.pop().unwrap();

        // for every identifier defined there, we need to pop once in its map entry
        for var in stack_level {
            debug_assert!(self.map.contains_key(&var));
            let shadow_stack = self.map.get_mut(&var).unwrap();
            // pop off stack
            shadow_stack.pop();
            // if it was the last one by that name, we don't need that key anymore
            if shadow_stack.is_empty() {
                self.map.remove(&var);
            }
        }
    }

    /// retrieve an item, if possible
    ///
    pub(crate) fn get<Q>(&self, name: &Q) -> Option<core::cell::Ref<'_, V>>
    where
        Q: ?Sized + Ord,
        K: Borrow<Q> + Ord,
    {
        self.map
            .get(name)
            .and_then(|v| v.last())
            .map(|scope_cell| RefCell::borrow(scope_cell))
    }

    /// retrieve an item, if possible, and indicate whether it came from an outer scope
    ///
    pub(crate) fn lookup<Q>(&self, name: &Q) -> LookupResult<std::cell::Ref<'_, V>>
    where
        Q: ?Sized + Ord,
        K: Borrow<Q> + Ord,
    {
        use LookupResult::{FoundInCurrentScope, FoundInOuterScope, NotFound};
        match self.get(name) {
            Some(x) => match self.stack_levels.last() {
                Some(set) if set.contains(name) => FoundInCurrentScope(x),
                _ => FoundInOuterScope(x),
            },
            None => NotFound,
        }
    }

    /// look up an identifier only from the outermost scope
    pub(crate) fn get_global<Q>(&self, name: &Q) -> Option<std::cell::Ref<'_, V>>
    where
        Q: ?Sized + Ord,
        K: Borrow<Q> + Ord,
    {
        let has_id = self.stack_levels.first()?.contains(name);
        if !has_id {
            return None;
        }
        self.map
            .get(name)
            .and_then(|v| v.first())
            .map(|scope_cell| RefCell::borrow(scope_cell))
    }

    /// add an item to the current scope
    pub(crate) fn insert<Q, IntoV>(&mut self, key: Q, value: IntoV)
    where
        Q: Into<K>,
        K: Ord + Clone,
        IntoV: Into<V>,
    {
        let key = key.into();
        let depth = self.stack_levels.len();
        debug_assert!(depth > 0);
        let stack_level = self.stack_levels.last_mut().unwrap();
        // TODO: use entry API
        if self.map.contains_key(&key) {
            let shadow_stack = self.map.get_mut(&key).unwrap();
            if stack_level.contains(&key) {
                // shadowing at the same depth -- replace value
                match shadow_stack.last_mut() {
                    Some(x) => *x.borrow_mut() = value.into(),
                    None => panic!(),
                }
            } else {
                // shadowing a value from an outer scope
                shadow_stack.push(Rc::new(RefCell::new(value.into())));
                stack_level.insert(key);
            }
        } else {
            // new var not shadowing
            self.map
                .insert(key.clone(), vec![Rc::new(RefCell::new(value.into()))]);
            stack_level.insert(key);
        }
    }

    /// add an item to the current scope referencing something from another scope
    pub(crate) fn insert_upvalue<Q>(&mut self, key: Q, value: Rc<RefCell<V>>)
    where
        Q: ?Sized + Ord + Into<K>,
        K: Borrow<Q> + Ord + Clone,
    {
        let depth = self.stack_levels.len();
        debug_assert!(depth > 0);
        let stack_level = self.stack_levels.last_mut().unwrap();
        // TODO: use entry API
        if self.map.contains_key(&key) {
            let shadow_stack = self.map.get_mut(&key).unwrap();
            if stack_level.contains(&key) {
                panic!("Internal error: upvalues shouldn't be shadowing existing identifiers at the same depth");
            } else {
                // shadowing any version of the value in an outer scope
                shadow_stack.push(value);
                stack_level.insert(key.into());
            }
        } else {
            // new var not shadowing
            let key = key.into();
            self.map.insert(key.clone(), vec![value]);
            stack_level.insert(key);
        }
    }

    /// check the stack depth of the nearest level containing the identifier, if any
    pub fn stack_depth_of_definition<Q>(&self, key: &Q) -> Option<usize>
    where
        Q: ?Sized + Ord,
        K: Borrow<Q> + Ord,
    {
        // TODO: is this searching in the right order?
        self.stack_levels.iter().rposition(|x| x.contains(key))
    }

    /// if found, gets you the rwlock to the item
    ///
    pub(crate) fn get_lock<Q>(&mut self, key: &Q) -> Option<Rc<RefCell<V>>>
    where
        Q: ?Sized + Ord,
        K: Borrow<Q> + Ord,
    {
        match self.map.get_mut(key) {
            Some(shadow_stack) => shadow_stack.last().cloned(),
            None => None,
        }
    }

    /// get a mutable reference to an item in the nearest scope we find it in
    ///
    pub(crate) fn get_mut<Q>(&mut self, key: &Q) -> Option<std::cell::RefMut<'_, V>>
    where
        Q: ?Sized + Ord,
        K: Borrow<Q> + Ord,
    {
        match self.map.get_mut(key) {
            Some(shadow_stack) => shadow_stack.last().map(|x| x.borrow_mut()),
            None => None,
        }
    }

    /// Check current depth of scopes
    ///
    /// Zero means globals, and each nesting scope adds one
    pub fn get_current_depth(&self) -> usize {
        self.stack_levels.len() - 1
    }

    /// Get an object you can use to get/set the object in the original scope
    pub(crate) fn get_upvalue<Q>(&self, key: &Q) -> Option<&Rc<RefCell<V>>>
    where
        Q: ?Sized + Ord,
        K: Borrow<Q> + Ord,
    {
        match self.map.get(key) {
            Some(shadow_stack) => shadow_stack.last(),
            None => None,
        }
    }

    /// Get a whole mess of upvalues a once
    pub(crate) fn get_upvalues<Q, I: Iterator<Item = Q>>(
        &self,
        captures: I,
    ) -> Result<BTreeMap<K, Rc<RefCell<V>>>, MsgTuple>
    where
        Q: ?Sized + Ord + Into<K> + AsRef<str>,
        K: Borrow<Q> + Ord,
    {
        let mut updated = BTreeMap::new();
        for name in captures {
            match self.get_upvalue(&name) {
                Some(value) => {
                    updated.insert(name.into(), Rc::clone(value));
                }
                None => return Err(loc::err::identifier_not_in_scope(name.as_ref())),
            }
        }
        Ok(updated)
    }

    /// examine and perhaps modify all items introduced in current scope (not outer scopes)
    pub(crate) fn get_all_at_current_depth(&self) -> Vec<(K, Rc<RefCell<V>>)>
    where
        K: Clone,
    {
        let mut v = Vec::new();
        if let Some(vars) = self.stack_levels.last() {
            for var in vars {
                if let Some(var_stack) = self.map.get(var) {
                    if let Some(item) = var_stack.last() {
                        v.push((var.clone(), Rc::clone(item)));
                    }
                }
            }
        }
        v
    }

    /// call the provided function on every value in the scope
    pub(crate) fn for_each_value<F: FnMut(&V)>(&self, mut f: F) {
        for vec in self.map.values() {
            for v in vec {
                f(&(**v).borrow());
            }
        }
    }
}

impl<K: Ord, V> Default for NestedScope<K, V> {
    fn default() -> Self {
        NestedScope::new()
    }
}
