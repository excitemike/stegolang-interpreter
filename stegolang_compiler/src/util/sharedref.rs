use std::{
    borrow::{Borrow, Cow},
    fmt::{Debug, Display},
    hash::Hash,
    ops::Deref,
    sync::Arc,
};

/// Wrapper for data whose lifetime is managed and you don't care how
pub(crate) enum SharedRef<T: 'static + ?Sized + ToOwned> {
    Static(&'static T),
    Arc(Arc<T>),
    Owned(Arc<T::Owned>),
}

impl<T: ?Sized + ToOwned> Clone for SharedRef<T> {
    /// make a new reference, but don't copy the data
    fn clone(&self) -> Self {
        match self {
            Self::Static(x) => Self::Static(x),
            Self::Arc(x) => Self::Arc(Arc::clone(x)),
            Self::Owned(x) => Self::Owned(Arc::clone(x)),
        }
    }
}

impl Default for SharedRef<str> {
    fn default() -> Self {
        "".into()
    }
}

impl<T: Default + ToOwned> Default for SharedRef<T> {
    fn default() -> Self {
        SharedRef::Arc(Arc::new(T::default()))
    }
}

impl<T> Display for SharedRef<T>
where
    T: Display + ?Sized + ToOwned,
    Self: Borrow<T>,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.borrow().fmt(f)
    }
}

impl<T> Debug for SharedRef<T>
where
    T: Debug + ?Sized + ToOwned,
    Self: Borrow<T>,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.borrow().fmt(f)
    }
}

impl<T> Hash for SharedRef<T>
where
    T: Hash + ToOwned,
{
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        let x: &T = self;
        x.hash(state);
    }
}

impl Hash for SharedRef<str> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        let x: &str = self;
        x.hash(state);
    }
}

impl<T> PartialEq for SharedRef<T>
where
    T: ?Sized + PartialEq + SharedRefEq + ToOwned,
{
    fn eq(&self, other: &Self) -> bool {
        let x = self.as_ref();
        x.eq(other.as_ref())
    }
}

impl PartialEq for SharedRef<str> {
    fn eq(&self, other: &Self) -> bool {
        let a: &str = self;
        let b: &str = other;
        a == b
    }
}

impl PartialEq<str> for SharedRef<str> {
    fn eq(&self, other: &str) -> bool {
        let a: &str = self;
        a == other
    }
}

impl PartialEq<&str> for SharedRef<str> {
    fn eq(&self, other: &&str) -> bool {
        let a: &str = self;
        a == *other
    }
}

impl<T: ?Sized + ToOwned> Eq for SharedRef<T> where T: Eq + SharedRefEq {}

impl Eq for SharedRef<str> {}

impl<T> PartialOrd for SharedRef<T>
where
    T: ?Sized + PartialOrd + SharedRefEq + ToOwned,
{
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.as_ref().partial_cmp(other.as_ref())
    }
}

impl<T> Ord for SharedRef<T>
where
    T: ?Sized + Ord + SharedRefEq + ToOwned,
{
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.as_ref().cmp(other.as_ref())
    }
}

impl PartialOrd for SharedRef<str> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for SharedRef<str> {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        let x: &str = self;
        x.cmp(other.as_ref())
    }
}

/// conversion to `SharedRef`
impl<T: ?Sized + ToOwned> From<&'static T> for SharedRef<T> {
    fn from(x: &'static T) -> Self {
        Self::Static(x)
    }
}

/// conversion to `SharedRef`
impl From<String> for SharedRef<str> {
    fn from(x: String) -> Self {
        Self::Arc(Arc::from(x))
    }
}

impl<T: ?Sized + ToOwned> From<Cow<'static, T>> for SharedRef<T> {
    fn from(c: Cow<'static, T>) -> Self {
        match c {
            Cow::Borrowed(x) => SharedRef::Static(x),
            Cow::Owned(x) => SharedRef::Owned(Arc::new(x)),
        }
    }
}

/// conversion to `SharedRef`
impl From<&SharedRef<str>> for SharedRef<str> {
    fn from(x: &SharedRef<str>) -> Self {
        SharedRef::clone(x)
    }
}

/// conversion to `SharedRef`
impl<T: ToOwned> From<T> for SharedRef<T> {
    fn from(x: T) -> Self {
        Self::Arc(Arc::new(x))
    }
}

/// use as a &T
impl Deref for SharedRef<str> {
    type Target = str;
    fn deref(&self) -> &Self::Target {
        match self {
            Self::Static(x) => x,
            Self::Arc(x) => x,
            Self::Owned(x) => x,
        }
    }
}

/// use as a &T
impl<T: ToOwned> Deref for SharedRef<T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        match self {
            Self::Static(x) => x,
            Self::Arc(x) => x,
            Self::Owned(..) => unreachable!(),
        }
    }
}

/// borrow the value
impl Borrow<str> for SharedRef<str> {
    fn borrow(&self) -> &str {
        match self {
            Self::Static(x) => x,
            Self::Arc(x) => x,
            Self::Owned(x) => x,
        }
    }
}

/// borrow the value
impl<T: ?Sized + ToOwned> Borrow<T> for SharedRef<T>
where
    T: Sized,
{
    fn borrow(&self) -> &T {
        match self {
            Self::Static(x) => x,
            Self::Arc(x) => x,
            Self::Owned(..) => unreachable!(),
        }
    }
}

/// borrow the value
impl<T> AsRef<T> for SharedRef<T>
where
    T: ?Sized + ToOwned,
{
    fn as_ref(&self) -> &T {
        match self {
            Self::Static(x) => x,
            Self::Arc(x) => x,
            Self::Owned(x) => (**x).borrow(),
        }
    }
}

/// marker trait for types to opt-in to equality compare through `SharedRef`
pub(crate) trait SharedRefEq {}
impl SharedRefEq for String {}
