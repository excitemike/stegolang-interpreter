use lazy_static::lazy_static;
use regex::Regex;
use unicode_segmentation::UnicodeSegmentation;

struct WrappedStringBuilder {
    text: String,
    preword_space: String,
    word: String,
    line_width: u32,
    space_width: u32,
    word_width: u32,
    limit: u32,
}

const WRAP_WORD_CHAR_REGEX_STR: &str = r"[_\d\p{Alphabetic}\p{Emoji_Presentation}\p{Extended_Pictographic}\p{Join_Control}]|[']|[\p{Diacritic}--\p{Grapheme_Base}]";
lazy_static! {
    static ref WRAP_WORD_CHAR_REGEX: Regex =
        Regex::new(&format!("^{WRAP_WORD_CHAR_REGEX_STR}")).unwrap();
}

impl WrappedStringBuilder {
    /// add to the wrapped text
    pub(crate) fn add(&mut self, s: &str) {
        let gs = s.graphemes(true);
        for g in gs {
            match g {
                "\r\n" | "\n" => self.hard_newline(),
                g if g.trim().is_empty() => self.space(g),
                g if WRAP_WORD_CHAR_REGEX.is_match(g) => self.word(g),
                g => self.nonword(g),
            }
        }
    }

    /// the current word ended
    fn end_current_word(&mut self) {
        // no word to end => bail
        if self.word_width == 0 {
            return;
        }
        let width_if_no_wrap = self.line_width + self.space_width + self.word_width;
        // word fits or is entire line => just add it
        if (width_if_no_wrap < self.limit) || (self.line_width == 0) {
            self.text.push_str(&self.preword_space);
            self.line_width += self.space_width;
            self.preword_space.clear();
            self.space_width = 0;

            self.text.push_str(&self.word);
            self.line_width += self.word_width;
            self.word.clear();
            self.word_width = 0;

            self.line_width = width_if_no_wrap;
            return;
        }
        // otherwise, word goes on a new line
        self.text.push('\n');

        // ignore space
        self.preword_space.clear();
        self.space_width = 0;

        self.text.push_str(&self.word);
        self.line_width = self.word_width;
        self.word.clear();
        self.word_width = 0;
    }

    /// a new line was specifically requested
    fn hard_newline(&mut self) {
        self.end_current_word();
        if self.space_width > 0 {
            self.preword_space.clear();
            self.space_width = 0;
        }
        debug_assert!(self.word.is_empty());
        debug_assert_eq!(self.word_width, 0);
        self.text.push('\n');
        self.line_width = 0;
    }

    /// Consume builder and produce final String
    pub(crate) fn end(mut self) -> String {
        self.end_current_word();
        debug_assert!(self.word.is_empty());
        debug_assert_eq!(self.word_width, 0);
        self.text
    }

    /// get ready to wrap
    pub(crate) fn new(wrap_width: u32) -> WrappedStringBuilder {
        WrappedStringBuilder {
            text: String::default(),
            preword_space: String::default(),
            word: String::default(),
            line_width: 0,
            space_width: 0,
            word_width: 0,
            limit: wrap_width,
        }
    }

    /// A nonword grapheme was encountered. Treat it as an entire word unto itself.
    fn nonword(&mut self, g: &str) {
        self.end_current_word();
        debug_assert!(self.word.is_empty());
        debug_assert_eq!(self.word_width, 0);
        self.word(g);
        debug_assert!(!self.word.is_empty());
        debug_assert_eq!(self.word_width, 1);
        self.end_current_word();
        debug_assert!(self.word.is_empty());
        debug_assert_eq!(self.word_width, 0);
    }

    /// a whitespace grapheme was encountered
    fn space(&mut self, g: &str) {
        self.end_current_word();
        debug_assert_eq!(self.word_width, 0);
        self.preword_space.push_str(g);
        self.space_width += 1;
    }

    /// a word grapheme was encountered
    fn word(&mut self, g: &str) {
        self.word.push_str(g);
        self.word_width += 1;
    }
}

/// Wrap the provided text at the provided width in graphemes
pub(crate) fn wrap_text(s: &str, wrap_width: u32) -> String {
    let mut state = WrappedStringBuilder::new(wrap_width);
    state.add(s);
    state.end()
}
