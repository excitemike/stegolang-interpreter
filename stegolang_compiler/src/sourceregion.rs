//! struct to refer to a region in the source file
use crate::{codelines::CodeLines, StringType};
use std::sync::Arc;

/// location information to use in error messages
#[derive(Clone, Debug, PartialOrd, Ord)]
pub struct SourceRegion {
    /// 1-based line number for the start of the region
    pub start_line_number: usize,

    /// 1-based column number for the start of the region
    pub start_column_number: usize,

    /// 1-based line number containing the end of the region
    pub end_line_number: usize,

    /// 1-based column number of the first position AFTER the region
    pub end_column_number: usize,

    /// source file
    pub file_label: StringType,

    /// reference to the code itself
    pub code: Option<Arc<CodeLines>>,
}

impl SourceRegion {
    /// get a line of the source code by its 1-based line number
    pub fn get_line(&self, line_number: usize) -> Option<&str> {
        self.code.as_ref().and_then(|c| c.get_line(line_number))
    }

    /// create a new source region
    /// `start_line_number`: 1-based line number for the start of the region
    /// `start_column_number`: 1-based column number for the start of the region
    /// `end_line_number`: 1-based line number containing the end of the region
    /// `end_column_number`: 1-based column number of the first position AFTER the region
    /// `file_label`: a way to refer to the source file
    /// `code`:  reference to the code itself
    pub fn new<S: Into<StringType>>(
        start_line_number: usize,
        start_column_number: usize,
        end_line_number: usize,
        end_column_number: usize,
        file_label: S,
        code: Option<Arc<CodeLines>>,
    ) -> Self {
        SourceRegion {
            start_line_number,
            start_column_number,
            end_line_number,
            end_column_number,
            file_label: file_label.into(),
            code,
        }
    }

    /// true when the beginn and end of the region are the same
    pub fn span_empty(&self) -> bool {
        (self.end_line_number == self.start_line_number)
            && (self.end_column_number == self.start_line_number)
    }
}

impl std::hash::Hash for SourceRegion {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.start_line_number.hash(state);
        self.start_column_number.hash(state);
        self.end_line_number.hash(state);
        self.end_column_number.hash(state);
        self.file_label.hash(state);
    }
}

impl PartialEq for SourceRegion {
    fn eq(&self, other: &Self) -> bool {
        self.start_line_number == other.start_line_number
            && self.start_column_number == other.start_column_number
            && self.end_line_number == other.end_line_number
            && self.end_column_number == other.end_column_number
            && self.file_label == other.file_label
    }
}

impl Eq for SourceRegion {}
