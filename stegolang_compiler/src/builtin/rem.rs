#![allow(clippy::cast_precision_loss)]
//!built-ins related to the remainder/ operator
use num::{bigint::ToBigInt, Zero};

use super::{
    loc, Context, ControlFlow, EvalCtx, ITy, NestedScope, Rc, SourceRegion, StegoObject,
    StringType, TiResult, TypedValue, TypedValueKind,
};
use crate::{
    impl_trait, make_native_fn, make_trait, simple_op_impl, ti::traitstruct::TraitSchemeRef,
};

make_native_fn!(
    rem_int64_impl
    (ctx, ok, err) =>
    (StegoObject::Int64(lhs), StegoObject::Int64(rhs)) =>
    {
        if rhs.is_zero() {
            err(loc::err::remainder_by_zero())
        } else {
            ok(ctx.env.ti_ctx.mk_int64(), StegoObject::Int64(lhs % rhs))
        }
    }
);
make_native_fn!(
    rem_bigint_impl
    (ctx, ok, err) =>
    (StegoObject::Integer(lhs), StegoObject::Integer(rhs)) =>
    {
        if rhs.is_zero() {
            err(loc::err::remainder_by_zero())
        } else {
            ok(ctx.env.ti_ctx.mk_bigint(), StegoObject::Integer(lhs % rhs))
        }
    }
);
make_native_fn!(
    rem_bigint_i64_impl
    (ctx, ok, err) =>
    (StegoObject::Integer(lhs), StegoObject::Int64(rhs)) =>
    {
        if rhs.is_zero() {
            err(loc::err::remainder_by_zero())
        } else {
            ok(ctx.env.ti_ctx.mk_bigint(), StegoObject::Integer(lhs % rhs.to_bigint().unwrap()))
        }
    }
);
make_native_fn!(
    rem_i64_bigint_impl
    (ctx, ok, err) =>
    (StegoObject::Int64(lhs), StegoObject::Integer(rhs)) =>
    {
        if rhs.is_zero() {
            err(loc::err::remainder_by_zero())
        } else {
            ok(ctx.env.ti_ctx.mk_bigint(), StegoObject::Integer(lhs.to_bigint().unwrap() % rhs))
        }
    }
);
simple_op_impl!(
    rem_f64_impl,
    StegoObject::Float64(lhs),
    StegoObject::Float64(rhs),
    Float64,
    StegoObject::Float64(lhs % rhs)
);
simple_op_impl!(
    rem_i64_f64_impl,
    StegoObject::Int64(lhs),
    StegoObject::Float64(rhs),
    Float64,
    StegoObject::Float64((*lhs as f64) % rhs)
);
simple_op_impl!(
    rem_f64_i64_impl,
    StegoObject::Float64(lhs),
    StegoObject::Int64(rhs),
    Float64,
    StegoObject::Float64(lhs % (*rhs as f64))
);

/// insert into environment
pub(crate) fn insert_builtins<'ctx>(
    ctx: &Context<'ctx>,
    scope: &mut NestedScope<StringType, TypedValue<'ctx>>,
) -> TiResult<()> {
    let (rem_tr_ty, rem_tr): (ITy<'ctx>, TraitSchemeRef<'ctx>) = make_trait!(
        ctx,
        trait Rem Rhs =
            type Result
            fn rem [lhs : Self] [rhs : Rhs] = [Result]
    );
    scope.insert(
        StringType::from("Rem"),
        TypedValue {
            ty: rem_tr_ty,
            kind: TypedValueKind::TraitScheme(rem_tr),
        },
    );
    impl_trait!(ctx,
        impl [Rem Int64] for [Int64] =
            type Result = Int64
            fn rem lhs rhs = rem_int64_impl
    );
    impl_trait!(ctx,
        impl [Rem Float64] for [Float64] =
            type Result = Float64
            fn rem lhs rhs = rem_f64_impl
    );
    impl_trait!(ctx,
        impl [Rem Float64] for [Int64] =
            type Result = Float64
            fn rem lhs rhs = rem_i64_f64_impl
    );
    impl_trait!(ctx,
        impl [Rem Int64] for [Float64] =
            type Result = Float64
            fn rem lhs rhs = rem_f64_i64_impl
    );
    impl_trait!(ctx,
        impl [Rem BigInt] for [BigInt] =
            type Result = BigInt
            fn rem lhs rhs = rem_bigint_impl
    );
    impl_trait!(ctx,
        impl [Rem BigInt] for [Int64] =
            type Result = BigInt
            fn rem lhs rhs = rem_i64_bigint_impl
    );
    impl_trait!(ctx,
        impl [Rem Int64] for [BigInt] =
            type Result = BigInt
            fn rem lhs rhs = rem_bigint_i64_impl
    );

    Ok(())
}
