#![allow(clippy::cast_precision_loss)]
//!built-ins related to the division operator
use num::{bigint::ToBigInt, Zero};

use super::{
    loc, Context, ControlFlow, EvalCtx, Rc, SourceRegion, StegoObject, StringType, TiResult,
    TypedValue, TypedValueKind,
};
use crate::{impl_trait, make_native_fn, make_trait, simple_op_impl, util::NestedScope};

make_native_fn!(
    div_i64_impl
    (ctx, ok, err) =>
    (StegoObject::Int64(lhs), StegoObject::Int64(rhs)) =>
    {
        if rhs.is_zero() {
            err(loc::err::div_by_zero())
        } else {
            ok(ctx.env.ti_ctx.mk_int64(), StegoObject::Int64(lhs / rhs))
        }
    }
);
make_native_fn!(
    div_bigint_impl
    (ctx, ok, err) =>
    (StegoObject::Integer(lhs), StegoObject::Integer(rhs)) =>
    {
        if rhs.is_zero() {
            err(loc::err::div_by_zero())
        } else {
            ok(ctx.env.ti_ctx.mk_bigint(), StegoObject::Integer(lhs / rhs))
        }
    }
);
simple_op_impl!(
    div_f64_impl,
    StegoObject::Float64(lhs),
    StegoObject::Float64(rhs),
    Float64,
    StegoObject::Float64(lhs / rhs)
);
simple_op_impl!(
    div_i64_f64_impl,
    StegoObject::Int64(lhs),
    StegoObject::Float64(rhs),
    Float64,
    StegoObject::Float64((*lhs as f64) / rhs)
);
simple_op_impl!(
    div_bigint_i64_impl,
    StegoObject::Integer(lhs),
    StegoObject::Int64(rhs),
    BigInt,
    StegoObject::Integer(lhs / rhs.to_bigint().unwrap())
);
simple_op_impl!(
    div_i64_bigint_impl,
    StegoObject::Int64(lhs),
    StegoObject::Integer(rhs),
    BigInt,
    StegoObject::Integer(lhs.to_bigint().unwrap() / rhs)
);

/// insert into environment
pub(crate) fn insert_builtins<'ctx>(
    ctx: &Context<'ctx>,
    scope: &mut NestedScope<StringType, TypedValue<'ctx>>,
) -> TiResult<()> {
    let (div_tr_ty, div_tr) = make_trait!(
        ctx,
        trait Div Rhs =
            type Result
            fn div [lhs : Self] [rhs : Rhs] = [Result]
    );
    scope.insert(
        StringType::from("Div"),
        TypedValue {
            ty: div_tr_ty,
            kind: TypedValueKind::TraitScheme(div_tr),
        },
    );
    impl_trait!(ctx,
        impl [Div Int64] for [Int64] =
            type Result = Int64
            fn div lhs rhs = div_i64_impl
    );
    impl_trait!(ctx,
        impl [Div BigInt] for [BigInt] =
            type Result = BigInt
            fn div lhs rhs = div_bigint_impl
    );
    impl_trait!(ctx,
        impl [Div Float64] for [Float64] =
            type Result = Float64
            fn div lhs rhs = div_f64_impl
    );
    impl_trait!(ctx,
        impl [Div Float64] for [Int64] =
            type Result = Float64
            fn div lhs rhs = div_i64_f64_impl
    );
    impl_trait!(ctx,
        impl [Div BigInt] for [Int64] =
            type Result = BigInt
            fn div lhs rhs = div_i64_bigint_impl
    );
    impl_trait!(ctx,
        impl [Div Int64] for [BigInt] =
            type Result = BigInt
            fn div lhs rhs = div_bigint_i64_impl
    );

    Ok(())
}
