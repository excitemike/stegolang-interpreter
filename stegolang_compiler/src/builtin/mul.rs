#![allow(clippy::cast_precision_loss)]
//!built-ins related to the multiplication operator
use super::{
    loc, Context, ControlFlow, EvalCtx, ITy, NestedScope, Rc, RcStr, SourceRegion, StegoObject,
    StringType, TiResult, TypedValue, TypedValueKind,
};
use crate::{
    impl_trait, loc::MsgTuple, make_native_fn, make_trait, simple_op_impl,
    ti::traitstruct::TraitSchemeRef,
};
use num::bigint::ToBigInt;
use std::convert::From;

simple_op_impl!(
    mul_int64_impl,
    StegoObject::Int64(lhs),
    StegoObject::Int64(rhs),
    Int64,
    StegoObject::Int64(lhs * rhs)
);
simple_op_impl!(
    mul_bigint_impl,
    StegoObject::Integer(lhs),
    StegoObject::Integer(rhs),
    BigInt,
    StegoObject::Integer(lhs * rhs)
);
simple_op_impl!(
    mul_bigint_int64,
    StegoObject::Integer(lhs),
    StegoObject::Int64(rhs),
    BigInt,
    StegoObject::Integer(lhs * rhs.to_bigint().unwrap())
);
simple_op_impl!(
    mul_int64_bigint,
    StegoObject::Int64(lhs),
    StegoObject::Integer(rhs),
    BigInt,
    StegoObject::Integer(lhs.to_bigint().unwrap() * rhs)
);
simple_op_impl!(
    mul_f64_impl,
    StegoObject::Float64(lhs),
    StegoObject::Float64(rhs),
    Float64,
    StegoObject::Float64(lhs * rhs)
);
simple_op_impl!(
    mul_i64_f64_impl,
    StegoObject::Int64(lhs),
    StegoObject::Float64(rhs),
    Float64,
    StegoObject::Float64((*lhs as f64) * rhs)
);
simple_op_impl!(
    mul_f64_i64_impl,
    StegoObject::Float64(lhs),
    StegoObject::Int64(rhs),
    Float64,
    StegoObject::Float64(lhs * (*rhs as f64))
);

/// used by the `mul_string` operations
fn mul_string<'a>(s: &RcStr, n: usize) -> Result<StegoObject<'a>, MsgTuple> {
    let strlen = s.len();
    if n > 0 && strlen > 0 {
        let size = n * strlen;
        if size > 1_000_000_000 {
            return Err(loc::err::string_multiply_too_big());
        }
        let mut result = String::with_capacity(size);
        for _ in 0..n {
            result.push_str(s);
        }
        Ok(StegoObject::StegoString(result.into()))
    } else {
        Ok(StegoObject::StegoString(RcStr::default()))
    }
}

make_native_fn!(
    mul_string_int64
    (ctx, ok, err) =>
    (StegoObject::StegoString(s), StegoObject::Int64(n)) =>
    {
        #[allow(clippy::cast_possible_truncation, clippy::cast_sign_loss)]
        let n = *n.max(&0) as usize;
        match mul_string(s, n) {
            Ok(s) =>
            ok(
                ctx.env.ti_ctx.mk_string(),
                s
            ),
            Err(e) => err(e)
        }
    }
);
make_native_fn!(
    mul_int64_string
    (ctx, ok, err) =>
    ( StegoObject::Int64(n), StegoObject::StegoString(s)) =>
    {
        #[allow(clippy::cast_possible_truncation, clippy::cast_sign_loss)]
        let n = *n.max(&0) as usize;
        match mul_string(s, n) {
            Ok(s) =>
            ok(
                ctx.env.ti_ctx.mk_string(),
                s
            ),
            Err(e) => err(e)
        }
    }
);

/// insert into environment
pub(crate) fn insert_builtins<'ctx>(
    ctx: &Context<'ctx>,
    scope: &mut NestedScope<StringType, TypedValue<'ctx>>,
) -> TiResult<()> {
    let (mul_tr_ty, mul_tr): (ITy<'ctx>, TraitSchemeRef<'ctx>) = make_trait!(ctx,
        trait Mul Rhs =
            type Result
            fn mul [lhs : Self] [rhs : Rhs] = [Result]
    );
    scope.insert(
        StringType::from("Mul"),
        TypedValue {
            ty: mul_tr_ty,
            kind: TypedValueKind::TraitScheme(mul_tr),
        },
    );
    impl_trait!(ctx,
        impl [Mul Int64] for [Int64] =
            type Result = Int64
            fn mul lhs rhs = mul_int64_impl
    );
    impl_trait!(ctx,
        impl [Mul BigInt] for [BigInt] =
            type Result = BigInt
            fn mul lhs rhs = mul_bigint_impl
    );
    impl_trait!(ctx,
        impl [Mul Float64] for [Float64] =
            type Result = Float64
            fn mul lhs rhs = mul_f64_impl
    );
    impl_trait!(ctx,
        impl [Mul Float64] for [Int64] =
            type Result = Float64
            fn mul lhs rhs = mul_i64_f64_impl
    );
    impl_trait!(ctx,
        impl [Mul Int64] for [Float64] =
            type Result = Float64
            fn mul lhs rhs = mul_f64_i64_impl
    );
    impl_trait!(ctx,
        impl [Mul Int64] for [String] =
            type Result = String
            fn mul lhs rhs = mul_string_int64
    );
    impl_trait!(ctx,
        impl [Mul String] for [Int64] =
            type Result = String
            fn mul lhs rhs = mul_int64_string
    );
    impl_trait!(ctx,
        impl [Mul Int64] for [BigInt] =
            type Result = BigInt
            fn mul lhs rhs = mul_bigint_int64
    );
    impl_trait!(ctx,
        impl [Mul BigInt] for [Int64] =
            type Result = BigInt
            fn mul lhs rhs = mul_int64_bigint
    );

    Ok(())
}
