//!built-ins related to the addition operator
use std::rc::Rc;

use num::ToPrimitive;

use super::{
    loc, Context, ControlFlow, EvalCtx, ITy, NestedScope, RcStr, StegoObject, TiResult, ToBigInt,
    TypedValue, TypedValueKind,
};
use crate::{
    impl_trait, make_native_fn, make_trait, simple_op_impl, sourceregion::SourceRegion,
    ti::traitstruct::TraitSchemeRef, StringType,
};

simple_op_impl!(
    add_int64_impl,
    StegoObject::Int64(lhs),
    StegoObject::Int64(rhs),
    Int64,
    StegoObject::Int64(lhs + rhs)
);
simple_op_impl!(
    add_bigint_impl,
    StegoObject::Integer(lhs),
    StegoObject::Integer(rhs),
    BigInt,
    StegoObject::Integer(lhs + rhs)
);
simple_op_impl!(
    add_bigint_i64_impl,
    StegoObject::Integer(lhs),
    StegoObject::Int64(rhs),
    BigInt,
    StegoObject::Integer(lhs + rhs.to_bigint().unwrap())
);
simple_op_impl!(
    add_i64_bigint_impl,
    StegoObject::Int64(lhs),
    StegoObject::Integer(rhs),
    BigInt,
    StegoObject::Integer(lhs.to_bigint().unwrap() + rhs)
);
simple_op_impl!(
    add_f64_impl,
    StegoObject::Float64(lhs),
    StegoObject::Float64(rhs),
    Float64,
    StegoObject::Float64(lhs + rhs)
);
simple_op_impl!(
    add_f64_bigint_impl,
    StegoObject::Float64(lhs),
    StegoObject::Integer(rhs),
    Float64,
    StegoObject::Float64(lhs + rhs.to_f64().unwrap())
);
simple_op_impl!(
    add_bigint_f64_impl,
    StegoObject::Integer(lhs),
    StegoObject::Float64(rhs),
    Float64,
    StegoObject::Float64(lhs.to_f64().unwrap() + rhs)
);
simple_op_impl!(
    add_f64_i64_impl,
    StegoObject::Float64(lhs),
    StegoObject::Int64(rhs),
    Float64,
    StegoObject::Float64(lhs + *rhs as f64)
);
simple_op_impl!(
    add_i64_f64_impl,
    StegoObject::Int64(lhs),
    StegoObject::Float64(rhs),
    Float64,
    StegoObject::Float64(*lhs as f64 + rhs)
);

make_native_fn!(
    add_string_impl
    (ctx, ok, _err) =>
    (StegoObject::StegoString(lhs), StegoObject::StegoString(rhs)) =>
    {
        let mut s = String::with_capacity(lhs.len() + rhs.len());
        s.push_str(lhs.as_ref());
        s.push_str(rhs.as_ref());
        ok(ctx.env.ti_ctx.mk_string(), StegoObject::StegoString(RcStr::from(s)))
    }
);

make_native_fn!(
    add_string_i64_impl
    (ctx, ok, _err) =>
    (StegoObject::StegoString(lhs), StegoObject::Int64(rhs)) =>
    {
        let s = format!("{lhs}{rhs}");
        ok(ctx.env.ti_ctx.mk_string(), StegoObject::StegoString(RcStr::from(s)))
    }
);
make_native_fn!(
    add_i64_string_impl
    (ctx, ok, _err) =>
    (StegoObject::Int64(lhs), StegoObject::StegoString(rhs)) =>
    {
        let s = format!("{lhs}{rhs}");
        ok(ctx.env.ti_ctx.mk_string(), StegoObject::StegoString(RcStr::from(s)))
    }
);
make_native_fn!(
    add_string_nothing_impl
    (ctx, ok, _err) =>
    (StegoObject::StegoString(lhs), StegoObject::UnitType) =>
    {
        let s = format!("{lhs}{}", crate::config::NOTHING_VALUE);
        ok(ctx.env.ti_ctx.mk_string(), StegoObject::StegoString(RcStr::from(s)))
    }
);
make_native_fn!(
    add_nothing_string_impl
    (ctx, ok, _err) =>
    (StegoObject::UnitType, StegoObject::StegoString(rhs)) =>
    {
        let s = format!("{}{rhs}", crate::config::NOTHING_VALUE);
        ok(ctx.env.ti_ctx.mk_string(), StegoObject::StegoString(RcStr::from(s)))
    }
);

#[allow(dead_code)]
fn add_string_anything_impl<'ctx>(
    ctx: &mut EvalCtx<'_, 'ctx>,
    source_region: &SourceRegion,
    params: &[TypedValue<'ctx>],
) -> Result<TypedValue<'ctx>, ControlFlow<'ctx>> {
    if let [TypedValue {
        ty: _,
        kind: TypedValueKind::Val(lhs),
    }, rhs] = params
    {
        if let StegoObject::StegoString(lhs) = &**lhs {
            let rhs = rhs.to_string();
            let mut s = String::with_capacity(lhs.len() + rhs.len());
            s.push_str(lhs);
            s.push_str(&rhs);
            Ok(TypedValue::val(
                ctx.env.ti_ctx.mk_string(),
                Rc::new(StegoObject::StegoString(RcStr::from(s))),
            ))
        } else {
            let short_msg = loc::type_panic();
            Err(ControlFlow::Error(crate::error::Error {
                short_msg: short_msg.into(),
                help: short_msg.into(),
                source_region: source_region.clone(),
                severity: crate::error::Severity::Error,
            }))
        }
    } else {
        let short_msg = loc::type_panic();
        Err(ControlFlow::Error(crate::error::Error {
            short_msg: short_msg.into(),
            help: short_msg.into(),
            source_region: source_region.clone(),
            severity: crate::error::Severity::Error,
        }))
    }
}

#[allow(dead_code)]
fn add_anything_string_impl<'ctx>(
    ctx: &mut EvalCtx<'_, 'ctx>,
    source_region: &SourceRegion,
    params: &[TypedValue<'ctx>],
) -> Result<TypedValue<'ctx>, ControlFlow<'ctx>> {
    if let [lhs, TypedValue {
        ty: _,
        kind: TypedValueKind::Val(rhs),
    }] = params
    {
        if let StegoObject::StegoString(rhs) = &**rhs {
            let lhs = lhs.to_string();
            let mut s = String::with_capacity(lhs.len() + rhs.len());
            s.push_str(&lhs);
            s.push_str(rhs);
            Ok(TypedValue::val(
                ctx.env.ti_ctx.mk_string(),
                Rc::new(StegoObject::StegoString(RcStr::from(s))),
            ))
        } else {
            let short_msg = loc::type_panic();
            Err(ControlFlow::Error(crate::error::Error {
                short_msg: short_msg.into(),
                help: short_msg.into(),
                source_region: source_region.clone(),
                severity: crate::error::Severity::Error,
            }))
        }
    } else {
        let short_msg = loc::type_panic();
        Err(ControlFlow::Error(crate::error::Error {
            short_msg: short_msg.into(),
            help: short_msg.into(),
            source_region: source_region.clone(),
            severity: crate::error::Severity::Error,
        }))
    }
}

/// insert into environment
pub(crate) fn insert_builtins<'ctx>(
    ctx: &Context<'ctx>,
    scope: &mut NestedScope<StringType, TypedValue<'ctx>>,
) -> TiResult<()> {
    let (add_tr_ty, add_tr): (ITy<'ctx>, TraitSchemeRef<'ctx>) = make_trait!(ctx,
        trait Add Rhs =
            type Result
            fn add [lhs : Self] [rhs : Rhs] = [Result]
    );
    scope.insert(
        StringType::from("Add"),
        TypedValue {
            ty: add_tr_ty,
            kind: TypedValueKind::TraitScheme(add_tr),
        },
    );
    impl_trait!(ctx,
        impl [Add Int64] for [Int64] =
            type Result = Int64
            fn add lhs rhs = add_int64_impl
    );
    impl_trait!(ctx,
        impl [Add BigInt] for [BigInt] =
            type Result = BigInt
            fn add lhs rhs = add_bigint_impl
    );
    impl_trait!(ctx,
        impl [Add BigInt] for [Int64] =
            type Result = BigInt
            fn add lhs rhs = add_i64_bigint_impl
    );
    impl_trait!(ctx,
        impl [Add Int64] for [BigInt] =
            type Result = BigInt
            fn add lhs rhs = add_bigint_i64_impl
    );
    impl_trait!(ctx,
        impl [Add Float64] for [Float64] =
            type Result = Float64
            fn add lhs rhs = add_f64_impl
    );
    impl_trait!(ctx,
        impl [Add Int64] for [Float64] =
            type Result = Float64
            fn add lhs rhs = add_f64_i64_impl
    );
    impl_trait!(ctx,
        impl [Add Float64] for [Int64] =
            type Result = Float64
            fn add lhs rhs = add_i64_f64_impl
    );
    impl_trait!(ctx,
        impl [Add BigInt] for [Float64] =
            type Result = Float64
            fn add lhs rhs = add_f64_bigint_impl
    );
    impl_trait!(ctx,
        impl [Add Float64] for [BigInt] =
            type Result = Float64
            fn add lhs rhs = add_bigint_f64_impl
    );
    impl_trait!(ctx,
        impl [Add String] for [String] =
            type Result = String
            fn add lhs rhs = add_string_impl
    );
    impl_trait!(ctx,
        impl [Add Int64] for [String] =
            type Result = String
            fn add lhs rhs = add_string_i64_impl
    );
    impl_trait!(ctx,
        impl [Add String] for [Int64] =
            type Result = String
            fn add lhs rhs = add_i64_string_impl
    );
    impl_trait!(ctx,
        impl [Add Nothing] for [String] =
            type Result = String
            fn add lhs rhs = add_string_nothing_impl
    );
    impl_trait!(ctx,
        impl [Add String] for [Nothing] =
            type Result = String
            fn add lhs rhs = add_nothing_string_impl
    );
    // TODO: how would you express a fallback so that it doesn't overlap
    // the specific?
    //impl_trait!(ctx,
    //    impl [Add T] for [String] where [T: ToString] =
    //        type Result = String
    //        fn add lhs rhs = add_string_anything_impl
    //);
    //impl_trait!(ctx,
    //    impl [Add String] for [T] where [T: ToString] =
    //        type Result = String
    //        fn add lhs rhs = add_anything_string_impl
    //);

    Ok(())
}
