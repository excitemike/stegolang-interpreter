#![allow(clippy::cast_precision_loss)]
//! built-in comparison trait and implementations
use std::rc::Rc;

use num::bigint::ToBigInt;

use super::{
    loc, Context, ControlFlow, EvalCtx, ITy, NestedScope, StegoObject, TiResult, TypedValue,
    TypedValueKind,
};
use crate::{
    error::Error, impl_trait, make_trait, simple_op_impl, sourceregion::SourceRegion,
    ti::traitstruct::TraitSchemeRef, StringType,
};

simple_op_impl!(
    eq_char,
    StegoObject::Char(lhs),
    StegoObject::Char(rhs),
    Char,
    StegoObject::from(lhs == rhs)
);
simple_op_impl!(
    eq_int64,
    StegoObject::Int64(lhs),
    StegoObject::Int64(rhs),
    Int64,
    StegoObject::from(lhs == rhs)
);
simple_op_impl!(
    eq_bigint,
    StegoObject::Integer(lhs),
    StegoObject::Integer(rhs),
    BigInt,
    StegoObject::from(lhs == rhs)
);
simple_op_impl!(
    eq_float64,
    StegoObject::Float64(lhs),
    StegoObject::Float64(rhs),
    Float64,
    StegoObject::from(lhs == rhs)
);
simple_op_impl!(
    eq_f64_i64,
    StegoObject::Float64(lhs),
    StegoObject::Int64(rhs),
    Float64,
    StegoObject::from(lhs == &(*rhs as f64))
);
simple_op_impl!(
    eq_i64_f64,
    StegoObject::Int64(lhs),
    StegoObject::Float64(rhs),
    Float64,
    StegoObject::from((*lhs as f64) == *rhs)
);
simple_op_impl!(
    eq_bigint_i64,
    StegoObject::Integer(lhs),
    StegoObject::Int64(rhs),
    BigInt,
    StegoObject::from(lhs == &rhs.to_bigint().unwrap())
);
simple_op_impl!(
    eq_i64_bigint,
    StegoObject::Int64(lhs),
    StegoObject::Integer(rhs),
    BigInt,
    StegoObject::from(lhs.to_bigint().unwrap() == *rhs)
);

fn eq_string<'ctx>(
    ctx: &mut EvalCtx<'_, 'ctx>,
    source_region: &SourceRegion,
    params: &[TypedValue<'ctx>],
) -> Result<TypedValue<'ctx>, ControlFlow<'ctx>> {
    if let [TypedValue {
        ty: _,
        kind: TypedValueKind::Val(left_obj),
    }, TypedValue {
        ty: _,
        kind: TypedValueKind::Val(right_obj),
    }] = params
    {
        if let Some(lhs) = left_obj.get_str() {
            if let Some(rhs) = right_obj.get_str() {
                let object = { StegoObject::from(lhs == rhs) };
                let ty = simple_op_impl!(get_type String ctx);
                return Ok(TypedValue::val(ty, Rc::new(object)));
            }
        }
    }
    let (short_msg, help) = loc::err::internal::native_fn_received_incorrect_types();
    return Err(ControlFlow::Error(Error {
        short_msg,
        help,
        source_region: source_region.clone(),
        severity: crate::error::Severity::Error,
    }));
}
fn eq_tuple<'ctx>(
    ctx: &mut EvalCtx<'_, 'ctx>,
    source_region: &SourceRegion,
    params: &[TypedValue<'ctx>],
) -> Result<TypedValue<'ctx>, ControlFlow<'ctx>> {
    if let [TypedValue {
        ty: _,
        kind: TypedValueKind::Val(lhs),
    }, TypedValue {
        ty: _,
        kind: TypedValueKind::Val(rhs),
    }] = params
    {
        if let StegoObject::Tuple(lhs) = &**lhs {
            if let StegoObject::Tuple(rhs) = &**rhs {
                for (l, r) in lhs.iter().zip(rhs.iter()) {
                    if l != r {
                        return Ok(TypedValue::bool(&ctx.env.ti_ctx, false));
                    }
                }
                return Ok(TypedValue::bool(&ctx.env.ti_ctx, true));
            }
        }
    }

    let (short_msg, help) = loc::err::internal::native_fn_received_incorrect_types();
    Err(ControlFlow::Error(Error {
        short_msg,
        help,
        source_region: source_region.clone(),
        severity: crate::error::Severity::Error,
    }))
}

fn eq_nothing<'ctx>(
    ctx: &mut EvalCtx<'_, 'ctx>,
    source_region: &SourceRegion,
    params: &[TypedValue<'ctx>],
) -> Result<TypedValue<'ctx>, ControlFlow<'ctx>> {
    if let [TypedValue {
        ty: _,
        kind: TypedValueKind::Nothing,
    }, TypedValue {
        ty: _,
        kind: TypedValueKind::Nothing,
    }] = params
    {
        Ok(TypedValue::val(
            ctx.env.ti_ctx.mk_bool(),
            Rc::new(StegoObject::from(true)),
        ))
    } else {
        let short_msg = loc::type_panic();
        Err(ControlFlow::Error(Error {
            short_msg: short_msg.into(),
            help: short_msg.into(),
            source_region: source_region.clone(),
            severity: crate::error::Severity::Error,
        }))
    }
}

/// insert into environment
pub(crate) fn insert_builtins<'ctx>(
    ctx: &Context<'ctx>,
    scope: &mut NestedScope<StringType, TypedValue<'ctx>>,
) -> TiResult<()> {
    let (eq_tr_ty, eq_tr): (ITy<'ctx>, TraitSchemeRef<'ctx>) = make_trait!(
        ctx,
        trait Eq Rhs =
            fn eq [lhs : Self] [rhs : Rhs] = [Bool]
    );
    scope.insert(
        StringType::from("Eq"),
        TypedValue {
            ty: eq_tr_ty,
            kind: TypedValueKind::TraitScheme(eq_tr),
        },
    );
    impl_trait!(ctx,
        impl [Eq Int64] for [Int64] =
            fn eq lhs rhs = eq_int64
    );
    impl_trait!(ctx,
        impl [Eq Float64] for [Float64] =
            fn eq lhs rhs = eq_float64
    );
    impl_trait!(ctx,
        impl [Eq BigInt] for [BigInt] =
            fn eq lhs rhs = eq_bigint
    );
    impl_trait!(ctx,
        impl [Eq Float64] for [Int64] =
            fn eq lhs rhs = eq_i64_f64
    );
    impl_trait!(ctx,
        impl [Eq Int64] for [Float64] =
            fn eq lhs rhs = eq_f64_i64
    );
    impl_trait!(ctx,
        impl [Eq BigInt] for [Int64] =
            fn eq lhs rhs = eq_i64_bigint
    );
    impl_trait!(ctx,
        impl [Eq Int64] for [BigInt] =
            fn eq lhs rhs = eq_bigint_i64
    );
    impl_trait!(ctx,
        impl [Eq String] for [String] =
            fn eq lhs rhs = eq_string
    );
    impl_trait!(ctx,
        impl [Eq Char] for [Char] =
            fn eq lhs rhs = eq_char
    );
    impl_trait!(ctx,
        impl [Eq Nothing] for [Nothing] =
            fn eq lhs rhs = eq_nothing
    );
    impl_trait!(ctx,
        impl [Eq (T,)] for [(T,)] where [T: Eq T] =
            fn eq lhs rhs = eq_tuple
    );
    impl_trait!(ctx,
        impl [Eq (T1,T2)] for [(T1,T2)] where [T1: Eq T1, T1: Eq T2] =
            fn eq lhs rhs = eq_tuple
    );
    impl_trait!(ctx,
        impl [Eq (T1,T2,T3)] for [(T1,T2,T3)] where [T1: Eq T1, T2: Eq T2, T3: Eq T3] =
            fn eq lhs rhs = eq_tuple
    );
    impl_trait!(ctx,
        impl [Eq (T1,T2,T3,T4)] for [(T1,T2,T3,T4)] where [T1: Eq T1, T2: Eq T2, T3: Eq T3, T4: Eq T4] =
            fn eq lhs rhs = eq_tuple
    );
    impl_trait!(ctx,
        impl [Eq (T1,T2,T3,T4,T5)] for [(T1,T2,T3,T4,T5)] where [T1: Eq T1, T2: Eq T2, T3: Eq T3, T4: Eq T4, T5: Eq T5] =
            fn eq lhs rhs = eq_tuple
    );
    impl_trait!(ctx,
        impl [Eq (T1,T2,T3,T4,T5,T6)] for [(T1,T2,T3,T4,T5,T6)] where [T1: Eq T1, T2: Eq T2, T3: Eq T3, T4: Eq T4, T5: Eq T5, T6: Eq T6] =
            fn eq lhs rhs = eq_tuple
    );
    impl_trait!(ctx,
        impl [Eq (T1,T2,T3,T4,T5,T6,T7)] for [(T1,T2,T3,T4,T5,T6,T7)] where [T1: Eq T1, T2: Eq T2, T3: Eq T3, T4: Eq T4, T5: Eq T5, T6: Eq T6, T7: Eq T7] =
            fn eq lhs rhs = eq_tuple
    );

    Ok(())
}
