use std::rc::Rc;

use crate::{
    controlflow::ControlFlow,
    impl_trait,
    interpreter::EvalCtx,
    loc, make_native_fn, make_trait,
    sourceregion::SourceRegion,
    stegoobject::StegoObject,
    ti::{traitstruct::TraitSchemeRef, Context, ITy, TiResult},
    typedvalue::{TypedValue, TypedValueKind},
    util::{NestedScope, RcStr},
    StringType,
};

make_native_fn!(
    int64_tostr
    (ctx, ok, _err) =>
    (StegoObject::Int64(this)) =>
    {
        ok(ctx.env.ti_ctx.mk_string(), StegoObject::StegoString(RcStr::from(this.to_string())))
    }
);

/// insert into environment
pub(crate) fn insert_builtins<'ctx>(
    ctx: &Context<'ctx>,
    scope: &mut NestedScope<StringType, TypedValue<'ctx>>,
) -> TiResult<()> {
    let (tostr_tr_ty, tostr_tr): (ITy<'ctx>, TraitSchemeRef<'ctx>) = make_trait!(ctx,
        trait ToStr =
            fn to_str [x : Self] = [String]
    );
    scope.insert(
        StringType::from("ToStr"),
        TypedValue {
            ty: tostr_tr_ty,
            kind: TypedValueKind::TraitScheme(tostr_tr),
        },
    );
    impl_trait!(ctx,
        impl [ToStr] for [Int64] =
            fn to_str self = int64_tostr
    );
    Ok(())
}
