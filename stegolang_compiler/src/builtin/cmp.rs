//! built-in comparison trait and implementations
use num::{bigint::ToBigInt, ToPrimitive};

use super::{
    loc, Context, ControlFlow, EvalCtx, ITy, NestedScope, Rc, StegoObject, TiResult, TypedValue,
    TypedValueKind,
};
use crate::{
    impl_trait, make_trait, simple_op_impl, sourceregion::SourceRegion,
    ti::traitstruct::TraitSchemeRef, StringType,
};

macro_rules! impl_helper{
    ($( $fname:ident ( $lhs:ident : $stego_object_variant_lhs:tt, $rhs:ident : $stego_object_variant_rhs:tt ) { $($code:tt)* } )*) => {
        $(simple_op_impl!(
            $fname,
            StegoObject::$stego_object_variant_lhs($lhs),
            StegoObject::$stego_object_variant_rhs($rhs),
            Bool,
            StegoObject::from( $($code)* )
        );)*
    }
}

impl_helper! {
    lt_int64   (lhs:Int64,  rhs:Int64)  { lhs <  rhs }
    gt_int64   (lhs:Int64,  rhs:Int64)  { lhs >  rhs }

    lt_bigint   (lhs:Integer,  rhs:Integer)  { lhs <  rhs }
    gt_bigint   (lhs:Integer,  rhs:Integer)  { lhs >  rhs }

    lt_i64_bigint   (lhs:Int64,  rhs:Integer)  { &(*lhs).to_bigint().unwrap() <  rhs }
    gt_i64_bigint   (lhs:Int64,  rhs:Integer)  { &(*lhs).to_bigint().unwrap() >  rhs }

    lt_i64_f64   (lhs:Int64,  rhs:Float64)  { (*lhs as f64) <  *rhs }
    gt_i64_f64   (lhs:Int64,  rhs:Float64)  { (*lhs as f64) >  *rhs }

    lt_bigint_i64   (lhs:Integer,  rhs:Int64)  { lhs <  &(*rhs).to_bigint().unwrap() }
    gt_bigint_i64   (lhs:Integer,  rhs:Int64)  { lhs >  &(*rhs).to_bigint().unwrap() }

    lt_bigint_f64   (lhs:Integer, rhs:Float64)  { lhs.to_f64().unwrap() < *rhs }
    gt_bigint_f64   (lhs:Integer, rhs:Float64)  { lhs.to_f64().unwrap() > *rhs }

    lt_f64   (lhs:Float64, rhs:Float64)  { lhs <  rhs }
    gt_f64   (lhs:Float64, rhs:Float64)  { lhs >  rhs }

    lt_f64_bigint   (lhs:Float64, rhs:Integer)  { *lhs <  rhs.to_f64().unwrap() }
    gt_f64_bigint   (lhs:Float64, rhs:Integer)  { *lhs >  rhs.to_f64().unwrap() }

    lt_f64_i64   (lhs:Float64, rhs:Int64)  { *lhs <  (*rhs as f64) }
    gt_f64_i64   (lhs:Float64, rhs:Int64)  { *lhs >  (*rhs as f64) }

    lt_char    (lhs:Char,   rhs:Char)   { lhs <  rhs }
    gt_char    (lhs:Char,   rhs:Char)   { lhs >  rhs }

    lt_string  (lhs:StegoString, rhs:StegoString) { lhs <  rhs }
    gt_string  (lhs:StegoString, rhs:StegoString) { lhs >  rhs }
}

/// insert into environment
pub(crate) fn insert_builtins<'ctx>(
    ctx: &Context<'ctx>,
    scope: &mut NestedScope<StringType, TypedValue<'ctx>>,
) -> TiResult<()> {
    let (cmp_tr_ty, cmp_tr): (ITy<'ctx>, TraitSchemeRef<'ctx>) = make_trait!(
        ctx,
        trait Cmp Rhs =
            fn gt [lhs : Self] [rhs : Rhs] = [Bool]
            fn lt [lhs : Self] [rhs : Rhs] = [Bool]
    );
    scope.insert(
        StringType::from("Cmp"),
        TypedValue {
            ty: cmp_tr_ty,
            kind: TypedValueKind::TraitScheme(cmp_tr),
        },
    );
    impl_trait!(ctx,
        impl [Cmp Int64] for [Int64] =
            fn lt lhs rhs = lt_int64
            fn gt lhs rhs = gt_int64
    );
    impl_trait!(ctx,
        impl [Cmp BigInt] for [BigInt] =
            fn lt lhs rhs = lt_bigint
            fn gt lhs rhs = gt_bigint
    );
    impl_trait!(ctx,
        impl [Cmp Int64] for [BigInt] =
            fn lt lhs rhs = lt_bigint_i64
            fn gt lhs rhs = gt_bigint_i64
    );
    impl_trait!(ctx,
        impl [Cmp BigInt] for [Int64] =
            fn lt lhs rhs = lt_i64_bigint
            fn gt lhs rhs = gt_i64_bigint
    );
    impl_trait!(ctx,
        impl [Cmp Char] for [Char] =
            fn lt lhs rhs = lt_char
            fn gt lhs rhs = gt_char
    );
    impl_trait!(ctx,
        impl [Cmp String] for [String] =
            fn lt lhs rhs = lt_string
            fn gt lhs rhs = gt_string
    );
    impl_trait!(ctx,
        impl [Cmp Float64] for [Float64] =
            fn lt lhs rhs = lt_f64
            fn gt lhs rhs = gt_f64
    );
    impl_trait!(ctx,
        impl [Cmp BigInt] for [Float64] =
            fn lt lhs rhs = lt_f64_bigint
            fn gt lhs rhs = gt_f64_bigint
    );
    impl_trait!(ctx,
        impl [Cmp Int64] for [Float64] =
            fn lt lhs rhs = lt_f64_i64
            fn gt lhs rhs = gt_f64_i64
    );
    impl_trait!(ctx,
        impl [Cmp Float64] for [Int64] =
            fn lt lhs rhs = lt_i64_f64
            fn gt lhs rhs = gt_i64_f64
    );
    impl_trait!(ctx,
        impl [Cmp Float64] for [BigInt] =
            fn lt lhs rhs = lt_bigint_f64
            fn gt lhs rhs = gt_bigint_f64
    );

    Ok(())
}
