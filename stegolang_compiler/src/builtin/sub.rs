#![allow(clippy::cast_precision_loss)]
//!built-ins related to the subtraction operator
use super::{
    loc, Context, ControlFlow, EvalCtx, ITy, NestedScope, Rc, SourceRegion, StegoObject,
    StringType, TiResult, ToBigInt, TypedValue, TypedValueKind,
};
use crate::{impl_trait, make_trait, simple_op_impl, ti::traitstruct::TraitSchemeRef};

// native function to subtract int64s
simple_op_impl!(
    sub_int64_impl,
    StegoObject::Int64(lhs),
    StegoObject::Int64(rhs),
    Int64,
    StegoObject::Int64(lhs - rhs)
);
simple_op_impl!(
    sub_bigint_impl,
    StegoObject::Integer(lhs),
    StegoObject::Integer(rhs),
    BigInt,
    StegoObject::Integer(lhs - rhs)
);
simple_op_impl!(
    sub_bigint_i64_impl,
    StegoObject::Integer(lhs),
    StegoObject::Int64(rhs),
    BigInt,
    StegoObject::Integer(lhs - rhs.to_bigint().unwrap())
);
simple_op_impl!(
    sub_i64_bigint_impl,
    StegoObject::Int64(lhs),
    StegoObject::Integer(rhs),
    BigInt,
    StegoObject::Integer(lhs.to_bigint().unwrap() - rhs)
);
simple_op_impl!(
    sub_f64_impl,
    StegoObject::Float64(lhs),
    StegoObject::Float64(rhs),
    Float64,
    StegoObject::Float64(lhs - rhs)
);

/// insert into environment
pub(crate) fn insert_builtins<'ctx>(
    ctx: &Context<'ctx>,
    scope: &mut NestedScope<StringType, TypedValue<'ctx>>,
) -> TiResult<()> {
    let (sub_tr_ty, sub_tr): (ITy<'ctx>, TraitSchemeRef<'ctx>) = make_trait!(
        ctx,
        trait Sub Rhs =
            type Result
            fn sub [lhs : Self] [rhs : Rhs] = [Result]
    );
    scope.insert(
        StringType::from("Sub"),
        TypedValue {
            ty: sub_tr_ty,
            kind: TypedValueKind::TraitScheme(sub_tr),
        },
    );
    impl_trait!(ctx,
        impl [Sub Int64] for [Int64] =
            type Result = Int64
            fn sub lhs rhs = sub_int64_impl
    );
    impl_trait!(ctx,
        impl [Sub BigInt] for [BigInt] =
            type Result = BigInt
            fn sub lhs rhs = sub_bigint_impl
    );
    impl_trait!(ctx,
        impl [Sub BigInt] for [Int64] =
            type Result = BigInt
            fn sub lhs rhs = sub_i64_bigint_impl
    );
    impl_trait!(ctx,
        impl [Sub Int64] for [BigInt] =
            type Result = BigInt
            fn sub lhs rhs = sub_bigint_i64_impl
    );
    impl_trait!(ctx,
        impl [Sub Float64] for [Float64] =
            type Result = Float64
            fn sub lhs rhs = sub_f64_impl
    );

    Ok(())
}
