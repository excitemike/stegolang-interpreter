use std::rc::Rc;

use super::{
    loc, Context, ControlFlow, EvalCtx, ITy, NestedScope, StegoObject, TiResult, TypedValue,
    TypedValueKind,
};
use crate::{
    impl_trait, make_trait, sourceregion::SourceRegion, ti::traitstruct::TraitSchemeRef, StringType,
};

fn set_contains<'ctx>(
    ctx: &mut EvalCtx<'_, 'ctx>,
    source_region: &SourceRegion,
    params: &[TypedValue<'ctx>],
) -> Result<TypedValue<'ctx>, ControlFlow<'ctx>> {
    if let [TypedValue {
        ty: _,
        kind: TypedValueKind::Set(hashset),
    }, item] = params
    {
        Ok(TypedValue::val(
            ctx.env.ti_ctx.mk_bool(),
            Rc::new(StegoObject::from(hashset.contains(item))),
        ))
    } else {
        let short_msg = loc::type_panic();
        Err(ControlFlow::Error(crate::error::Error {
            short_msg: short_msg.into(),
            help: short_msg.into(),
            source_region: source_region.clone(),
            severity: crate::error::Severity::Error,
        }))
    }
}

fn str_contains_char<'ctx>(
    ctx: &mut EvalCtx<'_, 'ctx>,
    source_region: &SourceRegion,
    params: &[TypedValue<'ctx>],
) -> Result<TypedValue<'ctx>, ControlFlow<'ctx>> {
    if let [TypedValue {
        ty: _,
        kind: TypedValueKind::Val(container),
    }, TypedValue {
        ty: _,
        kind: TypedValueKind::Val(item),
    }] = params
    {
        if let StegoObject::StegoString(container) = &**container {
            if let StegoObject::Char(item) = &**item {
                return Ok(TypedValue::val(
                    ctx.env.ti_ctx.mk_bool(),
                    Rc::new(StegoObject::from(container.contains(*item))),
                ));
            }
        }
    }

    let short_msg = loc::type_panic();
    Err(ControlFlow::Error(crate::error::Error {
        short_msg: short_msg.into(),
        help: short_msg.into(),
        source_region: source_region.clone(),
        severity: crate::error::Severity::Error,
    }))
}

fn str_contains_str<'ctx>(
    ctx: &mut EvalCtx<'_, 'ctx>,
    source_region: &SourceRegion,
    params: &[TypedValue<'ctx>],
) -> Result<TypedValue<'ctx>, ControlFlow<'ctx>> {
    if let [TypedValue {
        ty: _,
        kind: TypedValueKind::Val(container),
    }, TypedValue {
        ty: _,
        kind: TypedValueKind::Val(item),
    }] = params
    {
        if let StegoObject::StegoString(container) = &**container {
            if let StegoObject::StegoString(item) = &**item {
                return Ok(TypedValue::val(
                    ctx.env.ti_ctx.mk_bool(),
                    Rc::new(StegoObject::from(container.contains(item.as_ref()))),
                ));
            }
        }
    }
    let short_msg = loc::type_panic();
    Err(ControlFlow::Error(crate::error::Error {
        short_msg: short_msg.into(),
        help: short_msg.into(),
        source_region: source_region.clone(),
        severity: crate::error::Severity::Error,
    }))
}

/// insert into environment
pub(crate) fn insert_builtins<'ctx>(
    ctx: &Context<'ctx>,
    scope: &mut NestedScope<StringType, TypedValue<'ctx>>,
) -> TiResult<()> {
    let (contains_tr_ty, contains_tr): (ITy<'ctx>, TraitSchemeRef<'ctx>) = make_trait!(
        ctx,
        trait Contains Item =
            fn contains [container : Self] [item : Item] = [Bool]
    );
    scope.insert(
        StringType::from("Contains"),
        TypedValue {
            ty: contains_tr_ty,
            kind: TypedValueKind::TraitScheme(contains_tr),
        },
    );
    impl_trait!(ctx,
        impl [Contains T] for [ Set T ] where [T] =
            fn contains lhs rhs = set_contains
    );
    impl_trait!(ctx,
        impl [Contains Char] for [ String ] =
            fn contains lhs rhs = str_contains_char
    );
    impl_trait!(ctx,
        impl [Contains String] for [ String ] =
            fn contains lhs rhs = str_contains_str
    );

    Ok(())
}
