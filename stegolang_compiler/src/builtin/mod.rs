use crate::{
    config,
    controlflow::ControlFlow,
    fntypes::{fncode::FnCode, fnfamily::FnFamily, fnfamimpls::FnFamImpls},
    interpreter::EvalCtx,
    loc,
    pattern::{StegoPattern, StegoPatternKind},
    sourceregion::SourceRegion,
    stegoobject::{FnImpl, StegoObject},
    ti::{Context, ITy, Substitutions, TiError, TiResult, Type},
    typedvalue::{TypedValue, TypedValueKind},
    util::{NestedScope, RcStr},
    StringType,
};
use lazy_static::lazy_static;
use num::bigint::{BigInt, ToBigInt};
use std::rc::Rc;

lazy_static! {
    static ref IGNORE_PARAM: StringType = StringType::from(config::IGNORE_PARAM);
}

mod add;
mod cmp;
mod contains;
mod div;
mod eq;
mod mul;
mod rem;
mod sub;
mod tostr;

/// sourceregion marking the interpreter's own source
#[macro_export]
macro_rules! make_source_region {
    () => {
        $crate::sourceregion::SourceRegion::new(
            line!() as usize,
            column!() as usize,
            line!() as usize,
            column!() as usize,
            file!(),
            None,
        )
    };
}

/// helper for `macro_rules! make_type`
fn get_type_by_name<'a>(
    ctx: &Context<'a>,
    type_name: &StringType,
    sr: &SourceRegion,
) -> TiResult<(Substitutions<'a>, ITy<'a>)> {
    let (subs, ty) = ctx.lookup_type(type_name, sr)?;
    if !subs.is_empty() {
        return Err(TiError::new(
            loc::err::missing_type_parameter(type_name, &ty.to_string()),
            sr.clone(),
        ));
    }
    Ok((subs, ty))
}

/// shorthand for creating types
#[macro_export]
macro_rules! make_type {
    // Self
    ($ctx:ident, Self) => {{
        let ctx:&$crate::ti::Context = $ctx;
        let sr = $crate::make_source_region!();
        let name = $crate::config::SELF_TYPE_NAME.into();
        get_type_by_name(ctx, &name, &sr)?.1
    }};
    // Identifier
    ($ctx:ident, $name:expr) => {{
        let ctx:&$crate::ti::Context = $ctx;
        let name:$crate::StringType = $name.into();
        let sr = $crate::make_source_region!();
        get_type_by_name(ctx, &name, &sr)?.1
    }};
    // member of another type
    ($ctx:ident, Member $name:ident of $obj:tt) => {{
        let ctx:&$crate::ti::Context = $ctx;
        let name:$crate::StringType = stringify!($name).into();
        let obj:$crate::ti::ITy<'_> = $crate::make_type!(ctx, $obj);
        ctx.member(obj, &name)?
    }};
    // function scheme
    (
        $ctx:ident,
        Fn $(
            [ $names:ident : $($tys:tt)+ ]
        )* = $($return_type:tt)+
    ) => {{
        let ctx:&$crate::ti::Context = $ctx;
        let mut names:Vec<$crate::StringType> = Vec::new();
        let mut tys = Vec::new();
        $(
            names.push(stringify!($names).into());
            tys.push(make_type!(ctx, $($tys)* ));
        )*
        ctx.fn_scheme(names.iter(), tys.into_iter(), make_type!(ctx, $($return_type)*))
    }};
}

/// shorthand for creating traits
#[macro_export]
macro_rules! make_trait {
    // done
    (@ $ctx:ident $builder:ident)=>{$builder};
    // associated type
    (
        @
        $ctx:ident
        $builder:ident
        type $name:ident
        $($rest:tt)*
    ) => {{
        let ctx:&$crate::ti::Context = $ctx;
        let name:$crate::StringType = stringify!($name).into();
        let sr = $crate::make_source_region!();
        let placeholder = ctx.mk_unification_var(&sr, "associated type in trait")?;
        let $builder:$crate::ti::traitbuilder::TraitBuilder = $builder.add_type(name.clone(), placeholder);
        ctx.scopes.borrow_mut().insert(name.clone(), placeholder);
        make_trait!(@ $ctx $builder $($rest)*)
    }};
    // method
    (
        @
        $ctx:ident
        $builder:ident
        fn $fn_name:ident $(
            [ $names:ident : $($tys:tt)+ ]
        )* = [$( $return_type:tt )*]
        $($rest:tt)*
    ) => {{
        use $crate::builtin::get_type_by_name;
        let sr = $crate::make_source_region!();
        let ctx:&$crate::ti::Context = $ctx;
        let fn_name:$crate::StringType = stringify!($fn_name).into();
        let mut names:Vec<$crate::StringType> = Vec::new();
        let mut tys = Vec::new();
        // TODO: Use substitutions! (get_type_by_name may instantiate the type it finds)
        let mut _substitutions = $crate::ti::Substitutions::new();
        $({
            let name:$crate::StringType = stringify!($names).into();
            names.push(name.clone());
        })*
        $({
            let ty_name:$crate::StringType = stringify!($( $tys )*).into();
            let (s, ty) = get_type_by_name(ctx, &ty_name, &sr)?;
            tys.push(ty);
            _substitutions = ctx.compose(s, _substitutions)?;
        })*
        let return_type_name:$crate::StringType = stringify!($( $return_type )*).into();
        let (s, return_type) = get_type_by_name(ctx, &return_type_name, &sr)?;
        _substitutions = ctx.compose(s, _substitutions)?;
        let fn_type = ctx.mk_fn_type(tys.into_boxed_slice(), return_type);
        let $builder:$crate::ti::traitbuilder::TraitBuilder = $builder.add_method(fn_name, fn_type);
        make_trait!(@ $ctx $builder $($rest)*)
    }};

    // whole trait
    (
        $ctx:ident,
        trait $name:ident $( $type_params:ident )* =
        $( $body:tt )*
    ) => {{
        let sr = $crate::make_source_region!();
        let ctx:&$crate::ti::Context = $ctx;
        let name:$crate::StringType = stringify!($name).into();
        let (self_var, self_var_id) = ctx.mk_type_parameter(&sr)?;
        let (ty, tr) = ctx.in_new_scope(|ctx|{
            let type_params:Vec<($crate::StringType, $crate::ti::UnificationVarId)> = vec![
                $( {
                    let name:StringType = stringify!($type_params).into();
                    let (placeholder, placeholder_id) = ctx.mk_type_parameter(&sr)?;
                    ctx.scopes.borrow_mut().insert(name.clone(), placeholder);
                    (name, placeholder_id)
                } ),*
            ];
            ctx.scopes.borrow_mut().insert($crate::config::SELF_TYPE_NAME, self_var);
            let builder:$crate::ti::traitbuilder::TraitBuilder = ctx.build_trait(name.clone(), self_var_id, type_params);
            let (ty, tr) = make_trait!(@ ctx builder $($body)*).finish();
            Ok((ty, tr))
        })?;
        ctx.scopes.borrow_mut().insert(&name, ty);
        (ty, tr)
    }};
}

/// helper for `impl_trait`!
fn lookup_and_instantiate_type<'ctx>(
    ctx: &Context<'ctx>,
    type_name: &StringType,
    type_parameters: &[ITy<'ctx>],
    sr: &SourceRegion,
) -> TiResult<ITy<'ctx>> {
    let (_, ty) = get_type_by_name(ctx, type_name, sr)?;
    if let Type::TypeScheme(inner) = *ty {
        inner.instantiate_with(ctx, type_parameters, sr)
    } else {
        // I kind of expected a scheme here but at the time of writing I do get
        // things like non-scheme monomorphic Int64 directly in scope. Which is ok
        Ok(ty)
    }
}

/// shorthand for creating trait implementations
#[macro_export]
macro_rules! impl_trait {
    // done
    (@ $ctx:ident $builder:ident)=>{$builder};

    // parse_type - simple case - just a name to look up
    (@ $ctx:ident parse_type $type_name:ident)=>{{
        let type_name:$crate::StringType = stringify!($type_name).into();
        let sr = $crate::make_source_region!();
        $crate::builtin::lookup_and_instantiate_type($ctx, &type_name, &[], &sr)?
    }};

    // parse_type - type with parameters
    (@ $ctx:ident parse_type $type_name:ident $( $type_param:tt )* )=>{{
        let type_name:$crate::StringType = stringify!($type_name).into();
        let type_parameters = vec![
            $( impl_trait!(@ $ctx parse_type $type_param) ),*
        ];
        let sr = $crate::make_source_region!();
        $crate::builtin::lookup_and_instantiate_type($ctx, &type_name, type_parameters.as_slice(), &sr)?
    }};

    // tuple
    (@ $ctx:ident parse_type ( $( $( $elems:ident )+ ),* $(,)? ) )=>{{
        let types = vec![
            $(
                impl_trait!(@ $ctx parse_type $($elems)+)
            ),*
        ];
        $ctx.mk_tuple(types)
    }};

    // associated type in body
    (@ $ctx:ident $builder:ident type $lhs_name:ident = $rhs_name:ident $($rest:tt)*)=>{{
        let ctx:&$crate::ti::Context = $ctx;
        let lhs_name:$crate::StringType = stringify!($lhs_name).into();
        let rhs_name:$crate::StringType = stringify!($rhs_name).into();
        let sr = $crate::make_source_region!();
        let ty: $crate::ti::ITy<'_> = ctx.scopes.borrow().get(&rhs_name)
            .expect(&format!("type {} seems to be missing (type)", rhs_name))
            .expect_type(&sr)?;

        let $builder = $builder.assoc_type(lhs_name, ty);

        impl_trait!(@ ctx $builder $($rest)*)
    }};
    // method
    (
        @
        $ctx:ident
        $builder:ident
        fn $fn_name:ident $( $names:ident )* = $native_fn:ident
        $($rest:tt)*
    ) => {{
        use $crate::StringType;
        use std::rc::Rc;
        use core::cell::RefCell;
        use $crate::pattern::*;
        use $crate::fntypes::fncode::FnCode;
        use $crate::fntypes::fnfamily::FnFamily;
        use $crate::fntypes::fnfamimpls::FnFamImpls;
        use $crate::builtin::FnImpl;

        let fn_name:StringType = stringify!($fn_name).into();

        let sr = $crate::make_source_region!();
        let pattern_set = vec![
        $(
            StegoPattern {
                kind: StegoPatternKind::Capture(stringify!($names).into()),
                source_region: sr.clone()
            }
        ),*
        ];

        let fnfam = FnFamily {
            debug_name: StringType::clone(&fn_name),
            implementations: FnFamImpls::Simple(Rc::new(FnImpl {
                captures: Default::default(),
                code: FnCode::Native($native_fn),
                pattern_set: Rc::new(RefCell::new(pattern_set)),
            })),
            num_params: 1,
        };

        let $builder = $builder.method(fn_name, fnfam)?;
        impl_trait!(@ $ctx $builder $($rest)*)
    }};

    // whole implementation
    (
        $ctx:ident,
        impl [ $tr_name:ident $($tr_type_params:tt)* ] for [$( $for_ty:tt )+]
        $( where [ $($impl_type_params:tt)* ] )? =
            $( $body:tt )*
    ) => {{
        let sr = $crate::make_source_region!();
        let ctx:&$crate::ti::Context = $ctx;

        let implementation_params:Vec<(StringType, $crate::ti::ITy<'_>)> = vec![ $($(
            ( StringType::from( stringify!($impl_type_params) ), ctx.mk_type_parameter(&sr)?.0 )
        ),*)? ];

        // implemented trait and its params
        let tr_name:$crate::StringType = stringify!($tr_name).into();
        ctx.in_new_scope(|ctx|{
            for (imp_par_name, imp_par_ty) in implementation_params.iter() {
                ctx.scopes.borrow_mut().insert(imp_par_name, ctx.mk_typetype(*imp_par_ty));
            }
            let tr_type_params = vec![$( impl_trait!(@ ctx parse_type $tr_type_params) ),*];
            let scopes = ctx.scopes.borrow();
            let trait_scheme_ty = scopes.get_global(&tr_name).expect(&format!("trait {} seems to be missing", tr_name));
            let trait_scheme = trait_scheme_ty
                .expect_trait_scheme(&sr)?;
            let tr:$crate::ti::traitstruct::TraitStructRef<'ctx> = trait_scheme
                .instantiate_with(ctx, tr_type_params.as_slice(), &sr)?;

            // for-type and its params
            let ty_name:$crate::StringType = stringify!($($for_ty)+).into();
            let for_ty:$crate::ti::ITy<'_> =
                if let Some((_name, ty)) = implementation_params.iter().find(|(imp_par_name, _)|imp_par_name==&ty_name) {
                    *ty
                } else {
                    impl_trait!(@ ctx parse_type $( $for_ty )*)
                };
            let mut builder = ctx.implement(tr, for_ty, sr.clone());

            for (imp_par_name, imp_par_ty) in implementation_params {
                builder = builder.implementation_param(imp_par_name, imp_par_ty);
            }

            let builder = impl_trait!(@ ctx builder $( $body )*);
            builder.finish()
        })?;
    }};
}

/// usage: ```simple_op_impl!(
///     add_int64_impl,
///     StegoObject::Int64(lhs),
///     StegoObject::Int64(rhs),
///     Int64,
///     StegoObject::Int64(lhs + rhs)
/// );```
#[macro_export]
macro_rules! simple_op_impl {
    (get_type Int64 $ctx:ident) => {{
        let ctx = $ctx;
        ctx.env.ti_ctx.mk_int64()
    }};
    (get_type BigInt $ctx:ident) => {{
        let ctx = $ctx;
        ctx.env.ti_ctx.mk_bigint()
    }};
    (get_type Bool $ctx:ident) => {{
        let ctx = $ctx;
        ctx.env.ti_ctx.mk_bool()
    }};
    (get_type Float64 $ctx:ident) => {{
        let ctx = $ctx;
        ctx.env.ti_ctx.mk_float64()
    }};
    (get_type Char $ctx:ident) => {{
        let ctx = $ctx;
        ctx.env.ti_ctx.mk_char()
    }};
    (get_type String $ctx:ident) => {{
        let ctx = $ctx;
        ctx.env.ti_ctx.mk_string()
    }};
    ($fname:ident, $left_pat:pat, $right_pat:pat, $ty:ident, $($object_code:tt)*) => {
        #[allow(clippy::cast_precision_loss)]
        fn $fname <'ctx>(
            ctx: &mut EvalCtx<'_, 'ctx>,
            _: &SourceRegion,
            params: &[TypedValue<'ctx>],
        ) -> Result<TypedValue<'ctx>, ControlFlow<'ctx>> {
            if let [TypedValue {
                ty: _,
                kind: TypedValueKind::Val(left_obj),
            }, TypedValue {
                ty: _,
                kind: TypedValueKind::Val(right_obj),
            }] = params
            {
                if let ($left_pat, $right_pat) =
                    (&**left_obj, &**right_obj)
                {
                    let object = {$($object_code)*};
                    let ty = simple_op_impl!(get_type $ty ctx);
                    return Ok(TypedValue::val(
                        ty,
                        Rc::new(object),
                    ));
                }
            }
            panic!("{}", loc::type_panic());
        }
    };
}

#[macro_export]
macro_rules! count {
    () => (0usize);
    ($x:pat) => (1usize);
    ( $x:pat, $($pats:pat),* ) => (1usize + $crate::count!($($pats),*));
}

/// helper for making a native function to call from stegolang
#[macro_export]
macro_rules! make_native_fn{
    (
        $fname:ident
        ($ctx:ident, $okfn:ident, $errfn:ident) =>
        ($($pats:pat),*) =>
        $($code:tt)+ ) => {
        fn $fname<'ctx> (
            $ctx: &mut EvalCtx<'_, 'ctx>,
            source_region: &SourceRegion,
            params: &[TypedValue<'ctx>],
        ) -> Result<TypedValue<'ctx>, ControlFlow<'ctx>> {
            use itertools::Itertools;
            const ARITY:usize = $crate::count!($($pats),*);
            let $okfn = |ty, x|Ok(TypedValue::val(
                ty,
                Rc::new(x),
            ));
            let $errfn = |x:$crate::loc::MsgTuple|-> Result<TypedValue<'ctx>, ControlFlow> { $crate::controlflow::err(x, source_region.clone()) };

            let stego_objects:Vec<&StegoObject> = params.iter().map(|p|{
                if let TypedValue {
                    ty: _,
                    kind: TypedValueKind::Val(x),
                } = p {
                    &**x
                } else {
                    panic!("{}", loc::type_panic());
                }
            }).collect_vec();
            assert_eq!(ARITY, stego_objects.len(), "Native function received incorrect number of parameters");

            if let [
                $($pats),*
            ] = stego_objects.as_slice() {
                return { $($code)+ };
            }
            panic!("{}", loc::type_panic());
        }
    }
}

make_native_fn!(
    i64_to_bigint
    (ctx, ok, _err) =>
    (StegoObject::Int64(i)) =>
    {
        ok(ctx.env.ti_ctx.mk_bigint(), StegoObject::Integer(i.to_bigint().unwrap()))
    }
);

/// native print function
/// TODO: This needs to be generic and require `ToStr` trait
fn print<'ctx>(
    ctx: &mut EvalCtx<'_, 'ctx>,
    source_region: &SourceRegion,
    params: &[TypedValue<'ctx>],
) -> Result<TypedValue<'ctx>, ControlFlow<'ctx>> {
    let f = ctx.env.out_stream.as_write();
    if let [TypedValue { ty: _, kind }] = params {
        let write_result = match kind {
            TypedValueKind::Nothing => {
                write!(f, "{}", crate::config::NOTHING_VALUE)
            }
            TypedValueKind::Val(obj) => write!(f, "{obj}"),
            TypedValueKind::StegoFn {
                debug_name,
                fn_impl,
            } => write!(f, "({debug_name} {fn_impl})"),
            _ => todo!(),
        };
        write_result
            .map(|()| TypedValue::nothing(&ctx.env.ti_ctx))
            .map_err(|e| {
                ControlFlow::Error(crate::error::Error {
                    short_msg: loc::unable_to_write().into(),
                    help: format!("{e}").into(),
                    source_region: source_region.clone(),
                    severity: crate::error::Severity::Error,
                })
            })
    } else {
        let (short_msg, help) = loc::err::internal::native_fn_received_wrong_types();
        Err(ControlFlow::Error(crate::error::Error {
            short_msg,
            help,
            source_region: source_region.clone(),
            severity: crate::error::Severity::Error,
        }))
    }
}

/// native print function
fn println<'ctx>(
    ctx: &mut EvalCtx<'_, 'ctx>,
    source_region: &SourceRegion,
    params: &[TypedValue<'ctx>],
) -> Result<TypedValue<'ctx>, ControlFlow<'ctx>> {
    let x = print(ctx, source_region, params)?;
    writeln!(ctx.env.out_stream.as_write())
        .unwrap_or_else(|_| panic!("{}", loc::unable_to_write()));
    Ok(x)
}

// Native parse_int function.
// If given a string, attempts to parse it as an integer.
// Returns an integer or Nothing
// TODO: return a std lib type -- Maybe or Option
make_native_fn!(
    parse_int
    (ctx, ok, _err) =>
    (StegoObject::StegoString(s)) =>
    {
        let parse_result = s.parse::<BigInt>().ok();
        let stego_object_result = match parse_result {
            Some(x) => StegoObject::Integer(x),
            None => StegoObject::UnitType,
        };
        ok(ctx.env.ti_ctx.mk_bigint(), stego_object_result)
    }
);

macro_rules! insert_type {
    ($ctx:ident, $scope:ident, $name:expr, $ty:expr) => {{
        let ctx = $ctx;
        let name: StringType = $name.into();
        let ty: ITy<'ctx> = $ty;
        let tyval = TypedValue::typetype(ctx, ty);
        insert_value(ctx, $scope, name, tyval);
    }};
}
// TODO: extract most of this out to a non-macro called by the macro
macro_rules! insert_fn {
    ($ctx:ident, $scope:ident, $( forall $($typarnames:ident)* .)? fn $fn_name:ident $($fnparnames:ident : $fnpartypes:ident)* -> $return_type:ident = $native_fn:ident) => {{
        use std::rc::Rc;
        use core::cell::RefCell;
        let ctx = $ctx;
        let sr = $crate::make_source_region!();
        let fn_name:StringType = stringify!($fn_name).into();
        let typarnames:Vec<StringType> = vec![$( $(stringify!($typarnames).into()),* )?];

        let (typars, fnpartypes, return_type) = ctx.in_new_scope(|ctx| {
            let mut typars = Vec::with_capacity(typarnames.len());
            for name in typarnames.iter() {
                let (placeholder, placeholder_id) = ctx.mk_type_parameter(&sr)?;
                ctx.scopes.borrow_mut().insert(name.clone(), placeholder);
                typars.push(placeholder_id);
            }

            let mut fnpartypes:Vec<ITy<'ctx>> = Vec::new();
            // TODO: Use substitutions. (get_type_by_name may instantiate the type it finds)
            let mut _substitutions = $crate::ti::Substitutions::new();
            $({
                let ty_name:StringType = stringify!($fnpartypes).into();
                let (s, ty) = get_type_by_name(ctx, &ty_name, &sr)?;
                fnpartypes.push(ty);
                _substitutions = ctx.compose(s, _substitutions)?;
            })*
            let return_type:ITy<'ctx> = {
                let ty_name:StringType = stringify!($return_type).into();
                let (s, ty) = get_type_by_name(ctx, &ty_name, &sr)?;
                _substitutions = ctx.compose(s, _substitutions)?;
                ty
            };
            Ok((typars, fnpartypes, return_type))
        })?;

        let pattern_set = vec![
        $(
            StegoPattern {
                kind: StegoPatternKind::Capture(stringify!($fnparnames).into()),
                source_region: sr.clone()
            }
        ),*
        ];

        let fnfam = FnFamily {
            debug_name: StringType::clone(&fn_name),
            implementations: FnFamImpls::Simple(Rc::new(FnImpl {
                captures: Default::default(),
                code: FnCode::Native($native_fn),
                pattern_set: Rc::new(RefCell::new(pattern_set)),
            })),
            num_params: 1,
        };

        let fn_ty =  ctx.mk_generic_fn(typars.into_boxed_slice(), fnpartypes.into_iter(), return_type, Box::new(sr));
        ctx.scopes.borrow_mut().insert(fn_name.clone(), fn_ty);
        $scope.insert(
            fn_name,
            TypedValue::val(fn_ty, Rc::new(StegoObject::FnFamily(Rc::new(fnfam))))
        )
    }}
}

/// Add the types for built-in identifiers
pub(crate) fn insert_builtins<'ctx>(
    ctx: &Context<'ctx>,
    scope: &mut NestedScope<StringType, TypedValue<'ctx>>,
) -> TiResult<()> {
    insert_type!(ctx, scope, config::BIGINT_TYPE_NAME, ctx.mk_bigint());
    insert_type!(ctx, scope, config::BOOLEAN, ctx.mk_bool());
    insert_type!(ctx, scope, config::CHAR_TYPE_NAME, ctx.mk_char());
    insert_type!(ctx, scope, config::DICT_TYPE_CONSTRUCTOR_NAME, {
        let sr = make_source_region!();
        let (key, key_id) = ctx.mk_type_parameter(&sr)?;
        let (value, value_id) = ctx.mk_type_parameter(&sr)?;
        ctx.mk_type_scheme(Box::new([key_id, value_id]), ctx.mk_dict(key, value), &sr)
    });
    insert_type!(ctx, scope, config::FLOAT64_TYPE_NAME, ctx.mk_float64());
    insert_type!(ctx, scope, config::INT64_TYPE_NAME, ctx.mk_int64());
    insert_type!(ctx, scope, config::SET_TYPE_CONSTRUCTOR_NAME, {
        let sr = make_source_region!();
        let (elem, id) = ctx.mk_type_parameter(&sr)?;
        ctx.mk_type_scheme([id].into(), ctx.mk_set(elem), &sr)
    });
    insert_type!(ctx, scope, config::NOTHING_TYPE_NAME, ctx.mk_nothing());
    insert_type!(ctx, scope, config::STRING_TYPE_NAME, ctx.mk_string());
    insert_fn!(ctx, scope, fn parse_int s : String -> BigInt = parse_int);
    insert_fn!(ctx, scope, forall T . fn print t : T -> Nothing = print);
    insert_fn!(ctx, scope, forall T . fn println t : T -> Nothing = println);
    insert_fn!(ctx, scope, fn i64_to_bigint x : Int64 -> BigInt = i64_to_bigint);
    insert_value(
        ctx,
        scope,
        config::NOTHING_VALUE.into(),
        TypedValue::nothing(ctx),
    );

    add::insert_builtins(ctx, scope)?;
    cmp::insert_builtins(ctx, scope)?;
    contains::insert_builtins(ctx, scope)?;
    div::insert_builtins(ctx, scope)?;
    eq::insert_builtins(ctx, scope)?;
    mul::insert_builtins(ctx, scope)?;
    rem::insert_builtins(ctx, scope)?;
    sub::insert_builtins(ctx, scope)?;
    tostr::insert_builtins(ctx, scope)?;

    Ok(())
}

// insert into both type context and value scopes
fn insert_value<'ctx>(
    ctx: &Context<'ctx>,
    scope: &mut NestedScope<StringType, TypedValue<'ctx>>,
    name: StringType,
    typed_value: TypedValue<'ctx>,
) {
    ctx.scopes.borrow_mut().insert(name.clone(), typed_value.ty);
    scope.insert(name, typed_value);
}
