use std::{cell::RefCell, rc::Rc};

use crate::{pattern::StegoPattern, sourceregion::SourceRegion, StringType};

use super::{literal::Literal, DictExprElem, FnStmt, IfArm, Operator, SetExprElem};

/// abstract syntax tree
#[derive(Clone, Debug)]
pub(crate) struct AstNode {
    pub source_region: SourceRegion,
    pub kind: AstNodeKind,
}

impl AstNode {
    /// whether this is allowed in the left hand side of an assignment
    /// TODO: should probably be unnecessary because assignment targets should be parsed as patterns
    pub fn can_be_assignment_target(&self) -> bool {
        use AstNodeKind::{
            ApplyExpr, AssignStmt, BinOpExpr, BitwiseNot, ComparisonOpExpr, DictExpr, DoExpr,
            DotExpr, FnStmt, Identifier, IfExpr, InExpr, Lambda, Let, LiteralValue, LogicalNot,
            NamedExpr, ReturnStmt, SetExpr, Statements, Tuple, TypeOf, UnaryMinus, UnaryPlus,
            UnitType,
        };
        match self.kind {
            Identifier { .. } => true, // TODO: should be false, really. Those should be captures.
            ApplyExpr { .. }
            | AssignStmt { .. }
            | BinOpExpr { .. }
            | BitwiseNot(..)
            | ComparisonOpExpr { .. }
            | DictExpr(..)
            | DoExpr(..)
            | DotExpr { .. }
            | FnStmt { .. }
            | IfExpr { .. }
            | InExpr { .. }
            | Lambda { .. }
            | Let { .. }
            | LiteralValue(..)
            | LogicalNot(..)
            | NamedExpr { .. }
            | ReturnStmt(..)
            | SetExpr(..)
            | Statements { .. }
            | Tuple(..)
            | TypeOf(..)
            | UnaryMinus(..)
            | UnaryPlus(..)
            | UnitType => false,
        }
    }

    pub(crate) fn new<IntoAstNodeKind>(source_region: SourceRegion, kind: IntoAstNodeKind) -> Self
    where
        IntoAstNodeKind: Into<AstNodeKind>,
    {
        AstNode {
            source_region,
            kind: kind.into(),
        }
    }
}

/// data specific to the kind of node
#[derive(Clone, Debug)]
pub(crate) enum AstNodeKind {
    ApplyExpr {
        func: Box<AstNode>,
        args: Vec<AstNode>,
    },
    AssignStmt {
        lhs: Box<AstNode>,
        rhs: Box<AstNode>,
    },
    BitwiseNot(Box<AstNode>),
    BinOpExpr {
        first: Box<AstNode>,
        rest: Vec<(Operator, AstNode)>,
    },
    ComparisonOpExpr {
        first: Box<AstNode>,
        rest: Vec<(Operator, AstNode)>,
    },
    DictExpr(Vec<DictExprElem<AstNode>>),
    DoExpr(Box<AstNode>),
    DotExpr {
        lhs: Box<AstNode>,
        method_name: StringType,
    },
    FnStmt(FnStmt<AstNode>),
    Identifier {
        name: StringType,
    },
    InExpr {
        container: Box<AstNode>,
        invert: bool,
        item: Box<AstNode>,
    },
    IfExpr {
        arms: Vec<IfArm<AstNode>>,
        catch_all_else: Option<Box<AstNode>>,
    },
    Lambda {
        body: Rc<RefCell<AstNode>>,
        params: Rc<RefCell<Vec<StegoPattern>>>,
    },
    Let {
        pattern: Box<StegoPattern>,
        value_expr: Box<AstNode>,
    },
    LiteralValue(Literal),
    LogicalNot(Box<AstNode>),
    NamedExpr {
        name: StringType,
        value: Box<AstNode>,
    },
    ReturnStmt(Option<Box<AstNode>>),
    SetExpr(Vec<SetExprElem<AstNode>>),
    Statements {
        statements: Vec<AstNode>,
    },
    Tuple(Vec<AstNode>),
    TypeOf(Box<AstNode>),
    UnaryMinus(Box<AstNode>),
    UnaryPlus(Box<AstNode>),
    UnitType,
}

impl From<Literal> for AstNodeKind {
    fn from(l: Literal) -> Self {
        AstNodeKind::LiteralValue(l)
    }
}
