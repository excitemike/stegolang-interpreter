use crate::{config, util::RcStr};
use num::BigInt;

/// kinds of literals
#[derive(Clone, Debug)]
pub(crate) enum Literal {
    BigInt(BigInt), // TODO: maybe this should be an Arc so we don't have to clone BigInts while evaluating
    Bool(bool),
    Character(char),
    Float(f64),
    Int64(i64),
    String(RcStr),
    Nothing,
}
impl PartialEq for Literal {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::BigInt(l0), Self::BigInt(r0)) => l0 == r0,
            (Self::Bool(l0), Self::Bool(r0)) => l0 == r0,
            (Self::Character(l0), Self::Character(r0)) => l0 == r0,
            (Self::Float(l0), Self::Float(r0)) => l0 == r0,
            (Self::Int64(l0), Self::Int64(r0)) => l0 == r0,
            (Self::String(l0), Self::String(r0)) => l0 == r0,
            (Self::Nothing, Self::Nothing) => true,
            (
                Self::BigInt(_)
                | Self::Bool(_)
                | Self::Character(_)
                | Self::Float(_)
                | Self::Int64(_)
                | Self::String(_)
                | Self::Nothing,
                _,
            ) => false,
        }
    }
}
impl Eq for Literal {}

impl PartialOrd for Literal {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}
impl Ord for Literal {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        match (self, other) {
            (Self::BigInt(l0), Self::BigInt(r0)) => l0.cmp(r0),
            (Self::Bool(l0), Self::Bool(r0)) => l0.cmp(r0),
            (Self::Character(l0), Self::Character(r0)) => l0.cmp(r0),
            (Self::Float(l0), Self::Float(r0)) => {
                if l0 < r0 {
                    std::cmp::Ordering::Less
                } else if l0 > r0 {
                    std::cmp::Ordering::Greater
                } else {
                    std::cmp::Ordering::Equal
                }
            }
            (Self::Int64(l0), Self::Int64(r0)) => l0.cmp(r0),
            (Self::String(l0), Self::String(r0)) => l0.cmp(r0),
            (Self::Nothing, Self::Nothing) => std::cmp::Ordering::Equal,
            (
                l @ (Self::BigInt(_)
                | Self::Bool(_)
                | Self::Character(_)
                | Self::Float(_)
                | Self::Int64(_)
                | Self::String(_)
                | Self::Nothing),
                r,
            ) => l.discriminant_index().cmp(&r.discriminant_index()),
        }
    }
}
impl std::hash::Hash for Literal {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        #[allow(clippy::cast_possible_truncation)]
        match self {
            Literal::BigInt(x) => x.hash(state),
            Literal::Bool(x) => x.hash(state),
            Literal::Character(x) => x.hash(state),
            Literal::Float(x) => ((x * 1024.0 * 1024.0) as i64).hash(state),
            Literal::Int64(x) => x.hash(state),
            Literal::String(x) => x.hash(state),
            Literal::Nothing => 0usize.hash(state),
        }
    }
}

impl Literal {
    /// helper for implementing `PartialOrd`, `Ord` because what `core::mem::discriminant`
    /// gives you is not `Ord` or `PartialOrd`
    fn discriminant_index(&self) -> usize {
        match self {
            Literal::BigInt(..) => 0,
            Literal::Bool(..) => 1,
            Literal::Character(..) => 2,
            Literal::Float(..) => 3,
            Literal::Int64(..) => 4,
            Literal::String(..) => 5,
            Literal::Nothing => 6,
        }
    }
}

impl From<BigInt> for Literal {
    fn from(x: BigInt) -> Self {
        Literal::BigInt(x)
    }
}
impl From<bool> for Literal {
    fn from(x: bool) -> Self {
        Literal::Bool(x)
    }
}
impl From<char> for Literal {
    fn from(x: char) -> Self {
        Literal::Character(x)
    }
}
impl From<f64> for Literal {
    fn from(x: f64) -> Self {
        Literal::Float(x)
    }
}
impl From<RcStr> for Literal {
    fn from(x: RcStr) -> Self {
        Literal::String(x)
    }
}
impl From<i64> for Literal {
    fn from(x: i64) -> Self {
        Literal::Int64(x)
    }
}
impl From<()> for Literal {
    fn from((): ()) -> Self {
        Literal::Nothing
    }
}
impl std::fmt::Display for Literal {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Literal::BigInt(x) => x.fmt(f),
            Literal::Bool(x) => x.fmt(f),
            Literal::Character(x) => write!(f, "'{x}'"),
            Literal::Float(x) => x.fmt(f),
            Literal::Int64(x) => x.fmt(f),
            Literal::String(x) => write!(f, r#""{x}""#),
            Literal::Nothing => config::NOTHING_VALUE.fmt(f),
        }
    }
}
