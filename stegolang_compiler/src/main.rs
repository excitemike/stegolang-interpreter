#![warn(clippy::all)]

use std::env;
use std::path::Path;
use stegolanglib::interpreter::parse_and_evaluate_file;
use stegolanglib::repl::Repl;
use stegolanglib::util::output_stream::OutputStream;
use stegolanglib::ErrorCode;

/// print error message and exit
fn handle_io_error(error: &std::io::Error) -> ! {
    eprintln!("{error:?}");
    std::process::exit(1)
}

/// get the content of a file as a string
fn get_file_contents(path: &Path) -> String {
    // TODO - memory map the file?
    match std::fs::read_to_string(path) {
        Err(error) => handle_io_error(&error),
        Ok(buffer) => buffer,
    }
}

/// interpret single file
fn interpret_file(
    path: &Path,
    out_stream: &mut dyn OutputStream,
    err_stream: &mut dyn OutputStream,
) -> Result<(), ErrorCode> {
    let code = get_file_contents(path);
    let path = path.to_string_lossy().to_string();

    // parse and evaluate and print
    parse_and_evaluate_file(code, path, out_stream, err_stream)
}

/// interpret files given on the commandline
fn interpret_files(
    paths: &[String],
    out_stream: &mut dyn OutputStream,
    err_stream: &mut dyn OutputStream,
) -> Result<(), ErrorCode> {
    for path in paths {
        interpret_file(Path::new(path), out_stream, err_stream)?;
    }
    Ok(())
}

/// entry point - examine commandline args and decide what to do
fn main() -> Result<(), ErrorCode> {
    let args: Vec<String> = env::args().collect();
    let out_stream = &mut std::io::stdout();
    let err_stream = &mut std::io::stderr();
    match args.len() {
        1 => Repl::run_new(out_stream, err_stream).expect("Could not create Repl"),
        _ => interpret_files(&args[1..], out_stream, err_stream)?,
    }
    Ok(())
}
