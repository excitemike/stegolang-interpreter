//! data type for type solving constraints
use crate::{intern::interned::Interned, loc, sourceregion::SourceRegion, StringType};

use super::{
    freetypevars::FreeTypeVars,
    substitute::{ApplySubst, ApplySubstInPlace},
    traitstruct::TraitStructRef,
    Context, ITy, Substitutions, TiError, UnificationVarId,
};

pub(crate) type ICon<'a> = Interned<'a, Constraint<'a>>;

/// TODO: make these separate structs that we handle separately,
/// instead of switching on their type
#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub(crate) enum Constraint<'ctx> {
    /// expect the types to be equivalent
    Eq(ITy<'ctx>, ITy<'ctx>, Box<SourceRegion>),

    /// require there to be an implementation of `trait_struct`
    /// for the type `for_ty`
    Implementation {
        trait_struct: TraitStructRef<'ctx>,
        for_ty: ITy<'ctx>,
        source_region: Box<SourceRegion>,
    },

    /// expect member `name` of type `t0` to be of type `t1`
    Member {
        obj_ty: ITy<'ctx>,
        field_ty: ITy<'ctx>,
        name: StringType,
        source_region: Box<SourceRegion>,
    },

    /// equate a type with a method
    Method {
        for_ty: ITy<'ctx>,
        method_ty: ITy<'ctx>,
        trait_: TraitStructRef<'ctx>,
        name: StringType,
        source_region: Box<SourceRegion>,
    },
}

impl<'a> std::fmt::Display for Constraint<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Constraint::Eq(a, b, _) => write!(f, "{a} = {b}"),
            Constraint::Implementation {
                trait_struct,
                for_ty,
                source_region: _,
            } => write!(f, "{for_ty} implements {trait_struct}"),
            Constraint::Member {
                obj_ty,
                field_ty,
                name,
                source_region: _,
            } => write!(f, "{obj_ty}.{name} = {field_ty}"),
            Constraint::Method {
                for_ty,
                method_ty,
                trait_,
                name,
                source_region: _,
            } => write!(f, "({for_ty} as {trait_}).{name} = {method_ty}"),
        }
    }
}

impl<'a> ApplySubst<'a> for Constraint<'a> {
    fn apply_subst(
        &self,
        substitutions: &Substitutions<'a>,
        ctx: &Context<'a>,
    ) -> super::TiResult<Option<Self>> {
        match self {
            Constraint::Eq(a, b, cons_source_region) => {
                if let Some((a2, b2)) = (*a, *b).apply_subst(substitutions, ctx)? {
                    Ok(Some(Constraint::Eq(a2, b2, cons_source_region.clone())))
                } else {
                    Ok(None)
                }
            }
            Constraint::Implementation {
                trait_struct,
                for_ty,
                source_region,
            } => {
                let tr2 = trait_struct.apply_subst(substitutions, ctx)?;
                let for2 = for_ty.apply_subst(substitutions, ctx)?;
                if tr2.is_some() || for2.is_some() {
                    let trait_struct = tr2.unwrap_or(trait_struct);
                    let for_ty = for2.unwrap_or(*for_ty);
                    Ok(Some(Constraint::Implementation {
                        trait_struct,
                        for_ty,
                        source_region: Box::clone(source_region),
                    }))
                } else {
                    Ok(None)
                }
            }
            Constraint::Member {
                obj_ty,
                field_ty,
                name,
                source_region,
            } => {
                if let Some((obj_ty, field_ty)) =
                    (*obj_ty, *field_ty).apply_subst(substitutions, ctx)?
                {
                    Ok(Some(Constraint::Member {
                        obj_ty,
                        field_ty,
                        name: name.clone(),
                        source_region: source_region.clone(),
                    }))
                } else {
                    Ok(None)
                }
            }
            Constraint::Method {
                for_ty,
                method_ty,
                trait_,
                name,
                source_region,
            } => {
                let for2 = for_ty.apply_subst(substitutions, ctx)?;
                let method2 = method_ty.apply_subst(substitutions, ctx)?;
                let updated_trait = trait_.apply_subst(substitutions, ctx)?;
                if for2.is_some() || method2.is_some() || updated_trait.is_some() {
                    let for_ty = for2.unwrap_or(*for_ty);
                    let method_ty = method2.unwrap_or(*method_ty);
                    let trait_ = updated_trait.unwrap_or(*trait_);
                    Ok(Some(Constraint::Method {
                        for_ty,
                        method_ty,
                        trait_,
                        name: name.clone(),
                        source_region: source_region.clone(),
                    }))
                } else {
                    Ok(None)
                }
            }
        }
    }
}

impl<'a> ApplySubstInPlace<'a> for Constraint<'a> {
    fn apply_subst_in_place(
        &mut self,
        substitutions: &super::Substitutions<'a>,
        ctx: &super::Context<'a>,
    ) -> super::TiResult<bool> {
        match self {
            Constraint::Eq(a, b, _) => {
                let update_a = a.apply_subst(substitutions, ctx)?;
                let update_b = b.apply_subst(substitutions, ctx)?;
                if update_a.is_some() || update_b.is_some() {
                    *a = update_a.unwrap_or(*a);
                    *b = update_a.unwrap_or(*b);
                    Ok(true)
                } else {
                    Ok(false)
                }
            }
            Constraint::Implementation {
                trait_struct,
                for_ty,
                source_region: _,
            } => {
                let tr2 = trait_struct.apply_subst(substitutions, ctx)?;
                let for2 = for_ty.apply_subst(substitutions, ctx)?;
                if tr2.is_some() || for2.is_some() {
                    *trait_struct = tr2.unwrap_or(trait_struct);
                    *for_ty = for2.unwrap_or(*for_ty);
                    Ok(true)
                } else {
                    Ok(false)
                }
            }
            Constraint::Member {
                obj_ty,
                field_ty,
                name: _,
                source_region: _,
            } => {
                let obj_update = obj_ty.apply_subst(substitutions, ctx)?;
                let field_update = field_ty.apply_subst(substitutions, ctx)?;
                if obj_update.is_some() || field_update.is_some() {
                    *obj_ty = obj_update.unwrap_or(*obj_ty);
                    *field_ty = field_update.unwrap_or(*field_ty);
                    Ok(true)
                } else {
                    Ok(false)
                }
            }
            Constraint::Method {
                for_ty,
                method_ty,
                trait_,
                name: _,
                source_region: _,
            } => {
                let mut result = false;
                if let Some(for2) = for_ty.apply_subst(substitutions, ctx)? {
                    result = true;
                    *for_ty = for2;
                }
                if let Some(method2) = method_ty.apply_subst(substitutions, ctx)? {
                    result = true;
                    *method_ty = method2;
                }
                if let Some(tr2) = trait_.apply_subst(substitutions, ctx)? {
                    result = true;
                    *trait_ = tr2;
                }
                Ok(result)
            }
        }
    }
}

impl TiError {
    pub(crate) fn from_constraint(c: &Constraint<'_>) -> Self {
        match c {
            Constraint::Eq(a, b, sr) => TiError::new(
                loc::err::unable_to_unify_types(&a.to_string(), &b.to_string()),
                SourceRegion::clone(&**sr),
            ),
            // TODO: check for unification variables and give better error about unresolved types
            Constraint::Implementation {
                trait_struct,
                for_ty,
                source_region,
            } => TiError::new(
                loc::err::no_implementation_found(&trait_struct.to_string(), &for_ty.to_string()),
                SourceRegion::clone(&**source_region),
            ),
            // TODO: check for unification variables and give better error about unresolved types
            Constraint::Member {
                obj_ty,
                field_ty: _,
                name,
                source_region,
            } => TiError::new(
                loc::err::no_member_for_type(&obj_ty.to_string(), name),
                SourceRegion::clone(&**source_region),
            ),
            Constraint::Method {
                for_ty,
                method_ty: _,
                trait_,
                name: _,
                source_region,
            } => TiError::new(
                loc::err::no_implementation_found(&trait_.to_string(), &for_ty.to_string()),
                SourceRegion::clone(&**source_region),
            ),
        }
    }
}

impl<'a> FreeTypeVars<'a> for Constraint<'a> {
    fn free_unification_variables(
        &self,
        exclude: &[UnificationVarId],
        collection: &mut Vec<UnificationVarId>,
    ) {
        match self {
            Constraint::Eq(a, b, _) => {
                a.free_unification_variables(exclude, collection);
                b.free_unification_variables(exclude, collection);
            }
            Constraint::Implementation {
                trait_struct,
                for_ty,
                source_region: _,
            } => {
                let mut new_exclude = Vec::with_capacity(exclude.len() + 1);
                new_exclude.extend_from_slice(exclude);
                new_exclude.push(trait_struct.self_var);
                trait_struct.free_unification_variables(&new_exclude, collection);
                for_ty.free_unification_variables(&new_exclude, collection);
            }
            Constraint::Member {
                obj_ty,
                field_ty,
                name: _,
                source_region: _,
            } => {
                obj_ty.free_unification_variables(exclude, collection);
                field_ty.free_unification_variables(exclude, collection);
            }
            Constraint::Method {
                for_ty,
                method_ty,
                trait_,
                name: _,
                source_region: _,
            } => {
                trait_.free_unification_variables(exclude, collection);
                for_ty.free_unification_variables(exclude, collection);
                method_ty.free_unification_variables(exclude, collection);
            }
        }
    }

    fn has_any_free_unification_variables(&self, _exclude: &[UnificationVarId]) -> bool {
        todo!()
    }

    fn has_free_type_var(&self, _search_id: UnificationVarId) -> bool {
        todo!()
    }

    fn get_type_parameters(
        &self,
        exclude: &[UnificationVarId],
        collection: &mut Vec<UnificationVarId>,
    ) {
        match self {
            Constraint::Eq(a, b, _) => {
                a.get_type_parameters(exclude, collection);
                b.get_type_parameters(exclude, collection);
            }
            Constraint::Implementation {
                trait_struct,
                for_ty,
                source_region: _,
            } => {
                let mut new_exclude = Vec::with_capacity(exclude.len() + 1);
                new_exclude.extend_from_slice(exclude);
                new_exclude.push(trait_struct.self_var);
                trait_struct.get_type_parameters(&new_exclude, collection);
                for_ty.get_type_parameters(&new_exclude, collection);
            }
            Constraint::Member {
                obj_ty,
                field_ty,
                name: _,
                source_region: _,
            } => {
                obj_ty.get_type_parameters(exclude, collection);
                field_ty.get_type_parameters(exclude, collection);
            }
            Constraint::Method {
                for_ty,
                method_ty,
                trait_: _,
                name: _,
                source_region: _,
            } => {
                for_ty.get_type_parameters(exclude, collection);
                method_ty.get_type_parameters(exclude, collection);
            }
        }
    }

    fn has_any_type_parameters(&self, exclude: &[UnificationVarId]) -> bool {
        match self {
            Constraint::Eq(_, _, _) => todo!(),
            Constraint::Implementation {
                trait_struct,
                for_ty,
                source_region: _,
            } => {
                trait_struct.has_any_type_parameters(exclude)
                    || for_ty.has_any_type_parameters(exclude)
            }
            Constraint::Member {
                obj_ty: _,
                field_ty: _,
                name: _,
                source_region: _,
            } => todo!(),
            Constraint::Method {
                for_ty: _,
                method_ty: _,
                trait_: _,
                name: _,
                source_region: _,
            } => todo!(),
        }
    }

    fn has_type_parameter(&self, _search_id: UnificationVarId) -> bool {
        todo!()
    }
}
