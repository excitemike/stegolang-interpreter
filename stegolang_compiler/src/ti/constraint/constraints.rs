use rustc_hash::FxHashSet;

use crate::{ti::ITy, sourceregion::SourceRegion};

/// struct for tracking constraints, organized by type
pub(crate) struct Constraints<'a> {
    /// equality constraints
    equality: FxHashSet<(ITy<'a>, ITy<'a>, SourceRegion)>,

    /// generalized equality constraints awaiting specialization
    equality_schemes: FxHashSet<(ITy<'a>, ITy<'a>, SourceRegion)>,
    
    /// implementation constraints
    implementation: FxHashSet<(TraitStructRef<'ctx>, ITy<'a>, SourceRegion)>,
    
    /// generalized implementation constraints awaiting specialization
    implementation_schemes: FxHashSet<(TraitStructRef<'ctx>, ITy<'a>, SourceRegion)>,

    /// method constraints
    method: FxHashSet<(ITy<'a>, ITy<'a>, SourceRegion)>,

    /// generalized equality constraints awaiting specialization
    method_schemes: FxHashSet<(ITy<'a>, ITy<'a>, SourceRegion)>,
}
