//! Use this module to perform our extended version of Hindey-Milner type inference.
//! The end result will be a typed version of the syntax tree and residual constraints.
//!
//! The algorithm is more correctly called "Damas-Milner". Hindley-Milner is the type system.
//! Also known as "Algorithm W".
//! But of course that was only a starting point that weve deviated from
use crate::sourceregion::SourceRegion;

use super::substitute::ApplySubst;
use super::traitstruct::{TraitScheme, TraitSchemeRef, TraitStruct};
use super::{
    err, Context, FnType, FreeTypeVars, ITy, RefCell, Substitutions, TiError, TiResult,
    TraitStructRef, Type, UnificationVarId,
};

impl<'ctx> Context<'ctx> {
    /// Apply substitutions to environment but only in the most-recently-pushed scope.
    /// This let is how we prevent the inferred argument types at one function
    /// call site from affecting the types at other sites.
    /// Returns whether anything changed.
    /// TODO: track free type vars as things are added and replaced instead of
    /// walking the whole stack to find them later
    pub(crate) fn apply_substitutions_in_current_scope(
        &self,
        substitutions: &Substitutions<'ctx>,
    ) -> TiResult<bool> {
        if substitutions.is_empty() {
            return Ok(false);
        }
        let mut any_changed = false;
        let scope = self.scopes.borrow();
        let vars = scope.get_all_at_current_depth();
        for (_, v) in vars {
            let subst_result: Option<ITy<'ctx>> =
                RefCell::borrow(&v).apply_subst(substitutions, self)?;
            if let Some(ty) = subst_result {
                let mut guard = v.borrow_mut();
                *guard = ty;
                any_changed = true;
            }
        }
        Ok(any_changed)
    }

    /// create the substitution that is the same as applying `inner`, then `outer`
    pub(crate) fn compose(
        &self,
        mut outer: Substitutions<'ctx>,
        inner: Substitutions<'ctx>,
    ) -> TiResult<Substitutions<'ctx>> {
        if inner.is_empty() {
            return Ok(outer);
        }
        if outer.is_empty() {
            return Ok(inner);
        }
        for (k, v) in inner.iter() {
            let v = v.apply_subst(&outer, self)?.unwrap_or(*v);
            outer.insert(*k, v);
        }
        for (k, v) in inner.iter() {
            outer.insert_if_missing(*k, *v);
        }
        Ok(outer)
    }

    /// push any free unification variables from scope into the provided container
    pub(crate) fn free_vars(
        &self,
        exclude: &[UnificationVarId],
        collection: &mut Vec<UnificationVarId>,
    ) {
        // TODO: could this safely look only in the current scope?
        let scope = self.scopes.borrow();
        scope.for_each_value(|v| v.free_unification_variables(exclude, collection));
    }

    /// record substitutions and/or residual constraints based on the
    /// relationship between the types.
    /// If both are unification variables, it prefers to eliminate the first arg
    pub(crate) fn unify_eq(
        &self,
        t0: ITy<'ctx>,
        t1: ITy<'ctx>,
        source_region: &SourceRegion,
    ) -> TiResult<Substitutions<'ctx>> {
        // types already equal
        if t0 == t1 {
            return Ok(Substitutions::new());
        }
        // TODO: try to simplify types before continuing
        match (t0.inner(), t1.inner()) {
            // types already equal
            (l, r) if l == r => Ok(Substitutions::new()),
            (Type::TypeParameter { .. }, _) | (_, Type::TypeParameter { .. }) => Err(TiError::new(
                err::internal::unifying_type_param(),
                source_region.clone(),
            )),
            (Type::F(fn0), Type::F(fn1)) => self.unify_f(fn0, fn1, source_region),
            (Type::F(f), _) if f.params.is_empty() => {
                // nullary fn - TODO: I feel like I should disallow these
                self.unify_eq(t1, f.result, source_region)
            }
            (_, Type::F(f)) if f.params.is_empty() => {
                // nullary fn - TODO: I feel like I should disallow these
                self.unify_eq(t0, f.result, source_region)
            }
            (Type::UnificationVar { uniq_id, .. }, _) => {
                if t1.has_free_type_var(*uniq_id) {
                    todo!("handle types referring to themselves");
                } else {
                    Ok(Substitutions::single(*uniq_id, t1))
                }
            }
            (_, Type::UnificationVar { uniq_id, .. }) => {
                if t0.has_free_type_var(*uniq_id) {
                    todo!("handle types referring to themselves");
                } else {
                    Ok(Substitutions::single(*uniq_id, t0))
                }
            }
            (
                Type::TraitScheme(ts0 @ TraitScheme { .. }),
                Type::TraitScheme(ts1 @ TraitScheme { .. }),
            ) => self.unify_tr_scheme(ts0, ts1, source_region, t0, t1),
            (
                Type::TraitStruct(ts0 @ TraitStruct { .. }),
                Type::TraitStruct(ts1 @ TraitStruct { .. }),
            ) => self.unify_traits(ts0, ts1, source_region),
            (
                Type::Dict {
                    key: key0,
                    value: value0,
                },
                Type::Dict {
                    key: key1,
                    value: value1,
                },
            ) => {
                let s1 = self.unify_eq(*key0, *key1, source_region)?;
                let s2 = self.unify_eq(*value0, *value1, source_region)?;
                let s = self.compose(s2, s1)?;
                Ok(s)
            }
            (Type::Set(t0), Type::Set(t1)) | (Type::Type(t0), Type::Type(t1)) => {
                self.unify_eq(*t0, *t1, source_region)
            }
            (Type::Tuple(v0), Type::Tuple(v1)) if v0.len() == v1.len() => {
                let mut s = Substitutions::new();
                for (l, r) in v0.iter().zip(v1.iter()) {
                    let s2 = self.unify_eq(*l, *r, source_region)?;
                    s = self.compose(s2, s)?;
                }
                Ok(s)
            }
            // anything else is either a type error or a case I forgot to handle
            (left, right) => Err(TiError::new(
                err::type_mismatch(&left.to_string(), &right.to_string()),
                source_region.clone(),
            )),
        }
    }

    /// unification of function types
    pub(super) fn unify_f(
        &self,
        fn0: &FnType<'ctx>,
        fn1: &FnType<'ctx>,
        source_region: &SourceRegion,
    ) -> TiResult<Substitutions<'ctx>> {
        let mut s1 = Substitutions::new();
        for (a, b) in fn0.params.iter().zip(fn1.params.iter()) {
            let a = a.apply_subst(&s1, self)?.unwrap_or(*a);
            let b = b.apply_subst(&s1, self)?.unwrap_or(*b);
            let s2 = self.unify_eq(a, b, source_region)?;
            s1 = self.compose(s2, s1)?;
        }

        let num_consumed_params = std::cmp::min(fn0.params.len(), fn1.params.len());
        let num_unconsumed0 = fn0.params.len() - num_consumed_params;
        let num_unconsumed1 = fn1.params.len() - num_consumed_params;

        if num_unconsumed0 > 0 {
            let t2 = self.mk_fn_type_from_iter(
                fn0.params[num_consumed_params..].iter().copied(),
                fn0.result,
            );
            let s2 = self.unify_eq(t2, fn1.result, source_region)?;
            self.compose(s2, s1)
        } else if num_unconsumed1 > 0 {
            let t2 = self.mk_fn_type_from_iter(
                fn1.params[num_consumed_params..].iter().copied(),
                fn1.result,
            );
            let s2 = self.unify_eq(fn0.result, t2, source_region)?;
            self.compose(s2, s1)
        } else {
            let mut r0 = fn0.result;
            r0 = r0.apply_subst(&s1, self)?.unwrap_or(r0);
            let mut r1 = fn1.result;
            r1 = r1.apply_subst(&s1, self)?.unwrap_or(r1);
            let s2 = self.unify_eq(r0, r1, source_region)?;
            self.compose(s2, s1)
        }
    }

    /// unification of trait schemes
    fn unify_tr_scheme(
        &self,
        ts0: TraitSchemeRef<'ctx>,
        ts1: TraitSchemeRef<'ctx>,
        source_region: &SourceRegion,
        t0: ITy<'ctx>,
        t1: ITy<'ctx>,
    ) -> TiResult<Substitutions<'ctx>> {
        if !ts0.ty_pars.is_empty() || !ts1.ty_pars.is_empty() {
            todo!("use ty_pars");
        }
        let mut substitutions = Substitutions::new();
        let mismatch = (ts0.self_var != ts1.self_var)
            || (ts0.trait_name != ts1.trait_name)
            || (ts0.assoc_tys.len() != ts1.assoc_tys.len())
            || (ts0.methods.len() != ts1.methods.len())
            || {
                let mut mismatch = false;
                'outer: for (a, b) in [
                    (&ts0.assoc_tys, &ts1.assoc_tys),
                    (&ts0.methods, &ts1.methods),
                ] {
                    for ((k0, v0), (k1, v1)) in a.iter().zip(b.iter()) {
                        if k0 != k1 {
                            mismatch = true;
                            break 'outer;
                        }
                        let s2 = self.unify_eq(*v0, *v1, source_region)?;
                        substitutions = self.compose(s2, substitutions)?;
                    }
                }
                mismatch
            };
        if mismatch {
            Err(TiError::new(
                err::type_mismatch(&t0.to_string(), &t1.to_string()),
                source_region.clone(),
            ))
        } else {
            Ok(substitutions)
        }
    }

    /// unification of traits
    pub(crate) fn unify_traits(
        &self,
        ts0: TraitStructRef<'ctx>,
        ts1: TraitStructRef<'ctx>,
        source_region: &SourceRegion,
    ) -> TiResult<Substitutions<'ctx>> {
        let mut substitutions = Substitutions::new();
        // typar_choices is checked once we know it is not a mismatch
        let mismatch = (ts0.self_var != ts1.self_var)
            || (ts0.trait_name != ts1.trait_name)
            || (ts0.typar_choices.len() != ts1.typar_choices.len())
            || (ts0.assoc_tys.len() != ts1.assoc_tys.len())
            || (ts0.methods.len() != ts1.methods.len())
            || {
                let mut mismatch = false;
                'outer: for (a, b) in [
                    (&ts0.assoc_tys, &ts1.assoc_tys),
                    (&ts0.methods, &ts1.methods),
                ] {
                    for ((k0, v0), (k1, v1)) in a.iter().zip(b.iter()) {
                        if k0 != k1 {
                            mismatch = true;
                            break 'outer;
                        }
                        let s2 = self.unify_eq(*v0, *v1, source_region)?;
                        substitutions = self.compose(s2, substitutions)?;
                    }
                }
                mismatch
            };
        if mismatch {
            Err(TiError::new(
                err::type_mismatch(&ts0.to_string(), &ts1.to_string()),
                source_region.clone(),
            ))
        } else {
            for (ty0, ty1) in ts0.typar_choices.iter().zip(ts1.typar_choices.iter()) {
                let s = self.unify_eq(*ty0, *ty1, source_region)?;
                substitutions = self.compose(s, substitutions)?;
            }
            Ok(substitutions)
        }
    }
}

#[test]
fn sub_compose() {
    use crate::ti::arenas::Arenas;
    let arenas = Arenas::new();
    let ctx = Context::new(&arenas);
    let sr_none = crate::make_source_region!();
    let a = ctx.mk_unification_var(&sr_none, "test").unwrap();
    let b = ctx.mk_unification_var(&sr_none, "test").unwrap();
    let c = ctx.mk_unification_var(&sr_none, "test").unwrap();
    let a_id = a.expect_unification_var(&sr_none.clone()).unwrap();
    let b_id = b.expect_unification_var(&sr_none.clone()).unwrap();
    let c_id = c.expect_unification_var(&sr_none.clone()).unwrap();
    let s_b_to_a = Substitutions::single(b_id, a);
    let s_c_to_b = Substitutions::single(c_id, b);
    let s_c_to_a = ctx.compose(s_b_to_a, s_c_to_b).unwrap();
    drop(ctx);
    assert!(s_c_to_a.get(a_id).is_none());
    assert_eq!(s_c_to_a.get(b_id).unwrap(), a);
    assert_eq!(s_c_to_a.get(c_id).unwrap(), a);
}
