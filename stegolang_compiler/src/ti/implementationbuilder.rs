use super::{
    loc, traitstruct::TraitStructRef, ApplySubst, BTreeMap, Context, ITy, Implementation, Rc,
    SourceRegion, StringType, Substitutions, TiError, TiResult,
};
use crate::fntypes::fnfamily::FnFamily;

pub(crate) struct ImplementationBuilder<'builder, 'ctx> {
    ctx: &'builder Context<'ctx>,
    implementation_params: Vec<(StringType, ITy<'ctx>)>,
    implemented_for: ITy<'ctx>,
    implemented_trait: TraitStructRef<'ctx>,
    member_types: BTreeMap<StringType, ITy<'ctx>>,
    member_functions: BTreeMap<StringType, (ITy<'ctx>, Rc<FnFamily<'ctx>>)>,
    source_region: SourceRegion,
}
impl<'builder, 'ctx> ImplementationBuilder<'builder, 'ctx> {
    /// set an associated type in the implementation
    pub(crate) fn assoc_type<T>(mut self, name: T, ty: ITy<'ctx>) -> Self
    where
        T: Into<StringType>,
    {
        self.member_types.insert(name.into(), ty);
        self
    }

    /// type parameter on the implementation scheme itself
    pub(crate) fn implementation_param<T>(mut self, imp_par_name: T, imp_par_ty: ITy<'ctx>) -> Self
    where
        T: Into<StringType>,
    {
        self.implementation_params
            .push((imp_par_name.into(), imp_par_ty));
        self
    }

    /// add a method
    pub(crate) fn method<T>(mut self, name: T, fn_fam: FnFamily<'ctx>) -> TiResult<Self>
    where
        T: Into<StringType>,
    {
        let name = name.into();
        if let Some(ty) = self.implemented_trait.find_member(&name) {
            self.member_functions.insert(name, (ty, Rc::new(fn_fam)));
            Ok(self)
        } else {
            Err(TiError::new(
                loc::err::missing_method(name.as_ref(), &self.implemented_trait.to_string()),
                self.source_region,
            ))
        }
    }

    /// finish
    pub(crate) fn finish(self) -> TiResult<()> {
        // don't leak the self placeholder
        let mut subs = Substitutions::single(self.implemented_trait.self_var, self.implemented_for);
        // match associated types to their placeholders
        for (name, placeholder) in &self.implemented_trait.assoc_tys {
            match self.member_types.get(name) {
                Some(replacement) => {
                    let additional_subs =
                        self.ctx
                            .unify_eq(*placeholder, *replacement, &self.source_region)?;
                    subs = self.ctx.compose(subs, additional_subs)?;
                }
                None => {
                    return Err(TiError::new(
                        loc::err::missing_associated_type(
                            &self.implemented_trait.to_string(),
                            &self.implemented_for.to_string(),
                            name,
                        ),
                        self.source_region,
                    ))
                }
            }
        }
        let subs = subs;
        let member_types = self
            .member_types
            .apply_subst(&subs, self.ctx)?
            .unwrap_or(self.member_types);
        let member_functions = self
            .member_functions
            .apply_subst(&subs, self.ctx)?
            .unwrap_or(self.member_functions);
        let implementation = self.ctx.mk_implementation(Implementation {
            implemented_for: self.implemented_for,
            implemented_trait: self.implemented_trait,
            member_types,
            member_functions,
            source_region: self.source_region,
        });
        let n_params = self.implementation_params.len();
        self.ctx.alloc_impl_scheme(
            self.implementation_params.into_boxed_slice(),
            self.implemented_for,
            implementation,
        );
        // if it doens't have any type params, we can store it now instead of
        // a noop instantiation later
        if n_params == 0 {
            self.ctx.store_implementation(
                self.implemented_for,
                self.implemented_trait,
                implementation,
            );
        }
        // TODO: otherwise, store a scheme

        Ok(())
    }
    /// begin creating implementation
    pub(super) fn new(
        ctx: &'builder Context<'ctx>,
        implemented_trait: TraitStructRef<'ctx>,
        for_type: ITy<'ctx>,
        source_region: SourceRegion,
    ) -> Self {
        Self {
            ctx,
            implementation_params: Vec::default(),
            implemented_for: for_type,
            implemented_trait,
            member_types: BTreeMap::default(),
            member_functions: BTreeMap::default(),
            source_region,
        }
    }
}
