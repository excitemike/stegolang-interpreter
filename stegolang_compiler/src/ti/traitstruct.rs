use super::{freetypevars::FreeTypeVars, Context, ITy, TiResult, UnificationVarId};
use crate::{
    loc,
    sourceregion::SourceRegion,
    ti::{substitute::ApplySubst, substitutions::Substitutions, TiError},
    StringType,
};
use std::collections::BTreeMap;

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) struct TraitScheme<'ctx> {
    /// identifiers used for the type parameters
    pub(crate) ty_pars: Box<[(StringType, UnificationVarId)]>,
    /// type variable for "Self"
    pub(crate) self_var: UnificationVarId,
    /// name of the trait, for debug/error messages
    pub(crate) trait_name: StringType,
    /// associated types and their placeholders
    pub(crate) assoc_tys: BTreeMap<StringType, ITy<'ctx>>,
    /// associated method types
    pub(crate) methods: BTreeMap<StringType, ITy<'ctx>>,
}
pub(crate) type TraitSchemeRef<'ctx> = &'ctx TraitScheme<'ctx>;
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) struct TraitStruct<'ctx> {
    /// choices made for the type parameters
    pub(crate) typar_choices: Box<[ITy<'ctx>]>,
    /// placeholder for Self
    pub(crate) self_var: UnificationVarId,
    /// name of the trait, for debug/error messages
    pub(crate) trait_name: StringType,
    /// associated types and their placeholders
    pub(crate) assoc_tys: BTreeMap<StringType, ITy<'ctx>>,
    /// associated method types.
    pub(crate) methods: BTreeMap<StringType, ITy<'ctx>>,
}
pub(crate) type TraitStructRef<'ctx> = &'ctx TraitStruct<'ctx>;

impl<'ctx> std::fmt::Display for TraitStruct<'ctx> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.typar_choices.is_empty() {
            write!(f, "(trait {})", self.trait_name)
        } else {
            write!(f, "(trait {}", self.trait_name)?;
            for ty in &*self.typar_choices {
                write!(f, " {ty}")?;
            }
            ')'.fmt(f)
        }
    }
}

impl<'ctx> FreeTypeVars<'ctx> for TraitStruct<'ctx> {
    // TODO: traits need to exclude their self type and associated types
    fn free_unification_variables(
        &self,
        exclude: &[UnificationVarId],
        collection: &mut Vec<UnificationVarId>,
    ) {
        let TraitStruct {
            typar_choices,
            self_var: _,
            trait_name: _,
            assoc_tys,
            methods,
        } = self;
        for ty in typar_choices.iter() {
            ty.free_unification_variables(exclude, collection);
        }
        for ty in assoc_tys.values() {
            ty.free_unification_variables(exclude, collection);
        }
        for ty in methods.values() {
            ty.free_unification_variables(exclude, collection);
        }
    }
    // TODO: traits need to exclude their self type and associated types
    fn has_any_free_unification_variables(&self, exclude: &[UnificationVarId]) -> bool {
        let TraitStruct {
            typar_choices,
            self_var: _,
            trait_name: _,
            assoc_tys,
            methods,
        } = self;
        let check = |ty: &ITy<'ctx>| -> bool { ty.has_any_free_unification_variables(exclude) };
        typar_choices.iter().any(check)
            || assoc_tys.values().any(check)
            || methods.values().any(check)
    }
    // TODO: traits need to exclude their self type and associated types
    fn has_free_type_var(&self, search_id: UnificationVarId) -> bool {
        let TraitStruct {
            typar_choices,
            self_var: _,
            trait_name: _,
            assoc_tys,
            methods,
        } = self;
        let check = |ty: &ITy<'ctx>| -> bool { ty.has_free_type_var(search_id) };
        typar_choices.iter().any(check)
            || assoc_tys.values().any(check)
            || methods.values().any(check)
    }

    fn get_type_parameters(
        &self,
        exclude: &[UnificationVarId],
        collection: &mut Vec<UnificationVarId>,
    ) {
        let TraitStruct {
            typar_choices,
            self_var: _,
            trait_name: _,
            assoc_tys,
            methods,
        } = self;
        for ty in typar_choices.iter() {
            ty.get_type_parameters(exclude, collection);
        }
        for ty in assoc_tys.values() {
            ty.get_type_parameters(exclude, collection);
        }
        for ty in methods.values() {
            ty.get_type_parameters(exclude, collection);
        }
    }

    fn has_any_type_parameters(&self, exclude: &[UnificationVarId]) -> bool {
        let TraitStruct {
            typar_choices,
            self_var: _,
            trait_name: _,
            assoc_tys,
            methods,
        } = self;
        let check = |ty: &ITy<'ctx>| -> bool { ty.has_any_type_parameters(exclude) };
        typar_choices.iter().any(check)
            || assoc_tys.values().any(check)
            || methods.values().any(check)
    }

    fn has_type_parameter(&self, _search_id: UnificationVarId) -> bool {
        todo!()
    }
}

impl<'ctx> TraitScheme<'ctx> {
    /// If this has type parameters, replace them with fresh unification variables.
    /// It returns the substitutions, too, in case there is something else you
    /// need to update.
    pub(crate) fn instantiate(
        &self,
        ctx: &Context<'ctx>,
        source_region: &SourceRegion,
    ) -> TiResult<(Substitutions<'ctx>, TraitStructRef<'ctx>)> {
        if self.ty_pars.is_empty() {
            Ok((
                Substitutions::new(),
                ctx.alloc_trait(
                    Box::new([]),
                    self.self_var,
                    self.trait_name.clone(),
                    self.assoc_tys.clone(),
                    self.methods.clone(),
                ),
            ))
        } else {
            let mut substitutions = Substitutions::new();
            let mut typar_choices = Vec::with_capacity(self.ty_pars.len());
            for (_, trait_type_par) in &*self.ty_pars {
                let replacement = ctx
                    .mk_unification_var(source_region, "replacement for type par in TraitScheme")?;
                typar_choices.push(replacement);
                substitutions.insert(*trait_type_par, replacement);
            }
            ctx.instantiate_constraints(&substitutions)?;
            let typar_choices = typar_choices.into_boxed_slice();
            let trait_ = ctx.alloc_trait(
                typar_choices,
                self.self_var,
                self.trait_name.clone(),
                self.assoc_tys
                    .apply_subst(&substitutions, ctx)?
                    .unwrap_or_else(|| self.assoc_tys.clone()),
                self.methods
                    .apply_subst(&substitutions, ctx)?
                    .unwrap_or_else(|| self.methods.clone()),
            );
            Ok((substitutions, trait_))
        }
    }
    /// create a particular instance of the generic trait
    pub(crate) fn instantiate_with(
        &self,
        ctx: &Context<'ctx>,
        type_parameters: &[ITy<'ctx>],
        source_region: &SourceRegion,
    ) -> TiResult<TraitStructRef<'ctx>> {
        let num_required = self.ty_pars.len();
        let num_provided = type_parameters.len();
        match num_required.cmp(&num_provided) {
            std::cmp::Ordering::Equal => {
                if 0 == num_required {
                    Ok(ctx.alloc_trait(
                        Box::new([]),
                        self.self_var,
                        self.trait_name.clone(),
                        self.assoc_tys.clone(),
                        self.methods.clone(),
                    ))
                } else {
                    let mut substitutions = Substitutions::new();
                    let mut trait_scheme_par_choices = Vec::with_capacity(self.ty_pars.len());
                    // replace placeholder vars
                    for ((_, trait_scheme_par), &replacement) in
                        self.ty_pars.iter().zip(type_parameters)
                    {
                        trait_scheme_par_choices.push(replacement);
                        substitutions.insert(*trait_scheme_par, replacement);
                    }
                    ctx.instantiate_constraints(&substitutions)?;
                    // TODO: replace self var
                    let trait_scheme_par_choices = trait_scheme_par_choices.into_boxed_slice();
                    Ok(ctx.alloc_trait(
                        trait_scheme_par_choices,
                        self.self_var,
                        self.trait_name.clone(),
                        self.assoc_tys
                            .apply_subst(&substitutions, ctx)?
                            .unwrap_or_else(|| self.assoc_tys.clone()),
                        self.methods
                            .apply_subst(&substitutions, ctx)?
                            .unwrap_or_else(|| self.methods.clone()),
                    ))
                }
            }
            std::cmp::Ordering::Less => Err(TiError::new(
                loc::err::too_many_type_parameters(num_required, num_provided),
                source_region.clone(),
            )),
            std::cmp::Ordering::Greater => Err(TiError::new(
                loc::err::too_few_type_parameters(num_required, num_provided),
                source_region.clone(),
            )),
        }
    }
    /// instantiate the trait scheme using substitutions already determined
    pub(crate) fn instantiate_exactly(
        &self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<TraitStructRef<'ctx>> {
        if self.ty_pars.is_empty() {
            Ok(ctx.alloc_trait(
                Box::new([]),
                self.self_var,
                self.trait_name.clone(),
                self.assoc_tys.clone(),
                self.methods.clone(),
            ))
        } else {
            let mut trait_scheme_par_choices = Vec::with_capacity(self.ty_pars.len());
            for (name, id) in &*self.ty_pars {
                if let Some(replacement) = substitutions.get(*id) {
                    trait_scheme_par_choices.push(replacement);
                } else {
                    // TODO: error message
                    panic!("no choice for type par {name}");
                }
            }
            let assoc_tys = self
                .assoc_tys
                .apply_subst(substitutions, ctx)?
                .unwrap_or_else(|| self.assoc_tys.clone());
            let methods = self
                .methods
                .apply_subst(substitutions, ctx)?
                .unwrap_or_else(|| self.methods.clone());
            Ok(ctx.alloc_trait(
                trait_scheme_par_choices.into_boxed_slice(),
                self.self_var,
                self.trait_name.clone(),
                assoc_tys,
                methods,
            ))
        }
    }
}

impl<'ctx> std::fmt::Display for TraitScheme<'ctx> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.ty_pars.is_empty() {
            write!(f, "(trait {})", self.trait_name)
        } else {
            write!(f, "(trait {}", self.trait_name)?;
            for (name, _) in &*self.ty_pars {
                write!(f, " {name}")?;
            }
            ')'.fmt(f)
        }
    }
}

impl<'a> TraitStruct<'a> {
    /// if we instantiated `other`, could these unify?
    pub(crate) fn could_be_instance(&self, other: &TraitStruct<'a>) -> bool {
        if self.trait_name != other.trait_name {
            return false;
        }
        if self.assoc_tys.len() != other.assoc_tys.len() {
            return false;
        }
        if self.methods.len() != other.methods.len() {
            return false;
        }
        for (k, v1) in &self.assoc_tys {
            let Some(v2) = other.assoc_tys.get(k) else {
                return false;
            };
            if !v1.could_be_instance(*v2) {
                return false;
            }
        }
        for (k, v1) in &self.methods {
            let Some(v2) = other.methods.get(k) else {
                return false;
            };
            if !v1.could_be_instance(*v2) {
                return false;
            }
        }
        true
    }

    pub(crate) fn find_member(&self, member_name: &StringType) -> Option<ITy<'a>> {
        self.assoc_tys
            .get(member_name)
            .or_else(|| self.methods.get(member_name))
            .copied()
    }

    /// answers the question: is type inference complete on this item?
    pub(crate) fn is_final(&self) -> bool {
        !self.has_any_free_unification_variables(&[])
    }
}
