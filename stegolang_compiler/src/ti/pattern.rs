use super::{ITy, StringType, TiResult};
use crate::{pattern::StegoPattern, pattern::StegoPatternKind, typedvalue::TypedValue};

impl StegoPattern {
    /// get the bindings and values that result from destructuring using the pattern, or None if the pattern is refuted
    #[allow(clippy::unnecessary_wraps)] // will likely be fallible in the future
    pub(crate) fn destructure<'ctx>(
        &self,
        value: &TypedValue<'ctx>,
    ) -> TiResult<Option<impl Iterator<Item = (StringType, TypedValue<'ctx>)>>> {
        let mut bindings = Vec::with_capacity(1);
        match &self.kind {
            // capture: parameter is bound to the name directly
            StegoPatternKind::Capture(name) => {
                bindings.push((StringType::from(name.to_string()), value.clone()));
            }

            // no bindings in a literal pattern
            StegoPatternKind::Literal(_) => (),

            StegoPatternKind::Or(_) => todo!(),
            StegoPatternKind::Named(_, _) => todo!(),
        }
        Ok(Some(bindings.into_iter()))
    }

    /// get the bindings (names and types) that will result from destructuring using the pattern
    /// TODO: there are also substitutions possible here because it must also unify with the pattern
    /// For instance, the pattern can force it to be a enum variant or a tuple
    #[allow(clippy::unnecessary_wraps)] // will likely be fallible in the future
    pub(crate) fn destructure_type<'ctx>(
        &self,
        t: ITy<'ctx>,
    ) -> TiResult<impl Iterator<Item = (StringType, ITy<'ctx>)>> {
        let mut bindings = Vec::with_capacity(1);
        match &self.kind {
            // capture: parameter is bound to the name directly
            StegoPatternKind::Capture(name) => {
                bindings.push((name.clone(), t));
            }

            // no bindings in a literal pattern
            StegoPatternKind::Literal(_) => (),

            StegoPatternKind::Or(_) => todo!(),
            StegoPatternKind::Named(_, _) => todo!(),
        }
        Ok(bindings.into_iter())
    }

    /// get the binding names that will result from destructuring using the pattern
    #[allow(dead_code)] // TODO: use or delete
    #[allow(clippy::unnecessary_wraps)] // will likely be fallible in the future
    pub(crate) fn destructure_names(&self) -> TiResult<impl Iterator<Item = StringType>> {
        let mut bindings = Vec::with_capacity(1);
        match &self.kind {
            // capture: parameter is bound to the name directly
            StegoPatternKind::Capture(name) => {
                bindings.push(name.clone());
            }

            // no bindings in a literal pattern
            StegoPatternKind::Literal(_) => (),

            StegoPatternKind::Or(_) => todo!(),
            StegoPatternKind::Named(_, _) => todo!(),
        }
        Ok(bindings.into_iter())
    }
}
