use crate::{hlir::HlirNode, ti::Substitutions};

use super::{substitute::ApplySubstInPlace, Context, TiResult};

/// Generalize a type into a polytype - all the free type variables
/// could be different in the next instantiation.
///
/// # EXAMPLE
///
/// ```no_run
/// let identity = \x = x // let expressions generalize
/// println (identity 1)  // instantiated here so that we can use it with an int
/// println (identity "a")  // instantiated here so that we can use it with a string
/// ```
pub(crate) fn generalize<'a>(
    mut node: HlirNode<'a>,
    ctx: &Context<'a>,
) -> TiResult<(Substitutions<'a>, HlirNode<'a>)> {
    let (substitutions, type_scheme) = node.inferred_type.generalize(ctx, &node.source_region)?;
    if substitutions.is_empty() {
        return Ok((Substitutions::new(), node));
    }
    node.apply_subst_in_place(&substitutions, ctx)?;
    ctx.generalize_constraints(&substitutions)?;

    node.inferred_type = type_scheme;
    Ok((substitutions, node))
}
