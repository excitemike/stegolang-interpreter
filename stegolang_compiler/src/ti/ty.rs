#![allow(clippy::match_same_arms)]
use itertools::Itertools;

use super::{
    freetypevars::FreeTypeVars,
    substitute::ApplySubst,
    traitstruct::{TraitScheme, TraitSchemeRef, TraitStructRef},
    Context, Implementation, ImplementationRef, SchemeRef, Substitutions, TiError, TiResult,
    Variant,
};
use crate::{
    config,
    intern::interned::Interned,
    loc,
    sourceregion::SourceRegion,
    ti::{traitstruct::TraitStruct, Scheme},
    StringType,
};
use std::{borrow::Cow, collections::BTreeMap, hash::Hash};

pub(crate) type UnificationVarId = u32;

/// data for a function type
#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub(crate) struct FnType<'ctx> {
    pub(crate) params: Box<[ITy<'ctx>]>,
    pub(crate) result: ITy<'ctx>,
}
impl<'ctx> std::fmt::Display for FnType<'ctx> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "(Fn")?;

        for param in &*self.params {
            write!(f, " {param}")?;
        }
        write!(f, " {})", self.result)
    }
}

impl<'a> FnType<'a> {
    /// answers the question: is type inference complete on this item?
    pub(crate) fn is_final(&self) -> bool {
        self.params.iter().copied().all(Interned::is_final) && self.result.is_final()
    }
}

/// Represents a stegolang type
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
#[allow(clippy::enum_variant_names)]
pub(crate) enum Type<'ctx> {
    BigInt,
    Bool,
    Char,
    Dict {
        key: ITy<'ctx>,
        value: ITy<'ctx>,
    },

    /// refer to the element type of an iterable
    ElementOf(ITy<'ctx>, Box<SourceRegion>),

    /// possibly-partial, possibly-generic function
    /// TODO: FnType should smaller, boxed, or in a side table or something
    F(FnType<'ctx>),

    Float64,

    /// TODO: possibly shouldn't be wrapped up in a type
    Implementation(ImplementationRef<'ctx>),
    Int64,
    Nothing,
    Set(ITy<'ctx>),
    String,
    Struct(BTreeMap<StringType, ITy<'ctx>>),
    TraitScheme(TraitSchemeRef<'ctx>),
    TraitStruct(TraitStructRef<'ctx>),
    Tuple(Vec<ITy<'ctx>>), //TODO: better as Box<[TypeRef<'ctx>]>
    Type(ITy<'ctx>),

    /// placeholders used by generic types. They will get replaced when we we
    /// instantiate generic types into monotypes
    TypeParameter {
        uniq_id: UnificationVarId,
        source_region: SourceRegion,
    },
    TypeScheme(SchemeRef<'ctx>),

    /// Represents a union type. Also known as a sum type.
    /// In Rust, they are called enums. They are a bit like using tagged unions
    /// in languages like c/c++.
    Union(Vec<ITy<'ctx>>),

    /// used during type inference
    /// represents types that are not yet fully resolved
    /// TODO: box source region or have a side table or something to make Type smaller
    UnificationVar {
        uniq_id: UnificationVarId,
        source_region: SourceRegion,
        // TODO: remove eventually
        debug_note: Cow<'ctx, str>,
    },

    #[allow(dead_code)] // TODO: use or delete
    Vector(ITy<'ctx>),
    Variant(Variant<'ctx>),
}

pub(crate) type ITy<'ctx> = Interned<'ctx, Type<'ctx>>;

// TODO: The hope was that by interning, we wouldn't have to hash types, but
// but in the current arrangement that was breaking invariants the hashmap
// requires, leading to bugs whenver the hashmap resized
impl<'ctx> Hash for Type<'ctx> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        std::mem::discriminant(self).hash(state);
        match self {
            Type::BigInt => (),
            Type::Bool => (),
            Type::Char => (),
            Type::Dict { key, value } => {
                key.hash(state);
                value.hash(state);
            }
            Type::ElementOf(inner, _) => inner.hash(state),
            Type::F(f) => f.hash(state),
            Type::Float64 => (),
            Type::Implementation(inner) => inner.hash(state),
            Type::Int64 => (),
            Type::Nothing => (),
            Type::Set(inner) => inner.hash(state),
            Type::String => (),
            Type::Struct(inner) => inner.hash(state),
            Type::TraitScheme(inner) => inner.hash(state),
            Type::TraitStruct(inner) => inner.hash(state),
            Type::Tuple(v) => v.hash(state),
            Type::Type(inner) => inner.hash(state),
            Type::TypeScheme(inner) => inner.hash(state),
            Type::Union(inner) => inner.hash(state),
            Type::TypeParameter {
                uniq_id,
                source_region: _,
            }
            | Type::UnificationVar {
                uniq_id,
                source_region: _,
                debug_note: _,
            } => uniq_id.hash(state),
            Type::Vector(inner) => inner.hash(state),
            Type::Variant(inner) => inner.hash(state),
        }
    }
}

impl<'ctx> ITy<'ctx> {
    /// see if the types look like they could be compatible
    pub(crate) fn can_unify(self, other: ITy<'ctx>) -> bool {
        if self == other {
            true
        } else {
            match (self.inner(), other.inner()) {
                (Type::Nothing, Type::Nothing)
                | (Type::Bool, Type::Bool)
                | (Type::Int64, Type::Int64)
                | (Type::BigInt, Type::BigInt)
                | (Type::Float64, Type::Float64)
                | (Type::Char, Type::Char)
                | (Type::String, Type::String) => true,
                (Type::F(fn0), Type::F(fn1)) => {
                    let mut params_compatible = true;
                    for (a, b) in fn0.params.iter().zip(fn1.params.iter()) {
                        if !a.can_unify(*b) {
                            params_compatible = false;
                            break;
                        }
                    }
                    let funcs_compatible = fn0.result.can_unify(fn1.result);
                    params_compatible && funcs_compatible
                }
                (
                    Type::UnificationVar { uniq_id: id1, .. },
                    Type::UnificationVar { uniq_id: id2, .. },
                ) if id1 == id2 => true,
                (Type::UnificationVar { uniq_id, .. }, _) => !other.has_free_type_var(*uniq_id),
                (_, Type::UnificationVar { uniq_id, .. }) => !self.has_free_type_var(*uniq_id),
                (
                    Type::TypeParameter { uniq_id: id1, .. },
                    Type::TypeParameter { uniq_id: id2, .. },
                ) if id1 == id2 => true,
                (Type::TraitStruct(ts0), Type::TraitStruct(ts1)) => {
                    (ts0.trait_name == ts1.trait_name)
                        && (ts0.assoc_tys.len() == ts0.assoc_tys.len())
                        && (ts0.methods.len() == ts0.methods.len())
                        && ts0.assoc_tys.iter().fold(true, |init, (k0, v0)| {
                            init && match ts1.assoc_tys.get(k0) {
                                None => false,
                                Some(v1) => v0.can_unify(*v1),
                            }
                        })
                        && ts0.methods.iter().fold(true, |init, (k0, v0)| {
                            init && match ts1.methods.get(k0) {
                                None => false,
                                Some(v1) => v0.can_unify(*v1),
                            }
                        })
                }
                (Type::Set(t0), Type::Set(t1)) => t0.can_unify(*t1),
                (Type::Type(t0), Type::Type(t1)) => t0.can_unify(*t1),
                (
                    Type::Dict {
                        key: key0,
                        value: value0,
                    },
                    Type::Dict {
                        key: key1,
                        value: value1,
                    },
                ) => key0.can_unify(*key1) && value0.can_unify(*value1),
                (Type::Tuple(v0), Type::Tuple(v1)) => {
                    if v0.len() == v1.len() {
                        for (l, r) in v0.iter().zip(v1.iter()) {
                            if !l.can_unify(*r) {
                                return false;
                            }
                        }
                        true
                    } else {
                        false
                    }
                }
                (
                    Type::BigInt
                    | Type::Bool
                    | Type::Char
                    | Type::Dict { .. }
                    | Type::F(_)
                    | Type::Float64
                    | Type::Implementation(_)
                    | Type::Int64
                    | Type::Nothing
                    | Type::Set(_)
                    | Type::String
                    | Type::Struct(..)
                    | Type::TraitScheme(..)
                    | Type::TraitStruct(..)
                    | Type::Tuple(..)
                    | Type::Type(..)
                    | Type::TypeScheme(..)
                    | Type::Union(..)
                    | Type::Vector(..)
                    | Type::Variant(..),
                    _,
                )
                | (
                    _,
                    Type::BigInt
                    | Type::Bool
                    | Type::Char
                    | Type::Dict { .. }
                    | Type::F(_)
                    | Type::Float64
                    | Type::Implementation(_)
                    | Type::Int64
                    | Type::Nothing
                    | Type::Set(_)
                    | Type::String
                    | Type::Struct(..)
                    | Type::TraitScheme(..)
                    | Type::TraitStruct(..)
                    | Type::Tuple(..)
                    | Type::Type(..)
                    | Type::TypeScheme(..)
                    | Type::Union(..)
                    | Type::Vector(..)
                    | Type::Variant(..),
                ) => false,
                _ => todo!(
                    "unhandled case in can_unify\ntype 0:\n{}\ntype 1:\n{}",
                    self,
                    other
                ),
            }
        }
    }

    /// see if the types look like they could be compatible after instantiating `other`
    pub(crate) fn could_be_instance(self, other: ITy<'ctx>) -> bool {
        if self == other {
            return true;
        }
        match (self.inner(), other.inner()) {
            (Type::Nothing, Type::Nothing)
            | (Type::Bool, Type::Bool)
            | (Type::Int64, Type::Int64)
            | (Type::BigInt, Type::BigInt)
            | (Type::Float64, Type::Float64)
            | (Type::Char, Type::Char)
            | (Type::String, Type::String) => true,
            (
                Type::UnificationVar { uniq_id: id1, .. },
                Type::UnificationVar { uniq_id: id2, .. },
            ) if id1 == id2 => true,
            (Type::UnificationVar { uniq_id, .. }, _) => !other.has_free_type_var(*uniq_id),
            (_, Type::UnificationVar { uniq_id, .. }) => !self.has_free_type_var(*uniq_id),
            (
                _,
                Type::Nothing
                | Type::Bool
                | Type::Int64
                | Type::BigInt
                | Type::Float64
                | Type::Char
                | Type::String,
            ) => false,
            (Type::Dict { key: k1, value: v1 }, Type::Dict { key: k2, value: v2 }) => {
                k1.could_be_instance(*k2) && v1.could_be_instance(*v2)
            }
            (Type::F(fn0), Type::F(fn1)) => {
                for (a, b) in fn0.params.iter().zip(fn1.params.iter()) {
                    if !a.could_be_instance(*b) {
                        return false;
                    }
                }
                fn0.result.can_unify(fn1.result)
            }
            (Type::Set(t1), Type::Set(t2)) => t1.could_be_instance(*t2),
            (Type::TraitStruct(tr1), Type::TraitScheme(scheme)) => {
                (tr1.trait_name == scheme.trait_name)
                    && (tr1.assoc_tys.len() == scheme.assoc_tys.len())
                    && (tr1.methods.len() == scheme.methods.len())
                    && tr1.assoc_tys.iter().fold(true, |init, (k0, v0)| {
                        init && match scheme.assoc_tys.get(k0) {
                            None => false,
                            Some(v1) => v0.could_be_instance(*v1),
                        }
                    })
                    && tr1.methods.iter().fold(true, |init, (k0, v0)| {
                        init && match scheme.methods.get(k0) {
                            None => false,
                            Some(v1) => v0.could_be_instance(*v1),
                        }
                    })
            }
            (Type::Tuple(v1), Type::Tuple(v2)) => {
                if v1.len() == v2.len() {
                    v1.iter()
                        .zip(v2.iter())
                        .all(|(t1, t2)| t1.could_be_instance(*t2))
                } else {
                    false
                }
            }
            (Type::Type(t1), Type::Type(t2)) => t1.could_be_instance(*t2),
            (_, Type::TypeScheme(scheme)) => self.could_be_instance(scheme.ty),
            (_, Type::TypeParameter { .. }) => true,
            _ => todo!("unhandled case in could_be_instance\ntype 0:\n{self}\ntype 1:\n{other}"),
        }
    }

    /// Look for a member in the type without looking it up in traits
    /// Only useful for structs and implementations
    pub(crate) fn find_member_direct(self, member_name: &StringType) -> Option<ITy<'ctx>> {
        match self.inner() {
            Type::BigInt
            | Type::Bool
            | Type::Char
            | Type::Dict { .. }
            | Type::ElementOf(_, _)
            | Type::F(_)
            | Type::Float64
            | Type::Int64
            | Type::Nothing
            | Type::Set(_)
            | Type::String
            | Type::TraitScheme(_)
            | Type::TraitStruct(_)
            | Type::Tuple(_)
            | Type::Type(_)
            | Type::TypeParameter { .. }
            | Type::TypeScheme(_)
            | Type::Union(_)
            | Type::UnificationVar { .. }
            | Type::Vector(_) => None,
            Type::Implementation(implementation) => implementation
                .member_types
                .get(member_name)
                .copied()
                .or_else(|| {
                    implementation
                        .member_functions
                        .get(member_name)
                        .map(|x| x.0)
                }),
            Type::Struct(fields) => fields.get(member_name).copied(),
            Type::Variant(inner) => inner.variant_type.find_member_direct(member_name),
        }
    }

    /// Generalize a type into a polytype - all the free type variables
    /// could be different in the next instantiation.
    /// Returns both the generalized version of the type and the substitutions
    /// used to generalize
    pub(crate) fn generalize(
        self,
        ctx: &Context<'ctx>,
        source_region: &SourceRegion,
    ) -> TiResult<(Substitutions<'ctx>, Self)> {
        // we make type parameters out of the free variables of the original type
        // that are not free variables in the greater context
        let mut scope_unification_vars = Vec::new();
        ctx.free_vars(&[], &mut scope_unification_vars);
        let mut vars_to_generalize = Vec::new();
        self.free_unification_variables(&scope_unification_vars, &mut vars_to_generalize);
        if vars_to_generalize.is_empty() {
            return Ok((Substitutions::new(), self));
        }

        let mut substitutions = Substitutions::new();
        let mut ty_pars = Vec::new();
        for var in vars_to_generalize {
            let (ty_par, ty_par_id) = ctx.mk_type_parameter(source_region)?;
            ty_pars.push(ty_par_id);
            substitutions.insert(var, ty_par);
        }

        if let Some(updated) = self.apply_subst(&substitutions, ctx)? {
            Ok((
                substitutions,
                ctx.mk_type_scheme(ty_pars.into_boxed_slice(), updated, source_region),
            ))
        } else {
            // apply_subst will always have something to do, or else we would have early returned before it
            unreachable!();
        }
    }

    /// view into the type as dictionary key/value
    /// or None if not a dictionary type
    pub(crate) fn get_dict_data(self) -> Option<(ITy<'ctx>, ITy<'ctx>)> {
        match self.inner() {
            Type::Dict { key, value } => Some((*key, *value)),
            _ => None,
        }
    }

    /// deconstruct into parameter and return types, if a function type (`None` otherwise)
    pub(crate) fn get_fn_data(&self) -> Option<&FnType<'ctx>> {
        match self.inner() {
            Type::F(fn_type) => Some(fn_type),
            _ => None,
        }
    }

    /// if the type has type parameters, replace them with fresh unification variables
    /// returns `Ok(Some(Substitutions, ITy))` if a replacement was made, `Ok(None)` if no
    /// replacement was necessary, and `Err` on error
    pub(crate) fn instantiate(
        self,
        ctx: &Context<'ctx>,
        source_region: &SourceRegion,
    ) -> TiResult<Option<(Substitutions<'ctx>, ITy<'ctx>)>> {
        match self.inner() {
            Type::TraitScheme(trait_scheme) => {
                let (substitutions, tr) = trait_scheme.instantiate(ctx, source_region)?;
                Ok(Some((substitutions, ctx.mk_trait(tr))))
            },
            Type::TypeScheme(scheme) => {
                let (substitutions, ty) = scheme.instantiate(ctx, source_region)?;
                Ok(Some((substitutions, ty)))
            },

            // no type variables in these
            Type::BigInt
            | Type::Bool
            | Type::Char
            | Type::Float64
            | Type::Int64
            | Type::Nothing
            | Type::String
            | Type::UnificationVar { .. } => Ok(None),

            Type::Dict{ key, value} => {
                let k = key.instantiate(ctx, source_region)?;
                let v = value.instantiate(ctx, source_region)?;
                if k.is_none() && v.is_none() {
                    Ok(None)
                } else {
                    let (s1, key) = k.unwrap_or_else(||
                        (Substitutions::new(), *key));
                    let (s2, value) = v.unwrap_or_else(||
                        (Substitutions::new(), *value));
                    let s3 = ctx.compose(s2, s1)?;
                    let ty = ctx.mk_dict(key, value);
                    let ty = ty.apply_subst(&s3, ctx)?.unwrap_or(ty);
                    Ok(Some((s3, ty)))
                }
            },

            Type::Implementation(_)
            | Type::Struct(_) // TODO: structs should have type variables
            | Type::TraitStruct(_)
            | Type::Vector(_) => todo!("unhandled case in instantiate. ({self})"),

            Type::F(FnType { params, result }) => {
                let mut params2: Option<Vec<ITy<'_>>> = None;
                for (i, param) in params.iter().enumerate() {
                    if let Some((_, param)) = param.instantiate(ctx, source_region)? {
                        let v = params2.get_or_insert_with(||params.to_vec());
                        v[i] = param;
                    }
                }
                let result2 = result.instantiate(ctx, source_region)?;
                if params2.is_none() && result2.is_none() {
                    Ok(None)
                } else {
                    let params = params2.map_or_else(||params.clone(), Vec::into_boxed_slice);
                    let result = result2.map_or(*result, |(_, ty)|ty);
                    Ok(Some((Substitutions::new(), ctx.mk_fn_type(params, result))))
                }
            },

            Type::Set(inner) => inner.instantiate(ctx, source_region)
                .map(|x|x.map(|(substitutions, ty)|{
                    (substitutions, ctx.mk_set(ty))
                })),

            Type::Tuple(tys) => {
                let mut s = Substitutions::new();
                let mut v: Option<Vec<ITy<'ctx>>> = None;
                for (i,ty) in tys.iter().enumerate() {
                    if let Some((more_subs, ty)) = ty.instantiate(ctx, source_region)? {
                        if let Some(v) = &mut v {
                            v[i] = ty;
                            s = ctx.compose(more_subs, s)?;
                        } else {
                            let mut new_v = tys.clone();
                            new_v[i] = ty;
                            v = Some(new_v);
                            s = more_subs;
                        }
                    }
                }
                Ok(v.map(|v|(s, ctx.mk_tuple(v))))
            },
            Type::Type(inner) => inner.instantiate(ctx, source_region)
                .map(|x|x.map(|(substitutions, ty)|{
                    (substitutions, ctx.mk_typetype(ty))
                })),
            Type::Union(_)
            | Type::Variant(_) => Err(TiError::new(
                loc::err::internal::unhandled_case(&self, file!(), line!(), column!()),
                source_region.clone(),
            )),
            Type::ElementOf(x, elem_source_region) => match x.instantiate(ctx, source_region)? {
                Some((s, t)) => {
                    let t = ctx.mk_element_of(t, elem_source_region)?;
                    Ok(Some((s, t)))
                },
                None => Ok(None),
            },
            Type::TypeParameter { .. } => {
                // Type params get replaced by the scheme cases above. If we do 
                // it here, every use of a type parameter would get its own 
                // fresh unification var
                unreachable!();
            }
        }
    }

    /// replace the type's type parameters with the specified substitution
    pub(crate) fn instantiate_with(
        self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<Option<ITy<'ctx>>> {
        match self.inner() {
            Type::TypeScheme(scheme) => scheme.instantiate_exactly(substitutions, ctx).map(Some),
            Type::BigInt
            | Type::Bool
            | Type::Char
            | Type::Dict { .. }
            | Type::ElementOf(..)
            | Type::F(..)
            | Type::Float64
            | Type::Implementation(..)
            | Type::Int64
            | Type::Nothing
            | Type::Set(..)
            | Type::String
            | Type::Struct(..)
            | Type::TraitStruct(..)
            | Type::Tuple(..)
            | Type::Type(..)
            | Type::UnificationVar { .. }
            | Type::Union(..)
            | Type::Variant(..)
            | Type::Vector(..) => {
                // TODO: error message
                assert!(substitutions.is_empty());
                Ok(None)
            }
            Type::TraitScheme(..) | Type::TypeParameter { .. } => {
                todo!("unhandled in instantiate_with. {self}")
            }
        }
    }

    /// If the type is iterable, get the type it iterates over or None if not yet known
    /// otherwise Err
    pub(crate) fn iterable_type(
        self,
        ctx: &Context<'ctx>,
        source_region: &SourceRegion,
    ) -> TiResult<Option<ITy<'ctx>>> {
        match self.inner() {
            Type::BigInt
            | Type::Bool
            | Type::Char
            | Type::F(_)
            | Type::Float64
            | Type::Implementation(_)
            | Type::Int64
            | Type::Nothing
            | Type::Struct(_)
            | Type::TraitScheme(_)
            | Type::TraitStruct(_)
            | Type::Tuple(_)
            | Type::Type(_)
            | Type::TypeScheme(_)
            | Type::Union(_)
            | Type::Variant(_) => Err(TiError::new(
                loc::err::element_of_noniterable(&self.to_string()),
                source_region.clone(),
            )),

            Type::Set(t) | Type::Vector(t) => Ok(Some(*t)),

            Type::Dict { key, value } => Ok(Some(ctx.mk_tuple(vec![*key, *value]))),
            Type::String => Ok(Some(ctx.mk_char())),
            Type::TypeParameter { .. } | Type::UnificationVar { .. } | Type::ElementOf(_, _) => {
                Ok(None)
            }
        }
    }

    /// answers the question: is type inference complete on this item?
    pub(crate) fn is_final(self) -> bool {
        match self.inner() {
            Type::BigInt
            | Type::Bool
            | Type::Char
            | Type::Float64
            | Type::Int64
            | Type::Nothing
            | Type::String => true,
            Type::Dict { key, value } => key.is_final() && value.is_final(),
            Type::ElementOf(_, _) | Type::TypeParameter { .. } | Type::UnificationVar { .. } => {
                false
            }
            Type::F(inner) => inner.is_final(),
            Type::Implementation(_) => todo!(),
            Type::Set(inner) | Type::Vector(inner) => inner.is_final(),
            Type::Struct(_) => todo!(),
            Type::TraitScheme(_) => todo!(),
            Type::TraitStruct(_) => todo!(),
            Type::Tuple(v) => v.iter().copied().all(Interned::is_final),
            Type::Type(inner) => inner.is_final(),
            Type::TypeScheme(Scheme {
                ty_pars,
                ty,
                source_region: _,
            }) => ty_pars.is_empty() && ty.is_final(),
            Type::Union(_) => todo!(),
            Type::Variant(_) => todo!(),
        }
    }
}
impl<'ctx> std::fmt::Display for Type<'ctx> {
    // TODO: should use loc instead of hardcoding english words here?
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Type::TypeParameter {
                uniq_id,
                source_region: _,
            } => write!(
                f,
                r#"TypeParameter#{uniq_id}"#,
            )/*write!(
                f,
                r#"(Type parameter #{} from {}:{}:{})"#,
                uniq_id,
                source_region.file_label,
                source_region.start_line_number,
                source_region.start_column_number
            )*/,
            Type::UnificationVar {
                source_region: _,
                uniq_id,
                debug_note: _,
            } => write!(
                f,
                r#"unknown#{uniq_id}"#,
            )/*write!(
                f,
                r#"(unknown#{} from {}:{}:{} "{}")"#,
                uniq_id,
                source_region.file_label,
                source_region.start_line_number,
                source_region.start_column_number,
                debug_note
            )*/,
            Type::Nothing => config::NOTHING_TYPE_NAME.fmt(f),
            Type::Bool => config::BOOLEAN.fmt(f),
            Type::Int64 => config::INT64_TYPE_NAME.fmt(f),
            Type::BigInt => config::BIGINT_TYPE_NAME.fmt(f),
            Type::Float64 => config::FLOAT64_TYPE_NAME.fmt(f),
            Type::Char => config::CHAR_TYPE_NAME.fmt(f),
            Type::String => config::STRING_TYPE_NAME.fmt(f),
            Type::F(fn_type) => fn_type.fmt(f),
            Type::Dict { key, value } => {
                loc::dict_type_name(&key.to_string(), &value.to_string()).fmt(f)
            }
            Type::Set(t) => loc::set_type_name(&t.to_string()).fmt(f),
            Type::Tuple(types) => {
                '('.fmt(f)?;
                for (i, ty) in types.iter().enumerate() {
                    if i > 0 {
                        ", ".fmt(f)?;
                    }
                    ty.fmt(f)?;
                }
                if types.len() < 2 {
                    ','.fmt(f)?;
                }
                ')'.fmt(f)
            }
            Type::Struct(_) => todo!(),
            Type::Vector(t) => loc::vec_type_name(&t.to_string()).fmt(f),
            Type::Union(_) => todo!(),
            Type::Type(inner) => write!(f, "(Type: {inner})"),
            Type::TraitScheme(inner) => inner.fmt(f),
            Type::TraitStruct(inner) => inner.fmt(f),
            Type::Variant(Variant { name, variant_type }) => {
                write!(f, "(Variant \"{name}\" {variant_type})",)
            }
            Type::TypeScheme(Scheme {
                ty_pars,
                ty,
                source_region: _,
            }) => {
                if ty_pars.is_empty() {
                    ty.fmt(f)
                } else {
                    write!(f, "(forall ")?;
                    for id in ty_pars.iter() {
                        write!(f, "{id}, ")?;
                    }
                    write!(f, "{ty})")
                }
            }
            Type::Implementation(imp) => imp.fmt(f),
            Type::ElementOf(ty, _) => write!(f, "(Item of {ty})"),
        }
    }
}

impl<'ctx> FreeTypeVars<'ctx> for ITy<'ctx> {
    fn free_unification_variables(
        &self,
        exclude: &[UnificationVarId],
        collection: &mut Vec<UnificationVarId>,
    ) {
        match &**self {
            Type::UnificationVar { uniq_id, .. } => {
                if !exclude.contains(uniq_id) {
                    collection.push(*uniq_id);
                }
            }
            Type::ElementOf(x, _) => x.free_unification_variables(exclude, collection),
            Type::Nothing
            | Type::Bool
            | Type::Int64
            | Type::BigInt
            | Type::Float64
            | Type::Char
            | Type::String
            | Type::TypeParameter { .. } => (),
            Type::F(FnType { params, result }) => {
                for &param in &**params {
                    param.free_unification_variables(exclude, collection);
                }
                result.free_unification_variables(exclude, collection);
            }
            Type::Dict { key, value } => {
                key.free_unification_variables(exclude, collection);
                value.free_unification_variables(exclude, collection);
            }
            Type::Set(t) | Type::Vector(t) => t.free_unification_variables(exclude, collection),
            Type::Tuple(v) | Type::Union(v) => {
                for t in v {
                    t.free_unification_variables(exclude, collection);
                }
            }
            Type::Struct(m) => {
                for t in m.values() {
                    t.free_unification_variables(exclude, collection);
                }
            }
            Type::TraitScheme(TraitScheme {
                ty_pars: _,
                self_var: _,
                trait_name: _,
                assoc_tys,
                methods,
            }) => {
                for t in assoc_tys.values() {
                    t.free_unification_variables(exclude, collection);
                }
                for t in methods.values() {
                    t.free_unification_variables(exclude, collection);
                }
            }
            Type::TraitStruct(TraitStruct {
                typar_choices: _,
                self_var: _,
                trait_name: _,
                assoc_tys,
                methods,
            }) => {
                // TODO: there's kind of a judgement call here on whether the
                // chosen type parameters should be checked here. I think we
                // should instead check for unused type parameters and produce
                // a warning
                // IMPORTANT! keep in sync with the matching case in `has_free_unification_variables` and `has_free_type_var`
                for t in assoc_tys.values() {
                    t.free_unification_variables(exclude, collection);
                }
                for t in methods.values() {
                    t.free_unification_variables(exclude, collection);
                }
            }
            Type::Variant(Variant {
                name: _,
                variant_type,
            }) => variant_type.free_unification_variables(exclude, collection),
            Type::Type(inner) => inner.free_unification_variables(exclude, collection),
            Type::TypeScheme(Scheme {
                ty_pars: _,
                ty,
                source_region: _,
            }) => ty.free_unification_variables(exclude, collection),
            Type::Implementation(imp) => {
                for (t, _) in imp.member_functions.values() {
                    t.free_unification_variables(exclude, collection);
                }
                for t in imp.member_types.values() {
                    t.free_unification_variables(exclude, collection);
                }
                imp.implemented_for
                    .free_unification_variables(exclude, collection);
                imp.implemented_trait
                    .free_unification_variables(exclude, collection);
            }
        }
    }

    fn has_any_free_unification_variables(&self, exclude: &[UnificationVarId]) -> bool {
        match self.inner() {
            Type::TypeParameter { .. } => todo!(),
            Type::UnificationVar { uniq_id, .. } => !exclude.contains(uniq_id),
            Type::Nothing
            | Type::Bool
            | Type::Int64
            | Type::BigInt
            | Type::Float64
            | Type::Char
            | Type::String => false,
            Type::F(FnType { params, result }) => {
                result.has_any_free_unification_variables(exclude)
                    || params
                        .iter()
                        .any(|t| t.has_any_free_unification_variables(exclude))
            }
            Type::Dict { key, value } => {
                key.has_any_free_unification_variables(exclude)
                    || value.has_any_free_unification_variables(exclude)
            }
            Type::Set(t) | Type::Vector(t) => t.has_any_free_unification_variables(exclude),
            Type::Tuple(v) => !v
                .iter()
                .any(|t| t.has_any_free_unification_variables(exclude)),
            Type::Struct(m) => m
                .iter()
                .any(|(_, t)| t.has_any_free_unification_variables(exclude)),
            Type::Union(v) => !v
                .iter()
                .any(|v| v.has_any_free_unification_variables(exclude)),
            Type::TraitScheme(TraitScheme {
                ty_pars,
                self_var: _,
                trait_name: _,
                assoc_tys,
                methods,
            }) => {
                if !ty_pars.is_empty() {
                    todo!("exclude type parameters");
                }
                assoc_tys
                    .values()
                    .chain(methods.values())
                    .any(|t| t.has_any_free_unification_variables(exclude))
            }
            Type::TraitStruct(TraitStruct {
                typar_choices: _,
                self_var: _,
                trait_name: _,
                assoc_tys,
                methods,
            }) => {
                // TODO: see comment on the TraitStruct case in `free_unification_variables`
                assoc_tys
                    .values()
                    .chain(methods.values())
                    .any(|t| t.has_any_free_unification_variables(exclude))
            }
            Type::Variant(Variant {
                name: _,
                variant_type,
            }) => variant_type.has_any_free_unification_variables(exclude),
            Type::Type(_) => todo!(),
            Type::TypeScheme(_) => todo!(),
            Type::Implementation(imp) => {
                imp.member_functions
                    .values()
                    .any(|x| x.0.has_any_free_unification_variables(exclude))
                    || imp
                        .member_types
                        .values()
                        .any(|x| x.has_any_free_unification_variables(exclude))
                    || imp
                        .implemented_for
                        .has_any_free_unification_variables(exclude)
                    || imp
                        .implemented_trait
                        .has_any_free_unification_variables(exclude)
            }
            // TODO: if it didn't have free vars, it wouldn't be this kind of type, right?
            Type::ElementOf(x, _) => x.has_any_free_unification_variables(exclude),
        }
    }

    /// whether this type has the given particular unification variable
    fn has_free_type_var(&self, search_id: UnificationVarId) -> bool {
        match self.inner() {
            Type::TypeParameter { .. } => {
                // this is really asking about unification variables, not type parameters
                false
            }
            Type::UnificationVar { uniq_id, .. } => *uniq_id == search_id,
            Type::Nothing
            | Type::Bool
            | Type::Int64
            | Type::BigInt
            | Type::Float64
            | Type::Char
            | Type::String => false,
            Type::Set(t) | Type::Vector(t) => t.has_free_type_var(search_id),
            Type::F(FnType { params, result }) => {
                result.has_free_type_var(search_id)
                    || params.iter().any(|t| t.has_free_type_var(search_id))
            }
            Type::Dict { key, value } => {
                key.has_free_type_var(search_id) || value.has_free_type_var(search_id)
            }
            Type::Tuple(v) => v.iter().any(|t| t.has_free_type_var(search_id)),
            Type::Struct(m) => m.iter().any(|(_, t)| t.has_free_type_var(search_id)),
            Type::Union(v) => !v.iter().any(|v| v.has_free_type_var(search_id)),
            Type::TraitScheme(TraitScheme {
                ty_pars,
                self_var: _,
                trait_name: _,
                assoc_tys,
                methods,
            }) => {
                if !ty_pars.is_empty() {
                    todo!("exclude type parameters");
                }
                assoc_tys
                    .values()
                    .chain(methods.values())
                    .any(|t| t.has_free_type_var(search_id))
            }
            Type::TraitStruct(TraitStruct {
                typar_choices: _,
                self_var: _,
                trait_name: _,
                assoc_tys,
                methods,
            }) => {
                // IMPORTANT! keep changes in sync with the matching case in `has_free_unification_variables` and `free_unification_variables`
                assoc_tys
                    .values()
                    .chain(methods.values())
                    .any(|t| t.has_free_type_var(search_id))
            }
            Type::Variant(Variant {
                name: _,
                variant_type,
            }) => variant_type.has_free_type_var(search_id),
            Type::Type(_) => todo!(),
            Type::TypeScheme(scheme) => scheme.has_free_type_var(search_id),
            Type::Implementation(imp) => {
                imp.member_functions
                    .values()
                    .any(|x| x.0.has_free_type_var(search_id))
                    || imp
                        .member_types
                        .values()
                        .any(|x| x.has_free_type_var(search_id))
                    || imp.implemented_for.has_free_type_var(search_id)
                    || imp.implemented_trait.has_free_type_var(search_id)
            }
            Type::ElementOf(x, _) => x.has_free_type_var(search_id),
        }
    }

    fn get_type_parameters(
        &self,
        exclude: &[UnificationVarId],
        collection: &mut Vec<UnificationVarId>,
    ) {
        match self.inner() {
            Type::BigInt
            | Type::Bool
            | Type::Char
            | Type::UnificationVar { .. }
            | Type::Float64
            | Type::Int64
            | Type::Nothing
            | Type::String => (),
            Type::Dict { key, value } => {
                key.get_type_parameters(exclude, collection);
                value.get_type_parameters(exclude, collection);
            }
            Type::F(FnType { params, result }) => {
                params
                    .iter()
                    .chain(std::iter::once(result))
                    .for_each(|ty| ty.get_type_parameters(exclude, collection));
            }
            Type::Set(inner) => inner.get_type_parameters(exclude, collection),
            Type::Tuple(elements) => {
                elements
                    .iter()
                    .unique()
                    .for_each(|ty| ty.get_type_parameters(exclude, collection));
            }
            Type::Type(inner) => {
                inner.get_type_parameters(exclude, collection);
            }
            Type::TypeParameter {
                uniq_id,
                source_region: _,
            } => {
                if !exclude.iter().contains(uniq_id) && !collection.contains(uniq_id) {
                    collection.push(*uniq_id);
                }
            }
            Type::Implementation(Implementation {
                implemented_for,
                implemented_trait,
                member_types,
                member_functions,
                source_region: _,
            }) => {
                let mut updated_exclude = Vec::with_capacity(exclude.len() + 1);
                updated_exclude.extend(exclude);
                updated_exclude.push(implemented_trait.self_var);

                implemented_for.get_type_parameters(&updated_exclude, collection);
                implemented_trait.get_type_parameters(&updated_exclude, collection);
                for v in member_types.values() {
                    v.get_type_parameters(&updated_exclude, collection);
                }
                for (f_ty, _) in member_functions.values() {
                    f_ty.get_type_parameters(&updated_exclude, collection);
                }
            }
            _ => {
                todo!("The type {self} is not yet handled in get_type_parameters");
            }
        }
    }

    fn has_any_type_parameters(&self, exclude: &[UnificationVarId]) -> bool {
        match &**self {
            Type::BigInt
            | Type::Bool
            | Type::Char
            | Type::UnificationVar { .. }
            | Type::Float64
            | Type::Int64
            | Type::Nothing
            | Type::String => false,
            Type::Dict { key, value } => {
                key.has_any_type_parameters(exclude) || value.has_any_type_parameters(exclude)
            }
            Type::ElementOf(ty, _) => ty.has_any_type_parameters(exclude),
            Type::F(_) => todo!(),
            Type::Implementation(_) => todo!(),
            Type::Set(_) => todo!(),
            Type::Struct(_) => todo!(),
            Type::TraitScheme(_) => todo!(),
            Type::TraitStruct(_) => todo!(),
            Type::Tuple(_) => todo!(),
            Type::Type(_) => todo!(),
            Type::TypeParameter { uniq_id, .. } => !exclude.iter().contains(uniq_id),
            Type::TypeScheme(_) => todo!(),
            Type::Union(_) => todo!(),
            Type::Vector(_) => todo!(),
            Type::Variant(_) => todo!(),
        }
    }

    fn has_type_parameter(&self, _search_id: UnificationVarId) -> bool {
        todo!()
    }
}

impl<'ctx> Type<'ctx> {
    /// If the type is `TraitScheme`, extract the trait, otherwise Err
    pub(crate) fn expect_trait_scheme(
        &self,
        source_region: &SourceRegion,
    ) -> TiResult<&TraitScheme<'ctx>> {
        if let Type::TraitScheme(inner) = self {
            Ok(inner)
        } else {
            Err(TiError::new(
                loc::err::expected_a_trait(&self.to_string()),
                source_region.clone(),
            ))
        }
    }

    /// If the type is "Type", extract the inner type, otherwise Err
    pub(crate) fn expect_type(&self, source_region: &SourceRegion) -> TiResult<ITy<'ctx>> {
        if let Type::Type(inner) = self {
            Ok(*inner)
        } else {
            Err(TiError::new(
                loc::err::expected_a_type(&self.to_string()),
                source_region.clone(),
            ))
        }
    }

    /// If the type is `TypeParameter`, extract the id, otherwise Err
    pub(crate) fn expect_type_parameter(
        &self,
        source_region: &SourceRegion,
    ) -> TiResult<UnificationVarId> {
        if let Type::TypeParameter { uniq_id, .. } = self {
            Ok(*uniq_id)
        } else {
            Err(TiError::new(
                loc::err::internal::expected_a_type_parameter(&self.to_string()),
                source_region.clone(),
            ))
        }
    }

    /// If the type is a unification var, extract the id, otherwise Err
    pub(crate) fn expect_unification_var(
        &self,
        source_region: &SourceRegion,
    ) -> TiResult<UnificationVarId> {
        if let Type::UnificationVar { uniq_id, .. } = self {
            Ok(*uniq_id)
        } else {
            Err(TiError::new(
                loc::err::internal::expected_unif_var(&self.to_string()),
                source_region.clone(),
            ))
        }
    }
}
