//! all the arenas used by the type inference context and interning
use typed_arena::Arena;

use super::{
    constraint::Constraint,
    traitstruct::{TraitScheme, TraitStruct},
    ImplScheme, Implementation, Scheme, Type,
};

/// all the arenas used by the type inference context
pub struct Arenas<'a> {
    /// storage for constraints
    pub(crate) constraints: Arena<Constraint<'a>>,
    /// storage for implementation schemes
    pub(crate) implementation_schemes: Arena<ImplScheme<'a>>,
    /// storage for implementations
    pub(crate) implementations: Arena<Implementation<'a>>,
    /// storage for typeschemes
    pub(crate) schemes: Arena<Scheme<'a>>,
    /// storage for instantiated traits
    pub(crate) traits: Arena<TraitStruct<'a>>,
    /// storage for trait schemes
    pub(crate) traitschemes: Arena<TraitScheme<'a>>,
    /// storage for types
    pub(crate) types: Arena<Type<'a>>,
}

impl<'a> Default for Arenas<'a> {
    fn default() -> Self {
        Self::new()
    }
}

impl<'a> Arenas<'a> {
    pub(crate) fn new() -> Self {
        Self {
            constraints: Arena::default(),
            implementation_schemes: Arena::default(),
            implementations: Arena::default(),
            schemes: Arena::default(),
            traits: Arena::default(),
            traitschemes: Arena::default(),
            types: Arena::default(),
        }
    }
}
