//! interns and allocates frequently-used structs
use std::{ops::Deref, ptr, cmp::Ordering, hash::{Hasher, Hash}, fmt::{Debug, self, Display}, cell::RefCell, borrow::Borrow};

use rustc_hash::FxHashMap;

use super::{arenas::Arenas, Type, TypeRef};

type InternSet<T> = FxHashMap<T, ()>;

/// track objects that we want to have few instances of
pub(crate) struct Interner<'ctx>{
    /// allocates the structs
    arenas : &'ctx Arenas<'ctx>,
    /// track the allocated types
    types: RefCell<InternSet<TypeRef<'ctx>>>,
}

impl<'ctx> Interner<'ctx> {
    /// create a fresh Interner
    pub(crate) fn new(arenas: &'ctx Arenas<'ctx>) -> Self {
        Self {
            arenas,
            types: Default::default()
        }
    }
    
    /// intern a Type
    pub(crate) fn intern_ty(&self, ty: Type<'ctx>) -> TypeRef<'ctx> {
        let mut intern_set = self.types.borrow_mut();
        if let Some((k, _)) = intern_set.get_key_value(&ty) {
            return *k;
        }
        let ty_in_arena = self.arenas.types.alloc(ty);
        let interned_ty = Interned::<'ctx>(ty_in_arena, private::Private);
        intern_set.insert(ty_in_arena, ());
        interned_ty
    }
}

mod private {
    /// private type used to prevent construction of Interned except through the Interner
    #[derive(Clone, Copy, Debug)]
    pub(super) struct Private;
}

/// reference to an interned item
/// Should only be created by the interner and thus guaranteed to be the only 
/// `Interned` that is pointing at that type. That way equality and hashing can 
/// just use the ptr
pub(crate) struct Interned<'a, T>(pub(crate) &'a T, private::Private);

impl<'a, T> Clone for Interned<'a, T> {
    fn clone(&self) -> Self {
        *self
    }
}

impl<'a, T> Copy for Interned<'a, T> {}

impl<'a, T> Deref for Interned<'a, T> {
    type Target = T;

    #[inline]
    fn deref(&self) -> &T {
        self.0
    }
}

impl<'a, T> PartialEq for Interned<'a, T> {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        // Pointer equality implies equality, due to the uniqueness constraint.
        ptr::eq(self.0, other.0)
    }
}

impl<'a, T> Eq for Interned<'a, T> {}

impl<'a, T: PartialOrd> PartialOrd for Interned<'a, T> {
    fn partial_cmp(&self, other: &Interned<'a, T>) -> Option<Ordering> {
        // Pointer equality implies equality, due to the uniqueness constraint,
        // but the contents must be compared otherwise.
        if ptr::eq(self.0, other.0) {
            Some(Ordering::Equal)
        } else {
            let res = self.0.partial_cmp(other.0);
            debug_assert_ne!(res, Some(Ordering::Equal));
            res
        }
    }
}

impl<'a, T: Ord> Ord for Interned<'a, T> {
    fn cmp(&self, other: &Interned<'a, T>) -> Ordering {
        // Pointer equality implies equality, due to the uniqueness constraint,
        // but the contents must be compared otherwise.
        if ptr::eq(self.0, other.0) {
            Ordering::Equal
        } else {
            let res = self.0.cmp(other.0);
            debug_assert_ne!(res, Ordering::Equal);
            res
        }
    }
}

impl<'a, T> Hash for Interned<'a, T> {
    #[inline]
    fn hash<H: Hasher>(&self, s: &mut H) {
        // Pointer hashing is sufficient, due to the uniqueness constraint.
        ptr::hash(self.0, s)
    }
}

impl<T: Display> Display for Interned<'_, T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl<T: Debug> Debug for Interned<'_, T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl<T> Borrow<T> for Interned<'_, T> {
    fn borrow(&self) -> &T {
        todo!()
    }
}