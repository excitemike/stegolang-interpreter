use super::{
    freetypevars::FreeTypeVars, substitute::ApplySubst, substitutions::Substitutions, Context, ITy,
    TiResult, UnificationVarId,
};
use crate::sourceregion::SourceRegion;
use itertools::Itertools;

/// Represents Type/Function/Trait schemes. What another language might call generics.
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub(crate) struct Scheme<'ctx> {
    /// quantified type variables - thethings that will get replaced when we instantiate
    pub(crate) ty_pars: Box<[UnificationVarId]>,
    /// prototype the scheme uses when instantiating the type
    pub(crate) ty: ITy<'ctx>,
    /// where the scheme was defined
    pub(crate) source_region: Box<SourceRegion>,
}

impl<'ctx> std::hash::Hash for Scheme<'ctx> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.ty_pars.len().hash(state);
        self.ty.hash(state);
        self.source_region.hash(state);
    }
}

pub(crate) type SchemeRef<'ctx> = &'ctx Scheme<'ctx>;

impl<'ctx> FreeTypeVars<'ctx> for Scheme<'ctx> {
    fn free_unification_variables(
        &self,
        exclude: &[UnificationVarId],
        collection: &mut Vec<UnificationVarId>,
    ) {
        self.ty.free_unification_variables(exclude, collection);
    }

    fn has_any_free_unification_variables(&self, exclude: &[UnificationVarId]) -> bool {
        self.ty.has_any_free_unification_variables(exclude)
    }

    fn has_free_type_var(&self, search_id: UnificationVarId) -> bool {
        self.ty.has_free_type_var(search_id)
    }

    fn get_type_parameters(
        &self,
        exclude: &[UnificationVarId],
        collection: &mut Vec<UnificationVarId>,
    ) {
        for uniq_id in &*self.ty_pars {
            if !exclude.contains(uniq_id) && !collection.contains(uniq_id) {
                collection.push(*uniq_id);
            }
        }
        self.ty.get_type_parameters(exclude, collection);
    }

    fn has_any_type_parameters(&self, exclude: &[UnificationVarId]) -> bool {
        for uniq_id in &*self.ty_pars {
            if !exclude.contains(uniq_id) {
                return true;
            }
        }
        self.ty.has_any_type_parameters(exclude)
    }

    fn has_type_parameter(&self, search_id: UnificationVarId) -> bool {
        for uniq_id in &*self.ty_pars {
            if search_id == *uniq_id {
                return true;
            }
        }
        self.ty.has_type_parameter(search_id)
    }
}

impl<'ctx> Scheme<'ctx> {
    /// instantiate with fresh unification variables for type parameters
    pub(crate) fn instantiate(
        &self,
        ctx: &Context<'ctx>,
        source_region: &SourceRegion,
    ) -> TiResult<(Substitutions<'ctx>, ITy<'ctx>)> {
        let mut substitutions = Substitutions::new();
        for placeholder in &*self.ty_pars {
            let replacement = ctx.mk_unification_var(
                source_region,
                "replacement for type par (Scheme instantiate)",
            )?;
            substitutions.insert(*placeholder, replacement);
        }
        ctx.instantiate_constraints(&substitutions)?;
        let ty = self.ty.apply_subst(&substitutions, ctx)?.unwrap_or(self.ty);
        Ok((substitutions, ty))
    }

    /// instantiate the scheme with precalculated replacements for type parameters
    /// TODO: this is for eval and/or code gen. Should be using a different context
    pub(crate) fn instantiate_exactly(
        &self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<ITy<'ctx>> {
        if cfg!(debug_assertions) {
            let mut ty_pars = Vec::with_capacity(substitutions.len());
            self.get_type_parameters(&[], &mut ty_pars);
            assert_eq!(ty_pars.len(), substitutions.len());
            for ty_par in ty_pars {
                assert!(substitutions.get(ty_par).is_some());
            }
        }

        let ty = self.ty.apply_subst(substitutions, ctx)?.unwrap_or(self.ty);
        // TODO: don't work with constraints after type inference is over
        ctx.instantiate_constraints(substitutions)?;
        Ok(ty)
    }

    /// instantiate with known type parameters
    pub(crate) fn instantiate_with(
        &self,
        ctx: &Context<'ctx>,
        type_parameters: &[ITy<'ctx>],
        source_region: &SourceRegion,
    ) -> TiResult<ITy<'ctx>> {
        use itertools::EitherOrBoth::{Both, Left, Right};
        let mut substitutions = Substitutions::new();
        for eob in self.ty_pars.iter().zip_longest(type_parameters) {
            match eob {
                Both(placeholder, replacement) => {
                    substitutions.insert(*placeholder, *replacement);
                }
                Left(placeholder) => {
                    let replacement = ctx.mk_unification_var(
                        source_region,
                        "replacement for type par (Scheme instantiate_with)",
                    )?;
                    substitutions.insert(*placeholder, replacement);
                }
                // TODO: error
                Right(_) => (),
            }
        }
        let ty = self.ty.apply_subst(&substitutions, ctx)?.unwrap_or(self.ty);
        ctx.instantiate_constraints(&substitutions)?;
        Ok(ty)
    }
}
