use super::{traitstruct::TraitSchemeRef, Context, ITy, StringType, UnificationVarId};

pub(crate) struct TraitBuilder<'builder, 'ctx> {
    ctx: &'builder Context<'ctx>,
    name: StringType,
    self_var: UnificationVarId,
    type_parameters: Vec<(StringType, UnificationVarId)>,
    assoc_tys: Vec<(StringType, ITy<'ctx>)>,
    methods: Vec<(StringType, ITy<'ctx>)>,
}
impl<'builder, 'ctx> TraitBuilder<'builder, 'ctx> {
    /// add a method to the trait
    pub(crate) fn add_method<Name>(
        mut self,
        name: Name,
        method_type: ITy<'ctx>,
    ) -> TraitBuilder<'builder, 'ctx>
    where
        Name: Into<StringType>,
    {
        self.methods.push((name.into(), method_type));
        self
    }

    /// add an associated type to the trait
    pub(crate) fn add_type<Name>(
        mut self,
        name: Name,
        placeholder: ITy<'ctx>,
    ) -> TraitBuilder<'builder, 'ctx>
    where
        Name: Into<StringType>,
    {
        self.assoc_tys.push((name.into(), placeholder));
        self
    }

    /// finish the trait scheme
    pub(crate) fn finish(self) -> (ITy<'ctx>, TraitSchemeRef<'ctx>) {
        self.ctx.mk_trait_scheme(
            self.name,
            self.self_var,
            self.type_parameters.into_boxed_slice(),
            self.assoc_tys.iter().cloned().collect(),
            self.methods.iter().cloned().collect(),
        )
    }

    /// begin building a trait scheme
    pub(super) fn new<Name>(
        ctx: &'builder Context<'ctx>,
        name: Name,
        self_var: UnificationVarId,
        type_parameters: Vec<(StringType, UnificationVarId)>,
    ) -> TraitBuilder<'builder, 'ctx>
    where
        Name: Into<StringType>,
    {
        TraitBuilder {
            ctx,
            name: name.into(),
            self_var,
            type_parameters,
            assoc_tys: Vec::default(),
            methods: Vec::default(),
        }
    }
}
