use crate::sourceregion::SourceRegion;

use super::{BTreeMap, Debug, ITy, Occupied, TiResult, Type, UnificationVarId, Vacant};

type SubstMap<'ctx> = BTreeMap<UnificationVarId, ITy<'ctx>>;

/// substitutions to make while performing type inference
#[derive(Clone, Default)]
pub(crate) struct Substitutions<'ctx> {
    map: SubstMap<'ctx>,
}
impl<'a> Debug for Substitutions<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let is_pretty = f.alternate();
        write!(f, "Substitutions {{")?;
        if is_pretty {
            writeln!(f)?;
        }
        for (from, to) in self.iter() {
            if is_pretty {
                writeln!(f, "     {from} -> {to}")?;
            } else {
                write!(f, " {from} -> {to},  ")?;
            }
        }
        write!(f, "}}")?;
        if is_pretty {
            writeln!(f)?;
        }
        Ok(())
    }
}

impl<'ctx> Substitutions<'ctx> {
    /// look up a substitution
    pub(crate) fn get<K>(&self, k: K) -> Option<ITy<'ctx>>
    where
        UnificationVarId: From<K>,
    {
        self.map.get(&k.into()).copied()
    }

    /// add a substitution
    pub(crate) fn insert<K>(&mut self, k: K, t: ITy<'ctx>)
    where
        UnificationVarId: From<K>,
    {
        self.map.insert(k.into(), t);
    }

    /// add a substitution
    pub(crate) fn insert_if_missing<K>(&mut self, k: K, t: ITy<'ctx>)
    where
        UnificationVarId: From<K>,
    {
        match self.map.entry(k.into()) {
            Vacant(e) => {
                e.insert(t);
            }
            Occupied(_) => (),
        }
    }

    /// iterate over all substitutions
    pub(crate) fn into_iter(self) -> impl Iterator<Item = (UnificationVarId, ITy<'ctx>)> {
        self.map.into_iter()
    }

    pub(crate) fn is_empty(&self) -> bool {
        self.map.is_empty()
    }

    /// iterate over all substitutions
    pub(crate) fn iter(&self) -> impl Iterator<Item = (&UnificationVarId, &ITy<'ctx>)> {
        self.map.iter()
    }

    /// Number of substitutions in the collection
    pub(crate) fn len(&self) -> usize {
        self.map.len()
    }

    /// create a fresh substitution tracker
    pub(crate) fn new() -> Self {
        Self {
            map: BTreeMap::default(),
        }
    }

    /// after replacing out a variable, it no longer needs to be in the
    /// substitutions list, so you can use this to remove it
    pub(crate) fn remove_var(&mut self, ty: &Type<'_>, sr: &SourceRegion) -> TiResult<()> {
        let id = ty.expect_unification_var(sr)?;
        self.map.remove(&id);
        Ok(())
    }

    /// create a fresh substitution tracker
    pub(crate) fn single<K>(placeholder: K, replacement: ITy<'ctx>) -> Self
    where
        UnificationVarId: From<K>,
    {
        Self {
            map: BTreeMap::from_iter([(placeholder.into(), replacement)]),
        }
    }
}

impl<'a> FromIterator<(u32, ITy<'a>)> for Substitutions<'a> {
    fn from_iter<T: IntoIterator<Item = (u32, ITy<'a>)>>(iter: T) -> Self {
        Self {
            map: BTreeMap::from_iter(iter),
        }
    }
}
