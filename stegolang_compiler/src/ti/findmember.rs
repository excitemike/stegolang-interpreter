use super::traitstruct::TraitSchemeRef;
use super::ITy;
use crate::sourceregion::SourceRegion;
use crate::ti::Context;
use crate::ti::TiResult;
use crate::StringType;

/// get a maybe-mutable reference to a member of the type/scheme
pub(crate) trait FindMember<'ctx>
where
    Self: Sized,
{
    fn find_member(
        self,
        member_name: &StringType,
        ctx: &Context<'ctx>,
        source_region: &SourceRegion,
    ) -> TiResult<Option<ITy<'ctx>>>;
}

impl<'ctx> FindMember<'ctx> for TraitSchemeRef<'ctx> {
    fn find_member(
        self,
        member_name: &StringType,
        _ctx: &Context<'ctx>,
        _source_region: &SourceRegion,
    ) -> TiResult<Option<ITy<'ctx>>> {
        Ok(self
            .assoc_tys
            .get(member_name)
            .or_else(|| self.methods.get(member_name))
            .copied())
    }
}
