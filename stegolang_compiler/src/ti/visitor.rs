use super::{ITy, TiResult};
use crate::{
    ast::{DictExprElem, SetExprElem},
    hlir::{
        hlirnodekind::{HlirLetData, HlirNodeKind},
        HlirNode,
    },
    pattern::StegoPattern,
};

/// transform a typed syntax tree in place
pub(crate) trait Visitor<'ctx> {
    /// Don't override
    fn default_visit(&mut self, node: &mut HlirNode<'ctx>) -> TiResult<bool> {
        let mut any_changed = false;
        match &mut node.kind {
            HlirNodeKind::Identifier { .. }
            | HlirNodeKind::LiteralValue(_)
            | HlirNodeKind::UnitType => (),
            HlirNodeKind::ApplyExpr { func, args } => {
                any_changed = self.visit(func)? || any_changed;
                for node in args {
                    any_changed = self.visit(node)? || any_changed;
                }
            }
            HlirNodeKind::AssignStmt { lhs, rhs } => {
                any_changed = self.visit(lhs)? || any_changed;
                any_changed = self.visit(rhs)? || any_changed;
            }
            HlirNodeKind::BitwiseNot(node)
            | HlirNodeKind::DoExpr(node)
            | HlirNodeKind::LogicalNot(node)
            | HlirNodeKind::TypeOf(node)
            | HlirNodeKind::UnaryMinus(node)
            | HlirNodeKind::UnaryPlus(node) => any_changed = self.visit(node)? || any_changed,
            HlirNodeKind::BinOpExpr { first, rest }
            | HlirNodeKind::ComparisonOpExpr { first, rest } => {
                any_changed = self.visit(first)? || any_changed;
                for (_, node) in rest {
                    any_changed = self.visit(node)? || any_changed;
                }
            }
            HlirNodeKind::DictExpr(v) => any_changed = self.default_visit_dict(v)? || any_changed,
            HlirNodeKind::DotExpr {
                lhs,
                method_name: _,
            } => any_changed = self.visit(lhs)? || any_changed,
            HlirNodeKind::FnStmt(crate::hlir::fnstmt::FnStmt { body, params, .. })
            | HlirNodeKind::Lambda { body, params, .. } => {
                any_changed = self.default_visit_fn(
                    &mut body.borrow_mut(),
                    &mut params.borrow_mut(),
                    &mut node.inferred_type,
                )? || any_changed;
            }
            HlirNodeKind::IfExpr {
                arms,
                catch_all_else,
            } => {
                for arm in arms {
                    any_changed = self.visit(&mut arm.cond)? || any_changed;
                    any_changed = self.visit(&mut arm.consequent)? || any_changed;
                }
                if let Some(node) = catch_all_else {
                    any_changed = self.visit(node)? || any_changed;
                }
            }
            HlirNodeKind::InExpr {
                container,
                invert: _,
                item,
            } => {
                any_changed = self.visit(container)? || any_changed;
                any_changed = self.visit(item)? || any_changed;
            }
            HlirNodeKind::Let(HlirLetData {
                pattern,
                value_expr,
                substitutions: _,
            }) => {
                any_changed = self.visit_pattern(pattern)? || any_changed;
                any_changed = self.visit(value_expr)? || any_changed;
            }
            HlirNodeKind::NamedExpr { value, .. } => {
                any_changed = self.visit(value)? || any_changed;
            }
            HlirNodeKind::ReturnStmt(maybe_node) => {
                if let Some(node) = maybe_node {
                    any_changed = self.visit(node)? || any_changed;
                }
            }
            HlirNodeKind::SetExpr(v) => {
                for elem in v {
                    match elem {
                        SetExprElem::Spread(node) | SetExprElem::Value(node) => {
                            any_changed = self.visit(node)? || any_changed;
                        }
                    }
                }
            }
            HlirNodeKind::Statements { statements: v } | HlirNodeKind::Tuple(v) => {
                for node in v {
                    any_changed = self.visit(node)? || any_changed;
                }
            }
        }
        if let Some(ty) = self.visit_type(node.inferred_type)? {
            node.inferred_type = ty;
            any_changed = true;
        }
        Ok(any_changed)
    }

    /// don't override
    fn default_visit_dict(&mut self, v: &mut [DictExprElem<HlirNode<'ctx>>]) -> TiResult<bool> {
        let mut any_changed = true;
        for elem in v {
            match elem {
                crate::ast::DictExprElem::KeywordSpread(node) => {
                    any_changed = self.visit(node)? || any_changed;
                }
                crate::ast::DictExprElem::KeyValue(k, v) => {
                    any_changed = self.visit(k)? || any_changed;
                    any_changed = self.visit(v)? || any_changed;
                }
            }
        }
        Ok(any_changed)
    }

    /// don't override
    fn default_visit_fn(
        &mut self,
        body: &mut HlirNode<'ctx>,
        params: &mut [StegoPattern],
        result: &mut ITy<'ctx>,
    ) -> TiResult<bool> {
        let mut any_changed = true;
        for pattern in params {
            any_changed = self.visit_pattern(pattern)? || any_changed;
        }
        any_changed = self.visit(body)? || any_changed;
        if let Some(ty) = self.visit_type(*result)? {
            *result = ty;
            any_changed = true;
        }
        Ok(any_changed)
    }

    /// visit each node of the tree
    fn visit(&mut self, node: &mut HlirNode<'ctx>) -> TiResult<bool> {
        self.default_visit(node)
    }
    fn visit_pattern(&mut self, pat: &StegoPattern) -> TiResult<bool>;
    fn visit_type(&mut self, ty: ITy<'ctx>) -> TiResult<Option<ITy<'ctx>>>;
}
