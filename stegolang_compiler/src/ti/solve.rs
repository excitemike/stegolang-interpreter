use crate::{
    hlir::HlirNode,
    loc,
    sourceregion::SourceRegion,
    ti::{
        freetypevars::FreeTypeVars, substitute::ApplySubst, traitstruct::TraitStructRef, Context,
        FnType, ITy, Substitutions, TiError, TiResult, Type, UnificationVarId,
    },
    StringType,
};

use super::{constraint::Constraint, traitstruct::TraitSchemeRef};

pub(crate) enum SolveResult<'a> {
    NoProgress,
    Satisfied,
    NewConstraints,
    Substitute(UnificationVarId, ITy<'a>),
}

/// begin solving traits
pub(crate) fn solve_constraints<'a>(
    ctx: &Context<'a>,
    typed_node: &HlirNode<'a>,
) -> TiResult<Option<HlirNode<'a>>> {
    let mut any_change;
    let mut old_constraints = ctx.constraints.take();
    let mut updated_ast = None;

    'a: loop {
        if old_constraints.is_empty() {
            break 'a;
        }
        any_change = false;
        // save substitutions for a second pass
        let mut subs = Substitutions::new();
        for constraint in &old_constraints {
            let solve_result = solve_single_constraint(ctx, constraint)?;
            match solve_result {
                SolveResult::NoProgress => {
                    // put the constraint back
                    ctx.constraints.borrow_mut().push(*constraint);
                }
                SolveResult::Satisfied => {}
                SolveResult::NewConstraints => {
                    any_change = true;
                    // other solve methods already added the constraint
                }
                // save substitutions for a second pass
                SolveResult::Substitute(type_var, replacement) => {
                    any_change = true;
                    subs.insert(type_var, replacement);
                }
            }
        }
        // make substitutions before looping again
        let subs = subs;
        if !subs.is_empty() {
            // scope
            // TODO: I don't think I use scope anymore at this point in
            // compilation? So I should probably be using a context struct without it.
            ctx.apply_substitutions_in_current_scope(&subs)?;
            // constraints
            ctx.apply_subst_to_constraints(&subs)?;
            // AST
            let sub_result = updated_ast
                .as_ref()
                .unwrap_or(typed_node)
                .apply_subst(&subs, ctx)?;
            if sub_result.is_some() {
                updated_ast = sub_result;
            }
        }
        if !any_change {
            break;
        }
        old_constraints = ctx.constraints.take();
    }
    if !ctx.constraints.borrow().is_empty() {
        // TODO: no reason I can't report multiple errors here
        let cs = ctx.constraints.take();

        // eprintln!("unsolved constraints:\n  {}", cs.iter().join("\n  "));

        // for c in &cs {
        //     let mut free_vars = Vec::new();
        //     c.free_unification_variables(&[], &mut free_vars);
        //     if !free_vars.is_empty() {
        //         return Err(TiError::new(
        //             loc::err::unresolved_type(),
        //             c.inner().get_source_region().clone(),
        //         ));
        //     }
        // }

        if let Some(c) = cs.first() {
            return Err(TiError::from_constraint(c));
        }
    }
    Ok(updated_ast)
}

/// attempt solving to make `implementation` the implementation of
/// `trait_struct` for `for_ty`
fn solve_implementation<'a>(
    trait_struct: TraitStructRef<'a>,
    for_ty: ITy<'a>,
    ctx: &Context<'a>,
    source_region: &SourceRegion,
) -> TiResult<SolveResult<'a>> {
    if for_ty.is_final() {
        const MAX_POSSIBILITIES: usize = 4;
        let mut found_impl = None;
        let mut error_message_labels = vec![];
        for impl_scheme in ctx.implementation_schemes_for_trait(trait_struct) {
            // TODO: can we modify can_unify to not require instantiation first? maybe could_instantiate_and_unify?
            let implementation = impl_scheme.instantiate(ctx, source_region)?;
            if implementation.implemented_for.can_unify(for_ty) {
                let s0 = ctx.unify_traits(
                    implementation.implemented_trait,
                    trait_struct,
                    source_region,
                )?;
                let s1 = ctx.unify_eq(implementation.implemented_for, for_ty, source_region)?;
                let s = ctx.compose(s1, s0)?;
                let implementation = implementation
                    .apply_subst(&s, ctx)?
                    .unwrap_or(implementation);

                found_impl = found_impl.or(Some(implementation));
                error_message_labels.push(impl_scheme.debug_name());
                if error_message_labels.len() >= MAX_POSSIBILITIES {
                    break;
                }
            }
        }
        // Uh oh! More than one possibility!
        if error_message_labels.len() > 1 {
            let msg_tuple = loc::err::overlapping_implementations_of_trait(
                &trait_struct.to_string(),
                &for_ty.to_string(),
                error_message_labels.iter(),
            );
            return Err(TiError::new(msg_tuple, source_region.clone()));
        }
        // if found, we can set it equal to `implementation`
        if let Some(found_impl) = found_impl {
            if found_impl.has_any_free_unification_variables(&[]) {
                let mut found_any = false;
                if found_impl.implemented_for != for_ty {
                    found_any = true;
                    ctx.add_constraint(Constraint::Eq(
                        found_impl.implemented_for,
                        for_ty,
                        Box::new(source_region.clone()),
                    ));
                }
                if !found_any {
                    return Ok(SolveResult::NoProgress);
                }
                // we needed a finalized type to be finished but we can
                // retry this constraint later
                ctx.add_constraint(Constraint::Implementation {
                    trait_struct,
                    for_ty,
                    source_region: Box::new(source_region.clone()),
                });
            } else {
                ctx.store_implementation(for_ty, trait_struct, found_impl);
                let mut impl_vars = Vec::new();
                found_impl.get_type_parameters(&[], &mut impl_vars);
                return Ok(SolveResult::Satisfied);
            }

            return Ok(SolveResult::NewConstraints);
        }
    }
    Ok(SolveResult::NoProgress)
}

/// tries to do type/trait solving for one constraint in particular
pub(crate) fn solve_single_constraint<'a>(
    ctx: &Context<'a>,
    constraint: &Constraint<'a>, // TODO: consume so that I don't need to clone the sourceregion
) -> TiResult<SolveResult<'a>> {
    match constraint {
        Constraint::Eq(t0, t1, source_region) => solve_eq(ctx, *t0, *t1, source_region),
        Constraint::Implementation {
            trait_struct,
            for_ty,
            source_region,
        } => solve_implementation(trait_struct, *for_ty, ctx, source_region),
        Constraint::Member {
            obj_ty,
            field_ty,
            name,
            source_region,
        } => solve_member(*obj_ty, *field_ty, name, ctx, source_region),
        Constraint::Method {
            for_ty,
            method_ty,
            trait_,
            name,
            source_region,
        } => solve_method(*for_ty, *method_ty, name, trait_, ctx, source_region),
    }
}

/// try to return the substitutions that would make the two types equal,
/// failing that, see if the constraint can be simplified to another constraint
/// failing that, report that no progress could be made
pub(crate) fn solve_eq<'a>(
    ctx: &Context<'a>,
    t0: ITy<'a>,
    t1: ITy<'a>,
    source_region: &SourceRegion,
) -> TiResult<SolveResult<'a>> {
    // types already equal
    if t0 == t1 {
        return Ok(SolveResult::Satisfied);
    }
    // TODO: try to simplify types before continuing
    match (t0.inner(), t1.inner()) {
        // these cases are covered by the equality check above
        // (Type::Nothing, Type::Nothing)
        // | (Type::Bool, Type::Bool)
        // | (Type::Int64, Type::Int64)
        // | (Type::BigInt, Type::BigInt)
        // | (Type::Float64, Type::Float64)
        // | (Type::Char, Type::Char)
        // | (Type::String, Type::String) => Ok(Substitutions::new()),
        (Type::F(fn0), Type::F(fn1)) => Ok(solve_fns_eq(ctx, fn0, fn1, source_region)),
        (Type::F(f), _) if f.params.is_empty() => {
            ctx.add_constraint(Constraint::Eq(
                t1,
                f.result,
                Box::new(source_region.clone()),
            ));
            Ok(SolveResult::NewConstraints)
        }
        (_, Type::F(f)) if f.params.is_empty() => {
            ctx.add_constraint(Constraint::Eq(
                t0,
                f.result,
                Box::new(source_region.clone()),
            ));
            Ok(SolveResult::NewConstraints)
        }
        (Type::UnificationVar { uniq_id, .. }, _) => {
            if t1.has_free_type_var(*uniq_id) {
                todo!("handle types referring to themselves");
            } else {
                Ok(SolveResult::Substitute(*uniq_id, t1))
            }
        }
        (_, Type::UnificationVar { uniq_id, .. }) => {
            if t0.has_free_type_var(*uniq_id) {
                todo!("handle types referring to themselves");
            } else {
                Ok(SolveResult::Substitute(*uniq_id, t0))
            }
        }
        (Type::TraitScheme(ts0), Type::TraitScheme(ts1)) => {
            solve_trait_scheme_eq(ctx, ts0, ts1, source_region)
        }
        (Type::TraitStruct(ts0), Type::TraitStruct(ts1)) => {
            solve_trait_struct_eq(ctx, ts0, ts1, source_region)
        }
        (
            Type::Dict {
                key: key0,
                value: value0,
            },
            Type::Dict {
                key: key1,
                value: value1,
            },
        ) => {
            let mut any_changed = false;
            if key0 != key1 {
                any_changed = true;
                ctx.add_constraint(Constraint::Eq(
                    *key0,
                    *key1,
                    Box::new(source_region.clone()),
                ));
            }
            if value0 != value1 {
                any_changed = true;
                ctx.add_constraint(Constraint::Eq(
                    *value0,
                    *value1,
                    Box::new(source_region.clone()),
                ));
            }
            if any_changed {
                Ok(SolveResult::NewConstraints)
            } else {
                Ok(SolveResult::Satisfied)
            }
        }
        (Type::Set(t0), Type::Set(t1)) | (Type::Type(t0), Type::Type(t1)) => {
            ctx.add_constraint(Constraint::Eq(*t0, *t1, Box::new(source_region.clone())));
            Ok(SolveResult::NewConstraints)
        }
        (Type::Tuple(v0), Type::Tuple(v1)) if v0.len() == v1.len() => {
            let mut any_changed = false;
            for (l, r) in v0.iter().zip(v1.iter()) {
                if l != r {
                    any_changed = true;
                    ctx.add_constraint(Constraint::Eq(*l, *r, Box::new(source_region.clone())));
                }
            }
            if any_changed {
                Ok(SolveResult::NewConstraints)
            } else {
                Ok(SolveResult::Satisfied)
            }
        }
        // anything else is either a type error or a case I forgot to handle
        (left, right) => Err(TiError::new(
            loc::err::type_mismatch(&left.to_string(), &right.to_string()),
            source_region.clone(),
        )),
    }
}

/// try to return the substitutions that would make the two types equal,
/// failing that, see if the constraint can be simplified to another constraint
/// failing that, report that no progress could be made
fn solve_trait_scheme_eq<'a>(
    ctx: &Context<'a>,
    ts0: TraitSchemeRef<'a>,
    ts1: TraitSchemeRef<'a>,
    source_region: &SourceRegion,
) -> TiResult<SolveResult<'a>> {
    let mut any_change = false;
    if !ts0.ty_pars.is_empty() || !ts1.ty_pars.is_empty() {
        todo!("use ty_pars");
    }
    let mismatch = (ts0.self_var != ts1.self_var)
        || (ts0.trait_name != ts1.trait_name)
        || (ts0.assoc_tys.len() != ts1.assoc_tys.len())
        || (ts0.methods.len() != ts1.methods.len())
        || {
            let mut mismatch = false;
            'outer: for (a, b) in [
                (&ts0.assoc_tys, &ts1.assoc_tys),
                (&ts0.methods, &ts1.methods),
            ] {
                for ((k0, v0), (k1, v1)) in a.iter().zip(b.iter()) {
                    if k0 != k1 {
                        mismatch = true;
                        break 'outer;
                    }
                    any_change = true;
                    ctx.add_constraint(Constraint::Eq(*v0, *v1, Box::new(source_region.clone())));
                }
            }
            mismatch
        };
    if mismatch {
        Err(TiError::new(
            loc::err::type_mismatch(&ts0.to_string(), &ts1.to_string()),
            source_region.clone(),
        ))
    } else if any_change {
        Ok(SolveResult::NewConstraints)
    } else {
        Ok(SolveResult::Satisfied)
    }
}

/// try to return the substitutions that would make the two types equal,
/// failing that, see if the constraint can be simplified to another constraint
/// failing that, report that no progress could be made
fn solve_trait_struct_eq<'a>(
    ctx: &Context<'a>,
    ts0: TraitStructRef<'a>,
    ts1: TraitStructRef<'a>,
    source_region: &SourceRegion,
) -> TiResult<SolveResult<'a>> {
    let mut any_changed = false;
    // typar_choices is checked once we know it is not a mismatch
    let mismatch = (ts0.self_var != ts1.self_var)
        || (ts0.trait_name != ts1.trait_name)
        || (ts0.typar_choices.len() != ts1.typar_choices.len())
        || (ts0.assoc_tys.len() != ts1.assoc_tys.len())
        || (ts0.methods.len() != ts1.methods.len())
        || {
            let mut mismatch = false;
            'outer: for (a, b) in [
                (&ts0.assoc_tys, &ts1.assoc_tys),
                (&ts0.methods, &ts1.methods),
            ] {
                for ((k0, v0), (k1, v1)) in a.iter().zip(b.iter()) {
                    if k0 != k1 {
                        mismatch = true;
                        break 'outer;
                    }
                    if v0 != v1 {
                        any_changed = true;
                        ctx.add_constraint(Constraint::Eq(
                            *v0,
                            *v1,
                            Box::new(source_region.clone()),
                        ));
                    }
                }
            }
            mismatch
        };
    if mismatch {
        Err(TiError::new(
            loc::err::type_mismatch(&ts0.to_string(), &ts1.to_string()),
            source_region.clone(),
        ))
    } else {
        for (ty0, ty1) in ts0.typar_choices.iter().zip(ts1.typar_choices.iter()) {
            if ty0 != ty1 {
                any_changed = true;
                ctx.add_constraint(Constraint::Eq(*ty0, *ty1, Box::new(source_region.clone())));
            }
        }
        if any_changed {
            Ok(SolveResult::NewConstraints)
        } else {
            Ok(SolveResult::Satisfied)
        }
    }
}

/// try to return the substitutions that would make the two fn types equal,
/// failing that, see if the constraint can be simplified to another constraint
/// failing that, report that no progress could be made
fn solve_fns_eq<'a>(
    ctx: &Context<'a>,
    fn0: &FnType<'a>,
    fn1: &FnType<'a>,
    source_region: &SourceRegion,
) -> SolveResult<'a> {
    // parameters become equality constraints
    for (a, b) in fn0.params.iter().zip(fn1.params.iter()) {
        ctx.add_constraint(Constraint::Eq(*a, *b, Box::new(source_region.clone())));
    }

    // if one take more parameters than another, must compare a partial
    // application to a return type
    let r0 = if fn0.params.len() > fn1.params.len() {
        ctx.mk_fn_type_from_iter(fn0.params[fn1.params.len()..].iter().copied(), fn0.result)
    } else {
        fn0.result
    };
    let r1 = if fn1.params.len() < fn1.params.len() {
        ctx.mk_fn_type_from_iter(fn1.params[fn1.params.len()..].iter().copied(), fn1.result)
    } else {
        fn1.result
    };
    ctx.add_constraint(Constraint::Eq(r0, r1, Box::new(source_region.clone())));
    SolveResult::NewConstraints
}

/// try to resolve a constraint about a member of another type
fn solve_member<'a>(
    obj_ty: ITy<'a>,
    field_ty: ITy<'a>,
    name: &StringType,
    ctx: &Context<'a>,
    source_region: &SourceRegion,
) -> TiResult<SolveResult<'a>> {
    if let Type::UnificationVar { .. } = obj_ty.inner() {
        // if the containing type is still a unification var, we can make no progress
        Ok(SolveResult::NoProgress)
    } else if let Some(ty) = obj_ty.find_member_direct(name) {
        if ty == field_ty {
            // it checks out!
            Ok(SolveResult::Satisfied)
        } else {
            solve_eq(ctx, field_ty, ty, source_region)
        }
    } else if let Some(ty) = ctx.find_in_traits(obj_ty, name, source_region)? {
        if ty == field_ty {
            // it checks out!
            Ok(SolveResult::Satisfied)
        } else {
            solve_eq(ctx, field_ty, ty, source_region)
        }
    } else {
        Err(TiError::new(
            loc::err::member_of_unresolved_type(&obj_ty.to_string(), name),
            source_region.clone(),
        ))
    }
}

/// try to resolve a constraint about a method
fn solve_method<'a>(
    for_ty: ITy<'a>,
    method_ty: ITy<'a>,
    name: &StringType,
    trait_: TraitStructRef<'a>,
    ctx: &Context<'a>,
    source_region: &SourceRegion,
) -> TiResult<SolveResult<'a>> {
    if let Type::UnificationVar { .. } = for_ty.inner() {
        // if the containing type is still a unification var, we can make no progress
        return Ok(SolveResult::NoProgress);
    }
    // if we have no type variable as a type parameter to the trait,
    // we can use the cheaper search by trait
    let schemes = if trait_.is_final() {
        ctx.implementation_schemes_for_trait(trait_)
    } else {
        ctx.implementation_schemes_with_member_name(name)
    };
    let mut possibilities = Vec::new();
    for impl_scheme in schemes {
        if trait_.could_be_instance(impl_scheme.implementation.implemented_trait)
            && for_ty.could_be_instance(impl_scheme.implementation.implemented_for)
        {
            let impl_inst = impl_scheme.instantiate(ctx, source_region)?;
            if let Some((found_ty, _fn_fam)) = impl_inst.find_member_function(name) {
                possibilities.push((impl_inst, found_ty));
            } else {
                return Err(TiError::new(
                    loc::err::missing_method(name, &trait_.trait_name),
                    source_region.clone(),
                ));
            }
        }
    }

    if possibilities.len() > 1 {
        if for_ty.is_final() && trait_.is_final() {
            // found too many possibilities
            Err(TiError::new(
                loc::err::ambiguous_member_lookup(
                    name,
                    &for_ty.to_string(),
                    possibilities
                        .iter()
                        .map(|(impl_inst, _)| impl_inst.to_string()),
                ),
                source_region.clone(),
            ))
        } else {
            // maybe once other constraints narrow things down we can solve it.
            Ok(SolveResult::NoProgress)
        }
    } else if possibilities.is_empty() {
        // nothing resolved above, so we either need to wait for something to be figured out or there's an error
        if trait_.is_final() {
            Err(TiError::new(
                loc::err::no_implementation_found(&trait_.trait_name, &for_ty.to_string()),
                source_region.clone(),
            ))
        } else {
            Ok(SolveResult::NoProgress)
        }
    } else {
        let (_, found_ty) = possibilities.first().unwrap();
        if found_ty == &method_ty {
            Ok(SolveResult::Satisfied)
        } else {
            solve_eq(ctx, method_ty, *found_ty, source_region)
        }
    }
}
