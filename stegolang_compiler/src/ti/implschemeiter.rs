use super::ImplSchemeRef;

pub(crate) struct ImplSchemeIter<'ctx>(
    <std::vec::Vec<ImplSchemeRef<'ctx>> as std::iter::IntoIterator>::IntoIter,
);

impl<'ctx> ImplSchemeIter<'ctx> {
    pub fn new(src: &[ImplSchemeRef<'ctx>]) -> Self {
        let v = src.to_owned();
        Self(v.into_iter())
    }
}

impl<'ctx> Iterator for ImplSchemeIter<'ctx> {
    type Item = ImplSchemeRef<'ctx>;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }
}
