//! apply substitutions recursively
use itertools::Itertools;

use super::{
    substitutions::Substitutions,
    traitstruct::{TraitSchemeRef, TraitStructRef},
    visitor::Visitor,
    Context, ITy, Implementation, ImplementationRef, Scheme, TiResult, Type, UnificationVarId,
};
use crate::{
    ast::{DictExprElem, IfArm, Operator, SetExprElem},
    fntypes::{fnfamily::FnFamily, fnfamimpls::FnFamImpls},
    hlir::HlirNode,
    hlir::{
        fnstmt::FnStmt,
        hlirnodekind::{HlirLetData, HlirNodeKind},
    },
    pattern::{StegoPattern, StegoPatternKind},
    stegoobject::StegoObject,
    ti::FnType,
    typedvalue::{TypedValue, TypedValueKind},
    StringType,
};
use std::{
    borrow::{Borrow, Cow},
    cell::RefCell,
    collections::BTreeMap,
    rc::Rc,
    sync::Arc,
};

pub(super) struct Substituter<'substituter, 'ctx> {
    ctx: &'substituter Context<'ctx>,
    substitutions: &'substituter Substitutions<'ctx>,
    ignore_stack: Vec<Box<[ITy<'ctx>]>>,
}

type MemberFuncMap<'ctx> = BTreeMap<StringType, (ITy<'ctx>, Rc<FnFamily<'ctx>>)>;

impl<'substituter, 'ctx> Substituter<'substituter, 'ctx> {
    /// create a new substituter
    pub(super) fn new(
        ctx: &'substituter Context<'ctx>,
        substitutions: &'substituter Substitutions<'ctx>,
    ) -> Self {
        Self {
            ctx,
            substitutions,
            ignore_stack: Vec::default(),
        }
    }

    /// check whether a var has already been ignored
    fn is_ignored(&self, ty: ITy<'ctx>) -> bool {
        for ignores in self.ignore_stack.iter().rev() {
            if ignores.contains(&ty) {
                return true;
            }
        }
        false
    }

    /// remove the last set of types to ignore
    fn pop_ignore(&mut self) {
        self.ignore_stack.pop();
    }

    /// add a new set of types to ignore
    fn push_ignore(&mut self, ignore_list: &[ITy<'ctx>]) {
        self.ignore_stack
            .push(ignore_list.iter().copied().collect_vec().into_boxed_slice());
    }

    /// Do something while ignoring certain vars, making sure they get popped even if there is an error.
    fn while_ignoring<F, R>(&mut self, ignore_list: &[ITy<'ctx>], f: F) -> R
    where
        F: FnOnce(&mut Self) -> R,
    {
        if ignore_list.is_empty() {
            return f(self);
        }
        self.push_ignore(ignore_list);
        let result = f(self);
        self.pop_ignore();
        result
    }

    fn visit_pattern_kind(&mut self, kind: &StegoPatternKind) -> TiResult<bool> {
        // TODO: I don't see a way this ever could change anything? Should it even visit patterns at all?
        match kind {
            StegoPatternKind::Capture(_) | StegoPatternKind::Literal(_) => Ok(false),
            StegoPatternKind::Or(v) => {
                let mut any_changed = false;
                for pat in v {
                    any_changed = self.visit_pattern_kind(pat)? || any_changed;
                }
                Ok(any_changed)
            }
            StegoPatternKind::Named(_, pat) => self.visit_pattern(pat),
        }
    }

    /// visit each value in a map
    fn visit_member_funcs(
        &mut self,
        map: &MemberFuncMap<'ctx>,
    ) -> TiResult<Option<MemberFuncMap<'ctx>>> {
        if self.substitutions.is_empty() {
            return Ok(None);
        }
        let mut result: Option<MemberFuncMap<'ctx>> = None;

        for (k, (ty, fn_fam)) in map {
            // don't clone until we hit one that changes
            if let Some(ty) = self.visit_type(*ty)? {
                match &mut result {
                    // already cloned. just replace current element
                    Some(new_map) => {
                        new_map.insert(k.clone(), (ty, Rc::clone(fn_fam)));
                    }
                    // not cloned yet. clone now
                    None => {
                        let mut new_map = map.clone();
                        new_map.insert(k.clone(), (ty, Rc::clone(fn_fam)));
                        result = Some(new_map);
                    }
                }
            }
        }

        Ok(result)
    }

    /// visit each value in a map
    fn visit_map<K, Q: ?Sized>(
        &mut self,
        map: &BTreeMap<K, ITy<'ctx>>,
    ) -> TiResult<Option<BTreeMap<K, ITy<'ctx>>>>
    where
        K: Borrow<Q> + Ord + Clone,
        Q: Ord,
    {
        if self.substitutions.is_empty() {
            return Ok(None);
        }
        let mut result: Option<BTreeMap<K, ITy<'ctx>>> = None;

        for (k, v) in map {
            // don't clone until we hit one that changes
            if let Some(v) = self.visit_type(*v)? {
                match &mut result {
                    // already cloned. just replace current element
                    Some(new_map) => {
                        new_map.insert(k.clone(), v);
                    }
                    // not cloned yet. clone now
                    None => {
                        let mut new_map = map.clone();
                        new_map.insert(k.clone(), v);
                        result = Some(new_map);
                    }
                }
            }
        }

        Ok(result)
    }

    /// apply substitutions to a trait struct
    fn visit_trait_struct(
        &mut self,
        trait_struct: TraitStructRef<'ctx>,
    ) -> TiResult<Option<TraitStructRef<'ctx>>> {
        let typars = self.visit_boxed_slice(&trait_struct.typar_choices)?;
        let assoc_tys = self.visit_map(&trait_struct.assoc_tys)?;
        let methods = self.visit_map(&trait_struct.methods)?;

        if let (None, None, None) = (&typars, &assoc_tys, &methods) {
            return Ok(None);
        }
        let trait_ = self.ctx.alloc_trait(
            typars.unwrap_or_else(|| trait_struct.typar_choices.clone()),
            trait_struct.self_var,
            trait_struct.trait_name.clone(),
            assoc_tys.unwrap_or_else(|| trait_struct.assoc_tys.clone()),
            methods.unwrap_or_else(|| trait_struct.methods.clone()),
        );
        Ok(Some(trait_))
    }
    /// visit each element of a boxed slice
    fn visit_boxed_slice(&mut self, v: &[ITy<'ctx>]) -> TiResult<Option<Box<[ITy<'ctx>]>>> {
        if self.substitutions.is_empty() {
            return Ok(None);
        }
        let mut result: Option<Vec<ITy<'ctx>>> = None;
        for (i, x) in v.iter().enumerate() {
            // don't clone until we hit one that changes
            if let Some(x) = self.visit_type(*x)? {
                match &mut result {
                    // already cloned. just replace current element
                    Some(new_vec) => {
                        if let Some(dst) = new_vec.get_mut(i) {
                            *dst = x;
                        }
                    }
                    // not cloned yet. clone now
                    None => {
                        let mut new_vec = v.iter().copied().collect_vec();
                        if let Some(dst) = new_vec.get_mut(i) {
                            *dst = x;
                        }
                        result = Some(new_vec);
                    }
                }
            }
        }
        Ok(result.map(std::vec::Vec::into_boxed_slice))
    }

    /// do substitutions for a fn type
    fn visit_fn(&mut self, params: &[ITy<'ctx>], result: ITy<'ctx>) -> TiResult<Option<ITy<'ctx>>> {
        let mut any_params_changed = false;
        let mut updated_params = Vec::with_capacity(params.len());
        for &param in params {
            match self.visit_type(param)? {
                Some(param) => {
                    updated_params.push(param);
                    any_params_changed = true;
                }
                None => updated_params.push(param),
            }
        }
        let result_changed;
        let result = if let Some(result) = self.visit_type(result)? {
            result_changed = true;
            result
        } else {
            result_changed = false;
            result
        };
        if any_params_changed || result_changed {
            Ok(Some(
                self.ctx
                    .mk_fn_type(updated_params.into_boxed_slice(), result),
            ))
        } else {
            Ok(None)
        }
    }

    /// visit each element of a vec
    #[allow(clippy::ptr_arg)]
    fn visit_vec(&mut self, v: &Vec<ITy<'ctx>>) -> TiResult<Option<Vec<ITy<'ctx>>>> {
        if self.substitutions.is_empty() {
            return Ok(None);
        }
        let mut result: Option<Vec<ITy<'ctx>>> = None;
        for (i, x) in v.iter().enumerate() {
            // don't clone until we hit one that changes
            if let Some(x) = self.visit_type(*x)? {
                match &mut result {
                    // already cloned. just replace current element
                    Some(new_vec) => {
                        if let Some(dst) = new_vec.get_mut(i) {
                            *dst = x;
                        }
                    }
                    // not cloned yet. clone now
                    None => {
                        let mut new_vec = v.clone();
                        if let Some(dst) = new_vec.get_mut(i) {
                            *dst = x;
                        }
                        result = Some(new_vec);
                    }
                }
            }
        }
        Ok(result)
    }

    /// apply substitutions to implementation
    fn visit_implementation(
        &mut self,
        imp: ImplementationRef<'ctx>,
    ) -> TiResult<Option<ImplementationRef<'ctx>>> {
        if self.substitutions.is_empty() {
            return Ok(None);
        }
        let Implementation {
            implemented_for,
            implemented_trait,
            member_functions,
            member_types,
            source_region: impl_source_region,
        } = imp;
        let implemented_for = self.visit_type(*implemented_for)?;
        let implemented_trait = self.visit_trait_struct(implemented_trait)?;
        let member_functions = self.visit_member_funcs(member_functions)?;
        let member_types = self.visit_map(member_types)?;
        let any_changed = implemented_for.is_some()
            || implemented_trait.is_some()
            || member_functions.is_some()
            || member_types.is_some();

        if any_changed {
            Ok(Some(self.ctx.mk_implementation(Implementation {
                implemented_for: implemented_for.unwrap_or(imp.implemented_for),
                implemented_trait: implemented_trait.unwrap_or(imp.implemented_trait),
                member_functions: member_functions.unwrap_or_else(|| imp.member_functions.clone()),
                member_types: member_types.unwrap_or_else(|| imp.member_types.clone()),
                source_region: impl_source_region.clone(),
            })))
        } else {
            Ok(None)
        }
    }

    /// handles the work of `visit_type` after checks
    fn visit_type_internal(&mut self, ty: ITy<'ctx>) -> TiResult<Option<ITy<'ctx>>> {
        match ty.inner() {
            // replacing by unification var id
            Type::TypeParameter { uniq_id, .. } | Type::UnificationVar { uniq_id, .. } => {
                match self.substitutions.get(*uniq_id) {
                    Some(ty) => Ok(Some(ty)),
                    None => Ok(None),
                }
            }
            Type::Nothing
            | Type::Bool
            | Type::Int64
            | Type::BigInt
            | Type::Float64
            | Type::Char
            | Type::String => Ok(None),
            Type::F(FnType { params, result }) => self.visit_fn(params, *result),
            Type::Dict { key, value } => {
                let key2 = self.visit_type(*key)?;
                let value2 = self.visit_type(*value)?;
                if key2.is_some() || value2.is_some() {
                    Ok(Some(
                        self.ctx
                            .mk_dict(key2.unwrap_or(*key), value2.unwrap_or(*value)),
                    ))
                } else {
                    Ok(None)
                }
            }
            Type::Set(t) => self.visit_type(*t).map(|x| x.map(|t| self.ctx.mk_set(t))),
            Type::Tuple(tys) => Ok(self.visit_vec(tys)?.map(|tys| self.ctx.mk_tuple(tys))),
            Type::ElementOf(x, source_region) => match self.visit_type(*x)? {
                Some(x) => match x.iterable_type(self.ctx, source_region)? {
                    Some(x) => self.visit_type(x),
                    None => Ok(None),
                },
                None => Ok(None),
            },
            Type::Struct(members) => match self.visit_map(members)? {
                Some(members) => Ok(Some(self.ctx.mk_struct_type(members))),
                None => Ok(None),
            },
            Type::TraitScheme(ts) => {
                let assoc_tys = self.visit_map(&ts.assoc_tys)?;
                let methods = self.visit_map(&ts.methods)?;
                if let (None, None) = (&assoc_tys, &methods) {
                    return Ok(None);
                }
                Ok(Some(
                    self.ctx
                        .mk_trait_scheme(
                            ts.trait_name.clone(),
                            ts.self_var,
                            ts.ty_pars.clone(),
                            assoc_tys.unwrap_or_else(|| ts.assoc_tys.clone()),
                            methods.unwrap_or_else(|| ts.methods.clone()),
                        )
                        .0,
                ))
            }
            Type::TraitStruct(trait_) => match self.visit_trait_struct(trait_)? {
                Some(trait_) => Ok(Some(self.ctx.mk_trait(trait_))),
                None => Ok(None),
            },
            Type::Vector(t) => self.visit_type(*t),
            Type::Union(in_variants) => {
                let mut any_changed = false;
                let mut updated_variants = Vec::with_capacity(in_variants.len());
                for variant in in_variants {
                    match self.visit_type(*variant)? {
                        None => updated_variants.push(*variant),
                        Some(variant) => {
                            updated_variants.push(variant);
                            any_changed = true;
                        }
                    }
                }
                if any_changed {
                    Ok(Some(self.ctx.mk_union(updated_variants)))
                } else {
                    Ok(None)
                }
            }
            Type::Variant(crate::ti::Variant { name, variant_type }) => {
                match self.visit_type(*variant_type)? {
                    Some(variant_type) => Ok(Some(self.ctx.mk_variant(name.clone(), variant_type))),
                    None => Ok(None),
                }
            }
            Type::Type(scheme) => match self.visit_type(*scheme)? {
                None => Ok(None),
                Some(ty) => Ok(Some(self.ctx.mk_typetype(ty))),
            },
            Type::TypeScheme(Scheme {
                ty_pars: _,
                ty: inner,
                source_region: _,
            }) => self.visit_type(*inner),
            Type::Implementation(imp) => {
                let x = self.visit_implementation(imp)?;
                Ok(x.map(|i| self.ctx.mk_implementation_type(i)))
            }
        }
    }
}

impl<'substituter, 'ctx> Visitor<'ctx> for Substituter<'substituter, 'ctx> {
    fn visit(&mut self, node: &mut HlirNode<'ctx>) -> TiResult<bool> {
        if self.substitutions.is_empty() {
            return Ok(false);
        }
        self.default_visit(node)
    }

    fn visit_pattern(&mut self, pat: &crate::pattern::StegoPattern) -> TiResult<bool> {
        self.visit_pattern_kind(&pat.kind)
    }

    fn visit_type(&mut self, ty: ITy<'ctx>) -> TiResult<Option<ITy<'ctx>>> {
        if self.substitutions.is_empty() {
            return Ok(None);
        }
        if self.is_ignored(ty) {
            return Ok(None);
        }
        self.while_ignoring(&[ty], |f| f.visit_type_internal(ty))
    }
}

pub(crate) trait ApplySubst<'ctx>: Clone + Sized {
    /// apply substitutions to something. Returns Ok(Some(...)) with an updated
    /// version if any replacements were found,
    /// Ok(None) if nothing changed,
    /// and Err(..) if something went horribly wrong
    fn apply_subst(
        &self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<Option<Self>>;

    /// apply a single substitution
    fn single_subst<K>(
        &self,
        from_key: K,
        to_ty: ITy<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<Option<Self>>
    where
        UnificationVarId: From<K>,
    {
        let mut substitutions = Substitutions::new();
        substitutions.insert(from_key, to_ty);
        self.apply_subst(&substitutions, ctx)
    }
}
pub(super) trait ApplySubstInPlace<'ctx>: Sized {
    /// apply substitutions to something in place. Returns Ok(true) if the thing changed
    /// Ok(false) if nothing changed, and Err(..) if something went horribly wrong
    fn apply_subst_in_place(
        &mut self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<bool>;
}

// this lets the implementations below work for member_functions on the Implementation struct
impl<'ctx> ApplySubst<'ctx> for FnFamily<'ctx> {
    fn apply_subst(
        &self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<Option<Self>> {
        match &self.implementations {
            FnFamImpls::Simple(fn_impl) => fn_impl.apply_subst(substitutions, ctx).map(|fn_impl| {
                fn_impl.map(|fn_impl| FnFamily {
                    implementations: FnFamImpls::Simple(fn_impl),
                    debug_name: self.debug_name.clone(),
                    num_params: self.num_params,
                })
            }),
            FnFamImpls::Match { fn_impls } => RefCell::borrow(fn_impls)
                .apply_subst(substitutions, ctx)
                .map(|x| {
                    x.map(|fn_impls| FnFamily {
                        implementations: FnFamImpls::Match {
                            fn_impls: Rc::new(RefCell::new(fn_impls)),
                        },
                        debug_name: self.debug_name.clone(),
                        num_params: self.num_params,
                    })
                }),
        }
    }
}

// this lets the implementations below work through an Arc
impl<'ctx, T: ApplySubst<'ctx> + Clone> ApplySubst<'ctx> for Arc<T> {
    fn apply_subst(
        &self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<Option<Self>> {
        let x: &T = self;
        match x.apply_subst(substitutions, ctx)? {
            None => Ok(None),
            Some(x) => Ok(Some(Arc::new(x))),
        }
    }
}

// this lets the implementations below work through an Rc
impl<'ctx, T: ApplySubst<'ctx> + Clone> ApplySubst<'ctx> for Rc<T> {
    fn apply_subst(
        &self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<Option<Self>> {
        let x: &T = self;
        match x.apply_subst(substitutions, ctx)? {
            None => Ok(None),
            Some(x) => Ok(Some(Rc::new(x))),
        }
    }
}

// this lets the implementations below work for member_functions on the Implementation struct
impl<'ctx, T1, T2> ApplySubst<'ctx> for (T1, T2)
where
    T1: ApplySubst<'ctx> + Clone,
    T2: ApplySubst<'ctx> + Clone,
{
    fn apply_subst(
        &self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<Option<Self>> {
        if substitutions.is_empty() {
            return Ok(None);
        }
        let v0 = if let Some(v0) = self.0.apply_subst(substitutions, ctx)? {
            Cow::Owned(v0)
        } else {
            Cow::Borrowed(&self.0)
        };
        let v1 = if let Some(v1) = self.1.apply_subst(substitutions, ctx)? {
            Cow::Owned(v1)
        } else {
            Cow::Borrowed(&self.1)
        };
        match (v0, v1) {
            (Cow::Borrowed(_), Cow::Borrowed(_)) => Ok(None),
            (v0, v1) => Ok(Some((v0.into_owned(), v1.into_owned()))),
        }
    }
}

impl<'ctx, T: ApplySubst<'ctx> + Clone> ApplySubst<'ctx> for Vec<T> {
    fn apply_subst(
        &self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<Option<Self>> {
        if substitutions.is_empty() {
            return Ok(None);
        }
        let mut result: Option<Self> = None;
        for (i, x) in self.iter().enumerate() {
            // don't clone until we hit one that changes
            if let Some(x) = x.apply_subst(substitutions, ctx)? {
                match &mut result {
                    Some(new_vec) => {
                        if let Some(dst) = new_vec.get_mut(i) {
                            *dst = x;
                        }
                    }
                    None => {
                        let mut new_vec = self.clone();
                        if let Some(dst) = new_vec.get_mut(i) {
                            *dst = x;
                        }
                        result = Some(new_vec);
                    }
                }
            }
        }
        Ok(result)
    }
}

impl<'ctx, T: ApplySubst<'ctx> + Clone> ApplySubst<'ctx> for Box<[T]> {
    fn apply_subst(
        &self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<Option<Self>> {
        if substitutions.is_empty() {
            return Ok(None);
        }
        let mut result: Option<Self> = None;
        for (i, x) in self.iter().enumerate() {
            // don't clone until we hit one that changes
            if let Some(x) = x.apply_subst(substitutions, ctx)? {
                match &mut result {
                    Some(new_vec) => {
                        if let Some(dst) = new_vec.get_mut(i) {
                            *dst = x;
                        }
                    }
                    None => {
                        let mut new_vec = self.clone();
                        if let Some(dst) = new_vec.get_mut(i) {
                            *dst = x;
                        }
                        result = Some(new_vec);
                    }
                }
            }
        }
        Ok(result)
    }
}

impl<'ctx, T: ApplySubst<'ctx> + Clone> ApplySubst<'ctx> for Box<[(UnificationVarId, T)]> {
    fn apply_subst(
        &self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<Option<Self>> {
        if substitutions.is_empty() {
            return Ok(None);
        }
        let mut result: Option<Self> = None;
        for (i, (id, x)) in self.iter().enumerate() {
            // don't clone until we hit one that changes
            if let Some(x) = x.apply_subst(substitutions, ctx)? {
                match &mut result {
                    Some(new_vec) => {
                        if let Some(dst) = new_vec.get_mut(i) {
                            *dst = (*id, x);
                        }
                    }
                    None => {
                        let mut new_vec = self.clone();
                        if let Some(dst) = new_vec.get_mut(i) {
                            *dst = (*id, x);
                        }
                        result = Some(new_vec);
                    }
                }
            }
        }
        Ok(result)
    }
}

impl<'ctx, T: ApplySubst<'ctx> + Clone> ApplySubst<'ctx>
    for std::collections::BTreeMap<StringType, T>
{
    fn apply_subst(
        &self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<Option<Self>> {
        if substitutions.is_empty() {
            return Ok(None);
        }
        let mut result: Option<Self> = None;
        for (key, old_value) in self {
            if let Some(value) = old_value.apply_subst(substitutions, ctx)? {
                // don't clone until we hit one that changes
                match &mut result {
                    Some(map) => {
                        if let Some(dst) = map.get_mut(key) {
                            *dst = value;
                        }
                    }
                    None => {
                        let mut map = std::collections::BTreeMap::clone(self);
                        if let Some(dst) = map.get_mut(key) {
                            *dst = value;
                        }
                        result = Some(map);
                    }
                }
            }
        }
        Ok(result)
    }
}

impl<'ctx, T: ApplySubst<'ctx> + Ord + Clone> ApplySubst<'ctx> for std::collections::BTreeSet<T> {
    fn apply_subst(
        &self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<Option<Self>> {
        if substitutions.is_empty() {
            return Ok(None);
        }
        let mut iter = self.iter();
        while let Some(old_value) = iter.next() {
            if old_value.apply_subst(substitutions, ctx)?.is_some() {
                drop(iter);
                return self
                    .iter()
                    .map(|old_value| {
                        Ok(old_value
                            .apply_subst(substitutions, ctx)?
                            .unwrap_or_else(|| old_value.clone()))
                    })
                    .collect::<Result<_, _>>()
                    .map(Some);
            }
        }
        Ok(None)
    }
}

impl<'ctx> ApplySubst<'ctx> for ITy<'ctx> {
    fn apply_subst(
        &self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<Option<Self>> {
        if substitutions.is_empty() {
            return Ok(None);
        }
        let mut s = Substituter::new(ctx, substitutions);
        s.visit_type(*self)
    }
}
impl<'ctx> ApplySubstInPlace<'ctx> for super::HlirNode<'ctx> {
    fn apply_subst_in_place(
        &mut self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<bool> {
        let mut s = Substituter::new(ctx, substitutions);
        s.visit(self)
    }
}
impl<'ctx> ApplySubst<'ctx> for super::Variant<'ctx> {
    #[inline]
    fn apply_subst(
        &self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<Option<Self>> {
        match self.variant_type.apply_subst(substitutions, ctx)? {
            None => Ok(None),
            Some(variant_type) => Ok(Some(super::Variant {
                name: self.name.clone(),
                variant_type,
            })),
        }
    }
}

impl<'ctx> ApplySubst<'ctx> for TypedValue<'ctx> {
    fn apply_subst(
        &self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<Option<Self>> {
        let ty = self.ty.apply_subst(substitutions, ctx)?;
        let kind = self.kind.apply_subst(substitutions, ctx)?;
        if ty.is_some() || kind.is_some() {
            Ok(Some(TypedValue {
                ty: ty.unwrap_or(self.ty),
                kind: kind.unwrap_or_else(|| self.kind.clone()),
            }))
        } else {
            Ok(None)
        }
    }
}

impl<'ctx> ApplySubst<'ctx> for TypedValueKind<'ctx> {
    fn apply_subst(
        &self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<Option<Self>> {
        match self {
            TypedValueKind::Nothing | TypedValueKind::PlaceHolder => Ok(None),
            TypedValueKind::Set(inner) => {
                let mut it = inner.iter();
                // loop through ignoring until one changes
                while let Some(orig) = it.next() {
                    if let Some(updated) = orig.apply_subst(substitutions, ctx)? {
                        // do the rest in an inner loop, updating the cloned set
                        let mut updated_set = inner.clone();
                        updated_set.remove(orig);
                        updated_set.insert(updated);
                        for orig in &mut it {
                            if let Some(updated) = orig.apply_subst(substitutions, ctx)? {
                                updated_set.remove(orig);
                                updated_set.insert(updated);
                            }
                        }
                        return Ok(Some(TypedValueKind::Set(updated_set)));
                    }
                }
                Ok(None)
            }
            TypedValueKind::StegoFn {
                debug_name,
                fn_impl,
            } => match fn_impl.apply_subst(substitutions, ctx)? {
                None => Ok(None),
                Some(fn_impl) => Ok(Some(TypedValueKind::StegoFn {
                    debug_name: debug_name.clone(),
                    fn_impl,
                })),
            },
            TypedValueKind::FnFam(fn_fam) => match fn_fam.apply_subst(substitutions, ctx)? {
                None => Ok(None),
                Some(updated) => Ok(Some(TypedValueKind::FnFam(updated))),
            },
            TypedValueKind::TraitScheme(x) => match x.apply_subst(substitutions, ctx)? {
                None => Ok(None),
                Some(x) => Ok(Some(TypedValueKind::TraitScheme(x))),
            },
            TypedValueKind::TraitStruct(x) => match x.apply_subst(substitutions, ctx)? {
                None => Ok(None),
                Some(x) => Ok(Some(TypedValueKind::TraitStruct(x))),
            },
            TypedValueKind::Type(x) => match x.apply_subst(substitutions, ctx)? {
                None => Ok(None),
                Some(x) => Ok(Some(TypedValueKind::Type(x))),
            },
            TypedValueKind::Val(obj) => match &**obj {
                StegoObject::False
                | StegoObject::True
                | StegoObject::Int64(_)
                | StegoObject::Float64(_)
                | StegoObject::Integer(_)
                | StegoObject::Char(_)
                | StegoObject::Dict(_)
                | StegoObject::StegoString(_)
                | StegoObject::StegoImmString(_)
                | StegoObject::Tuple(_)
                | StegoObject::UnitType => Ok(None),
                StegoObject::FnFamily(fn_fam) => match fn_fam.apply_subst(substitutions, ctx)? {
                    None => Ok(None),
                    Some(x) => Ok(Some(TypedValueKind::Val(Rc::new(StegoObject::FnFamily(x))))),
                },
                StegoObject::FnPartial(fn_partial) => {
                    match fn_partial.apply_subst(substitutions, ctx)? {
                        None => Ok(None),
                        Some(x) => Ok(Some(TypedValueKind::Val(Rc::new(StegoObject::FnPartial(
                            x,
                        ))))),
                    }
                }
            },
        }
    }
}

impl<'ctx> ApplySubst<'ctx> for TraitSchemeRef<'ctx> {
    fn apply_subst(
        &self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<Option<Self>> {
        let assoc_tys = self.assoc_tys.apply_subst(substitutions, ctx)?;
        let methods = self.methods.apply_subst(substitutions, ctx)?;
        if let (None, None) = (&assoc_tys, &methods) {
            return Ok(None);
        }
        Ok(Some(
            ctx.mk_trait_scheme(
                self.trait_name.clone(),
                self.self_var,
                self.ty_pars.clone(),
                assoc_tys.unwrap_or_else(|| self.assoc_tys.clone()),
                methods.unwrap_or_else(|| self.methods.clone()),
            )
            .1,
        ))
    }
}

impl<'ctx> ApplySubst<'ctx> for TraitStructRef<'ctx> {
    fn apply_subst(
        &self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<Option<Self>> {
        let typars = self.typar_choices.apply_subst(substitutions, ctx)?;
        let assoc_tys = self.assoc_tys.apply_subst(substitutions, ctx)?;
        let methods = self.methods.apply_subst(substitutions, ctx)?;

        match (typars, assoc_tys, methods) {
            (None, None, None) => Ok(None),
            (typars, assoc_tys, methods) => Ok(Some(ctx.alloc_trait(
                typars.unwrap_or_else(|| self.typar_choices.clone()),
                self.self_var,
                self.trait_name.clone(),
                assoc_tys.unwrap_or_else(|| self.assoc_tys.clone()),
                methods.unwrap_or_else(|| self.methods.clone()),
            ))),
        }
    }
}

impl<'ctx, NodeType> ApplySubst<'ctx> for DictExprElem<NodeType>
where
    NodeType: ApplySubst<'ctx> + Clone,
{
    fn apply_subst(
        &self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<Option<Self>> {
        if substitutions.is_empty() {
            return Ok(None);
        }
        match self {
            DictExprElem::KeywordSpread(node) => node
                .apply_subst(substitutions, ctx)
                .map(|x| x.map(DictExprElem::KeywordSpread)),
            DictExprElem::KeyValue(key_node, value_node) => {
                let substed_key_node = key_node.apply_subst(substitutions, ctx)?;
                let substed_value_node = value_node.apply_subst(substitutions, ctx)?;
                if substed_key_node.is_some() || substed_value_node.is_some() {
                    Ok(Some(DictExprElem::KeyValue(
                        substed_key_node.unwrap_or_else(|| key_node.clone()),
                        substed_value_node.unwrap_or_else(|| value_node.clone()),
                    )))
                } else {
                    Ok(None)
                }
            }
        }
    }
}

impl<'ctx> ApplySubst<'ctx> for super::HlirNode<'ctx> {
    fn apply_subst(
        &self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<Option<Self>> {
        if substitutions.is_empty() {
            return Ok(None);
        }

        let maybe_inferred_type = self.inferred_type.apply_subst(substitutions, ctx)?;
        let maybe_kind = self.kind.apply_subst(substitutions, ctx)?;

        if let (None, None) = (&maybe_inferred_type, &maybe_kind) {
            return Ok(None);
        }

        Ok(Some(HlirNode {
            inferred_type: maybe_inferred_type.unwrap_or(self.inferred_type),
            source_region: self.source_region.clone(),
            kind: maybe_kind.unwrap_or_else(|| self.kind.clone()),
        }))
    }
}

impl<'ctx> ApplySubst<'ctx> for HlirNodeKind<'ctx> {
    fn apply_subst(
        &self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<Option<Self>> {
        if substitutions.is_empty() {
            return Ok(None);
        }
        match self {
            HlirNodeKind::LiteralValue(_)
            | HlirNodeKind::UnitType
            | HlirNodeKind::ReturnStmt(None) => Ok(None),
            HlirNodeKind::ApplyExpr { func, args } => {
                let maybe_func = func.apply_subst(substitutions, ctx)?;
                let maybe_args = args.apply_subst(substitutions, ctx)?;
                if let (None, None) = (&maybe_func, &maybe_args) {
                    Ok(None)
                } else {
                    Ok(Some(HlirNodeKind::ApplyExpr {
                        func: Box::new(maybe_func.unwrap_or_else(|| (**func).clone())),
                        args: maybe_args.unwrap_or_else(|| args.clone()),
                    }))
                }
            }
            HlirNodeKind::AssignStmt { lhs, rhs } => {
                let maybe_lhs = lhs.apply_subst(substitutions, ctx)?;
                let maybe_rhs = rhs.apply_subst(substitutions, ctx)?;
                if let (None, None) = (&maybe_lhs, &maybe_rhs) {
                    Ok(None)
                } else {
                    Ok(Some(HlirNodeKind::AssignStmt {
                        lhs: Box::new(maybe_lhs.unwrap_or_else(|| (**lhs).clone())),
                        rhs: Box::new(maybe_rhs.unwrap_or_else(|| (**rhs).clone())),
                    }))
                }
            }
            HlirNodeKind::BitwiseNot(_) => todo!(),
            HlirNodeKind::BinOpExpr { first, rest } => {
                let maybe_first = first.apply_subst(substitutions, ctx)?;
                let maybe_rest = rest.apply_subst(substitutions, ctx)?;
                if let (None, None) = (&maybe_first, &maybe_rest) {
                    Ok(None)
                } else {
                    Ok(Some(HlirNodeKind::BinOpExpr {
                        first: maybe_first.map_or_else(|| first.clone(), Box::new),
                        rest: maybe_rest.unwrap_or_else(|| rest.clone()),
                    }))
                }
            }
            HlirNodeKind::ComparisonOpExpr { first, rest } => {
                let maybe_first = first.apply_subst(substitutions, ctx)?;
                let maybe_rest = rest.apply_subst(substitutions, ctx)?;
                if let (None, None) = (&maybe_first, &maybe_rest) {
                    Ok(None)
                } else {
                    Ok(Some(HlirNodeKind::ComparisonOpExpr {
                        first: maybe_first.map_or_else(|| first.clone(), Box::new),
                        rest: maybe_rest.unwrap_or_else(|| rest.clone()),
                    }))
                }
            }
            HlirNodeKind::DictExpr(_) => todo!(),
            HlirNodeKind::DoExpr(inner) => inner
                .apply_subst(substitutions, ctx)
                .map(|o| o.map(|updated| HlirNodeKind::DoExpr(updated.into()))),
            HlirNodeKind::DotExpr { lhs, method_name } => {
                lhs.apply_subst(substitutions, ctx).map(|x| {
                    x.map(|x| HlirNodeKind::DotExpr {
                        lhs: x.into(),
                        method_name: method_name.clone(),
                    })
                })
            }
            HlirNodeKind::FnStmt(f) => {
                if let Some(f) = f.apply_subst(substitutions, ctx)? {
                    Ok(Some(HlirNodeKind::FnStmt(f)))
                } else {
                    Ok(None)
                }
            }
            HlirNodeKind::Identifier {
                name,
                substitutions: id_subs,
            } => {
                if let Some(id_subs) = id_subs.apply_subst(substitutions, ctx)? {
                    Ok(Some(HlirNodeKind::Identifier {
                        name: name.clone(),
                        substitutions: id_subs,
                    }))
                } else {
                    Ok(None)
                }
            }
            HlirNodeKind::IfExpr {
                arms,
                catch_all_else,
            } => {
                let maybe_arms = arms.apply_subst(substitutions, ctx)?;
                let maybe_catch_all_else = match catch_all_else {
                    Some(x) => x.apply_subst(substitutions, ctx)?,
                    None => None,
                };
                if let (None, None) = (&maybe_arms, &maybe_catch_all_else) {
                    Ok(None)
                } else {
                    Ok(Some(HlirNodeKind::IfExpr {
                        arms: maybe_arms.unwrap_or_else(|| arms.clone()),
                        catch_all_else: maybe_catch_all_else
                            .map_or_else(|| catch_all_else.clone(), |x| Some(Box::new(x))),
                    }))
                }
            }
            HlirNodeKind::InExpr { .. } => todo!(),
            HlirNodeKind::Lambda {
                body,
                captures,
                params,
                placeholder_name,
            } => RefCell::borrow(body)
                .apply_subst(substitutions, ctx)
                .map(|ok| {
                    ok.map(|body| HlirNodeKind::Lambda {
                        body: Rc::new(RefCell::new(body)),
                        captures: captures.clone(),
                        params: Rc::clone(params),
                        placeholder_name: placeholder_name.clone(),
                    })
                }),
            HlirNodeKind::Let(HlirLetData {
                pattern,
                value_expr,
                substitutions: let_subs,
            }) => value_expr.apply_subst(substitutions, ctx).map(|x| {
                x.map(|x| {
                    HlirNodeKind::Let(HlirLetData {
                        pattern: pattern.clone(),
                        value_expr: x.into(),
                        substitutions: let_subs.clone(),
                    })
                })
            }),
            HlirNodeKind::LogicalNot(_) => todo!(),
            HlirNodeKind::NamedExpr { .. } => todo!(),
            HlirNodeKind::ReturnStmt(Some(value)) => value
                .apply_subst(substitutions, ctx)
                .map(|value| value.map(|value| HlirNodeKind::ReturnStmt(Some(value.into())))),
            HlirNodeKind::SetExpr(_) => todo!(),
            HlirNodeKind::Statements { statements } => {
                // TODO: wait and see if any change before allocating/cloning
                let mut updated = Vec::with_capacity(statements.len());
                let mut any_changed = false;
                for node in statements {
                    match node.apply_subst(substitutions, ctx)? {
                        Some(node) => {
                            updated.push(node);
                            any_changed = true;
                        }
                        None => updated.push(node.clone()),
                    }
                }
                if any_changed {
                    Ok(Some(HlirNodeKind::Statements {
                        statements: updated,
                    }))
                } else {
                    Ok(None)
                }
            }
            HlirNodeKind::Tuple(v) => {
                // outer loop: ignore until we find that one has changed
                for (i, node) in v.iter().enumerate() {
                    match node.apply_subst(substitutions, ctx)? {
                        None => (),
                        // one changed. make a new vec
                        Some(node) => {
                            // copy the initial bunch that we already know are unchanged
                            let mut updated = Vec::with_capacity(v.len());
                            for unchanged_node in &v[..i] {
                                updated.push(unchanged_node.clone());
                            }
                            // the current one is changed
                            updated.push(node);
                            // now handle the rest
                            for node in &v[(i + 1)..] {
                                match node.apply_subst(substitutions, ctx)? {
                                    None => updated.push(node.clone()),
                                    Some(node) => updated.push(node),
                                }
                            }
                            return Ok(Some(HlirNodeKind::Tuple(updated)));
                        }
                    }
                }
                // early returned if any changed
                Ok(None)
            }
            HlirNodeKind::TypeOf(node) => node
                .apply_subst(substitutions, ctx)
                .map(|x| x.map(|node| HlirNodeKind::TypeOf(Box::new(node)))),
            HlirNodeKind::UnaryMinus(node) => node
                .apply_subst(substitutions, ctx)
                .map(|x| x.map(|node| HlirNodeKind::UnaryMinus(Box::new(node)))),
            HlirNodeKind::UnaryPlus(_) => todo!(),
        }
    }
}

impl<'ctx, NodeType: ApplySubst<'ctx> + Clone> ApplySubst<'ctx> for SetExprElem<NodeType> {
    fn apply_subst(
        &self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<Option<Self>> {
        if substitutions.is_empty() {
            return Ok(None);
        }
        match self {
            SetExprElem::Spread(node) => node
                .apply_subst(substitutions, ctx)
                .map(|x| x.map(SetExprElem::Spread)),
            SetExprElem::Value(node) => node
                .apply_subst(substitutions, ctx)
                .map(|x| x.map(SetExprElem::Value)),
        }
    }
}

impl<'ctx, T: ApplySubst<'ctx>> ApplySubst<'ctx> for IfArm<T> {
    fn apply_subst(
        &self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<Option<Self>> {
        if substitutions.is_empty() {
            return Ok(None);
        }
        let maybe_cond = self.cond.apply_subst(substitutions, ctx)?;
        let maybe_consequent = self.consequent.apply_subst(substitutions, ctx)?;
        if let (None, None) = (&maybe_cond, &maybe_consequent) {
            Ok(None)
        } else {
            Ok(Some(IfArm {
                cond: maybe_cond.unwrap_or_else(|| self.cond.clone()),
                consequent: maybe_consequent.unwrap_or_else(|| self.consequent.clone()),
            }))
        }
    }
}
impl<'ctx> ApplySubst<'ctx> for Operator {
    fn apply_subst(
        &self,
        _substitutions: &Substitutions<'ctx>,
        _ctx: &Context<'ctx>,
    ) -> TiResult<Option<Self>> {
        Ok(None)
    }
}

impl<'ctx> ApplySubst<'ctx> for FnStmt<'ctx> {
    fn apply_subst(
        &self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<Option<Self>> {
        if substitutions.is_empty() {
            return Ok(None);
        }
        let FnStmt {
            body,
            captures,
            fn_ty,
            name,
            params,
            placeholder_needed,
        } = self;
        let updated_body = RefCell::borrow(body).apply_subst(substitutions, ctx)?;
        let updated_fn_ty = fn_ty.apply_subst(substitutions, ctx)?;
        let updated_params = RefCell::borrow(params).apply_subst(substitutions, ctx)?;
        if updated_body.is_some() || updated_fn_ty.is_some() || updated_params.is_some() {
            let body = updated_body.map_or_else(|| Rc::clone(body), |x| Rc::new(RefCell::new(x)));
            let captures = captures.clone();
            let fn_ty = updated_fn_ty.unwrap_or(*fn_ty);
            let name = name.clone();
            let params =
                updated_params.map_or_else(|| params.clone(), |x| Rc::new(RefCell::new(x)));
            let placeholder_needed = *placeholder_needed;
            Ok(Some(FnStmt {
                body,
                captures,
                fn_ty,
                name,
                params,
                placeholder_needed,
            }))
        } else {
            Ok(None)
        }
    }
}

impl<'ctx> ApplySubst<'ctx> for StegoPattern {
    fn apply_subst(
        &self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<Option<Self>> {
        let StegoPattern {
            kind,
            source_region,
        } = self;
        if let Some(kind) = kind.apply_subst(substitutions, ctx)? {
            Ok(Some(StegoPattern {
                kind,
                source_region: source_region.clone(),
            }))
        } else {
            Ok(None)
        }
    }
}

impl<'ctx> ApplySubst<'ctx> for StegoPatternKind {
    fn apply_subst(
        &self,
        substitutions: &Substitutions<'ctx>,
        ctx: &Context<'ctx>,
    ) -> TiResult<Option<Self>> {
        match self {
            StegoPatternKind::Capture(_) | StegoPatternKind::Literal(_) => Ok(None),
            StegoPatternKind::Or(v) => {
                if let Some(v) = v.apply_subst(substitutions, ctx)? {
                    Ok(Some(StegoPatternKind::Or(v)))
                } else {
                    Ok(None)
                }
            }
            StegoPatternKind::Named(name, inner) => {
                if let Some(inner) = inner.apply_subst(substitutions, ctx)? {
                    Ok(Some(StegoPatternKind::Named(name.clone(), Box::new(inner))))
                } else {
                    Ok(None)
                }
            }
        }
    }
}

impl<'a, T> ApplySubstInPlace<'a> for Vec<T>
where
    T: ApplySubstInPlace<'a>,
{
    fn apply_subst_in_place(
        &mut self,
        substitutions: &Substitutions<'a>,
        ctx: &Context<'a>,
    ) -> TiResult<bool> {
        let mut any_changed = false;
        for item in &mut *self {
            any_changed = item.apply_subst_in_place(substitutions, ctx)? || any_changed;
        }
        Ok(any_changed)
    }
}
