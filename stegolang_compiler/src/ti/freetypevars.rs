use super::UnificationVarId;

/// trait for querying about something's unification variables and/or type parameters
/// TODO: methods should probably take the context and source region and
/// return result so that we can properly drill into `Type::Member` variants
/// TODO: needs to also track seen things so that we don't get stuck in loops
/// or do redundant checks
pub(crate) trait FreeTypeVars<'ctx> {
    /// look at all of the type's free unification variables
    fn free_unification_variables(
        &self,
        exclude: &[UnificationVarId],
        collection: &mut Vec<UnificationVarId>,
    );

    /// check if the type has any free unification variables
    fn has_any_free_unification_variables(&self, exclude: &[UnificationVarId]) -> bool;

    /// check if the type depends on a particular unification var
    fn has_free_type_var(&self, search_id: UnificationVarId) -> bool;

    /// look at all of the thing's type parameters
    fn get_type_parameters(
        &self,
        exclude: &[UnificationVarId],
        collection: &mut Vec<UnificationVarId>,
    );

    /// check if the type involves any type parameters
    fn has_any_type_parameters(&self, exclude: &[UnificationVarId]) -> bool;

    /// check if the type depends on a particular type parameters
    fn has_type_parameter(&self, search_id: UnificationVarId) -> bool;
}
