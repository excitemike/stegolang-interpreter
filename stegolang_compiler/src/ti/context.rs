#![allow(clippy::default_trait_access)]
use std::borrow::Cow;
use std::rc::Rc;

use rustc_hash::{FxHashMap, FxHashSet};

use super::arenas::Arenas;
use super::constraint::{Constraint, ICon};
use super::freetypevars::FreeTypeVars;
use super::implschemeiter::ImplSchemeIter;
use super::substitute::ApplySubst;
use super::traitstruct::TraitSchemeRef;
use super::UnificationVarId;
use super::{
    loc, make_unification_var_id, traitbuilder::TraitBuilder, BTreeMap, ITy, ImplSchemeRef,
    Implementation, ImplementationRef, Itertools, RefCell, SourceRegion, Substitutions, TiError,
    TiResult, TraitStructRef, Type,
};
use crate::intern::interner::Interner;
use crate::ti::traitstruct::TraitStruct;
use crate::typedvalue::TypedValue;
use crate::{ast::literal::Literal, util::NestedScope, StringType};
type ImplementationTable<'a> =
    FxHashMap<ITy<'a>, FxHashMap<TraitStructRef<'a>, ImplementationRef<'a>>>;

/// type inference context as we walk the AST
pub(crate) struct Context<'ctx> {
    /// utility to intern and store items
    interner: Interner<'ctx>,

    /// lookup implementation schemes by the trait they are implementing
    implementation_schemes_by_trait:
        RefCell<FxHashMap<TraitStructRef<'ctx>, Vec<ImplSchemeRef<'ctx>>>>,

    /// lookup implementation schemes by the names of their members
    implementation_schemes_by_member_name: RefCell<FxHashMap<StringType, Vec<ImplSchemeRef<'ctx>>>>,

    /// for tracking scope as we examine the AST
    pub(crate) scopes: RefCell<NestedScope<StringType, ITy<'ctx>>>,

    /// accumulate warnings as we infer types
    pub(crate) warnings: RefCell<Vec<TiError>>,

    /// accumulate constraints as we infer types
    pub(crate) constraints: RefCell<Vec<ICon<'ctx>>>,

    /// constraints regarding generics will need to be instantiated later
    generic_constraints: RefCell<FxHashMap<UnificationVarId, FxHashSet<ICon<'ctx>>>>,

    /// look up constraints by var id
    constraints_by_type_var: RefCell<FxHashMap<UnificationVarId, FxHashSet<ICon<'ctx>>>>,

    /// during type/trait solving: store found implementations for later lookup
    /// during codegen or eval
    impl_table: RefCell<ImplementationTable<'ctx>>,
}

impl<'ctx> Context<'ctx> {
    /// accumulate a constraint for later type solving
    pub(crate) fn add_constraint(&self, c: Constraint<'ctx>) {
        use std::collections::hash_map::Entry;
        let mut ty_pars = vec![];
        c.get_type_parameters(&[], &mut ty_pars);
        let mut free_vars = vec![];
        c.free_unification_variables(&[], &mut free_vars);

        let c = self.interner.intern_constraint(c);

        if ty_pars.is_empty() {
            let mut map = self.constraints_by_type_var.borrow_mut();
            for id in free_vars {
                match map.entry(id) {
                    Entry::Occupied(mut e) => {
                        e.get_mut().insert(c);
                    }
                    Entry::Vacant(e) => {
                        e.insert(std::iter::once(c).collect());
                    }
                }
            }
            self.constraints.borrow_mut().push(c);
        } else {
            // if it had type parameters, we have to save it for when we later
            // instantiate
            for uniq_id in ty_pars {
                let mut guard = self.generic_constraints.borrow_mut();
                match guard.entry(uniq_id) {
                    Entry::Occupied(mut e) => {
                        e.get_mut().insert(c);
                    }
                    Entry::Vacant(e) => {
                        e.insert(std::iter::once(c).collect());
                    }
                }
            }
        }
    }

    /// used to create implementations
    pub(crate) fn alloc_impl_scheme(
        &self,
        ty_pars: Box<[(StringType, ITy<'ctx>)]>,
        self_var: ITy<'ctx>,
        implementation: ImplementationRef<'ctx>,
    ) -> ImplSchemeRef<'ctx> {
        let scheme = self
            .interner
            .alloc_impl_scheme(ty_pars, self_var, implementation);
        // update our index by trait
        self.implementation_schemes_by_trait
            .borrow_mut()
            .entry(implementation.implemented_trait)
            .or_default()
            .push(scheme);
        // update index by member name
        let mut guard = self.implementation_schemes_by_member_name.borrow_mut();
        let map = &mut *guard;
        let iter = implementation
            .member_types
            .keys()
            .cloned()
            .chain(implementation.member_functions.keys().cloned());
        for name in iter {
            map.entry(name.clone()).or_default().push(scheme);
        }
        scheme
    }

    /// construct a `TraitStruct`
    pub(crate) fn alloc_trait(
        &self,
        typar_choices: Box<[ITy<'ctx>]>,
        self_var: UnificationVarId,
        trait_name: StringType,
        assoc_tys: BTreeMap<StringType, ITy<'ctx>>,
        methods: BTreeMap<StringType, ITy<'ctx>>,
    ) -> TraitStructRef<'ctx> {
        self.interner
            .alloc_trait(typar_choices, self_var, trait_name, assoc_tys, methods)
    }

    /// as type vars are solved, we will need to update old constraints that
    /// reference them
    pub(crate) fn apply_subst_to_constraints(
        &self,
        substitutions: &Substitutions<'ctx>,
    ) -> TiResult<()> {
        let n = self.constraints.borrow().len();
        if n == 0 {
            return Ok(());
        }

        // replace in each ready constraint
        {
            let old_constraints = self.constraints.replace(Vec::with_capacity(n));
            for old_constraint in old_constraints {
                if let Some(new_constraint) = old_constraint.apply_subst(substitutions, self)? {
                    self.add_constraint(new_constraint);
                } else {
                    self.constraints.borrow_mut().push(old_constraint);
                }
            }
        }
        // replace in generic constraints as well
        {
            let mut map = self.generic_constraints.borrow_mut();
            let mut to_add = Vec::with_capacity(map.len());
            for set in map.values_mut() {
                let old_set = std::mem::take(set);
                for c in old_set {
                    if let Some(c) = c.apply_subst(substitutions, self)? {
                        to_add.push(c);
                    } else {
                        set.insert(c);
                    }
                }
            }
            drop(map);
            for c in to_add {
                self.add_constraint(c);
            }
        }
        // we no longer need indices for replaced out vars
        {
            let mut map = self.constraints_by_type_var.borrow_mut();
            for (k, _) in substitutions.iter() {
                map.remove(k);
            }
        }

        Ok(())
    }

    /// construct a trait
    pub(crate) fn build_trait<'builder, Name>(
        &'builder self,
        name: Name,
        self_var: UnificationVarId,
        type_parameters: Vec<(StringType, UnificationVarId)>,
    ) -> TraitBuilder<'builder, 'ctx>
    where
        Name: Into<StringType>,
    {
        TraitBuilder::new(self, name, self_var, type_parameters)
    }

    /// Look for a member in the trait implementations of the given type.
    /// Does not look in the type itself, does not add constraints.
    pub(crate) fn find_in_traits(
        &self,
        ty: ITy<'ctx>,
        member_name: &StringType,
        source_region: &SourceRegion,
    ) -> TiResult<Option<ITy<'ctx>>> {
        // traits, implementations, and schemes never themselves have traits
        match ty.inner() {
            Type::BigInt
            | Type::Bool
            | Type::Char
            | Type::Dict { .. }
            | Type::ElementOf(_, _)
            | Type::F(_)
            | Type::Float64
            | Type::Int64
            | Type::Nothing
            | Type::Set(_)
            | Type::String
            | Type::Struct(_)
            | Type::Tuple(_)
            | Type::Union(_)
            | Type::Vector(_)
            | Type::Variant(_)
            | Type::UnificationVar { .. } => (),
            Type::TypeParameter { .. } => todo!("unhandled case for {ty}"),
            Type::Implementation(_)
            | Type::TraitScheme(_)
            | Type::TraitStruct(_)
            | Type::Type(_)
            | Type::TypeScheme(_) => {
                return Ok(None);
            }
        }

        // if the for type is still undetermined, we are unlikely to get helpful
        // results yet
        if !ty.is_final() {
            return Ok(None);
        }

        let mut impl_options = Vec::new();
        let mut all_found_in_impls = Vec::new();
        let implementations = self.implementation_schemes_with_member_name(member_name);
        for impl_scheme in implementations {
            if ty.could_be_instance(impl_scheme.implementation.implemented_for) {
                if let Some(&assoc_ty) = impl_scheme.implementation.member_types.get(member_name) {
                    impl_options.push(impl_scheme.to_string());
                    all_found_in_impls.push(assoc_ty.expect_type(source_region)?);
                } else if impl_scheme
                    .implementation
                    .find_member_function(member_name)
                    .is_some()
                {
                    let impl_inst = impl_scheme.instantiate(self, source_region)?;
                    if let Some((fn_scheme, _fn_fam)) = impl_inst.find_member_function(member_name)
                    {
                        impl_options.push(impl_scheme.to_string());
                        all_found_in_impls.push(fn_scheme);
                    }
                }
            }
        }
        match all_found_in_impls.len() {
            0 => Ok(None),
            1 => Ok(all_found_in_impls.pop()),
            _ => {
                // we aren't going to be able to resolve it later either
                Err(TiError::new(
                    loc::err::ambiguous_member_lookup(
                        &format!("{member_name}"),
                        &ty.to_string(),
                        impl_options.into_iter(),
                    ),
                    source_region.clone(),
                ))
            }
        }
    }

    /// When an item is generalized, constraints on those types will need to be
    /// generalized, too
    pub(crate) fn generalize_constraints(
        &self,
        substitutions: &Substitutions<'ctx>,
    ) -> TiResult<()> {
        let mut constraints_to_generalize = FxHashSet::<ICon<'ctx>>::default();
        for (key, _) in substitutions.iter() {
            if let Some(cs) = self.constraints_by_type_var.borrow().get(key) {
                constraints_to_generalize.extend(cs.iter());
            }
        }
        for c in &constraints_to_generalize {
            if let Some(new_constraint) = c.apply_subst(substitutions, self)? {
                self.remove_constraint(*c);
                self.add_constraint(new_constraint);
            }
        }
        Ok(())
    }

    /// lookup a method on the given type
    pub(crate) fn get_method_for_eval(
        &self,
        ty: ITy<'ctx>,
        method_name: &StringType,
        source_region: &SourceRegion,
    ) -> TiResult<Option<TypedValue<'ctx>>> {
        let mut answer = None;
        let mut found = Vec::new();
        let impl_table = self.impl_table.borrow();
        let Some(map) = impl_table.get(&ty) else {
            return Ok(None);
        };
        for (tr, imp) in map {
            if let Some((ty, fn_fam)) = imp.member_functions.get(method_name) {
                let value = TypedValue::fn_fam(*ty, Rc::clone(fn_fam));
                answer.get_or_insert(value);
                found.push(tr);
            }
        }
        if found.is_empty() {
            Ok(None)
        } else if found.len() == 1 {
            Ok(answer)
        } else {
            // ambiguous case
            Err(TiError::new(
                loc::err::ambiguous_member_lookup(method_name, &ty.to_string(), found.iter()),
                source_region.clone(),
            ))
        }
    }

    /// Find the implementation of the given trait for the given type, if any
    /// NOTE: use `implementation_of_trait` during type inference
    pub(crate) fn get_trait_implementation_for_eval(
        &self,
        implemented_for_type: ITy<'ctx>,
        implemented_trait: &TraitStruct<'ctx>,
        source_region: &SourceRegion,
    ) -> TiResult<ImplementationRef<'ctx>> {
        self.impl_table
            .borrow()
            .get(&implemented_for_type)
            .and_then(|m| m.get(implemented_trait))
            .copied()
            .ok_or_else(|| {
                let msg_tuple = loc::err::no_implementation_found(
                    &implemented_trait.to_string(),
                    &implemented_for_type.to_string(),
                );
                TiError::new(msg_tuple, source_region.clone())
            })
    }

    /// Mainly for use during constraint solving. Get an iterator of
    /// implementation schemes for a trait
    pub(crate) fn implementation_schemes_for_trait(
        &self,
        implemented_trait: TraitStructRef<'ctx>,
    ) -> ImplSchemeIter<'ctx> {
        if let Some(schemes) = self
            .implementation_schemes_by_trait
            .borrow()
            .get(implemented_trait)
        {
            ImplSchemeIter::new(schemes)
        } else {
            ImplSchemeIter::new(&[])
        }
    }

    /// Used when searching for members of a value. Get an iterator over all
    /// implementation schemes that have a member with the given name
    pub(crate) fn implementation_schemes_with_member_name(
        &self,
        name: &StringType,
    ) -> ImplSchemeIter<'ctx> {
        if let Some(schemes) = self
            .implementation_schemes_by_member_name
            .borrow()
            .get(name)
        {
            ImplSchemeIter::new(schemes)
        } else {
            ImplSchemeIter::new(&[])
        }
    }

    /// get or create unification variable for the implementation of a trait
    /// NOTE: use `get_trait_implementation_for_eval` during evaluation
    pub(crate) fn implementation_of_trait(
        &self,
        implemented_trait: TraitStructRef<'ctx>,
        implemented_for_type: ITy<'ctx>,
        source_region: &SourceRegion,
    ) -> Option<ImplementationRef<'ctx>> {
        let search_result = self
            .impl_table
            .borrow()
            .get(&implemented_for_type)
            .and_then(|m| m.get(implemented_trait))
            .copied();
        if let Some(ty) = search_result {
            return Some(ty);
        }
        self.add_constraint(Constraint::Implementation {
            trait_struct: implemented_trait,
            for_ty: implemented_for_type,
            source_region: Box::new(source_region.clone()),
        });
        None
    }

    /// When an item is instantiated, constraints on those types will need to be
    /// instantiated, too
    pub(crate) fn instantiate_constraints(
        &self,
        substitutions: &Substitutions<'ctx>,
    ) -> TiResult<()> {
        let mut to_instantiate = FxHashSet::<ICon<'ctx>>::default();
        for (key, _) in substitutions.iter() {
            if let Some(cs) = self.generic_constraints.borrow().get(key) {
                to_instantiate.extend(cs.iter());
            }
        }
        for c in &to_instantiate {
            if let Some(new_constraint) = c.apply_subst(substitutions, self)? {
                self.add_constraint(new_constraint);
            }
        }
        Ok(())
    }

    /// Find a type by name in context. Ok(None) if not found, produces an error
    /// if found but the name does not refer to a type
    pub(crate) fn lookup_type(
        &self,
        name: &StringType,
        source_region: &SourceRegion,
    ) -> TiResult<(Substitutions<'ctx>, ITy<'ctx>)> {
        match self.scopes.borrow().get(name) {
            Some(guard) => match guard.inner() {
                Type::Type(ty) => Ok((Substitutions::new(), *ty)),
                Type::TypeScheme(scheme) => scheme.instantiate(self, source_region),
                Type::TraitScheme(scheme) => {
                    let (subs, trait_struct) = scheme.instantiate(self, source_region)?;
                    Ok((subs, self.mk_trait(trait_struct)))
                }
                Type::TypeParameter { .. } | Type::UnificationVar { .. } => {
                    Ok((Substitutions::new(), *guard))
                }
                _ => Err(TiError::new(
                    loc::err::not_a_type(name.as_ref(), &guard.to_string()),
                    source_region.clone(),
                )),
            },
            None => Err(TiError::new(
                loc::err::identifier_not_in_scope(name.as_ref()),
                source_region.clone(),
            )),
        }
    }

    /// Refer to a member of another type.
    /// May generate a constraint
    pub(crate) fn member(
        &self,
        obj: ITy<'ctx>,
        name: &StringType,
        source_region: &SourceRegion,
    ) -> TiResult<ITy<'ctx>> {
        if let Some(ty) = obj.find_member_direct(name) {
            Ok(ty)
        } else if let Some(ty) = self.find_in_traits(obj, name, source_region)? {
            Ok(ty)
        } else {
            let var = self.mk_unification_var(source_region, "for member constraint")?;
            self.add_constraint(Constraint::Member {
                obj_ty: obj,
                field_ty: var,
                name: name.clone(),
                source_region: Box::new(source_region.clone()),
            });
            Ok(var)
        }
    }

    /// single instance of the `BigInt` type
    pub(crate) fn mk_bigint(&self) -> ITy<'ctx> {
        self.interner.intern_bigint()
    }

    /// single instance of the `Bool` type
    pub(crate) fn mk_bool(&self) -> ITy<'ctx> {
        self.interner.intern_bool()
    }

    /// single instance of the `Char` type
    pub(crate) fn mk_char(&self) -> ITy<'ctx> {
        self.interner.intern_char()
    }

    /// construct `Dict` type
    pub(crate) fn mk_dict(&self, key: ITy<'ctx>, value: ITy<'ctx>) -> ITy<'ctx> {
        self.interner.intern_dict(key, value)
    }

    /// single instance of the `Float64` type
    pub(crate) fn mk_float64(&self) -> ITy<'ctx> {
        self.interner.intern_float64()
    }

    /// construct a function type
    pub(crate) fn mk_fn_type(&self, params: Box<[ITy<'ctx>]>, result: ITy<'ctx>) -> ITy<'ctx> {
        self.interner.intern_fn(params, result)
    }

    /// construct a function type
    pub(crate) fn mk_fn_type_from_iter<ParamI>(
        &self,
        params: ParamI,
        result: ITy<'ctx>,
    ) -> ITy<'ctx>
    where
        ParamI: Iterator<Item = ITy<'ctx>>,
    {
        self.interner
            .intern_fn(params.collect_vec().into_boxed_slice(), result)
    }

    /// create a type expression for a function when you do not yet know the types involved
    pub(crate) fn mk_fn_var(
        &self,
        num_params: usize,
        source_region: &SourceRegion,
    ) -> TiResult<ITy<'ctx>> {
        let mut params = Vec::with_capacity(num_params);
        for _ in 0..num_params {
            params.push(self.mk_unification_var(source_region, "mk_fn_var")?);
        }
        let params = params.into_boxed_slice();
        let ty = self
            .interner
            .intern_fn(params, self.mk_unification_var(source_region, "mk_fn_var")?);
        Ok(ty)
    }

    /// type for one element of an iterable
    pub(crate) fn mk_element_of(
        &self,
        iter_ty: ITy<'ctx>,
        source_region: &SourceRegion,
    ) -> TiResult<ITy<'ctx>> {
        match iter_ty.iterable_type(self, source_region)? {
            Some(ty) => Ok(ty),
            None => Ok(self
                .interner
                .intern_element_of(iter_ty, source_region.clone())),
        }
    }

    /// type for generic functions
    pub(crate) fn mk_generic_fn<ParamI>(
        &self,
        type_params: Box<[UnificationVarId]>,
        params: ParamI,
        return_type: ITy<'ctx>,
        source_region: Box<SourceRegion>,
    ) -> ITy<'ctx>
    where
        ParamI: Iterator<Item = ITy<'ctx>>,
    {
        let params = params.collect_vec().into_boxed_slice();
        let fn_ty = self.interner.intern_fn(params, return_type);
        self.interner
            .intern_type_scheme(type_params, fn_ty, source_region)
    }

    /// used to create implementations
    pub(crate) fn mk_implementation(
        &self,
        implementation: Implementation<'ctx>,
    ) -> ImplementationRef<'ctx> {
        self.interner.alloc_implementation(implementation)
    }

    /// wrap an implementation up as a type
    pub(crate) fn mk_implementation_type(&self, imp: ImplementationRef<'ctx>) -> ITy<'ctx> {
        self.interner.intern_implementation(imp)
    }

    /// single instance of the Int64 type
    pub(crate) fn mk_int64(&self) -> ITy<'ctx> {
        self.interner.intern_int64()
    }

    /// single instance of the Nothing type
    pub(crate) fn mk_nothing(&self) -> ITy<'ctx> {
        self.interner.intern_nothing()
    }

    /// construct `Set` type
    pub(crate) fn mk_set(&self, elem: ITy<'ctx>) -> ITy<'ctx> {
        self.interner.intern_set(elem)
    }

    /// single instance of the String type
    pub(crate) fn mk_string(&self) -> ITy<'ctx> {
        self.interner.intern_string()
    }

    /// create a struct type
    pub(crate) fn mk_struct_type(&self, members: BTreeMap<StringType, ITy<'ctx>>) -> ITy<'ctx> {
        self.interner.intern_struct(members)
    }

    /// wrap a `TraitStruct` up as a `Type`
    pub(crate) fn mk_trait(&self, trait_: TraitStructRef<'ctx>) -> ITy<'ctx> {
        self.interner.intern_trait(trait_)
    }

    /// create a trait
    pub(crate) fn mk_trait_scheme(
        &self,
        trait_name: StringType,
        self_var: UnificationVarId,
        ty_pars: Box<[(StringType, UnificationVarId)]>,
        assoc_tys: BTreeMap<StringType, ITy<'ctx>>,
        methods: BTreeMap<StringType, ITy<'ctx>>,
    ) -> (ITy<'ctx>, TraitSchemeRef<'ctx>) {
        let tr = self
            .interner
            .alloc_trait_scheme(ty_pars, self_var, trait_name, assoc_tys, methods);
        let ty = self.interner.intern_trait_scheme(tr);
        (ty, tr)
    }

    /// construct a tuple type
    pub(crate) fn mk_tuple(&self, types: Vec<ITy<'ctx>>) -> ITy<'ctx> {
        self.interner.intern_tuple(types)
    }

    /// construct a type scheme wrapped up as a type
    pub(crate) fn mk_type_scheme(
        &self,
        ty_pars: Box<[UnificationVarId]>,
        ty: ITy<'ctx>,
        source_region: &SourceRegion,
    ) -> ITy<'ctx> {
        self.interner
            .intern_type_scheme(ty_pars, ty, Box::new(source_region.clone()))
    }

    /// construct a placeholder for generic type parameters, to be replaced during instantiation
    pub(crate) fn mk_type_parameter(
        &self,
        source_region: &SourceRegion,
    ) -> TiResult<(ITy<'ctx>, UnificationVarId)> {
        let uniq_id =
            make_unification_var_id().map_err(|e| TiError::new(e, source_region.clone()))?;
        Ok((
            self.interner.intern_type_parameter(uniq_id, source_region),
            uniq_id,
        ))
    }

    /// type that types have
    pub(crate) fn mk_typetype(&self, inner: ITy<'ctx>) -> ITy<'ctx> {
        self.interner.intern_typetype(inner)
    }

    /// create a fresh unification variable
    pub(crate) fn mk_unification_var<S: Into<Cow<'static, str>>>(
        &self,
        source_region: &SourceRegion,
        debug_note: S,
    ) -> TiResult<ITy<'ctx>> {
        let uniq_id =
            make_unification_var_id().map_err(|e| TiError::new(e, source_region.clone()))?;
        Ok(self
            .interner
            .intern_unification_var(uniq_id, source_region, debug_note))
    }

    /// create union type
    pub(crate) fn mk_union(&self, variants: Vec<ITy<'ctx>>) -> ITy<'ctx> {
        self.interner.intern_union(variants)
    }

    /// create a variant to go with a union type
    pub(crate) fn mk_variant<Name>(&self, name: Name, variant_type: ITy<'ctx>) -> ITy<'ctx>
    where
        Name: Into<StringType>,
    {
        self.interner.intern_variant(name.into(), variant_type)
    }

    /// create a vector type
    pub(crate) fn mk_vector(&self, element_type: ITy<'ctx>) -> ITy<'ctx> {
        self.interner.intern_vector(element_type)
    }

    /// construct a new type inference context
    pub(crate) fn new(arenas: &'ctx Arenas<'ctx>) -> Context<'ctx> {
        Context {
            interner: Interner::new(arenas),
            implementation_schemes_by_trait: RefCell::new(FxHashMap::default()),
            implementation_schemes_by_member_name: RefCell::new(FxHashMap::default()),
            scopes: Default::default(),
            warnings: Default::default(),
            constraints: Default::default(),
            constraints_by_type_var: Default::default(),
            generic_constraints: Default::default(),
            impl_table: Default::default(),
        }
    }

    /// remove a previously added constraint
    pub(crate) fn remove_constraint(&self, c: ICon<'ctx>) {
        self.constraints.borrow_mut().retain(|v| v != &c);
        for set in self.generic_constraints.borrow_mut().values_mut() {
            set.retain(|v| v != &c);
        }
    }

    /// during type/trait solving: store a figured-out implementation for later
    /// lookup during evaluation or code generation
    pub(crate) fn store_implementation(
        &self,
        for_ty: ITy<'ctx>,
        trait_struct: TraitStructRef<'ctx>,
        implementation: ImplementationRef<'ctx>,
    ) -> ImplementationRef<'ctx> {
        // TODO: report error if it is already there
        self.impl_table
            .borrow_mut()
            .entry(for_ty)
            .or_default()
            .entry(trait_struct)
            .or_insert(implementation)
    }

    /// construct a trait type
    #[inline]
    pub(crate) fn type_from_literal_kind(&self, kind: &Literal) -> ITy<'ctx> {
        match kind {
            Literal::BigInt(_) => self.mk_bigint(),
            Literal::Bool(_) => self.mk_bool(),
            Literal::Character(_) => self.mk_char(),
            Literal::Float(_) => self.mk_float64(),
            Literal::Int64(_) => self.mk_int64(),
            Literal::String(_) => self.mk_string(),
            Literal::Nothing => self.mk_nothing(),
        }
    }

    /// emit a warning
    pub(crate) fn warn(&self, msg_tuple: loc::MsgTuple, source_region: SourceRegion) {
        let mut v = self.warnings.borrow_mut();
        let e = TiError::new(msg_tuple, source_region);
        if !v.contains(&e) {
            v.push(e);
        }
    }
}
