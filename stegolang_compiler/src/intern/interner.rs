//! Utility for interning values that could come up repeatedly

use std::{borrow::Cow, cell::RefCell, collections::BTreeMap};

use crate::{
    sourceregion::SourceRegion,
    ti::{
        arenas::Arenas,
        constraint::Constraint,
        traitstruct::{TraitScheme, TraitStruct},
        FnType, ImplScheme, Implementation, Scheme, Type, UnificationVarId, Variant,
    },
    StringType,
};

use super::{interned::Interned, internset::InternSet};

/// utility for interning things
pub struct Interner<'a> {
    /// arena storage for all the stuff
    arenas: &'a Arenas<'a>,
    /// interned constraints
    constraints: RefCell<InternSet<'a, Constraint<'a>>>,
    /// interned types
    types: RefCell<InternSet<'a, Type<'a>>>,
}

impl<'a> Interner<'a> {
    /// store an implementation scheme
    pub(crate) fn alloc_impl_scheme(
        &self,
        ty_pars: Box<[(StringType, Interned<'a, Type<'a>>)]>,
        self_var: Interned<'a, Type<'a>>,
        implementation: &'a Implementation<'a>,
    ) -> &'a ImplScheme<'a> {
        self.arenas.implementation_schemes.alloc(ImplScheme {
            ty_pars,
            self_var,
            implementation,
        })
    }

    /// store an implementation
    pub(crate) fn alloc_implementation(
        &self,
        implementation: Implementation<'a>,
    ) -> &'a Implementation<'a> {
        self.arenas.implementations.alloc(implementation)
    }

    /// store a scheme
    pub(crate) fn alloc_scheme(
        &self,
        ty_pars: Box<[UnificationVarId]>,
        ty: Interned<'a, Type<'a>>,
        source_region: Box<SourceRegion>,
    ) -> &'a Scheme<'a> {
        self.arenas.schemes.alloc(Scheme {
            ty_pars,
            ty,
            source_region,
        })
    }

    /// store a trait
    pub(crate) fn alloc_trait(
        &self,
        typar_choices: Box<[Interned<'a, Type<'a>>]>,
        self_var: UnificationVarId,
        trait_name: StringType,
        assoc_tys: BTreeMap<StringType, Interned<'a, Type<'a>>>,
        methods: BTreeMap<StringType, Interned<'a, Type<'a>>>,
    ) -> &'a TraitStruct<'a> {
        self.arenas.traits.alloc(TraitStruct {
            typar_choices,
            self_var,
            trait_name,
            assoc_tys,
            methods,
        })
    }

    /// store a trait
    pub(crate) fn alloc_trait_scheme(
        &self,
        ty_pars: Box<[(StringType, UnificationVarId)]>,
        self_var: UnificationVarId,
        trait_name: StringType,
        assoc_tys: BTreeMap<StringType, Interned<'a, Type<'a>>>,
        methods: BTreeMap<StringType, Interned<'a, Type<'a>>>,
    ) -> &'a TraitScheme<'a> {
        self.arenas.traitschemes.alloc(TraitScheme {
            ty_pars,
            self_var,
            trait_name,
            assoc_tys,
            methods,
        })
    }

    /// get single instance of the `Bigint` type
    pub(crate) fn intern_bigint(&self) -> Interned<'a, Type<'a>> {
        self.intern_type(Type::BigInt)
    }

    /// get single instance of the `Bool` type
    pub(crate) fn intern_bool(&self) -> Interned<'a, Type<'a>> {
        self.intern_type(Type::Bool)
    }

    /// get single instance of the `Char` type
    pub(crate) fn intern_char(&self) -> Interned<'a, Type<'a>> {
        self.intern_type(Type::Char)
    }

    /// avoid making repeated constraints
    pub(crate) fn intern_constraint(&self, c: Constraint<'a>) -> Interned<'a, Constraint<'a>> {
        let mut intern_set = self.constraints.borrow_mut();
        intern_set.intern(c, |value| self.arenas.constraints.alloc(value))
    }

    /// intern or get previously-interned `Dict` type
    pub(crate) fn intern_dict(
        &self,
        key: Interned<'a, Type<'a>>,
        value: Interned<'a, Type<'a>>,
    ) -> Interned<'a, Type<'a>> {
        self.intern_type(Type::Dict { key, value })
    }

    /// intern or get previously-interned "element-of" type
    pub(crate) fn intern_element_of(
        &self,
        iter_ty: Interned<'a, Type<'a>>,
        source_region: SourceRegion,
    ) -> Interned<'a, Type<'a>> {
        self.intern_type(Type::ElementOf(iter_ty, Box::new(source_region)))
    }

    /// intern or get previously-interned fn type
    pub(crate) fn intern_fn(
        &self,
        params: Box<[Interned<'a, Type<'a>>]>,
        result: Interned<'a, Type<'a>>,
    ) -> Interned<'a, Type<'a>> {
        self.intern_type(Type::F(FnType { params, result }))
    }

    /// intern or get previously-interned fn type
    pub(crate) fn intern_implementation(
        &self,
        implementation: &'a Implementation<'a>,
    ) -> Interned<'a, Type<'a>> {
        self.intern_type(Type::Implementation(implementation))
    }

    /// get single instance of the `Int64` type
    pub(crate) fn intern_int64(&self) -> Interned<'a, Type<'a>> {
        self.intern_type(Type::Int64)
    }

    /// get single instance of the `Float64` type
    pub(crate) fn intern_float64(&self) -> Interned<'a, Type<'a>> {
        self.intern_type(Type::Float64)
    }

    /// get single instance of the `Nothing` type
    pub(crate) fn intern_nothing(&self) -> Interned<'a, Type<'a>> {
        self.intern_type(Type::Nothing)
    }

    /// intern or get previously-interned `Set` type
    pub(crate) fn intern_set(&self, elem: Interned<'a, Type<'a>>) -> Interned<'a, Type<'a>> {
        self.intern_type(Type::Set(elem))
    }

    /// get single instance of the `String` type
    pub(crate) fn intern_string(&self) -> Interned<'a, Type<'a>> {
        self.intern_type(Type::String)
    }

    /// intern or get previously-interned `Struct`
    pub(crate) fn intern_struct(
        &self,
        members: BTreeMap<StringType, Interned<'a, Type<'a>>>,
    ) -> Interned<'a, Type<'a>> {
        self.intern_type(Type::Struct(members))
    }

    /// intern or get previously-interned trait type using an already-allocated `TraitStruct`
    pub(crate) fn intern_trait(&self, tr: &'a TraitStruct<'a>) -> Interned<'a, Type<'a>> {
        self.intern_type(Type::TraitStruct(tr))
    }

    /// wrap a `TraitScheme` as a `Type`
    pub(crate) fn intern_trait_scheme(&self, tr: &'a TraitScheme<'a>) -> Interned<'a, Type<'a>> {
        self.intern_type(Type::TraitScheme(tr))
    }

    /// intern or get previously-interned `Tuple` type
    pub(crate) fn intern_tuple(
        &self,
        types: Vec<Interned<'a, Type<'a>>>,
    ) -> Interned<'a, Type<'a>> {
        self.intern_type(Type::Tuple(types))
    }

    /// private helper for `intern_*` methods.
    fn intern_type(&self, t: Type<'a>) -> Interned<'a, Type<'a>> {
        let mut intern_set = self.types.borrow_mut();
        intern_set.intern(t, |value| self.arenas.types.alloc(value))
    }

    /// placeholders used by generic types. They will get replaced when we we
    /// instantiate generic types into monotypes
    pub(crate) fn intern_type_parameter(
        &self,
        uniq_id: UnificationVarId,
        source_region: &SourceRegion,
    ) -> Interned<'a, Type<'a>> {
        self.intern_type(Type::TypeParameter {
            uniq_id,
            source_region: source_region.clone(),
        })
    }

    /// intern or get previously-interned type scheme
    pub(crate) fn intern_type_scheme(
        &self,
        ty_pars: Box<[UnificationVarId]>,
        ty: Interned<'a, Type<'a>>,
        source_region: Box<SourceRegion>,
    ) -> Interned<'a, Type<'a>> {
        self.intern_type(Type::TypeScheme(self.alloc_scheme(
            ty_pars,
            ty,
            source_region,
        )))
    }

    /// intern or get previously-interned `Type` for the type that types have
    pub(crate) fn intern_typetype(&self, inner: Interned<'a, Type<'a>>) -> Interned<'a, Type<'a>> {
        self.intern_type(Type::Type(inner))
    }

    /// intern a type var
    pub(crate) fn intern_unification_var<S: Into<Cow<'static, str>>>(
        &self,
        uniq_id: UnificationVarId,
        source_region: &SourceRegion,
        debug_note: S,
    ) -> Interned<'a, Type<'a>> {
        self.intern_type(Type::UnificationVar {
            uniq_id,
            source_region: source_region.clone(),
            debug_note: debug_note.into(),
        })
    }

    /// intern a sum type
    pub(crate) fn intern_union(
        &self,
        variants: Vec<Interned<'a, Type<'a>>>,
    ) -> Interned<'a, Type<'a>> {
        self.intern_type(Type::Union(variants))
    }

    /// intern a variant of a sum type
    pub(crate) fn intern_variant(
        &self,
        name: StringType,
        variant_type: Interned<'a, Type<'a>>,
    ) -> Interned<'a, Type<'a>> {
        self.intern_type(Type::Variant(Variant { name, variant_type }))
    }

    /// intern a vector
    pub(crate) fn intern_vector(
        &self,
        element_type: Interned<'a, Type<'a>>,
    ) -> Interned<'a, Type<'a>> {
        self.intern_type(Type::Vector(element_type))
    }

    /// construct an interner
    pub(crate) fn new(arenas: &'a Arenas<'a>) -> Self {
        Interner {
            arenas,
            constraints: RefCell::new(InternSet::default()),
            types: RefCell::new(InternSet::default()),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::ti::arenas::Arenas;

    use super::Interner;

    #[test]
    fn interner_1() {
        let arenas = Arenas::new();
        let interner = Interner::new(&arenas);
        assert!(interner.intern_bool() == interner.intern_bool());
        assert!(interner.intern_string() == interner.intern_string());
        assert!(interner.intern_nothing() != interner.intern_bool());
        assert!(
            interner.intern_vector(interner.intern_bool())
                == interner.intern_vector(interner.intern_bool())
        );
        assert!(
            interner.intern_vector(interner.intern_bool())
                != interner.intern_vector(interner.intern_int64())
        );
    }
}
