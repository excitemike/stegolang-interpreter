use std::{
    borrow::Borrow,
    hash::{BuildHasher, BuildHasherDefault, Hasher},
};

use super::interned::Interned;
use hashbrown::HashMap;
use rustc_hash::FxHasher;
use std::hash::Hash;

/// track interned items
pub(crate) struct InternSet<'a, T>(HashMap<Interned<'a, T>, (), BuildHasherDefault<FxHasher>>);

impl<'a, T> InternSet<'a, T>
where
    T: Eq + Hash,
{
    /// If the value already exists in the set, return that one.
    /// Otherwise, add it to the set (and still return it wrapped in `Interned`)
    pub fn intern<F>(&mut self, value: T, alloc_fn: F) -> Interned<'a, T>
    where
        Interned<'a, T>: Borrow<T>,
        T: Hash + Eq + std::fmt::Debug + std::fmt::Display,
        F: FnOnce(T) -> &'a T,
    {
        let mut state = self.0.hasher().build_hasher();
        value.hash(&mut state);
        let hash = state.finish();
        let entry = self.0.raw_entry_mut().from_key_hashed_nocheck(hash, &value);
        let result = match entry {
            hashbrown::hash_map::RawEntryMut::Occupied(e) => *e.key(),
            hashbrown::hash_map::RawEntryMut::Vacant(e) => {
                let interned = Interned::new_unchecked(alloc_fn(value));
                *e.insert_hashed_nocheck(hash, interned, ()).0
            }
        };
        result
    }
}

impl<'a, T> Default for InternSet<'a, T> {
    fn default() -> Self {
        Self(HashMap::default())
    }
}
