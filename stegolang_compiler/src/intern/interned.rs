use std::{
    borrow::Borrow,
    fmt::{Debug, Display},
    ops::Deref,
};

mod private {
    /// inaccessible from outside to prevent construction of Interned except
    /// through Interner
    #[derive(Clone, Copy, Debug)]
    pub struct Zst;
}

/// handle to an interned item
pub struct Interned<'a, T>(pub(crate) &'a T, pub private::Zst);

impl<'a, T> Borrow<T> for Interned<'a, T> {
    fn borrow(&self) -> &T {
        self.0
    }
}
impl<'a, T> Clone for Interned<'a, T> {
    fn clone(&self) -> Self {
        *self
    }
}
impl<'a, T> Copy for Interned<'a, T> {}
impl<'a, T> Debug for Interned<'a, T>
where
    T: Debug,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        //self.0.fmt(f)
        write!(f, "Interned({:?} / {:p})", self.0, self.0)
    }
}
impl<'a, T> Deref for Interned<'a, T> {
    type Target = T;
    #[inline]
    fn deref(&self) -> &Self::Target {
        self.0
    }
}
impl<'a, T> Display for Interned<'a, T>
where
    T: Display,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}
impl<'a, T> Eq for Interned<'a, T> {}
impl<'a, T> std::hash::Hash for Interned<'a, T>
where
    T: std::hash::Hash,
{
    #[inline]
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.0.hash(state);
    }
}
impl<'a, T> Ord for Interned<'a, T>
where
    T: Ord,
{
    #[inline]
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        let a: *const T = self.0;
        let b: *const T = other.0;
        <*const T>::cmp(&a, &b)
    }
}
impl<'a, T> PartialEq for Interned<'a, T> {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        std::ptr::eq(self.0, other.0)
    }
}
impl<'a, T> PartialOrd for Interned<'a, T>
where
    T: PartialOrd,
{
    #[inline]
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        let a: *const T = self.0;
        let b: *const T = other.0;
        <*const T>::partial_cmp(&a, &b)
    }
}
impl<'a, T> Interned<'a, T> {
    pub(super) fn new_unchecked(value: &'a T) -> Self {
        Interned(value, private::Zst)
    }
    /// borrow the wrapped value
    #[inline]
    pub(crate) fn inner(&self) -> &T {
        self.0
    }
}
