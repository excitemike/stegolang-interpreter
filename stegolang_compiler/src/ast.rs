//! data types for abstract syntax tree
use std::{cell::RefCell, rc::Rc};

use crate::{config, pattern::StegoPattern, StringType};

pub(crate) mod astnode;
pub(crate) mod literal;

/// arms of a match expression
#[allow(dead_code)] // TODO: implement match expression
#[derive(Debug)]
pub struct MatchArm<T> {
    pattern: Box<T>,
    block: Box<T>,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
// condition -> consequent
pub(crate) struct IfArm<T> {
    pub cond: T,
    pub consequent: T,
}

/// elements of a dict expression
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) enum DictExprElem<T> {
    KeywordSpread(T),
    KeyValue(T, T),
}

/// elements of a set expression
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) enum SetExprElem<T> {
    Spread(T),
    Value(T),
}

/// operators in stegolang
#[derive(Clone, Copy, Debug, PartialOrd, Ord, PartialEq, Eq, Hash)]
pub enum Operator {
    Times,
    Divide,
    Minus,
    Remainder,
    Plus,
    Equals,
    NotEquals,
    LessThanOrEqualTo,
    LessThan,
    GreaterThanOrEqualTo,
    GreaterThan,
}

impl Operator {
    /// whether the method result needs  to be inverted
    pub(crate) fn get_invert(self) -> bool {
        use Operator::{
            Divide, Equals, GreaterThan, GreaterThanOrEqualTo, LessThan, LessThanOrEqualTo, Minus,
            NotEquals, Plus, Remainder, Times,
        };
        match self {
            Times | Divide | Minus | Remainder | Plus | Equals | LessThan | GreaterThan => false,
            NotEquals | LessThanOrEqualTo | GreaterThanOrEqualTo => true,
        }
    }

    /// method name associated with the operator
    pub(crate) fn get_method_name(self) -> StringType {
        use Operator::{
            Divide, Equals, GreaterThan, GreaterThanOrEqualTo, LessThan, LessThanOrEqualTo, Minus,
            NotEquals, Plus, Remainder, Times,
        };
        StringType::from(match self {
            Times => config::MULTIPLY_METHOD_NAME,
            Divide => config::DIVIDE_METHOD_NAME,
            Minus => config::SUBTRACT_METHOD_NAME,
            Remainder => config::REMAINDER_METHOD_NAME,
            Plus => config::ADD_METHOD_NAME,
            Equals | NotEquals => config::EQUALS_METHOD_NAME,
            LessThanOrEqualTo | GreaterThan => config::GREATERTHAN_METHOD_NAME,
            LessThan | GreaterThanOrEqualTo => config::LESSTHAN_METHOD_NAME,
        })
    }

    /// trait name associated with the operator
    pub(crate) fn get_trait_name(self) -> StringType {
        use Operator::{
            Divide, Equals, GreaterThan, GreaterThanOrEqualTo, LessThan, LessThanOrEqualTo, Minus,
            NotEquals, Plus, Remainder, Times,
        };
        StringType::from(match self {
            Times => config::MULTIPLY_TRAIT_NAME,
            Divide => config::DIVIDE_TRAIT_NAME,
            Minus => config::SUBTRACT_TRAIT_NAME,
            Remainder => config::REMAINDER_TRAIT_NAME,
            Plus => config::ADD_TRAIT_NAME,
            Equals | NotEquals => config::EQUALS_TRAIT_NAME,
            LessThanOrEqualTo | LessThan | GreaterThanOrEqualTo | GreaterThan => {
                config::COMPARE_TRAIT_NAME
            }
        })
    }

    /// printable string for operator
    pub(crate) fn to_str(self) -> &'static str {
        match self {
            Operator::Times => config::MULTIPLY_OPERATOR,
            Operator::Divide => config::DIVIDE_OPERATOR,
            Operator::Minus => config::MINUS,
            Operator::Remainder => config::REMAINDER_OPERATOR,
            Operator::Plus => config::PLUS,
            Operator::Equals => config::EQ_OPERATOR,
            Operator::NotEquals => config::NEQ_OPERATOR,
            Operator::LessThanOrEqualTo => config::LTE_OPERATOR,
            Operator::LessThan => config::LT_OPERATOR,
            Operator::GreaterThanOrEqualTo => config::GTE_OPERATOR,
            Operator::GreaterThan => config::GT_OPERATOR,
        }
    }
}

/// data for the `FnStmt` `AstNodeKind`
/// If you modify this struct, make sure you update the `PartialOrd` and Ord implementations
#[derive(Clone, Debug, PartialEq, Eq)]
pub(crate) struct FnStmt<NodeType> {
    pub(crate) body: Rc<RefCell<NodeType>>,
    pub(crate) name: StringType,
    pub(crate) params: Rc<RefCell<Vec<StegoPattern>>>,
    /// whether a placeholder needs to inserted into context to make recursion possible
    pub(crate) placeholder_needed: bool,
}

impl<NodeType> PartialOrd for FnStmt<NodeType>
where
    NodeType: PartialOrd,
{
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        match self.body.partial_cmp(&other.body) {
            Some(core::cmp::Ordering::Equal) => {}
            ord => return ord,
        }
        match self.name.partial_cmp(&other.name) {
            Some(core::cmp::Ordering::Equal) => {}
            ord => return ord,
        }
        match self.params.partial_cmp(&other.params) {
            Some(core::cmp::Ordering::Equal) => {}
            ord => return ord,
        }
        self.placeholder_needed
            .partial_cmp(&other.placeholder_needed)
    }
}

impl<NodeType> Ord for FnStmt<NodeType>
where
    NodeType: Ord,
{
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        match self.body.cmp(&other.body) {
            core::cmp::Ordering::Equal => {}
            ord => return ord,
        }
        match self.name.cmp(&other.name) {
            core::cmp::Ordering::Equal => {}
            ord => return ord,
        }
        match self.params.cmp(&other.params) {
            core::cmp::Ordering::Equal => {}
            ord => return ord,
        }
        self.placeholder_needed.cmp(&other.placeholder_needed)
    }
}
