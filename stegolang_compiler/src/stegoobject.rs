use crate::{
    ast::literal::Literal,
    config,
    fntypes::fncode::FnCode,
    fntypes::{fnfamily::FnFamily, fnfamimpls::FnFamImpls},
    loc::{self, MsgTuple},
    pattern::StegoPattern,
    ti::substitute::ApplySubst,
    typedvalue::{OwnedValue, TypedValue},
    util::{RcStr, RcVal},
    StringType,
};
use itertools::Itertools;
use num::{
    bigint::{BigInt, ToBigInt},
    traits::{ToPrimitive, Zero},
};
use rustc_hash::FxHashMap;
use std::{
    cell::RefCell,
    collections::BTreeMap,
    fmt::Display,
    hash::{Hash, Hasher},
    rc::Rc,
    sync::Arc,
};

/// function implementation struct for `FnFamily` to use
#[derive(Clone, Debug)]
pub struct FnImpl<'ctx> {
    /// patterns this impl is gated on
    pub(crate) pattern_set: Rc<RefCell<PatternSet>>,

    /// captured environment
    pub(crate) captures: BTreeMap<StringType, Rc<RefCell<TypedValue<'ctx>>>>,

    /// what to run
    pub(crate) code: FnCode<'ctx>,
}
impl<'a> std::fmt::Display for FnImpl<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "({}-ary Fn)", self.pattern_set.borrow().len())
    }
}
impl<'ctx> PartialEq for FnImpl<'ctx> {
    fn eq(&self, other: &Self) -> bool {
        self.pattern_set == other.pattern_set
            && self.captures.len() == other.captures.len()
            && self.code == other.code
            && {
                self.captures.iter().all(|(k, v1)| {
                    if let Some(v2) = other.captures.get(k) {
                        Rc::ptr_eq(v1, v2)
                    } else {
                        false
                    }
                })
            }
    }
}
impl<'ctx> Eq for FnImpl<'ctx> {}
impl<'ctx> std::hash::Hash for FnImpl<'ctx> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        for pat in self.pattern_set.borrow().iter() {
            pat.hash(state);
        }
        for (k, v) in &self.captures {
            k.hash(state);
            v.borrow().hash(state);
        }
        self.code.hash(state);
    }
}
impl<'ctx> PartialOrd for FnImpl<'ctx> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}
impl<'ctx> Ord for FnImpl<'ctx> {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        match self.pattern_set.cmp(&other.pattern_set) {
            core::cmp::Ordering::Equal => {}
            ord => return ord,
        }
        match self.code.cmp(&other.code) {
            core::cmp::Ordering::Equal => {}
            ord => return ord,
        }
        match self.captures.len().cmp(&other.captures.len()) {
            core::cmp::Ordering::Equal => {}
            ord => return ord,
        }
        for ((k1, v1), (k2, v2)) in self.captures.iter().zip(other.captures.iter()) {
            match k1.cmp(k2) {
                core::cmp::Ordering::Equal => {}
                ord => return ord,
            }
            let p1 = Rc::as_ptr(v1) as usize;
            let p2 = Rc::as_ptr(v2) as usize;
            match p1.cmp(&p2) {
                core::cmp::Ordering::Equal => {}
                ord => return ord,
            }
        }
        core::cmp::Ordering::Equal
    }
}
impl<'a> ApplySubst<'a> for FnImpl<'a> {
    fn apply_subst(
        &self,
        substitutions: &crate::ti::Substitutions<'a>,
        ctx: &crate::ti::Context<'a>,
    ) -> crate::ti::TiResult<Option<Self>> {
        let Some(code) = self.code.apply_subst(substitutions, ctx)? else {
            return Ok(None);
        };
        Ok(Some(FnImpl {
            pattern_set: Rc::clone(&self.pattern_set),
            captures: self.captures.clone(),
            code,
        }))
    }
}

/// An ordered group of patterns
pub(crate) type PatternSet = Vec<StegoPattern>;

/// struct for native functions
#[derive(Clone, Debug)]
pub(crate) struct FnNativeUnary<'ctx> {
    // function pointer
    pub(crate) _f: Arc<FnImpl<'ctx>>,
    /// what to call the function in error messages
    pub(crate) _debug_name: RcStr,
}

/// data for partial application of a function
#[derive(Clone, Debug)]
pub(crate) struct FnPartial<'ctx> {
    pub(crate) family: Rc<FnFamily<'ctx>>,
    pub(crate) already_supplied_arguments: Vec<TypedValue<'ctx>>,
}
impl<'a> ApplySubst<'a> for FnPartial<'a> {
    fn apply_subst(
        &self,
        substitutions: &crate::ti::Substitutions<'a>,
        ctx: &crate::ti::Context<'a>,
    ) -> crate::ti::TiResult<Option<Self>> {
        let family2 = self.family.apply_subst(substitutions, ctx)?;
        let args2 = self
            .already_supplied_arguments
            .apply_subst(substitutions, ctx)?;
        if family2.is_none() && args2.is_none() {
            Ok(None)
        } else {
            let family = family2.unwrap_or_else(|| Rc::clone(&self.family));
            let already_supplied_arguments =
                args2.unwrap_or_else(|| self.already_supplied_arguments.clone());
            Ok(Some(FnPartial {
                family,
                already_supplied_arguments,
            }))
        }
    }
}

/// AST interpreter's implementation of stego objects
#[derive(Clone, Debug)]
pub(crate) enum StegoObject<'ctx> {
    False,
    True,
    Int64(i64),

    FnFamily(Rc<FnFamily<'ctx>>),

    FnPartial(FnPartial<'ctx>),
    Float64(f64),
    Integer(BigInt),

    Char(char),

    /// TODO: needs type restrictions
    Dict(FxHashMap<RcVal<'ctx>, RcVal<'ctx>>),

    // TODO: deprecate
    StegoString(RcStr), // TODO: string slices.

    StegoImmString(StringType), // TODO: string slices.

    Tuple(Vec<RcVal<'ctx>>),

    UnitType,
}

impl<'ctx> From<bool> for StegoObject<'ctx> {
    fn from(x: bool) -> Self {
        if x {
            StegoObject::True
        } else {
            StegoObject::False
        }
    }
}

impl<'ctx> From<BigInt> for StegoObject<'ctx> {
    fn from(x: BigInt) -> Self {
        StegoObject::Integer(x)
    }
}

impl<'ctx> From<i32> for StegoObject<'ctx> {
    fn from(x: i32) -> Self {
        StegoObject::Int64(x.into())
    }
}

impl<'ctx> From<i64> for StegoObject<'ctx> {
    fn from(x: i64) -> Self {
        StegoObject::Int64(x)
    }
}

impl<'ctx> From<f64> for StegoObject<'ctx> {
    fn from(x: f64) -> Self {
        StegoObject::Float64(x)
    }
}

impl<'ctx> From<char> for StegoObject<'ctx> {
    fn from(c: char) -> Self {
        StegoObject::Char(c)
    }
}

impl<'ctx> From<RcStr> for StegoObject<'ctx> {
    fn from(s: RcStr) -> Self {
        StegoObject::StegoString(s)
    }
}

impl<'ctx> From<StringType> for StegoObject<'ctx> {
    fn from(s: StringType) -> Self {
        StegoObject::StegoImmString(s)
    }
}

impl<'ctx, T> From<&T> for StegoObject<'ctx>
where
    StegoObject<'ctx>: From<T>,
    T: Copy,
{
    fn from(x: &T) -> Self {
        StegoObject::from(*x)
    }
}

impl<'ctx> From<Literal> for StegoObject<'ctx> {
    fn from(x: Literal) -> Self {
        use Literal::{BigInt, Bool, Character, Float, Int64, Nothing, String};
        match x {
            BigInt(x) => StegoObject::from(x),
            Bool(x) => StegoObject::from(x),
            Character(x) => StegoObject::from(x),
            Float(x) => StegoObject::from(x),
            Int64(x) => StegoObject::from(x),
            String(x) => StegoObject::from(x),
            Nothing => StegoObject::UnitType,
        }
    }
}

impl<'ctx> StegoObject<'ctx> {
    /// whether the value should act like true
    pub fn is_truthy(&self) -> bool {
        use StegoObject::{
            Char, Dict, False, Float64, FnFamily, FnPartial, Int64, Integer, StegoImmString,
            StegoString, True, Tuple, UnitType,
        };
        match self {
            True => true,
            False | UnitType => false,
            Integer(bigint) => !bigint.is_zero(),
            Int64(i) => *i != 0,
            Float64(f) => *f != 0.0,
            Char(c) => *c != '\0',
            Dict(hashmap) => !hashmap.is_empty(),
            StegoString(s) => !s.is_empty(),
            StegoImmString(s) => !s.is_empty(),
            Tuple(v) => !v.is_empty(),
            FnFamily { .. } | FnPartial { .. } => unreachable!(),
        }
    }

    /// check to see if nemeric type
    /// TODO: typeclasses
    pub fn is_numeric(&self) -> bool {
        use StegoObject::{Float64, Int64, Integer};
        matches!(self, Integer(..) | Int64(..) | Float64(..))
    }

    /// check to see whether you can iterate over the object
    /// TODO: typeclasses
    pub fn is_iterable(&self) -> bool {
        use StegoObject::{
            Char, Dict, False, Float64, FnFamily, FnPartial, Int64, Integer, StegoImmString,
            StegoString, True, Tuple, UnitType,
        };
        match self {
            True | False | Int64(..) | Float64(..) | Integer(..) | Char(..) => false,
            Dict(..) | StegoString(..) | StegoImmString(..) | UnitType | Tuple(..) => true,
            FnFamily { .. } | FnPartial { .. } => unreachable!(),
        }
    }

    /// check to see if it can be a dict key or held in a set
    /// TODO: typeclasses
    pub fn is_hashable(&self) -> bool {
        use StegoObject::{
            Char, Dict, False, Float64, FnFamily, FnPartial, Int64, Integer, StegoImmString,
            StegoString, True, Tuple, UnitType,
        };
        match self {
            True | False | Int64(..) | Float64(..) | Integer(..) | Char(..) | StegoString(..)
            | StegoImmString(..) | UnitType => true,
            Tuple(v) => v.iter().all(move |x| x.is_hashable()),
            Dict(..) => false,
            FnFamily { .. } | FnPartial { .. } => unreachable!(),
        }
    }

    /// try to convert to i64
    /// TODO: too permissive I think
    #[allow(clippy::cast_possible_truncation)]
    pub fn to_i64(&self) -> Result<i64, MsgTuple> {
        use StegoObject::{
            Char, Dict, False, Float64, FnFamily, FnPartial, Int64, Integer, StegoImmString,
            StegoString, True, Tuple, UnitType,
        };
        match self {
            True => Ok(1i64),
            False => Ok(0i64),
            Int64(i) => Ok(*i),
            Float64(f) => Ok(*f as i64),
            Char(c) => Ok(*c as i64),
            StegoString(_s) => todo!("parse to i64"),
            StegoImmString(_s) => todo!("parse to i64"),
            Integer(..) | Tuple(..) | Dict(..) | UnitType => {
                Err(loc::err::bad_to_i64(&format!("{}", self.repr())))
            }
            FnFamily { .. } | FnPartial { .. } => unreachable!(),
        }
    }

    /// try to convert to f64
    /// TODO: too permissive I think
    #[allow(clippy::cast_possible_truncation, clippy::cast_precision_loss)]
    pub fn to_f64(&self) -> Result<f64, MsgTuple> {
        use StegoObject::{
            Char, Dict, False, Float64, FnFamily, FnPartial, Int64, Integer, StegoImmString,
            StegoString, True, Tuple, UnitType,
        };
        match self {
            True => Ok(1f64),
            False => Ok(0f64),
            Integer(bigint) => match bigint.to_f64() {
                Some(x) => Ok(x),
                None => {
                    if bigint >= &BigInt::zero() {
                        Ok(f64::INFINITY)
                    } else {
                        Ok(f64::NEG_INFINITY)
                    }
                }
            },
            Int64(i) => Ok(*i as f64),
            Float64(f) => Ok(*f),
            Char(c) => Ok(*c as i64 as f64),
            StegoString(_s) => todo!("parse to f64"),
            StegoImmString(_s) => todo!("parse to f64"),
            Tuple(..) | Dict(..) | UnitType => {
                Err(loc::err::bad_to_f64(&format!("{}", self.repr())))
            }
            FnFamily { .. } | FnPartial { .. } => unreachable!(),
        }
    }

    /// convert to something more sharable
    pub fn to_owned(&self) -> OwnedValue {
        match self {
            StegoObject::False => OwnedValue::Bool(false),
            StegoObject::True => OwnedValue::Bool(true),
            StegoObject::Int64(i) => OwnedValue::Int64(*i),
            StegoObject::FnFamily(fn_fam) => OwnedValue::FnFamily(fn_fam.debug_name.to_string()),
            StegoObject::FnPartial(_) => todo!(),
            StegoObject::Float64(f) => OwnedValue::Float64(*f),
            StegoObject::Integer(i) => OwnedValue::BigInt(i.clone()),
            StegoObject::Char(c) => OwnedValue::Char(*c),
            StegoObject::Dict(dict) => {
                let iter = dict
                    .iter()
                    .map(|(k, v)| ((**k).to_owned(), (**v).to_owned()));
                OwnedValue::Dict(iter.collect())
            }
            StegoObject::StegoString(s) => OwnedValue::String((**s).to_owned()),
            StegoObject::StegoImmString(s) => OwnedValue::String((**s).to_owned()),
            StegoObject::Tuple(v) => {
                OwnedValue::Tuple(v.iter().map(|o| (**o).to_owned()).collect_vec())
            }
            StegoObject::UnitType => OwnedValue::Nothing,
        }
    }

    /// get the code you could use to build the object
    pub fn repr<'a>(&'a self) -> StegoObjectRepr<'a, 'ctx>
    where
        'ctx: 'a,
    {
        StegoObjectRepr::<'a, 'ctx> { object: self }
    }

    /// if it is one of the string types, get a &str
    pub fn get_str(&self) -> Option<&str> {
        match self {
            StegoObject::StegoString(s) => Some(&**s),
            StegoObject::StegoImmString(s) => Some(&**s),
            _ => None,
        }
    }
}

impl<'ctx> PartialEq for StegoObject<'ctx> {
    /// equals operator
    #[allow(clippy::float_cmp)]
    fn eq(&self, other: &Self) -> bool {
        use StegoObject::{
            Char, Dict, False, Float64, FnFamily, FnPartial, Int64, Integer, StegoImmString,
            StegoString, True, Tuple, UnitType,
        };
        #[allow(unreachable_patterns)]
        match (self, other) {
            (True, True) | (False, False) | (UnitType, UnitType) => true,
            (Integer(a), Integer(b)) => *a == *b,
            (Int64(a), Int64(b)) => *b == *a,
            (Integer(a), Int64(b)) => b.to_bigint().unwrap() == *a,
            (Int64(a), Integer(b)) => *b == a.to_bigint().unwrap(),
            (Float64(a), Float64(b)) => *b == *a,
            (Float64(a), b) if b.is_numeric() => b.to_f64() == Ok(*a),
            (a, Float64(b)) if a.is_numeric() => a.to_f64() == Ok(*b),
            (Int64(a), b) if b.is_numeric() => b.to_i64() == Ok(*a),
            (a, Int64(b)) if a.is_numeric() => a.to_i64() == Ok(*b),
            (Char(c1), Char(c2)) => c1 == c2,
            (StegoString(s1), StegoString(s2)) => (**s1) == (**s2),
            (StegoImmString(s1), StegoString(s2)) => (**s1) == (**s2),
            (StegoString(s1), StegoImmString(s2)) => (**s1) == (**s2),
            (StegoImmString(s1), StegoImmString(s2)) => (**s1) == (**s2),
            (Tuple(v1), Tuple(v2)) => {
                (v1.len() == v2.len()) && v1.iter().zip(&**v2).all(|(a, b)| a.eq(b))
            }
            // as a special case, an empty tuple and the unit type are equal
            // (though the empty tuple representation should hopefully never be constructed)
            (Tuple(v), UnitType) | (UnitType, Tuple(v)) => v.is_empty(),
            (Dict(a), Dict(b)) => a == b,

            (FnFamily { .. } | FnPartial { .. }, _) | (_, FnFamily { .. } | FnPartial { .. }) => {
                unreachable!()
            }

            // aside from coersion for numeric types and a special case for empty tuple and nothing
            // (both of which are represented by the unit type), objects of different types are never equal
            (True | False, _)
            | (_, True | False)
            | (
                Float64(..) | Integer(..) | Int64(..) | Char(..) | StegoString(..)
                | StegoImmString(..) | Tuple(..) | Dict(..),
                ..,
            )
            | (
                ..,
                Float64(..) | Integer(..) | Int64(..) | Char(..) | StegoString(..)
                | StegoImmString(..) | Tuple(..) | Dict(..),
            ) => false,
        }
    }
}

impl<'ctx> Eq for StegoObject<'ctx> {}

impl<'ctx> Hash for StegoObject<'ctx> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        use StegoObject::{
            Char, Dict, False, Float64, FnFamily, FnPartial, Int64, Integer, StegoImmString,
            StegoString, True, Tuple, UnitType,
        };
        match self {
            True => true.hash(state),
            False => false.hash(state),
            Int64(x) => (*x).hash(state),
            Float64(x) => (*x).to_bits().hash(state),
            Integer(x) => x.hash(state),
            Char(x) => x.hash(state),
            Dict(..) => unreachable!("Dicts are not hashable"),
            StegoString(x) => x.hash(state),
            StegoImmString(x) => x.hash(state),
            Tuple(x) => x.hash(state),
            UnitType => (),
            FnFamily { .. } | FnPartial { .. } => unreachable!(),
        }
    }
}

impl<'ctx> Display for StegoObject<'ctx> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // Convert a StegoObject for display. Especially useful when running in interactive mode
        use StegoObject::{
            Char, Dict, False, Float64, FnFamily, FnPartial, Int64, Integer, StegoImmString,
            StegoString, True, Tuple, UnitType,
        };
        match self {
            False => write!(f, "{}", config::FALSE)?,
            True => write!(f, "{}", config::TRUE)?,
            Integer(x) => write!(f, "{}", &x.to_str_radix(10))?,
            Int64(x) => write!(f, "{}", &x.to_string())?,
            Float64(x) => write!(f, "{}", &x.to_string())?,
            Char(c) => write!(f, "{c}")?,
            StegoString(x) => write!(f, "{x}")?,
            StegoImmString(x) => write!(f, "{x}")?,
            Tuple(v) => {
                write!(f, "{}", config::OPEN_PAREN)?;
                let mut iter = v.iter();
                if let Some(x) = iter.next() {
                    write!(f, "{x}")?;
                }
                for x in iter {
                    write!(f, "{}{}", config::SEQUENCE_DELIM, x)?;
                }
                if v.len() == 1 {
                    write!(f, "{}", config::SEQUENCE_DELIM)?;
                }
                write!(f, "{}", config::CLOSE_PAREN)?;
            }
            Dict(hashmap) => {
                write!(f, "{}", config::OPEN_CURLY)?;
                let mut iter = hashmap.iter();
                if let Some((k, v)) = iter.next() {
                    write!(f, "{}{}{}", k, config::KEY_VALUE_SEPARATOR, v)?;
                }
                for elem in iter {
                    let (k, v) = elem;
                    write!(
                        f,
                        "{}{}{}{}",
                        config::SEQUENCE_DELIM,
                        k,
                        config::KEY_VALUE_SEPARATOR,
                        v
                    )?;
                }
                write!(f, "{}", config::CLOSE_CURLY)?;
            }

            UnitType => write!(f, "{}", crate::config::NOTHING_VALUE)?,
            FnFamily { .. } | FnPartial { .. } => write!(f, "{}", self.repr())?,
        }
        Ok(())
    }
}

/// adapt a `StegoObject` as a Display with repr format
pub struct StegoObjectRepr<'a, 'ctx> {
    object: &'a StegoObject<'ctx>,
}
impl<'a, 'ctx> Display for StegoObjectRepr<'a, 'ctx> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use StegoObject::{
            Char, Dict, False, Float64, FnFamily, FnPartial, Int64, Integer, StegoImmString,
            StegoString, True, Tuple, UnitType,
        };
        match self.object {
            False | True | Integer(..) | Int64(..) | Float64(..) | UnitType => {
                write!(f, "{}", self.object)
            }
            Char(c) => write!(
                f,
                "{}{}{}",
                config::CHARACTER_BEGIN,
                c,
                config::CHARACTER_END
            ),
            StegoString(x) => write!(f, "\"{}\"", x.escape_debug().collect::<String>()),
            StegoImmString(x) => write!(f, "{}{}{}", config::STRING_BEGIN, x, config::STRING_END),
            Tuple(v) => {
                write!(f, "{}", config::OPEN_PAREN)?;
                let mut iter = v.iter();
                if let Some(x) = iter.next() {
                    write!(f, "{}", x.repr())?;
                }
                for x in iter {
                    write!(f, "{}{}", config::SEQUENCE_DELIM, x.repr())?;
                }
                if v.len() == 1 {
                    write!(f, "{}", config::SEQUENCE_DELIM)?;
                }
                write!(f, "{}", config::CLOSE_PAREN)
            }
            Dict(hashmap) => {
                write!(f, "{}", config::OPEN_CURLY)?;
                let mut iter = hashmap.iter();
                if let Some((k, v)) = iter.next() {
                    write!(f, "{}{}{}", k.repr(), config::KEY_VALUE_SEPARATOR, v.repr())?;
                }
                for elem in iter {
                    let (k, v) = elem;
                    write!(
                        f,
                        "{}{}{}{}",
                        config::SEQUENCE_DELIM,
                        k.repr(),
                        config::KEY_VALUE_SEPARATOR,
                        v.repr()
                    )?;
                }
                write!(f, "{}", config::CLOSE_CURLY)
            }

            FnFamily(x) => match &x.implementations {
                FnFamImpls::Simple(imp) => match &imp.code {
                    FnCode::Statements(code) => write!(f, "{:?}", code.borrow()),
                    FnCode::Native(native_fn) => write!(
                        f,
                        "(native fn {}@{native_fn:p} taking {} params)",
                        x.debug_name, x.num_params
                    ),
                },
                FnFamImpls::Match { fn_impls: _ } => {
                    write!(
                        f,
                        "<<function family \"{}\" accepting {} param(s)>>",
                        x.debug_name, x.num_params
                    )
                }
            },

            FnPartial(fn_partial) => {
                write!(
                    f,
                    "<<partial function: \"{}\" with {} argument(s) bound>>", // TODO: should this string change based on localization?
                    fn_partial.family.debug_name,
                    fn_partial.already_supplied_arguments.len()
                )
            }
        }
    }
}

/// adapt a `StegoObject` as a Display
pub struct StegoObjectFmt<'a, 'ctx> {
    object: &'a StegoObject<'ctx>,
}
impl<'a, 'ctx> Display for StegoObjectFmt<'a, 'ctx> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // running in a cmd terminal or something
        use StegoObject::{
            Char, Dict, False, Float64, FnFamily, FnPartial, Int64, Integer, StegoImmString,
            StegoString, True, Tuple, UnitType,
        };
        match self.object {
            False | True | Integer(..) | Int64(..) | Float64(..) | UnitType => {
                write!(f, "{}", self.object)?;
            }
            Char(c) => write!(f, "{c}")?,
            StegoString(x) => write!(f, "{x}")?,
            StegoImmString(x) => write!(f, "{x}")?,
            Tuple(v) => {
                write!(f, "(")?;
                let mut iter = v.iter();
                if let Some(x) = iter.next() {
                    write!(f, "{}", x.repr())?;
                }
                for x in iter {
                    write!(f, ",{}", x.repr())?;
                }
                if v.len() == 1 {
                    write!(f, ",")?;
                }
                write!(f, ")")?;
            }
            Dict(hashmap) => {
                write!(f, "{{")?;
                let mut iter = hashmap.iter();
                if let Some((k, v)) = iter.next() {
                    write!(f, "{}:{}", k.repr(), v.repr())?;
                }
                for elem in iter {
                    let (k, v) = elem;
                    write!(f, ",{}:{}", k.repr(), v.repr())?;
                }
                write!(f, "}}")?;
            }

            FnFamily { .. } | FnPartial { .. } => self.object.repr().fmt(f)?,
        }
        Ok(())
    }
}
