//! module to handle error reporting

use crate::config;
use crate::error::{Error, Severity};
use crate::loc;
use crate::sourceregion::SourceRegion;
use crate::util::measure_width_in_columns;
use crate::writeunwrap;
use lazy_static::lazy_static;
use std::io::Write;
use std::{
    borrow::Cow,
    cmp::max,
    cmp::Ordering::{Equal, Greater, Less},
};
use unicode_segmentation::UnicodeSegmentation;

lazy_static! {
    static ref ERR_HDR_STYLE: console::Style = console::Style::new().red().bold();
    static ref ERR_SQUIGGLE_STYLE: console::Style = console::Style::new().red().bold().blink();
    static ref ERR_TOKEN_STYLE: console::Style = console::Style::new().red().bold().underlined();
    static ref ERR_LABEL_STYLE: console::Style = console::Style::new().cyan();
    static ref WARNING_HDR_STYLE: console::Style = console::Style::new().yellow().bold();
    static ref WARNING_SQUIGGLE_STYLE: console::Style =
        console::Style::new().yellow().bold().blink();
    static ref WARNING_TOKEN_STYLE: console::Style =
        console::Style::new().yellow().bold().underlined();
    static ref WARNING_LABEL_STYLE: console::Style = console::Style::new().cyan();
}

/// trait for errors that `report_error` can handle
pub trait ReportableError {
    /// location at which the error occurred
    fn loc(&self) -> &SourceRegion;
    /// short message to describe the error
    fn short_msg(&self) -> &str;
    /// longer explanation of the error and how it might be fixed
    fn help(&self) -> &str;
    /// error severity
    fn severity(&self) -> Severity;
}

/// write an error message to the given stream
pub fn report_error<S: std::io::Write + ?Sized, E>(stream: &mut S, e: &E)
where
    E: ReportableError,
{
    let hdr_style = match e.severity() {
        Severity::Error => &*ERR_HDR_STYLE,
        Severity::Warning => &*WARNING_HDR_STYLE,
    };
    let label_style = match e.severity() {
        Severity::Error => &*ERR_LABEL_STYLE,
        Severity::Warning => &*WARNING_LABEL_STYLE,
    };
    let severity_str = match e.severity() {
        Severity::Error => loc::severity_error(),
        Severity::Warning => loc::severity_warning(),
    };

    // write headermatch e.severity() {
    let header = match e.severity() {
        Severity::Error => loc::error_header(),
        Severity::Warning => loc::warning_header(),
    };
    writeunwrap!(stream, "{}\n", hdr_style.apply_to(header));

    let loc = e.loc();
    let file = &loc.file_label;

    // build formatted error location
    let err_prefix = if loc.span_empty() {
        format!("{file}:{} {severity_str}:", loc.start_line_number)
    } else {
        format!(
            "{}:{}:{} {}:",
            file, loc.start_line_number, loc.start_column_number, severity_str
        )
    };

    // write location and short message
    writeunwrap!(
        stream,
        "{} {}\n\n",
        label_style.apply_to(err_prefix),
        e.short_msg(),
    );

    // source lines
    print_source_line_for_error(stream, loc, e.severity());

    writeunwrap!(stream, "{}\n\n", e.help());
}

/// replace tabs with spaces for error display
fn replace_tabs(src: &str, start_column: usize) -> Cow<str> {
    debug_assert!(start_column > 0);
    if src.contains('\t') {
        let mut dst = String::new();
        let mut column = start_column;
        for g in src.graphemes(true) {
            if g == "\t" {
                let num_spaces = config::TAB_STOP - (column - 1) % config::TAB_STOP;
                assert!(num_spaces > 0);
                for _ in 0..num_spaces - 1 {
                    dst.push('-');
                }
                dst.push('>');
                column += num_spaces;
            } else {
                dst.push_str(g);
                column += 1;
            }
        }
        Cow::Owned(dst)
    } else {
        Cow::Borrowed(src)
    }
}

/// write the code with the error to the given stream
fn print_source_line_for_error<S>(stream: &mut S, source_region: &SourceRegion, severity: Severity)
where
    S: std::io::Write + ?Sized,
{
    let Some(source_line) = source_region.get_line(source_region.start_line_number) else {
        return;
    };

    let label_style = match severity {
        Severity::Error => &*ERR_LABEL_STYLE,
        Severity::Warning => &*WARNING_LABEL_STYLE,
    };
    let token_style = match severity {
        Severity::Error => &*ERR_TOKEN_STYLE,
        Severity::Warning => &*WARNING_TOKEN_STYLE,
    };
    let squiggle_style = match severity {
        Severity::Error => &*ERR_SQUIGGLE_STYLE,
        Severity::Warning => &*WARNING_SQUIGGLE_STYLE,
    };
    let num_lines = 1 + source_region.end_line_number - source_region.start_line_number;
    let mut graphemes = source_line.graphemes(true);
    let mut pre_token_bytes = 0;
    for _ in 0..(source_region.start_column_number - 1) {
        match graphemes.next() {
            Some(g) => pre_token_bytes += g.len(),
            None => break,
        }
    }
    let pre_token_str = &source_line[..pre_token_bytes];
    let tok_end = if num_lines == 1 {
        let mut token_bytes = 0;
        let mut column = source_region.start_column_number;
        while column < source_region.end_column_number {
            match graphemes.next() {
                Some(g) => {
                    token_bytes += g.len();
                    column += measure_width_in_columns(g, column);
                }
                None => break,
            }
        }
        pre_token_bytes + token_bytes
    } else {
        // TODO: we could and should print multiple lines
        source_line.len()
    };
    let token_str = &source_line[pre_token_bytes..tok_end].trim_end_matches(&['\r', '\n'][..]);
    let post_token_str = if tok_end < source_line.len() {
        source_line[tok_end..].trim_end()
    } else {
        ""
    };
    let line_prefix = label_style.apply_to(format!(" {} |", source_region.start_line_number));

    writeunwrap!(
        stream,
        "{}{}{}{}\n",
        line_prefix,
        replace_tabs(pre_token_str, 1),
        token_style.apply_to(replace_tabs(token_str, source_region.start_column_number)),
        replace_tabs(post_token_str, source_region.end_column_number)
    );

    // TODO: count graphemes and truncate source line to fit onscreen

    #[allow(
        clippy::cast_sign_loss,
        clippy::cast_possible_truncation,
        clippy::cast_precision_loss
    )]
    let num_digits: usize = 1 + (source_region.end_line_number as f64).log10() as usize;
    let margin: usize = 2 + num_digits;
    let offset: usize = source_region.start_column_number - 1;
    let err_columns = if num_lines == 1 {
        max(
            1,
            source_region.end_column_number - source_region.start_column_number,
        )
    } else {
        max(
            1,
            measure_width_in_columns(
                &source_line[pre_token_bytes..],
                source_region.start_column_number,
            ),
        )
    };

    writeunwrap!(
        stream,
        "{} {}{} \n",
        " ".repeat(margin),
        " ".repeat(offset),
        squiggle_style.apply_to("^".repeat(err_columns))
    );
}

/// print out errors
pub fn print_errors<S: std::io::Write, ErrorI: Iterator<Item = Error>>(
    errors: ErrorI,
    stream: &mut S,
) {
    let mut error_count = 0;
    let mut warning_count = 0;
    for error in errors {
        let Error { severity, .. } = error;
        match severity {
            Severity::Error => match error_count.cmp(&config::MAX_ERRORS_TO_DISPLAY) {
                Less => {
                    report_error(stream, &error);
                    error_count += 1;
                }
                Equal => {
                    report_error(
                        stream,
                        &Error {
                            short_msg: loc::err::error_limit_reached().into(),
                            help: String::new().into(),
                            source_region: error.source_region,
                            severity: Severity::Error,
                        },
                    );
                    error_count += 1;
                }
                Greater => (),
            },
            Severity::Warning => {
                if error_count == 0 {
                    match warning_count.cmp(&config::MAX_WARNINGS_TO_DISPLAY) {
                        Less => {
                            report_error(stream, &error);
                            warning_count += 1;
                        }
                        Equal => {
                            report_error(
                                stream,
                                &Error {
                                    short_msg: loc::err::warning_limit_reached().into(),
                                    help: String::new().into(),
                                    source_region: error.source_region,
                                    severity: Severity::Warning,
                                },
                            );
                            warning_count += 1;
                        }
                        Greater => (),
                    }
                }
            }
        }
    }
}

pub(crate) fn say_goodbye() {
    println!("{}", loc::goodbye());
    if console::user_attended() {
        let pause_ms = 16;
        let anim = [
            "\r)           ",
            "\r^)          ",
            "\r_^)         ",
            "\r^_^)        ",
            "\r (^_^)      ",
            "\r  (^_^)     ",
            "\r  (^_^)_|   ",
            "\r  (^_^)_|   ",
            "\r  (^_^)_|   ",
            "\r  (^_^)_|   ",
            "\r  (^_^)_/   ",
            "\r  (^_^)_/   ",
            "\r  (^_^)_/   ",
            "\r  (^_^)_/   ",
            "\r  (^_^)_|   ",
            "\r  (^_^)_|   ",
            "\r  (^_^)_|   ",
            "\r  (^_^)_|   ",
            "\r  (^_^)_/   ",
            "\r  (^_^)_/   ",
            "\r  (^_^)_/   ",
            "\r  (^_^)_/   ",
            "\r  (^_^)_|   ",
            "\r  (^_^)_|   ",
            "\r  (^_^)_|   ",
            "\r  (^_^)_|   ",
            "\r  (^_^)_/   ",
            "\r  (^_^)_/   ",
            "\r  (^_^)_/   ",
            "\r  (^_^)_/   ",
            "\r  (^_^)_|   ",
            "\r  (^_^)_|   ",
            "\r  (^_^)_|   ",
            "\r  (^_^)_|   ",
            "\r  (^_^)_/   ",
            "\r  (^_^)_/   ",
            "\r  (^_^)_/   ",
            "\r  (^_^)_/   ",
            "\r  (^_^)     ",
            "\r (^_^)      ",
            "\r(^_^)       ",
            "\r^_^)        ",
            "\r_^)         ",
            "\r^)          ",
            "\r)           ",
            "\r            ",
        ];
        for frame in anim {
            print!("{frame}");
            std::thread::sleep(std::time::Duration::from_millis(pause_ms));
            let _ = std::io::stdout().flush();
        }
    }
}
