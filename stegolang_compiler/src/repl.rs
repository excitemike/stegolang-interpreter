use crate::{
    config,
    controlflow::ControlFlow,
    env::Env,
    error::Severity,
    intern::interned::Interned,
    interpreter::EvalCtx,
    loc, parse,
    reporting::{print_errors, report_error},
    ti::{arenas::Arenas, TiError, Type},
    typedvalue::{TypedValue, TypedValueKind},
    util::output_stream::OutputStream,
};
use std::io::{self, BufRead, Write};

/// possible outcomes from processing a line of input
#[derive(PartialEq, Eq)]
enum ReplResult {
    Quit,
    Done,

    /// a statement was not completed, accept omre input before running
    #[allow(dead_code)] // TODO: implement multiline statements in repl
    Continue,
}

fn handle_io_error(error: &std::io::Error) {
    eprintln!("{}:{}", loc::io_error_label(), error);
    std::process::exit(1)
}

/// test for repl exit command
fn is_repl_quit(input: &str) -> bool {
    input.trim_end() == config::REPL_EXIT_COMMAND
}

/// state and methods for running a stegolang Read Eval Print Loop
pub struct Repl<'a> {
    env: Env<'a>,
}

impl<'a> Repl<'a> {
    /// create new Repl
    ///
    /// # Errors
    ///
    /// If something goes wrong while initializing the repl, the error will contain
    /// a message tuple explaining what it was
    pub fn new(
        arenas: &'a Arenas<'a>,
        out_stream: &'a mut dyn OutputStream,
        err_stream: &'a mut dyn OutputStream,
    ) -> Result<Repl<'a>, loc::MsgTuple> {
        Ok(Repl {
            env: Env::new(arenas, out_stream, err_stream).map_err(|e| e.msg_tuple)?,
        })
    }

    /// prompt, read input, evaluate, print, repeat
    pub fn run(&mut self) {
        println!("{}", loc::repl_header());
        while ReplResult::Quit != self.read_eval_and_print_stdio() {}
        crate::reporting::say_goodbye();
    }

    /// create and run a new repl
    ///
    /// # Errors
    ///
    /// If something goes wrong while initializing the repl, the error will contain
    /// a message tuple explaining what it was
    pub fn run_new(
        out_stream: &mut dyn OutputStream,
        err_stream: &mut dyn OutputStream,
    ) -> Result<(), loc::MsgTuple> {
        let arenas = Arenas::new();
        Repl {
            env: Env::new(&arenas, out_stream, err_stream).map_err(|e| e.msg_tuple)?,
        }
        .run();
        Ok(())
    }

    /// prompt, read input using the provided buffer, appending to what's there,
    /// then evaluate and print
    fn read_eval_and_print_stdio(&mut self) -> ReplResult {
        self.read_eval_and_print_custom(&mut io::stdin().lock(), &mut io::stdout().lock())
    }

    /// prompt, read input using the provided buffer, appending to what's there,
    /// then evaluate and print
    fn read_eval_and_print_custom<R, W>(&mut self, reader: &mut R, writer: &mut W) -> ReplResult
    where
        R: BufRead,
        W: Write,
    {
        // prompt
        print!("{}", loc::repl_prompt());
        writer.flush().unwrap_or_else(|x| handle_io_error(&x));

        // read
        let mut code = String::with_capacity(config::REPL_LINE_DEFAULT_CAPACITY);
        match reader.read_line(&mut code) {
            Err(e) => {
                handle_io_error(&e);
                ReplResult::Done
            }
            Ok(0) => ReplResult::Quit,
            Ok(_) => self.eval_and_print(code, writer),
        }
    }

    /// evaluate repl input and print result
    /// TODO: duplicates code in interpreter.rs!
    fn eval_and_print<T, W>(&mut self, code: T, writer: &mut W) -> ReplResult
    where
        T: Into<String> + Clone + AsRef<str> + std::marker::Send + 'static,
        W: Write,
    {
        // check for repl quit
        if is_repl_quit(code.as_ref()) {
            return ReplResult::Quit;
        }

        // parse
        let parse_result = parse::parse_with_env(config::REPL_FILE, code, &mut self.env);

        // check for parse errors
        let err_stream = &mut self.env.err_stream.as_write();
        let (ast, warnings) = match parse_result {
            Err(errors) => {
                print_errors(errors.into_iter(), err_stream);
                return ReplResult::Done;
            }
            Ok(x) => x,
        };

        // print warnings
        print_errors(warnings.into_iter(), err_stream);

        // convert to hlir
        let ast = match ast {
            None => return ReplResult::Done,
            Some(x) => x,
        };
        let hlir = match self.env.ti_ctx.make_hlir_from_ast(&ast) {
            Err(e) => {
                print_errors([e.into()].into_iter(), err_stream);
                return ReplResult::Done;
            }
            Ok(hlir) => hlir,
        };

        // print warnings
        let warnings = self.env.ti_ctx.warnings.take();
        print_errors(
            warnings.into_iter().map(|w| {
                let TiError {
                    msg_tuple,
                    source_region,
                } = w;
                crate::error::Error {
                    short_msg: msg_tuple.0,
                    help: msg_tuple.1,
                    source_region,
                    severity: Severity::Warning,
                }
            }),
            &mut self.env.err_stream.as_write(),
        );

        // interpret hlir
        let mut ctx = EvalCtx::new(&mut self.env);
        let result_value = match ctx.evaluate_ast(&hlir) {
            Err(ControlFlow::Error(error)) => {
                report_error(ctx.env.err_stream.as_write(), &error);
                return ReplResult::Done;
            }
            Err(ControlFlow::Break(_) | ControlFlow::Continue | ControlFlow::Return(_)) => {
                panic!("invalid control flow!")
            }
            Ok(result_value) | Err(ControlFlow::Exit(result_value)) => result_value,
        };

        // print
        let print_result = match &result_value {
            // don't bother printing if nothing was returned
            TypedValue {
                ty: Interned(Type::Nothing, _),
                ..
            } => Ok(()),
            TypedValue {
                kind: TypedValueKind::Val(obj),
                ..
            } => writeln!(writer, "{}", obj.repr()),
            TypedValue {
                kind: TypedValueKind::StegoFn { .. },
                ..
            } => writeln!(writer, "{result_value}"),
            TypedValue { ty, .. } => {
                writeln!(writer, "{ty}")
            }
        };
        print_result.unwrap_or_else(|e| {
            writeln!(
                self.env.err_stream.as_write(),
                "{}\n{}",
                loc::unable_to_write(),
                e
            )
            .unwrap();
        });
        ReplResult::Done
    }
}

// unit tests

#[test]
fn create_repl() {
    // creating a new Repl
    let arenas = Arenas::new();
    Repl::new(&arenas, &mut std::io::stdout(), &mut std::io::stderr()).unwrap();
}
#[test]
fn repl_state() {
    let arenas = Arenas::new();
    let out_stream = &mut std::io::stdout();
    let err_stream = &mut std::io::stderr();
    let mut repl = Repl::new(&arenas, out_stream, err_stream).unwrap();
    let mut output = Vec::new();
    repl.read_eval_and_print_custom(&mut &b"let x = 0"[..], &mut output);
    repl.read_eval_and_print_custom(&mut &b"x"[..], &mut output);
    let s = String::from_utf8(output).expect("Not UTF-8");
    assert_eq!(s, "0\n");
}
