use std::rc::Rc;

use super::fnfamimpls::FnFamImpls;
use crate::{
    loc::{self, MsgTuple},
    pattern::StegoPatternKind,
    sourceregion::SourceRegion,
    stegoobject::FnImpl,
    ti::TiError,
    typedvalue::TypedValue,
    StringType,
};

/// data for function dispatching
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) struct FnFamily<'ctx> {
    /// Implementations we can dispatch to
    pub(crate) implementations: FnFamImpls<'ctx>,
    /// what to call the function in error messages
    pub(crate) debug_name: StringType,
    /// how many parameters the function receives
    pub(crate) num_params: usize,
}

impl<'ctx> FnFamily<'ctx> {
    /// add an implementation to the family
    pub(crate) fn add_fn_impl(&self, fn_impl: Rc<FnImpl<'ctx>>) -> Result<(), MsgTuple> {
        if let FnFamImpls::Match { fn_impls } = &self.implementations {
            fn_impls.borrow_mut().push(fn_impl);
            Ok(())
        } else {
            Err(loc::err::unreachable_fn_impl())
        }
    }

    /// Choose the first implementation for which the patterns are all matched
    ///
    /// TODO: Maybe it should also return the results of destructuring so we
    ///       don't have to match against the patterns twice
    pub(crate) fn find_implementation(
        &self,
        arguments: &[TypedValue<'ctx>],
        source_region: &SourceRegion,
    ) -> Result<Rc<FnImpl<'ctx>>, TiError> {
        match &self.implementations {
            // easy case - only one implementation to choose from
            FnFamImpls::Simple(fn_impl) => Ok(Rc::clone(fn_impl)),
            FnFamImpls::Match { fn_impls } => {
                // locking and shadowing with the handles
                let fn_impls = fn_impls.borrow();

                // start testing the args against each implementation's patterns
                for fn_impl in fn_impls.iter() {
                    // bail if lengths don't line up
                    if fn_impl.pattern_set.borrow().len() != arguments.len() {
                        return Err(TiError::new(
                            loc::err::cant_find_implementation(),
                            source_region.clone(),
                        ));
                    }

                    // each argument has to match the corresponding pattern
                    let all_passed =
                        fn_impl
                            .pattern_set
                            .borrow()
                            .iter()
                            .zip(arguments)
                            .all(|(pattern, _)| {
                                // TODO: call into a dedicated pattern matching module
                                match pattern.kind {
                                    StegoPatternKind::Capture(_) => true,
                                    StegoPatternKind::Literal(_) => todo!(),
                                    StegoPatternKind::Or(_) => todo!(),
                                    StegoPatternKind::Named(..) => todo!(),
                                }
                            });

                    if all_passed {
                        // All patterns passed! This is the implementation we want.
                        return Ok(Rc::clone(fn_impl));
                    }
                }
                Err(TiError::new(
                    loc::err::arguments_refuted(&self.debug_name),
                    source_region.clone(),
                ))
            }
        }
    }
}
