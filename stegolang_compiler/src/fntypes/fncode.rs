use std::{cell::RefCell, rc::Rc};

use crate::{
    controlflow::ControlFlow, hlir::HlirNode, interpreter::EvalCtx, sourceregion::SourceRegion,
    ti::substitute::ApplySubst, typedvalue::TypedValue,
};

// fn ptr type for stegolang built-in functionsimplemented in Rust
pub(crate) type NativeFn<'env> = fn(
    &mut EvalCtx<'_, 'env>,
    &SourceRegion,
    &[TypedValue<'env>],
) -> Result<TypedValue<'env>, ControlFlow<'env>>;

/// the actual code for a function implementation
#[derive(Clone)]
pub(crate) enum FnCode<'ctx> {
    Statements(Rc<RefCell<HlirNode<'ctx>>>),
    Native(NativeFn<'ctx>),
}
impl<'a> ApplySubst<'a> for FnCode<'a> {
    fn apply_subst(
        &self,
        substitutions: &crate::ti::Substitutions<'a>,
        ctx: &crate::ti::Context<'a>,
    ) -> crate::ti::TiResult<Option<Self>> {
        match self {
            FnCode::Statements(stmts) => {
                let stmts = stmts.borrow();
                let Some(stmts) = stmts.apply_subst(substitutions, ctx)? else {
                    return Ok(None);
                };
                Ok(Some(FnCode::Statements(Rc::new(RefCell::new(stmts)))))
            }
            FnCode::Native(_) => Ok(None),
        }
    }
}

impl<'ctx> PartialEq for FnCode<'ctx> {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Statements(l0), Self::Statements(r0)) => l0 == r0,
            (Self::Native(l0), Self::Native(r0)) => {
                let l0 = l0 as *const NativeFn<'ctx>;
                let r0 = r0 as *const NativeFn<'ctx>;
                l0 == r0
            }
            _ => false,
        }
    }
}

impl<'ctx> Eq for FnCode<'ctx> {}

impl<'ctx> std::hash::Hash for FnCode<'ctx> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        core::mem::discriminant(self).hash(state);
        match self {
            Self::Statements(x) => {
                // seems bad to hash the whole tree. If I do just the source
                // location and the inferred type, that should be enough to
                // make collisions difficult I would think
                x.borrow().inferred_type.hash(state);
                x.borrow().source_region.hash(state);
            }
            Self::Native(fnptr) => std::ptr::hash(fnptr, state),
        }
    }
}

impl<'ctx> PartialOrd for FnCode<'ctx> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}
impl<'ctx> Ord for FnCode<'ctx> {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        match (self, other) {
            (Self::Statements(l0), Self::Statements(r0)) => l0.cmp(r0),
            (Self::Native(l0), Self::Native(r0)) => {
                let l0 = l0 as *const NativeFn<'ctx>;
                let r0 = r0 as *const NativeFn<'ctx>;
                l0.cmp(&r0)
            }
            (Self::Statements(..), Self::Native(..)) => std::cmp::Ordering::Less,
            (Self::Native(..), Self::Statements(..)) => std::cmp::Ordering::Greater,
        }
    }
}

impl<'ctx> std::fmt::Debug for FnCode<'ctx> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("<FnCode>")
    }
}
