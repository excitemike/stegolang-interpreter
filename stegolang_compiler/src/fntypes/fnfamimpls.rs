use crate::stegoobject::FnImpl;
use std::{cell::RefCell, hash::Hash, rc::Rc};

/// A function family can be created locked to a single implementation
/// Or it can have implementations with refutable patterns and choose an
/// implementation at runtime based on pattern matching
#[derive(Clone, Debug)]
pub(crate) enum FnFamImpls<'ctx> {
    /// Exactly one implementation exists. No pattern matching.
    Simple(Rc<FnImpl<'ctx>>),

    /// Do pattern matching to know which implementation will be run
    Match {
        /// for each implementation: the code to run
        fn_impls: Rc<RefCell<Vec<Rc<FnImpl<'ctx>>>>>,
    },
}

impl<'ctx> PartialEq for FnFamImpls<'ctx> {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Simple(l0), Self::Simple(r0)) => l0 == r0,
            (
                Self::Match {
                    fn_impls: l_fn_impls,
                },
                Self::Match {
                    fn_impls: r_fn_impls,
                },
            ) => {
                let l_fn_impls = l_fn_impls.borrow();
                let r_fn_impls = r_fn_impls.borrow();
                *l_fn_impls == *r_fn_impls
            }
            _ => false,
        }
    }
}

impl<'ctx> Eq for FnFamImpls<'ctx> {}

impl<'ctx> PartialOrd for FnFamImpls<'ctx> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl<'ctx> Ord for FnFamImpls<'ctx> {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        match (self, other) {
            (Self::Simple(l0), Self::Simple(r0)) => l0.cmp(r0),
            (
                Self::Match {
                    fn_impls: l_fn_impls,
                },
                Self::Match {
                    fn_impls: r_fn_impls,
                },
            ) => {
                let l_fn_impls = l_fn_impls.borrow();
                let r_fn_impls = r_fn_impls.borrow();
                (l_fn_impls).cmp(&*r_fn_impls)
            }
            (Self::Simple(_), _) => std::cmp::Ordering::Less,
            (Self::Match { .. }, Self::Simple(_)) => std::cmp::Ordering::Greater,
        }
    }
}

impl<'ctx> Hash for FnFamImpls<'ctx> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        core::mem::discriminant(self).hash(state);
        match self {
            FnFamImpls::Simple(x) => x.hash(state),
            FnFamImpls::Match { fn_impls } => fn_impls.borrow().hash(state),
        }
    }
}
