use crate::{
    error::EvalResult,
    hlir::HlirNode,
    intern::interned::Interned,
    interpreter::EvalCtx,
    stegoobject::StegoObject,
    ti::Type,
    typedvalue::{TypedValue, TypedValueKind},
};

impl<'envborrow, 'ctx> EvalCtx<'envborrow, 'ctx> {
    pub(crate) fn eval_unary_minus(&mut self, ast: &HlirNode<'ctx>) -> EvalResult<'ctx> {
        match self.evaluate_ast(ast)? {
            TypedValue {
                ty: Interned(Type::BigInt, _),
                kind,
            } => match kind {
                TypedValueKind::Val(obj) => match &*obj {
                    StegoObject::Integer(bigint) => Ok(TypedValue::val(
                        self.env.ti_ctx.mk_bigint(),
                        StegoObject::Integer(-bigint).into(),
                    )),
                    _ => todo!("handle error"),
                },
                _ => todo!("handle error"),
            },
            TypedValue {
                ty: Interned(Type::Int64, _),
                kind,
            } => match kind {
                TypedValueKind::Val(obj) => match &*obj {
                    StegoObject::Int64(n) => Ok(TypedValue::val(
                        self.env.ti_ctx.mk_int64(),
                        StegoObject::Int64(-n).into(),
                    )),
                    _ => todo!("handle error"),
                },
                _ => todo!("handle error"),
            },
            TypedValue {
                ty: Interned(Type::Float64, _),
                kind,
            } => match kind {
                TypedValueKind::Val(obj) => match &*obj {
                    StegoObject::Float64(n) => Ok(TypedValue::val(
                        self.env.ti_ctx.mk_float64(),
                        StegoObject::Float64(-n).into(),
                    )),
                    _ => todo!("handle error"),
                },
                _ => todo!("handle error"),
            },
            _ => todo!("should be going to a trait for this"),
        }
    }
}
