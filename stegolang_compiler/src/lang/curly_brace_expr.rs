use std::rc::Rc;

use rustc_hash::{FxHashMap, FxHashSet};

use crate::{
    ast::DictExprElem::{self, KeyValue, KeywordSpread},
    ast::{
        astnode::{AstNode, AstNodeKind},
        SetExprElem,
    },
    config::{
        CLOSE_CURLY, KEY_VALUE_SEPARATOR, KWSPREAD_OPERATOR, OPEN_CURLY, SEQUENCE_DELIM,
        SPREAD_OPERATOR,
    },
    controlflow::err,
    error::{Error, EvalResult},
    hlir::HlirNode,
    interpreter::EvalCtx,
    loc,
    parse::Parser,
    skip_inside_expr, skip_read_and_require, skip_required,
    sourceregion::SourceRegion,
    stegoobject::StegoObject,
    typedvalue::{TypedValue, TypedValueKind},
    util::RcVal,
};

impl<'envborrow, 'ctx> Parser<'envborrow, 'ctx> {
    /// helper for `read_curly_brace_expr` - reads the rest of a dictionary comprehension
    /// after you maybe already read the first item
    /// TODO: for/let/if clauses of a comprehension
    fn continue_dictcomp(
        &mut self,
        already_read_key: Option<AstNode>,
        start_line_number: usize,
        start_column_number: usize,
    ) -> Result<Option<AstNode>, Vec<Error>> {
        let mut elems: Vec<DictExprElem<AstNode>> = Vec::new();
        let mut is_first = true;

        if let Some(key_expr) = already_read_key {
            //key and separator were already read

            // value
            let value_expr = skip_read_and_require!(self, read_named_expr, {
                return self.expected_something(loc::err::expected_value_expr_for_dict);
            });
            elems.push(KeyValue(key_expr, value_expr));
            is_first = false;
        }

        loop {
            skip_inside_expr!(self, {
                let (short_msg, help) = loc::err::unterminated_curly();
                return self.error(short_msg, help, start_line_number, start_column_number);
            });
            let had_delim = is_first || self.skip_exact(SEQUENCE_DELIM);

            if self.bracketted_expr_end(
                CLOSE_CURLY,
                loc::err::unterminated_curly,
                start_line_number,
                start_column_number,
            )? {
                let source_region = self.make_source_region(start_line_number, start_column_number);
                let kind = AstNodeKind::DictExpr(elems);
                return Ok(Some(AstNode::new(source_region, kind)));
            };

            if had_delim {
                // keyword spread
                if self.skip_exact(KWSPREAD_OPERATOR) {
                    let x = skip_read_and_require!(self, read_named_expr, {
                        return self.expected_something(loc::err::expected_kwspread_expr);
                    });
                    elems.push(KeywordSpread(x));
                } else {
                    // key
                    let key_expr = skip_read_and_require!(self, read_named_expr, {
                        return self.expected_something(loc::err::expected_key_expr_for_dict);
                    });

                    //separator
                    skip_required!(self, skip_exact, KEY_VALUE_SEPARATOR, {
                        return self.expected_something(loc::err::expected_keyvalue_separator);
                    });

                    // value
                    let value_expr = skip_read_and_require!(self, read_named_expr, {
                        return self.expected_something(loc::err::expected_value_expr_for_dict);
                    });
                    elems.push(KeyValue(key_expr, value_expr));
                }
                is_first = false;
            } else {
                let (short_msg, help) = loc::err::unterminated_curly();
                return self.error(short_msg, help, start_line_number, start_column_number);
            }
        }
    }

    /// helper for `read_curly_brace_expr` - reads the rest of a dictionary comprehension
    /// after you maybe already read the first item
    fn continue_setcomp(
        &mut self,
        already_read: Option<AstNode>,
        start_line_number: usize,
        start_column_number: usize,
    ) -> Result<Option<AstNode>, Vec<Error>> {
        let mut elems: Vec<SetExprElem<AstNode>> = Vec::new();
        let mut is_first = true;

        // TODO: for/let/if clauses of a comprehension

        if let Some(expr) = already_read {
            elems.push(SetExprElem::Value(expr));
            is_first = false;
        }

        loop {
            skip_inside_expr!(self, {
                let (short_msg, help) = loc::err::unterminated_curly();
                return self.error(short_msg, help, start_line_number, start_column_number);
            });
            let had_delim = is_first || self.skip_exact(SEQUENCE_DELIM);

            if self.bracketted_expr_end(
                CLOSE_CURLY,
                loc::err::unterminated_curly,
                start_line_number,
                start_column_number,
            )? {
                let source_region = self.make_source_region(start_line_number, start_column_number);
                let kind = AstNodeKind::SetExpr(elems);
                return Ok(Some(AstNode::new(source_region, kind)));
            };

            if had_delim {
                // spread
                if self.skip_exact(SPREAD_OPERATOR) {
                    let x = skip_read_and_require!(self, read_named_expr, {
                        return self.expected_something(loc::err::expected_spread_expr);
                    });
                    elems.push(SetExprElem::Spread(x));
                } else {
                    let expr = skip_read_and_require!(self, read_named_expr, {
                        return self.expected_something(loc::err::expected_expr_for_set);
                    });
                    elems.push(SetExprElem::Value(expr));
                }
                is_first = false;
            } else {
                let (short_msg, help) = loc::err::unterminated_curly();
                return self.error(short_msg, help, start_line_number, start_column_number);
            }
        }
    }

    // TODO: I don't actually like colons OR curly braces :(
    // could instead be like
    //
    // ```stegolang
    // ["some key" = "some value",]` // kinda like php
    // ```
    //
    // and/or
    //
    // ```stegolang
    // let some_dict = dict
    //     "some key" = "some value"
    // let some_set = set
    //     "some value"
    // ```
    //
    // and/or
    //
    // ```stegolang
    // let some_dict = dict "some key" = "some value" ; "some key" = "some value"
    // let some_set = set "some_value" ; "some other value"
    // ```
    //

    ///> *curly_brace_expr* ← *empty_dict* &#124; *empty_set* &#124; *dict_literal* &#124; *set_literal* &#124; *identifier*
    ///>
    ///> *empty_dict* ← `{` `}`
    ///>
    ///> *empty_set* ← `{` `,` `}`
    ///>
    ///> *dict_literal* ← `{` *kwspread_or_keyvalue* ( `,` *kwspread_or_keyvalue* )<sup>+</sup> `,`<sup>?</sup> `}`
    ///>
    ///> *kwspread_or_keyvalue* ← *kwspread* &#124; *keyvalue*
    ///>
    ///> *kwspread* ← `**` *named_expr*
    ///>
    ///> *keyvalue* ← *named_expr* `:` *named_expr*
    ///>
    ///> *set_literal* ← `{` *spread_expr* ( `,` *spread_expr* )<sup>*</sup> `,`<sup>?</sup> `}`
    ///>
    ///
    pub(crate) fn read_curly_brace_expr(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        let start_line_number = self.get_line_number();
        let start_column_number = self.get_column_number();

        // has to begin with open brace
        if !self.skip_exact(OPEN_CURLY) {
            return self.read_identifier();
        }

        // if it ends now, it is an empty dictionary
        if self.bracketted_expr_end(
            CLOSE_CURLY,
            loc::err::unterminated_curly,
            start_line_number,
            start_column_number,
        )? {
            let source_region = self.make_source_region(start_line_number, start_column_number);
            let kind = AstNodeKind::DictExpr(Vec::new());
            return Ok(Some(AstNode {
                source_region,
                kind,
            }));
        };

        // now if it has a comma, must be the empty set
        skip_inside_expr!(self, {
            return self.expected_something(loc::err::expected_expression_for_dict_or_set_literal);
        });
        if self.skip_exact(SEQUENCE_DELIM) {
            skip_required!(self, skip_exact, CLOSE_CURLY, {
                return self.expected_something(loc::err::expected_end_of_empty_set_literal);
            });
            let source_region = self.make_source_region(start_line_number, start_column_number);
            let kind = AstNodeKind::SetExpr(Vec::new());
            return Ok(Some(AstNode {
                source_region,
                kind,
            }));
        }

        skip_inside_expr!(self, {
            return self.expected_something(loc::err::expected_expression_for_dict_or_set_literal);
        });
        if let Some(s) = self.get_code_by_byte(self.byte..) {
            // keyword spread operator means it has to be a dict
            if s.starts_with(KWSPREAD_OPERATOR) {
                return self.continue_dictcomp(None, start_line_number, start_column_number);
            }
            // spread operator means it has to be a set
            if s.starts_with(SPREAD_OPERATOR) {
                return self.continue_setcomp(None, start_line_number, start_column_number);
            }
        }

        // a non-empty dict or set literal will have at least one thing for sure now
        let Some(expr) = self.read_named_expr()? else {
            return self.expected_something(loc::err::expected_expression_for_dict_or_set_literal);
        };

        // key value pair means it's a dict
        skip_inside_expr!(self, {
            return self.expected_something(loc::err::expected_expression_for_dict_or_set_literal);
        });
        if self.skip_exact(KEY_VALUE_SEPARATOR) {
            return self.continue_dictcomp(Some(expr), start_line_number, start_column_number);
        }

        // only alternative left is a set
        self.continue_setcomp(Some(expr), start_line_number, start_column_number)
    }
}

impl<'envborrow, 'ctx> EvalCtx<'envborrow, 'ctx> {
    ///
    pub(crate) fn eval_dict_literal(
        &mut self,
        elems: &[DictExprElem<HlirNode<'ctx>>],
        source_region: &SourceRegion,
    ) -> EvalResult<'ctx> {
        let mut hashmap: FxHashMap<RcVal<'ctx>, RcVal<'ctx>> = FxHashMap::default();
        let mut key_type = None;
        let mut value_type = None;
        for elem in elems {
            match elem {
                DictExprElem::KeyValue(k, v) => {
                    let key = self.evaluate_ast(k)?;
                    match key_type {
                        None => {
                            key_type = Some(key.ty);
                        }
                        Some(t) if t == key.ty => (),
                        Some(expected_ty) => {
                            return err(
                                loc::err::type_mismatch(
                                    &expected_ty.to_string(),
                                    &key.ty.to_string(),
                                ),
                                k.source_region.clone(),
                            );
                        }
                    }

                    let value = self.evaluate_ast(v)?;
                    match value_type {
                        None => {
                            value_type = Some(value.ty);
                        }
                        Some(t) if t == value.ty => (),
                        Some(expected_ty) => {
                            return err(
                                loc::err::type_mismatch(
                                    &expected_ty.to_string(),
                                    &value.ty.to_string(),
                                ),
                                v.source_region.clone(),
                            );
                        }
                    }

                    let (is_hashable, type_name) = { (key.is_hashable(), key.ty.to_string()) };
                    if is_hashable {
                        hashmap.insert(key.expect_stegoobject(), value.expect_stegoobject());
                    } else {
                        return err(
                            loc::err::not_hashable(type_name),
                            k.get_source_region().clone(),
                        );
                    }
                }
                DictExprElem::KeywordSpread(ast_node) => {
                    let typed_value = self.evaluate_ast(ast_node)?;
                    let ty = typed_value.ty;
                    let object = &*typed_value.expect_stegoobject();
                    match object {
                        StegoObject::Dict(dict) => {
                            for (k, v) in dict {
                                let is_hashable = k.is_hashable();
                                if is_hashable {
                                    hashmap.insert(Rc::clone(k), Rc::clone(v));
                                } else {
                                    return err(
                                        loc::err::not_hashable(ty.to_string()),
                                        ast_node.get_source_region().clone(),
                                    );
                                }
                            }
                        }
                        _ => {
                            return err(
                                loc::err::expected_dictionary_for_kwspread(ty.to_string()),
                                ast_node.get_source_region().clone(),
                            )
                        }
                    }
                }
            }
        }
        let key_type = match key_type {
            Some(x) => x,
            None => self
                .env
                .ti_ctx
                .mk_unification_var(source_region, "key type for empty dict")?,
        };
        let value_type = match value_type {
            Some(x) => x,
            None => self
                .env
                .ti_ctx
                .mk_unification_var(source_region, "value type for empty dict")?,
        };
        Ok(TypedValue::val(
            self.env.ti_ctx.mk_dict(key_type, value_type),
            StegoObject::Dict(hashmap).into(),
        ))
    }

    ///
    pub(crate) fn eval_set_literal(
        &mut self,
        elems: &[SetExprElem<HlirNode<'ctx>>],
        source_region: &SourceRegion,
    ) -> EvalResult<'ctx> {
        let mut hashset: FxHashSet<TypedValue<'_>> = FxHashSet::default();
        let mut set_member_ty = None;
        for elem in elems {
            match elem {
                SetExprElem::Spread(elem) => {
                    let eval_result = self.evaluate_ast(elem)?;
                    let ty = eval_result.ty;
                    if eval_result.is_iterable() {
                        for item in eval_result.iter() {
                            if item.is_hashable() {
                                hashset.insert(item);
                            } else {
                                return err(
                                    loc::err::not_hashable(ty.to_string()),
                                    elem.get_source_region().clone(),
                                );
                            }
                        }
                    } else {
                        unreachable!()
                    }
                }
                SetExprElem::Value(elem) => {
                    let result = self.evaluate_ast(elem)?;
                    match set_member_ty {
                        None => set_member_ty = Some(result.ty),
                        Some(t) if t == result.ty => (),
                        Some(expected_type) => {
                            return err(
                                loc::err::type_mismatch(
                                    &expected_type.to_string(),
                                    &result.ty.to_string(),
                                ),
                                elem.get_source_region().clone(),
                            );
                        }
                    }
                    if result.is_hashable() {
                        hashset.insert(result);
                    } else {
                        return err(
                            loc::err::not_hashable(result.ty.to_string()),
                            elem.get_source_region().clone(),
                        );
                    }
                }
            }
        }
        let set_member_ty = match set_member_ty {
            Some(x) => x,
            None => self
                .env
                .ti_ctx
                .mk_unification_var(source_region, "element type for empty set")?,
        };
        Ok(TypedValue::new(
            self.env.ti_ctx.mk_set(set_member_ty),
            TypedValueKind::Set(hashset),
        ))
    }
}
