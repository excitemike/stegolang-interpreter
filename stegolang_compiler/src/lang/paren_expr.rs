use crate::{
    ast::astnode::{AstNode, AstNodeKind},
    config::{OPEN_PAREN, SEQUENCE_DELIM},
    error::Error,
    loc,
    parse::Parser,
};

impl<'envborrow, 'ctx> Parser<'envborrow, 'ctx> {
    ///> *paren_expr* ← *zero_tuple_expr* &#124; *group_expr* &#124; *one_tuple_expr* &#124; *n_tuple_expr*
    ///>
    ///> *zero_tuple_expr* ← `()`
    ///>
    ///> *group_expr* ← `(` *spread_expr* `)`
    ///>
    ///> *one_tuple_expr* ← `(` *spread_expr* `,)`
    ///>
    ///> *n_tuple_expr* ← `(` *spread_expr* ( `,` *spread_expr* )<sup>+</sup> ','<sup>?</sup> `)`
    ///
    #[allow(unused_braces)]
    pub(crate) fn read_paren_expr(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        let start_line_number = self.get_line_number();
        let start_column_number = self.get_column_number();

        // has to begin with an open paren
        if !self.skip_exact(OPEN_PAREN) {
            return self.read_square_bracket_expr();
        }

        // if it ends now it is a 0-tuple
        if self.bracketted_expr_end(
            crate::config::CLOSE_PAREN,
            loc::err::unterminated_paren,
            start_line_number,
            start_column_number,
        )? {
            let source_region = self.make_source_region(start_line_number, start_column_number);
            let kind = AstNodeKind::UnitType;
            return Ok(Some(AstNode::new(source_region, kind)));
        }

        // not a zero-tuple so there must be at least one expression
        let Some(first_expr) = self.read_named_expr()? else {
            return self.expected_something(loc::err::expected_expression_for_paren);
        };

        // if it ends now, it's a grouped expression
        if self.bracketted_expr_end(
            crate::config::CLOSE_PAREN,
            loc::err::unterminated_paren,
            start_line_number,
            start_column_number,
        )? {
            return Ok(Some(first_expr));
        }

        // if we got this far, it must be a tuple of some kind, so it needs a comma now
        if !self.skip_exact(SEQUENCE_DELIM) {
            return self.expected_something(loc::err::expected_comma_for_tuple);
        }

        // if it ends now, it's a 1-tuple
        if self.bracketted_expr_end(
            crate::config::CLOSE_PAREN,
            loc::err::unterminated_paren,
            start_line_number,
            start_column_number,
        )? {
            let source_region = self.make_source_region(start_line_number, start_column_number);
            let kind = AstNodeKind::Tuple(vec![first_expr]);
            return Ok(Some(AstNode::new(source_region, kind)));
        };

        // n-tuple
        let Some(second_expr) = self.read_named_expr()? else {
            return self.expected_something(loc::err::expected_anything_for_tuple);
        };

        let mut v = vec![first_expr, second_expr];

        loop {
            if self.bracketted_expr_end(
                crate::config::CLOSE_PAREN,
                loc::err::unterminated_paren,
                start_line_number,
                start_column_number,
            )? {
                let source_region = self.make_source_region(start_line_number, start_column_number);
                let kind = AstNodeKind::Tuple(v);
                return Ok(Some(AstNode::new(source_region, kind)));
            };
            if self.skip_exact(SEQUENCE_DELIM) {
                if self.bracketted_expr_end(
                    crate::config::CLOSE_PAREN,
                    loc::err::unterminated_paren,
                    start_line_number,
                    start_column_number,
                )? {
                    let source_region =
                        self.make_source_region(start_line_number, start_column_number);
                    let kind = AstNodeKind::Tuple(v);
                    return Ok(Some(AstNode::new(source_region, kind)));
                };
                if let Some(x) = self.read_named_expr()? {
                    v.push(x);
                } else {
                    return self.expected_something(loc::err::expected_anything_for_tuple);
                }
            } else {
                return self.expected_something(loc::err::expected_anything_for_tuple);
            }
        }
    }
}
