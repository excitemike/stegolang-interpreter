//! parse statements
use crate::ast::astnode::AstNodeKind;
use crate::{ast::astnode::AstNode, config::STATEMENT_SEPARATOR, error::Error, loc};
use crate::{lang::AdvanceResult, parse::Parser};

impl<'envborrow, 'ctx> Parser<'envborrow, 'ctx> {
    ///> *statement_block* ← *statement* ( ( `;` &#124; NEWSTATEMENT ) *statement* )<sup>\*</sup>
    pub(crate) fn read_statement_block(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        let mut statements = Vec::new();
        let mut errors = Vec::new();
        let region_builder = self.begin_source_region();

        loop {
            // next statement
            match self.read_statement() {
                // on error, record errors and try to skip
                Err(v) => {
                    errors.extend(v);
                    self.skip_bad_statement()?;
                }
                // if nothing was found, I guess we're done
                Ok(None) => break,
                // got one - accumulate
                Ok(Some(stmt)) => {
                    statements.push(stmt);
                }
            }

            // move to next
            match self.skip_comments_and_whitespace() {
                // handle errors
                Err(v) => {
                    errors.extend(v);
                    self.skip_bad_statement()?;
                }
                // interpret indentation effects
                Ok(x) => {
                    match x {
                        // part of the same statement -- needs a semicolon in that case
                        AdvanceResult::Continue => {
                            let region_builder = self.begin_source_region();
                            if self.read_statement_separator() {
                                match self.skip_comments_and_whitespace()? {
                                    AdvanceResult::Continue => {}
                                    AdvanceResult::NewStatement => {
                                        // warn about semicolon
                                        let (short_msg, help) =
                                            loc::err::unneccessary_statement_separator();
                                        self.record_warning(short_msg, help, region_builder(self));
                                    }
                                    AdvanceResult::EndBlock | AdvanceResult::EndProgram => {
                                        // warn AND break
                                        let (short_msg, help) =
                                            loc::err::unneccessary_statement_separator();
                                        self.record_warning(short_msg, help, region_builder(self));
                                        break;
                                    }
                                }
                            } else if errors.is_empty()
                                && !matches!(
                                    self.peek_word(),
                                    crate::config::ELSE
                                        | crate::config::CLOSE_CURLY
                                        | crate::config::CLOSE_PAREN
                                        | crate::config::CLOSE_SQUARE
                                )
                            {
                                errors.extend(
                                    self.expected_something::<(), _, _>(
                                        loc::err::expected_end_of_statement,
                                    )
                                    .unwrap_err(),
                                );
                                self.skip_bad_statement()?;
                            }
                        }
                        // moving on to next statement -- loop again
                        AdvanceResult::NewStatement => {}
                        // end of block/program
                        AdvanceResult::EndBlock | AdvanceResult::EndProgram => break,
                    }
                }
            }
        }

        if errors.is_empty() {
            if statements.is_empty() {
                Ok(None)
            } else {
                let source_region = region_builder(self);
                let kind = AstNodeKind::Statements { statements };
                Ok(Some(AstNode {
                    source_region,
                    kind,
                }))
            }
        } else {
            Err(errors)
        }
    }

    /// read the statement separator
    fn read_statement_separator(&mut self) -> bool {
        self.skip_exact(STATEMENT_SEPARATOR)
    }

    ///> *statement* ← *fn_stmt*
    ///>     / *async_stmt*
    ///>     / *loop_stmt*
    ///>     / *for_stmt*
    ///>     / *while_stmt*
    ///>     / *block*
    ///>     / *break_stmt*
    ///>     / *continue_stmt*
    ///>     / *return_stmt*
    ///>     / *assert_stmt*
    ///>     / *import_stmt*
    ///>     / *let_stmt*
    ///>     / *assign_stmt*
    ///
    pub(crate) fn read_statement(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        // TODO: cover all the choices.
        if let x @ Some(..) = self.read_fn_stmt()? {
            return Ok(x);
        }
        if let x @ Some(..) = self.read_let_stmt()? {
            return Ok(x);
        }
        if let x @ Some(..) = self.read_return_stmt()? {
            return Ok(x);
        }

        self.read_assign_stmt()
    }
}
