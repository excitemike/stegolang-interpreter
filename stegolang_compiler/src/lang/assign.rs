use crate::{
    ast::astnode::{AstNode, AstNodeKind},
    config::ASSIGNMENT_OPERATOR,
    controlflow::err,
    error::Error,
    error::EvalResult,
    hlir::{hlirnodekind::HlirNodeKind, HlirNode},
    interpreter::EvalCtx,
    loc,
    parse::IntoParseResult,
    parse::Parser,
    skip_inside_expr,
    sourceregion::SourceRegion,
    typedvalue::TypedValue,
};

impl<'envborrow, 'ctx> Parser<'envborrow, 'ctx> {
    ///> *assign_stmt* ← ( *named_pattern_expr* `=` )<sup>?</sup> *named_expr*
    ///
    /// assign right-hand side to the target specified by the left-hand-side
    ///
    pub(crate) fn read_assign_stmt(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        let region_builder = self.begin_source_region();

        // lhs
        // TODO: this should be a pattern
        let Some(lhs) = self.read_named_expr()? else {
            return Ok(None);
        };

        skip_inside_expr!(self, { return Ok(Some(lhs)) });

        // equals
        if !self.skip_exact(ASSIGNMENT_OPERATOR) {
            return Ok(Some(lhs));
        }

        // check that it is a valid assignment target
        let can_assign = lhs.can_be_assignment_target();
        if !can_assign {
            let source_region = region_builder(self);
            return Err(loc::err::bad_assign_target()).into_parse_result(&source_region);
        }

        // TODO: require that the pattern be irrefutable

        skip_inside_expr!(self, {
            return self.expected_something(loc::err::expected_expression_for_assignment);
        });

        // finally the value
        let Some(rvalue) = self.read_named_expr()? else {
            return self.expected_something(loc::err::expected_expression_for_assignment);
        };

        let source_region = region_builder(self);

        Ok(Some(AstNode {
            source_region,
            kind: AstNodeKind::AssignStmt {
                lhs: Box::new(lhs),
                rhs: Box::new(rvalue),
            },
        }))
    }
}

impl<'envborrow, 'ctx> EvalCtx<'envborrow, 'ctx> {
    /// execute assignment
    pub fn do_assign_stmt(
        &mut self,
        source_region: &SourceRegion,
        lhs: &HlirNode<'ctx>,
        rhs: &HlirNode<'ctx>,
    ) -> EvalResult<'ctx> {
        let value = self.evaluate_ast(rhs)?;

        match &lhs.kind {
            // TODO: this should be pattern, not identifier
            HlirNodeKind::Identifier {
                name,
                substitutions: _,
            } => {
                // TODO: verify that the type is compatible
                let mut target = self.get_mut(name);
                match &mut target {
                    None => err(
                        loc::err::no_implicit_decl(name),
                        lhs.get_source_region().clone(),
                    ),
                    Some(target_guard) => {
                        if target_guard.ty == value.ty {
                            target_guard.kind = value.kind.clone();
                            drop(target);
                            Ok(TypedValue::nothing(&self.env.ti_ctx))
                        } else {
                            err(
                                loc::err::type_mismatch_assign(
                                    &target_guard.ty.to_string(),
                                    &value.ty.to_string(),
                                ),
                                source_region.clone(),
                            )
                        }
                    }
                }
            }
            _ => err(
                loc::err::internal_error_can_not_assign(&format!("{lhs:?}")),
                source_region.clone(),
            ),
        }
    }
}
