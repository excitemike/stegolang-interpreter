use crate::{
    ast::astnode::{AstNode, AstNodeKind::TypeOf},
    config::TYPEOF,
    error::{Error, EvalResult},
    hlir::HlirNode,
    interpreter::EvalCtx,
    loc,
    parse::Parser,
    skip_inside_expr,
    stegoobject::StegoObject,
    typedvalue::TypedValue,
};

impl<'envborrow, 'ctx> Parser<'envborrow, 'ctx> {
    /// Read an expression that uses the typeof operator or something of higher precedence
    ///
    ///> *typeof_expr* ← `typeof`<sup>?</sup> *control_expr*
    ///
    pub(crate) fn read_typeof_expr(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        // begins with `typeof` or it's just control_expr
        if !self.skip_exact_word(TYPEOF) {
            return self.read_control_expr();
        }

        skip_inside_expr!(self, {
            return self.expected_something(loc::err::expected_expr_for_typeof);
        });

        // operand
        match self.read_control_expr()? {
            Some(node) => Ok(Some(AstNode::new(
                node.source_region.clone(),
                TypeOf(Box::new(node)),
            ))),
            None => self.expected_something(loc::err::expected_expr_for_typeof),
        }
    }
}

impl<'envborrow, 'ctx> EvalCtx<'envborrow, 'ctx> {
    #[allow(clippy::unnecessary_wraps)] // keep consistent interface with other eval functions
    pub(crate) fn eval_typeof(&mut self, node: &HlirNode<'ctx>) -> EvalResult<'ctx> {
        // TODO: This can still have unification variables because we are reusing
        // the same nodes for every instantiation of a function scheme. To get
        // the final type here, we need to know the type substitutions that are relevant
        // at this exact time, or else have a substituted copy of the subtree for
        // each instantiation
        Ok(TypedValue::val(
            self.env.ti_ctx.mk_string(),
            StegoObject::StegoImmString(node.inferred_type.to_string().into()).into(),
        ))
    }
}
