use crate::{
    ast::astnode::{AstNode, AstNodeKind::LogicalNot},
    config::LOGICAL_NOT,
    error::Error,
    loc,
    parse::Parser,
    skip_inside_expr,
};

impl<'envborrow, 'ctx> Parser<'envborrow, 'ctx> {
    // TODO implement
    pub(crate) fn read_logical_or_expr(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        self.read_logical_and_expr()
    }
    // TODO implement
    pub(crate) fn read_logical_and_expr(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        self.read_logical_not_expr()
    }

    ///> *logical_not_expr* ← `!` *logical_not_expr* &$124; *comparison_expr*
    ///
    /// TODO: this doesn't seem like the right precedence. consider `! 1 < 0`
    pub(crate) fn read_logical_not_expr(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        let end_source_region = self.begin_source_region();

        if !self.skip_exact(LOGICAL_NOT) {
            return self.read_comparison_expr();
        }

        skip_inside_expr!(self, {
            return self.expected_something(loc::err::expected_unary_subexpr);
        });
        match self.read_logical_not_expr()? {
            Some(sub_expr) => {
                let source_region = end_source_region(self);
                let kind = LogicalNot(Box::new(sub_expr));
                Ok(Some(AstNode {
                    source_region,
                    kind,
                }))
            }
            None => self.expected_something(loc::err::expected_unary_subexpr),
        }
    }
}
