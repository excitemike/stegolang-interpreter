use crate::{
    ast::astnode::{AstNode, AstNodeKind::NamedExpr},
    config::NAMED_EXPR_OP,
    error::Error,
    loc,
    parse::Parser,
    pattern::StegoPatternKind,
    skip_read_and_require, skip_required,
    typedvalue::TypedValue,
    StringType,
};

use super::AdvanceResult::Continue;

impl<'envborrow, 'ctx> Parser<'envborrow, 'ctx> {
    /// peek ahead to see if we have an identifier followed by `@`
    fn looks_like_named_expr(&mut self) -> bool {
        let backup = self.make_backup_point();

        let result = match self.read_name() {
            None => false,
            Some(_) => match self.skip_whitespace() {
                Continue => self.skip_exact(NAMED_EXPR_OP),
                _ => false,
            },
        };

        self.backup(backup);
        result
    }

    /// *named_expr* ← ( *capture_pattern* `@` )<sup>?</sup> *typeof_expr*
    ///
    pub(crate) fn read_named_expr(
        &mut self,
    ) -> Result<Option<crate::ast::astnode::AstNode>, Vec<Error>> {
        let end_source_region = self.begin_source_region();

        // peek ahead to see if it looks like "name @ ..."
        if !self.looks_like_named_expr() {
            return self.read_typeof_expr();
        }

        let Some(first_pat) = self.read_capture_pattern()? else {
            // this should be unreachable, because we already peeked ahead, but we have an easy fallback
            return self.read_typeof_expr();
        };

        let StegoPatternKind::Capture(name) = &first_pat.kind else {
            todo!()
        };

        // @
        skip_required!(self, skip_exact, NAMED_EXPR_OP, {
            // should be unreachable because of the 'looks_like_named_expr' check earlier
            return self.expected_something(loc::err::unexpected_at);
        });

        // rhs
        let value_node = skip_read_and_require!(self, read_typeof_expr, {
            return self.expected_something(loc::err::named_expr_value_expected);
        });

        // and we place it into the environment under that name
        self.env.scope.insert(
            StringType::from(name),
            TypedValue::nothing(&self.env.ti_ctx),
        );

        // TODO: use value side to do type inference with the pattern side

        let source_region = end_source_region(self);
        let kind = NamedExpr {
            name: StringType::clone(name),
            value: Box::new(value_node),
        };
        Ok(Some(AstNode {
            source_region,
            kind,
        }))
    }
}
