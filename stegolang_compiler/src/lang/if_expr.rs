//! code for parsing, interpreting, and compiling if statements
use super::AdvanceResult;
use crate::{
    ast::astnode::{AstNode, AstNodeKind::IfExpr},
    ast::IfArm,
    config::{ELSE, IF, THEN},
    error::{Error, EvalResult},
    hlir::HlirNode,
    interpreter::EvalCtx,
    loc,
    parse::Parser,
    skip_inside_expr,
    sourceregion::SourceRegion,
    typedvalue::TypedValue,
};

impl<'envborrow, 'ctx> Parser<'envborrow, 'ctx> {
    /// helper for `read_consequent_block`
    fn read_consequent_block_post_indent<MakeSourceRegionFn>(
        &mut self,
        pushed: bool,
        _make_source_region: MakeSourceRegionFn,
    ) -> Result<AstNode, Vec<Error>>
    where
        MakeSourceRegionFn: FnOnce(&Parser) -> SourceRegion,
    {
        if !pushed {
            return self.expected_something(loc::err::expected_consequent);
        }
        // the consequent statements
        let Some(consequent) = self.read_statement_block()? else {
            return self.expected_something(loc::err::expected_consequent);
        };
        Ok(consequent)
    }

    /// read a block of statements for an if or an else
    fn read_consequent_block(&mut self) -> Result<AstNode, Vec<Error>> {
        // skip whitespace to push indent for consequent block
        self.in_new_scope(|parser| {
            parser.try_push_indentation_level(
                Parser::read_consequent_block_post_indent,
                parser.begin_source_region(),
            )
        })?
    }

    ///> *if_stmt* ← `if` *named_expr* `then` *statements* ( `else` *statements* )<sup>?</sup>
    ///
    /// conditionally run code.
    /// note that a final catch-all `else` block is required when used as an expression
    pub(crate) fn read_if_expr(
        &mut self,
    ) -> Result<Option<crate::ast::astnode::AstNode>, Vec<Error>> {
        let region_builder = self.begin_source_region();

        // has to begin with "if"
        if !self.skip_exact_word(IF) {
            return Ok(None);
        }

        // then you need a condition
        skip_inside_expr!(self, {
            return self.expected_something(loc::err::expected_condition_expr);
        });
        let Some(cond) = self.read_named_expr()? else {
            return self.expected_something(loc::err::expected_condition_expr);
        };

        // `then` before the statements
        skip_inside_expr!(self, {
            return self.expected_something(loc::err::expected_then);
        });
        if !self.skip_exact_word(THEN) {
            return self.expected_something(loc::err::expected_then);
        }

        // the consequent statements
        let consequent = self.read_consequent_block()?;

        let mut arms = vec![IfArm { cond, consequent }];

        // optional else/elseif blocks
        loop {
            match self.skip_comments_and_whitespace()? {
                AdvanceResult::Continue | AdvanceResult::NewStatement => {}
                _ => break,
            }
            if !self.skip_exact_word(ELSE) {
                break;
            }

            // chaining in `else if`s?
            skip_inside_expr!(self, {
                return self.expected_something(loc::err::expected_consequent);
            });
            if self.skip_exact_word(IF) {
                // cond
                skip_inside_expr!(self, {
                    return self.expected_something(loc::err::expected_condition_expr);
                });
                let Some(cond) = self.read_named_expr()? else {
                    return self.expected_something(loc::err::expected_condition_expr);
                };

                // then
                skip_inside_expr!(self, {
                    return self.expected_something(loc::err::expected_then);
                });
                if !self.skip_exact_word(THEN) {
                    return self.expected_something(loc::err::expected_then);
                }

                // the consequent statements
                let consequent = self.read_consequent_block()?;

                arms.push(IfArm { cond, consequent });
            } else {
                // the consequent statements for final, catch-all else
                let consequent = self.read_consequent_block()?;
                let source_region = region_builder(self);
                let kind = IfExpr {
                    arms,
                    catch_all_else: Some(Box::new(consequent)),
                };
                return Ok(Some(AstNode::new(source_region, kind)));
            }
        }

        // no else block means void type
        let source_region = region_builder(self);

        for _arm in &arms {
            // TODO: constrain arm.cond.evaluate_type to be boolean
            // TODO: constrain arm.consequent.evaluate_type to be nothing
        }

        let kind = IfExpr {
            arms,
            catch_all_else: None,
        };
        Ok(Some(AstNode {
            source_region,
            kind,
        }))
    }
}

impl<'envborrow, 'ctx> EvalCtx<'envborrow, 'ctx> {
    /// evaluate an if expression
    pub(crate) fn evaluate_if(
        &mut self,
        arms: &[IfArm<HlirNode<'ctx>>],
        catch_all_else: &Option<Box<HlirNode<'ctx>>>,
    ) -> EvalResult<'ctx> {
        let mut arms_iter = arms.iter();
        loop {
            match arms_iter.next() {
                // fell through to catch-all
                None => match catch_all_else {
                    None => break Ok(TypedValue::nothing(&self.env.ti_ctx)),
                    Some(ast_node) => break self.with_scope(|ctx| ctx.evaluate_ast(ast_node)),
                },
                Some(arm) => {
                    let cond_result = self.evaluate_ast(&arm.cond)?;
                    if cond_result.is_truthy() {
                        break self.with_scope(|ctx| ctx.evaluate_ast(&arm.consequent));
                    }
                }
            }
        }
    }
}
