//! code for parsing, interpreting, and compiling do expressions
use crate::config::DO;
use crate::error::{Error, EvalResult};
use crate::hlir::HlirNode;
use crate::interpreter::EvalCtx;
use crate::loc;
use crate::sourceregion::SourceRegion;
use crate::typedvalue::TypedValue;
use crate::{
    ast::astnode::{
        AstNode,
        AstNodeKind::{DoExpr, Statements},
    },
    parse::Parser,
};

impl<'envborrow, 'ctx> Parser<'envborrow, 'ctx> {
    /// helper for `read_do_expr`
    /// what to do for an empty do statement
    fn do_expr_warning<F>(&mut self, region_builder: F) -> AstNode
    where
        F: FnOnce(&Parser) -> SourceRegion,
    {
        let (short_msg, help) = loc::err::empty_do_block();
        let source_region = region_builder(self);
        let kind = Statements {
            statements: Vec::new(),
        };
        self.record_warning(short_msg, help, source_region.clone());
        AstNode {
            source_region,
            kind,
        }
    }

    /// helper for `read_do_expr`
    fn read_do_body<MakeSourceRegionFn>(
        &mut self,
        pushed: bool,
        make_source_region: MakeSourceRegionFn,
    ) -> Result<AstNode, Vec<Error>>
    where
        MakeSourceRegionFn: FnOnce(&Parser) -> SourceRegion,
    {
        if !pushed {
            return Ok(self.do_expr_warning(make_source_region));
        }

        // the statements
        match self.read_statement_block()? {
            None => Ok(self.do_expr_warning(make_source_region)),
            Some(statements) => Ok(statements),
        }
    }

    ///> *do_expr* ← `do` *statements*
    ///
    /// creates a block of statements with its own scope
    pub(crate) fn read_do_expr(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        let region_builder1 = self.begin_source_region();
        let region_builder2 = self.begin_source_region();

        // begins with do
        if !self.skip_exact_word(DO) {
            return Ok(None);
        }

        // body is in a new scope
        let body = self.in_new_scope(|parser| {
            // skip whitespace to push indent for block
            parser.try_push_indentation_level(Parser::read_do_body, region_builder1)
        })??;

        Ok(Some(AstNode {
            source_region: region_builder2(self),
            kind: DoExpr(Box::new(body)),
        }))
    }
}

impl<'envborrow, 'ctx> EvalCtx<'envborrow, 'ctx> {
    /// evaluate a do
    pub(crate) fn evaluate_do(&mut self, statements: &[HlirNode<'ctx>]) -> EvalResult<'ctx> {
        let mut result = Ok(TypedValue::nothing(&self.env.ti_ctx));
        self.with_scope(|ctx| {
            for node in statements {
                result = ctx.evaluate_ast(node);
            }
            result
        })
    }
}
