use crate::{
    ast::astnode::AstNode,
    config::{NAMED_PATTERN_OP, OPEN_CURLY, OPEN_PAREN, OPEN_SQUARE, PATTERN_SEPARATOR},
    error::Error,
    loc,
    parse::Parser,
    pattern::{StegoPattern, StegoPatternKind},
    skip_inside_expr, skip_read_and_require, skip_required, StringType,
};

impl<'envborrow, 'ctx> Parser<'envborrow, 'ctx> {
    ///> *pattern* ← *at_pattern*
    ///
    pub(crate) fn read_pattern(&mut self) -> Result<Option<StegoPattern>, Vec<Error>> {
        self.read_at_pattern()
    }

    ///> *at_pattern* ← ( *name* `@` )<sup>?</sup> *or_pattern*
    ///
    fn read_at_pattern(&mut self) -> Result<Option<StegoPattern>, Vec<Error>> {
        let region_builder = self.begin_source_region();

        // name
        let first = self.read_or_pattern()?;
        let capture_name = match &first {
            Some(StegoPattern {
                kind: StegoPatternKind::Capture(name),
                ..
            }) => name,
            Some(_) => return Ok(first),
            None => return Ok(None),
        };

        // @
        skip_required!(self, skip_exact, NAMED_PATTERN_OP, {
            return Ok(first);
        });

        // pattern
        let pattern = skip_read_and_require!(self, read_or_pattern, {
            return self.expected_something(loc::err::at_pattern_subpattern_expected);
        });

        Ok(Some(StegoPattern {
            kind: StegoPatternKind::Named(StringType::clone(capture_name), Box::new(pattern)),
            source_region: region_builder(self),
        }))
    }

    ///> *or_pattern* ← *closed_pattern* ( `|` *closed_pattern* ) <sup>*</sup>
    ///
    fn read_or_pattern(&mut self) -> Result<Option<StegoPattern>, Vec<Error>> {
        let region_builder = self.begin_source_region();

        // first subpattern
        let first = match self.read_closed_pattern()? {
            None => return Ok(None),
            Some(x) => x,
        };

        skip_inside_expr!(self, { return Ok(Some(first)) });

        // can also be zero or more additional, each preceded by pipe symbol

        if let Some(s) = self.get_code_by_byte(self.byte..) {
            if !s.starts_with(PATTERN_SEPARATOR) {
                return Ok(Some(first));
            }
        } else {
            return Ok(Some(first));
        }

        let mut patterns: Vec<StegoPatternKind> = vec![first.kind; 3];

        // TODO: error types are not the same

        while self.skip_exact(PATTERN_SEPARATOR) {
            skip_inside_expr!(self, {
                return self.expected_something(loc::err::pattern_after_pipe_expected);
            });

            match self.read_closed_pattern()? {
                None => return self.expected_something(loc::err::pattern_after_pipe_expected),
                Some(pattern) => patterns.push(pattern.kind),
            }

            skip_inside_expr!(self, { break });
        }

        Ok(Some(StegoPattern {
            kind: StegoPatternKind::Or(patterns),
            source_region: region_builder(self),
        }))
    }

    ///> *closed_pattern* ← *mapping_pattern*
    ///>     / *vector_pattern*
    ///>     / *tuple_pattern*
    ///>     / *group_pattern*
    ///>     / *range_pattern*
    ///>     / *constant_pattern*
    ///>     / *literal_pattern*
    ///>     / *capture_pattern*
    ///
    /// TODO: I left out algebraic data type patterns
    fn read_closed_pattern(&mut self) -> Result<Option<StegoPattern>, Vec<Error>> {
        let s = match self.get_code_by_byte(self.byte..) {
            None => return Ok(None),
            Some(x) => x,
        };

        if s.starts_with(OPEN_CURLY) {
            return self.read_mapping_pattern();
        }
        if s.starts_with(OPEN_SQUARE) {
            return self.read_vector_pattern();
        }
        if s.starts_with(OPEN_PAREN) {
            return match self.read_tuple_pattern() {
                Ok(None) => self.read_group_pattern(),
                x => x,
            };
        }

        if let Some(x) = self.read_range_pattern()? {
            return Ok(Some(x));
        }

        if let Some(x) = self.read_constant_pattern()? {
            return Ok(Some(x));
        }

        if let Some(x) = self.read_literal_pattern()? {
            return Ok(Some(x));
        }

        self.read_capture_pattern()
    }

    ///> *mapping_pattern* ← `{` ( *key_value_pattern* ( `,` *key_value_pattern* )<sup>*</sup> `,`<sup>?</sup> )<sup>?</sup> `}`
    ///
    // TODO implement
    fn read_mapping_pattern(&mut self) -> Result<Option<StegoPattern>, Vec<Error>> {
        todo!()
    }

    ///> *vector_pattern* ← `[` ( *value_pattern* ( `,` *value_pattern* )<sup>*</sup> `,`<sup>?</sup> )<sup>?</sup> `]`
    ///
    // TODO include spread operator pattern
    // TODO implement
    fn read_vector_pattern(&mut self) -> Result<Option<StegoPattern>, Vec<Error>> {
        todo!()
    }

    ///> *tuple_pattern* ← *zero_tuple_pattern* / *one_tuple_pattern* / *n_tuple_pattern*
    ///
    // TODO include spread operator pattern
    #[allow(dead_code)] // TODO implement
    fn read_tuple_pattern(&mut self) -> Result<Option<StegoPattern>, Vec<Error>> {
        todo!()
    }

    ///> *zero_tuple_pattern* ← `()`
    ///
    #[allow(dead_code)] // TODO implement
    fn read_zero_tuple_pattern(&mut self) -> Result<Option<StegoPattern>, Vec<Error>> {
        todo!()
    }

    ///> *one_tuple_pattern* ← `(` *value_pattern* `,)`
    ///
    #[allow(dead_code)] // TODO implement
    fn read_one_tuple_pattern(&mut self) -> Result<Option<StegoPattern>, Vec<Error>> {
        todo!()
    }

    ///> *n_tuple_pattern* ← `(` *value_pattern* ( `,` *value_pattern* )<sup>+</sup> ','<sup>?</sup> `)`
    ///
    #[allow(dead_code)] // TODO implement
    fn read_n_tuple_pattern(&mut self) -> Result<Option<StegoPattern>, Vec<Error>> {
        todo!()
    }

    ///> *group_pattern* ← `(` *pattern* `)`
    ///
    // TODO implement
    fn read_group_pattern(&mut self) -> Result<Option<StegoPattern>, Vec<Error>> {
        todo!()
    }

    ///> *range_pattern* ← `..`
    ///
    #[allow(clippy::unused_self, clippy::unnecessary_wraps)] // TODO implement
    fn read_range_pattern(&mut self) -> Result<Option<StegoPattern>, Vec<Error>> {
        Ok(None)
    }

    ///> *constant_pattern* ← *identifier* ( `.` *identifier* )<sup>+</sup>
    ///
    #[allow(clippy::unused_self, clippy::unnecessary_wraps)] // TODO implement
    fn read_constant_pattern(&mut self) -> Result<Option<StegoPattern>, Vec<Error>> {
        Ok(None)
    }

    ///> *literal_pattern* ← *literal*
    ///
    // TODO implement
    fn read_literal_pattern(&mut self) -> Result<Option<StegoPattern>, Vec<Error>> {
        let region_builder = self.begin_source_region();
        let literal = self.read_literal();
        let source_region = region_builder(self);
        match literal? {
            None => Ok(None),
            Some(literal_kind) => Ok(Some(StegoPattern {
                kind: StegoPatternKind::Literal(literal_kind),
                source_region,
            })),
        }
    }

    ///> *capture_pattern* ← *identifier*
    ///
    pub(crate) fn read_capture_pattern(&mut self) -> Result<Option<StegoPattern>, Vec<Error>> {
        let region_builder = self.begin_source_region();
        let name = self.read_name();
        let source_region = region_builder(self);

        match name {
            Some(s) => Ok(Some(StegoPattern {
                kind: StegoPatternKind::Capture(s.into()),
                source_region,
            })),
            None => self.read_literal_pattern(),
        }
    }

    ///> *key_value_pattern* ← ( (*literal_pattern* &#124; *constant_pattern*) `:` *or_pattern* ) &#124; *keyword_spread_pattern*
    ///
    #[allow(dead_code)] // TODO implement
    fn read_key_value_pattern(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        todo!()
    }

    ///> *value_pattern* ← *spread_pattern* &#124; *pattern*
    ///
    #[allow(dead_code)] // TODO implement
    fn read_value_pattern(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        todo!()
    }
}
