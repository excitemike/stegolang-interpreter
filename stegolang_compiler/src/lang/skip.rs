use crate::{
    config::{
        BLOCK_COMMENT_END, BLOCK_COMMENT_END_BYTES, BLOCK_COMMENT_END_COLS, BLOCK_COMMENT_START,
        BLOCK_COMMENT_START_BYTES, BLOCK_COMMENT_START_COLS, BOM, CHARACTER_BEGIN, CHARACTER_END,
        CLOSE_CURLY, CLOSE_PAREN, CLOSE_SQUARE, IDENTIFIER_CHAR_REGEX, LINE_COMMENT_START,
        OPEN_CURLY, OPEN_PAREN, OPEN_SQUARE, SHEBANG, STATEMENT_SEPARATOR, STRING_BEGIN,
        STRING_END, TAB_STOP,
    },
    error::Error,
    loc,
    parse::Parser,
};
use unicode_segmentation::UnicodeSegmentation;

/// effects of indentation as we skip whitespace
#[derive(Copy, Clone, Debug)]
pub(crate) enum AdvanceResult {
    // nothing special
    Continue,
    // current statement has ended
    NewStatement,
    // end of one or more blocks
    EndBlock,
    // end of input
    EndProgram,
}

impl<'envborrow, 'ctx> Parser<'envborrow, 'ctx> {
    /// interpret indentation significance based on current column number and indentation level
    fn check_indentation(&mut self) -> AdvanceResult {
        if self.eof() {
            return AdvanceResult::EndProgram;
        };

        match self.column_number.cmp(self.get_indentation_level()) {
            // increased indentation - ignore
            std::cmp::Ordering::Greater => AdvanceResult::Continue,
            // same level - next statement
            std::cmp::Ordering::Equal => AdvanceResult::NewStatement,
            // decreased indent - pop indentation stack then reconsider
            std::cmp::Ordering::Less => AdvanceResult::EndBlock,
        }
    }

    /// Error recovery! Read up until a dedent or EOF
    pub(crate) fn skip_bad_statement(&mut self) -> Result<(), Vec<Error>> {
        while let AdvanceResult::Continue = self.skip_comments_and_whitespace()? {
            if self.skip_exact(STATEMENT_SEPARATOR) {
                break;
            }
            self.skip_item()?;
        }
        Ok(())
    }

    /// move past a block comment
    pub(crate) fn skip_block_comment(&mut self) -> Result<(), Vec<Error>> {
        // track depth
        let mut depth = 1usize;
        // remember where we started in case we need to make an error message
        let start_line = self.line_number;
        let start_column = self.column_number;
        // skip beginning marker
        self.move_forward(0, *BLOCK_COMMENT_START_COLS, *BLOCK_COMMENT_START_BYTES);
        // keep reading until we've eaten the matching end marker
        loop {
            if let Some(s) = self.get_code_by_byte(self.byte..) {
                if s.starts_with(BLOCK_COMMENT_START) {
                    depth += 1;
                    self.move_forward(0, *BLOCK_COMMENT_START_COLS, *BLOCK_COMMENT_START_BYTES);
                } else if s.starts_with(BLOCK_COMMENT_END) {
                    self.move_forward(0, *BLOCK_COMMENT_END_COLS, *BLOCK_COMMENT_END_BYTES);
                    if depth <= 1 {
                        // done!
                        return Ok(());
                    }
                    depth -= 1;
                } else {
                    let grapheme = self.peek_grapheme().unwrap();
                    let is_whitespace = grapheme.trim().is_empty();
                    let len = grapheme.len();
                    if is_whitespace {
                        self.skip_whitespace();
                    } else {
                        self.move_forward(0, 1, len);
                    }
                }
            } else {
                let (short_msg, help) = loc::err::unterminated_comment();
                return self.error(short_msg, help, start_line, start_column);
            }
        }
    }

    /// skip UTF BOM
    pub(crate) fn skip_bom(&mut self) -> AdvanceResult {
        if let Some(s) = self.get_code_by_byte(self.byte..) {
            if s.starts_with(BOM) {
                self.move_forward(0, 0, BOM.len());
            }
            AdvanceResult::Continue
        } else {
            AdvanceResult::EndProgram
        }
    }

    /// skip a curly brace expression
    pub(crate) fn skip_bracketted(&mut self, open: &str, close: &str) -> Result<(), Vec<Error>> {
        if self.skip_exact(open) {
            loop {
                match self.skip_comments_and_whitespace()? {
                    AdvanceResult::Continue => {}
                    _ => return Ok(()),
                }
                if let Some(grapheme) = self.peek_grapheme() {
                    if grapheme == close {
                        self.skip_exact(close);
                        break Ok(());
                    }
                    self.skip_bad_statement()?;
                } else {
                    break Ok(());
                }
            }
        } else {
            Ok(())
        }
    }

    /// move forward past whitespace and comments
    pub(crate) fn skip_comments_and_whitespace(&mut self) -> Result<AdvanceResult, Vec<Error>> {
        let mut byte = self.byte;
        loop {
            if let Some(s) = self.get_code_by_byte(self.byte..) {
                if s.starts_with(LINE_COMMENT_START) {
                    self.skip_to_end_of_line();
                } else if s.starts_with(BLOCK_COMMENT_START) {
                    self.skip_block_comment()?;
                } else {
                    let result = self.skip_whitespace();
                    // if it ever doesn't move us, bail
                    if self.byte == byte {
                        break Ok(result);
                    }
                    byte = self.byte;
                }
            } else {
                break Ok(AdvanceResult::EndProgram);
            }
        }
    }

    /// Skip forward past an exact string of symbols/punctuation, if we're at it.
    /// Returns true if it did skip.
    pub(crate) fn skip_exact(&mut self, s: &str) -> bool {
        debug_assert!(!s.trim().is_empty());
        if let Some(code) = self.get_code_by_byte(self.byte..) {
            if code.starts_with(s) {
                let columns = s.graphemes(true).count();
                let bytes = s.len();
                self.move_forward(0, columns, bytes);
                return true;
            }
        }
        false
    }

    /// Skip forward past an exact string of identifier characters, if we're at it.
    /// This method returns true if it did skip.
    pub(crate) fn skip_exact_word(&mut self, s: &str) -> bool {
        debug_assert!(!s.trim().is_empty());
        if let Some(code) = self.get_code_by_byte(self.byte..) {
            if code.starts_with(s) && !IDENTIFIER_CHAR_REGEX.is_match(&code[s.len()..]) {
                let columns = s.graphemes(true).count();
                let bytes = s.len();
                self.move_forward(0, columns, bytes);
                return true;
            }
        }
        false
    }

    /// skip shebang
    ///> *shebang* ← `#!` ~NEWLINE<sup>\*</sup>
    ///
    pub(crate) fn skip_shebang(&mut self) {
        if let Some(s) = self.get_code_by_byte(self.byte..) {
            if s.starts_with(SHEBANG) {
                self.skip_to_end_of_line();
            }
        }
    }

    /// move forward to the end of the line
    pub(crate) fn skip_to_end_of_line(&mut self) {
        if let Some(s) = self.get_code_by_byte(self.byte..) {
            match s.find('\n') {
                None => {
                    let columns = s.graphemes(true).count();
                    let bytes = s.len();
                    self.column_number += columns;
                    self.byte += bytes;
                }
                Some(bytes) => {
                    self.line_number += 1;
                    self.column_number = 1;
                    self.byte += bytes + 1;
                }
            }
        }
    }

    /// during error recovery, skip the next token, whatever it is
    pub(crate) fn skip_item(&mut self) -> Result<(), Vec<Error>> {
        loop {
            let grapheme = match self.peek_grapheme() {
                None => break Ok(()),
                Some(x) => x,
            };
            // update bookkeeping for our position as it changes
            if grapheme.trim().is_empty() {
                break Ok(());
            }
            match grapheme {
                crate::config::LINE_COMMENT_START => self.skip_to_end_of_line(),
                crate::config::OPEN_CURLY => self.skip_bracketted(OPEN_CURLY, CLOSE_CURLY)?,
                crate::config::OPEN_PAREN => self.skip_bracketted(OPEN_PAREN, CLOSE_PAREN)?,
                crate::config::OPEN_SQUARE => self.skip_bracketted(OPEN_SQUARE, CLOSE_SQUARE)?,
                crate::config::CHARACTER_BEGIN => {
                    self.skip_bracketted(CHARACTER_BEGIN, CHARACTER_END)?;
                }
                crate::config::STRING_BEGIN => self.skip_bracketted(STRING_BEGIN, STRING_END)?,
                crate::config::BLOCK_COMMENT_START => self.skip_block_comment()?,
                // stop once we hit whitespace
                _ if grapheme.trim().is_empty() => break Ok(()),
                _ => {
                    let bytes = grapheme.len();
                    self.move_forward(0, 1, bytes);
                }
            }
        }
    }

    /// move forward past whitespace in the code
    pub(crate) fn skip_whitespace(&mut self) -> AdvanceResult {
        let mut line_number = self.line_number;
        let mut column_number = self.column_number;
        let mut byte = self.byte;
        let mut tab_warning = None;
        let mut graphemes = self
            .get_code_by_byte(self.byte..)
            .unwrap_or("")
            .graphemes(true)
            .peekable();
        loop {
            let grapheme = match graphemes.peek() {
                None => return AdvanceResult::EndProgram,
                Some(x) => *x,
            };
            // update bookkeeping for our position as it changes
            match grapheme {
                // line breaks
                "\r\n" | "\n" => {
                    graphemes.next();
                    line_number += 1;
                    column_number = 1;
                    byte += grapheme.len();
                }
                // warn about tabs
                "\t" => {
                    if tab_warning.is_none() {
                        let (short_msg, help) = loc::err::tab_character();
                        tab_warning = Some((
                            short_msg,
                            help,
                            self.make_source_region(line_number, column_number),
                        ));
                    }
                    graphemes.next();
                    column_number += TAB_STOP - ((column_number - 1) % TAB_STOP);
                    byte += grapheme.len();
                }
                // other whitespace
                _ if grapheme.trim().is_empty() => {
                    graphemes.next();
                    column_number += 1;
                    byte += grapheme.len();
                }
                // not whitespace at all. Stop!
                _ => break,
            }
        }
        self.column_number = column_number;
        self.byte = byte;
        self.line_number = line_number;
        self.did_tab_warning = self.did_tab_warning || tab_warning.is_some();
        if let Some((short_msg, help, source_region)) = tab_warning {
            self.record_warning(short_msg, help, source_region);
        }
        self.check_indentation()
    }
}

/// skip comments and whitespace, and if the indentation in it means the current
/// expression ends, run the given code
///
#[macro_export]
#[allow(clippy::module_name_repetitions)]
macro_rules! skip_inside_expr {
    ($self:expr, $if_expr_ended:tt) => {{
        match $self.skip_comments_and_whitespace()? {
            $crate::lang::AdvanceResult::Continue => {}
            _ => $if_expr_ended,
        }
    }};
}

/// skip comments and whitespace, call the given read function
/// if the required thing is missing, run $`code_if_missing`
#[macro_export]
#[allow(clippy::module_name_repetitions)]
macro_rules! skip_read_and_require {
    ($self:expr, $read_fn:ident, $code_if_missing:tt) => {{
        match $self.skip_comments_and_whitespace()? {
            $crate::lang::AdvanceResult::Continue => match $self.$read_fn()? {
                None => $code_if_missing,
                Some(x) => x,
            },
            _ => $code_if_missing,
        }
    }};
}

/// skip comments and whitespace, call the given skip function
/// if the required thing is missing, run $`code_if_missing`
#[macro_export]
#[allow(clippy::module_name_repetitions)]
macro_rules! skip_required {
    ($self:expr, $skip_fn:ident, $req_item:expr, $code_if_missing:tt) => {{
        match $self.skip_comments_and_whitespace()? {
            $crate::lang::AdvanceResult::Continue if $self.$skip_fn($req_item) => {}
            _ => $code_if_missing,
        }
    }};
}
