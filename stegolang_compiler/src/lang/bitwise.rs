use crate::{
    ast::astnode::AstNode,
    error::{Error, EvalResult},
    hlir::HlirNode,
    intern::interned::Interned,
    interpreter::EvalCtx,
    parse::Parser,
    stegoobject::StegoObject,
    ti::Type,
    typedvalue::{TypedValue, TypedValueKind},
};

impl<'envborrow, 'ctx> Parser<'envborrow, 'ctx> {
    // TODO implement
    pub(crate) fn read_bitwise_or_expr(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        self.read_bitwise_xor_expr()
    }
    // TODO implement
    pub(crate) fn read_bitwise_xor_expr(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        self.read_bitwise_and_expr()
    }
    // TODO implement
    pub(crate) fn read_bitwise_and_expr(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        self.read_bitshift_expr()
    }
    // TODO implement
    pub(crate) fn read_bitshift_expr(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        self.addsub_expr()
    }
}

impl<'envborrow, 'ctx> EvalCtx<'envborrow, 'ctx> {
    pub(crate) fn do_bitwise_not(&mut self, ast: &HlirNode<'ctx>) -> EvalResult<'ctx> {
        match &self.evaluate_ast(ast)? {
            TypedValue {
                ty: Interned(Type::BigInt, _),
                kind: TypedValueKind::Val(obj),
            } => match &**obj {
                StegoObject::Integer(bigint) => Ok(TypedValue {
                    ty: self.env.ti_ctx.mk_bigint(),
                    kind: TypedValueKind::Val(StegoObject::Integer(!bigint).into()),
                }),
                _ => todo!("handle type tracking error"),
            },
            TypedValue {
                ty: Interned(Type::Int64, _),
                kind: TypedValueKind::Val(obj),
            } => match &**obj {
                StegoObject::Int64(n) => Ok(TypedValue {
                    ty: self.env.ti_ctx.mk_int64(),
                    kind: TypedValueKind::Val(StegoObject::Int64(!n).into()),
                }),
                _ => todo!("handle type tracking error"),
            },
            _ => todo!("error if not integral type"),
        }
    }
}
