use crate::{
    ast::astnode::{AstNode, AstNodeKind::Identifier},
    error::Error,
    parse::Parser,
};

impl<'envborrow, 'ctx> Parser<'envborrow, 'ctx> {
    /// read an identifier
    pub(crate) fn read_identifier(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        let make_source_region = self.begin_source_region();
        let Some(name) = self.read_name() else {
            // We're not at an identifier.
            match self.read_literal()? {
                Some(literal_kind) => {
                    return Ok(Some(AstNode::new(make_source_region(self), literal_kind)));
                }
                None => return Ok(None),
            }
        };

        // look it up in context to find type
        let kind = Identifier { name: name.into() };
        Ok(Some(AstNode {
            source_region: make_source_region(self),
            kind,
        }))
    }
}
