use crate::{ast::astnode::AstNode, error::Error, parse::Parser};

impl<'envborrow, 'ctx> Parser<'envborrow, 'ctx> {
    /// Parse an await expression or higher-priority syntax
    ///
    ///> *await_expr*             ←     `await`<sup>?</sup> *apply_expr*
    ///
    /// TODO implement
    pub(crate) fn read_await_expr(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        self.read_apply_expr()
    }
}
