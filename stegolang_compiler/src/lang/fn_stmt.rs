//! code for parsing, interpreting, and compiling fn statements
use crate::{
    ast::astnode::{AstNode, AstNodeKind},
    config::{FN_EQ, FN_KEYWORD},
    controlflow::{err, ControlFlow},
    error::{Error, EvalResult},
    fntypes::{fncode::FnCode, fnfamily::FnFamily, fnfamimpls::FnFamImpls},
    intern::interned::Interned,
    interpreter::EvalCtx,
    loc,
    parse::{IntoParseResult, Parser},
    pattern::StegoPatternKind,
    skip_inside_expr,
    sourceregion::SourceRegion,
    stegoobject::{FnImpl, StegoObject},
    ti::{ITy, Type},
    typedvalue::{TypedValue, TypedValueKind},
    StringType,
};
use std::{cell::RefCell, rc::Rc};

impl<'envborrow, 'ctx> Parser<'envborrow, 'ctx> {
    ///> *fn_stmt* ← `fn` *name* *pattern*<sup>\*</sup> `=` *statements*
    ///
    /// define a function
    ///
    /// TODO: Allow commas. like
    ///
    ///> *fn_stmt* ← `fn` *name* ( `,`<sup>?</sup> *pattern* )<sup>\*</sup> `,`<sup>?</sup> `=` *statements*
    ///
    /// TODO: Also modifiers. like
    ///
    ///> *fn_stmt* ← `fn` *name* ( `,`<sup>?</sup> *fn_param_mod*<sup>?</sup> *pattern* )<sup>\*</sup> `,`<sup>?</sup> `=` *statements*
    ///
    pub(crate) fn read_fn_stmt(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        use crate::util::nestedscope::LookupResult::FoundInCurrentScope;
        let region_builder = self.begin_source_region();

        // begins with fn
        if !self.skip_exact_word(FN_KEYWORD) {
            return Ok(None);
        }

        skip_inside_expr!(self, {
            return self.expected_something(loc::err::fn_name_expected);
        });

        // function name
        let fname_region_maker = self.begin_source_region();
        let Some(fname) = self.read_name() else {
            return self.expected_something(loc::err::fn_name_expected);
        };
        let fname = StringType::from(fname);
        let fname_region = fname_region_maker(self);

        skip_inside_expr!(self, {
            return self.expected_something(loc::err::fn_eq_expected);
        });

        // param_list
        let mut params = Vec::new();
        let Some(first_pat) = self.read_pattern()? else {
            return self.expected_something(loc::err::fn_pattern_expected);
        };
        params.push(first_pat);
        while let Some(param) = self.read_pattern()? {
            params.push(param);
        }

        skip_inside_expr!(self, {
            return self.expected_something(loc::err::fn_eq_expected);
        });

        // then the equals
        if !self.skip_exact(FN_EQ) {
            return self.expected_something(loc::err::fn_eq_expected);
        }

        let lookup_result = self.env.scope.lookup(&fname);
        // insert placeholder if needed to allow recursion
        let placeholder_needed = match &lookup_result {
            FoundInCurrentScope(x) => {
                let TypedValue { ty: _, kind } = &**x;
                match kind {
                    TypedValueKind::Val(obj) => !matches!(&**obj, StegoObject::FnFamily(..)),
                    _ => true,
                }
            }
            _ => true,
        };
        drop(lookup_result);

        let fn_type = self
            .env
            .ti_ctx
            .mk_fn_var(params.len(), &fname_region)
            .into_parse_result(&fname_region)?;

        if placeholder_needed {
            self.env.scope.insert(
                StringType::clone(&fname),
                TypedValue {
                    ty: fn_type,
                    kind: TypedValueKind::PlaceHolder,
                },
            );
        }

        let body = self.read_lambda_body()?;

        // now insert for reals
        if !placeholder_needed {
            self.env.scope.insert(
                StringType::clone(&fname),
                TypedValue {
                    ty: fn_type,
                    kind: TypedValueKind::PlaceHolder,
                },
            );
        }

        let source_region = region_builder(self);
        let params = Rc::new(RefCell::new(params));

        Ok(Some(AstNode {
            source_region,
            kind: AstNodeKind::FnStmt(crate::ast::FnStmt {
                body,
                name: fname,
                params,
                placeholder_needed,
            }),
        }))
    }
}

impl<'envborrow, 'ctx> EvalCtx<'envborrow, 'ctx> {
    /// add a new fn family for the provided impl
    pub(crate) fn add_fn_fam(
        &mut self,
        fn_impl: Rc<FnImpl<'ctx>>,
        fname: StringType,
        fn_ty: ITy<'ctx>,
    ) -> Rc<FnFamily<'ctx>> {
        let num_params = fn_impl.pattern_set.borrow().len();
        let implementations = if fn_impl
            .pattern_set
            .borrow()
            .iter()
            .all(|p| matches!(p.kind, StegoPatternKind::Capture(..)))
        {
            FnFamImpls::Simple(fn_impl)
        } else {
            FnFamImpls::Match {
                fn_impls: Rc::new(RefCell::new(vec![fn_impl])),
            }
        };
        let fn_family = FnFamily {
            implementations,
            debug_name: StringType::clone(&fname),
            num_params,
        };
        let fn_family = Rc::new(fn_family);

        let scope_item = TypedValue {
            ty: fn_ty,
            kind: TypedValueKind::Val(Rc::new(StegoObject::FnFamily(Rc::clone(&fn_family)))),
        };
        self.env.scope.insert(fname, scope_item);
        fn_family
    }

    /// evaluate function definition
    pub(crate) fn evaluate_fn_stmt(
        &mut self,
        fn_stmt: &crate::hlir::fnstmt::FnStmt<'ctx>,
        source_region: &SourceRegion,
    ) -> EvalResult<'ctx> {
        let crate::hlir::fnstmt::FnStmt {
            body,
            captures,
            fn_ty,
            name,
            params,
            placeholder_needed,
        } = fn_stmt;
        // insert a placeholder if needed for recursion
        if *placeholder_needed {
            self.env.scope.insert(
                StringType::from(name),
                TypedValue {
                    ty: *fn_ty,
                    kind: TypedValueKind::PlaceHolder,
                },
            );
        }

        let captures = self
            .env
            .scope
            .get_upvalues(captures.iter().cloned())
            .map_err(|x| ControlFlow::err(x, source_region.clone()))?;

        let fn_impl = Rc::new(FnImpl {
            captures,
            code: FnCode::Statements(Rc::clone(body)),
            pattern_set: Rc::clone(params),
        });

        // add to fn family or make a new one
        if let Some(lock) = self.env.scope.get_lock(name) {
            let guard = lock.borrow();
            if let TypedValue {
                ty: Interned(Type::F { .. }, _),
                kind: TypedValueKind::Val(ref obj),
            } = &*guard
            {
                let obj_ref: &StegoObject = obj;
                if let StegoObject::FnFamily(ref fn_fam) = obj_ref {
                    fn_fam
                        .add_fn_impl(fn_impl)
                        .or_else(|msg_tuple| err(msg_tuple, source_region.clone()))?;
                    return Ok(TypedValue::nothing(&self.env.ti_ctx));
                }
                todo!("handle error");
            }
        }

        // note the early return above

        self.add_fn_fam(fn_impl, StringType::from(name), *fn_ty);

        Ok(TypedValue::nothing(&self.env.ti_ctx))
    }
}
