use crate::{
    ast::astnode::{AstNode, AstNodeKind},
    config::{
        EQ_OPERATOR, GTE_OPERATOR, GT_OPERATOR, IN_OPERATOR, LTE_OPERATOR, LT_OPERATOR,
        NEQ_OPERATOR, NOT_IN_OPERATOR,
    },
    error::Error,
    loc,
    parse::Parser,
    skip_inside_expr,
};

impl<'envborrow, 'ctx> Parser<'envborrow, 'ctx> {
    ///> *in_operator_expr* ← *bitwise_or_expr* ( ( `not in` / `in` ) *bitwise_or_expr* )<sup>?</sup>
    ///
    pub(crate) fn read_inoperator_expr(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        let region_builder = self.begin_source_region();

        // search item
        let item = self.read_bitwise_or_expr()?;
        if item.is_none() {
            return Ok(None);
        }
        let item = item.unwrap();

        skip_inside_expr!(self, { return Ok(Some(item)) });

        let invert = if self.skip_exact(NOT_IN_OPERATOR) {
            true
        } else if self.skip_exact(IN_OPERATOR) {
            false
        } else {
            return Ok(Some(item));
        };

        skip_inside_expr!(self, { return Ok(Some(item)) });

        let item = item.into();
        if let Some(container) = self.read_bitwise_or_expr()? {
            let container = container.into();
            Ok(Some(AstNode {
                source_region: region_builder(self),
                kind: AstNodeKind::InExpr {
                    container,
                    invert,
                    item,
                },
            }))
        } else {
            self.expected_something(loc::err::expected_rhs)
        }
    }

    ///> *`comparison_expr`* ← *`in_operator_expr`* ( ( `==` &#124; `!=` &#124; `<=` &#124; `<` &#124; `>=` &#124; `>` &#124; ) *`in_operator_expr`* )<sup>*</sup>
    ///
    pub(crate) fn read_comparison_expr(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        use crate::ast::Operator::{
            Equals, GreaterThan, GreaterThanOrEqualTo, LessThan, LessThanOrEqualTo, NotEquals,
        };
        self.read_binop(
            &[
                (EQ_OPERATOR, Equals),
                (NEQ_OPERATOR, NotEquals),
                (LTE_OPERATOR, LessThanOrEqualTo),
                (LT_OPERATOR, LessThan),
                (GTE_OPERATOR, GreaterThanOrEqualTo),
                (GT_OPERATOR, GreaterThan),
            ],
            Parser::read_inoperator_expr,
            |_parser: &mut Parser<'_, 'ctx>, source_region, first, rest| {
                Ok(AstNode {
                    source_region,
                    kind: AstNodeKind::ComparisonOpExpr { first, rest },
                })
            },
        )
    }
}
