//! parse function application

use crate::ast::astnode::{AstNode, AstNodeKind};
use crate::fntypes::fnfamily::FnFamily;
use crate::hlir::HlirNode;
use crate::stegoobject::{FnPartial, StegoObject};
use crate::typedvalue::{TypedValue, TypedValueKind};
use crate::{controlflow::err, interpreter::EvalCtx, loc, sourceregion::SourceRegion};
use crate::{
    error::{Error, EvalResult},
    parse::Parser,
    skip_inside_expr,
};
use itertools::Itertools;
use std::cmp::Ordering;
use std::rc::Rc;

impl<'envborrow, 'ctx> Parser<'envborrow, 'ctx> {
    ///> *apply_expr* ← *dot_expr* ( *dot_expr* )<sup>\*</sup>
    pub(crate) fn read_apply_expr(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        let region_builder = self.begin_source_region();

        // function
        let first = match self.read_dot_expr()? {
            None => return Ok(None),
            Some(x) => x,
        };

        // args
        skip_inside_expr!(self, { return Ok(Some(first)) });
        let mut args = Vec::new();
        while let Some(arg) = self.read_dot_expr()? {
            args.push(arg);
            skip_inside_expr!(self, { break });
        }

        if args.is_empty() {
            return Ok(Some(first));
        }

        let source_region = region_builder(self);

        Ok(Some(AstNode::new(
            source_region,
            AstNodeKind::ApplyExpr {
                func: Box::new(first),
                args,
            },
        )))
    }
    
    ///> *apply_expr* ← *paren_expr* ( *paren_expr* )<sup>\*</sup>
    pub(crate) fn read_dotless_apply_expr(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        let region_builder = self.begin_source_region();

        // function
        let first = match self.read_paren_expr()? {
            None => return Ok(None),
            Some(x) => x,
        };

        // args
        skip_inside_expr!(self, { return Ok(Some(first)) });
        let mut args = Vec::new();
        while let Some(arg) = self.read_paren_expr()? {
            args.push(arg);
            skip_inside_expr!(self, { break });
        }

        if args.is_empty() {
            return Ok(Some(first));
        }

        let source_region = region_builder(self);

        Ok(Some(AstNode::new(
            source_region,
            AstNodeKind::ApplyExpr {
                func: Box::new(first),
                args,
            },
        )))
    }
}

impl<'envborrow, 'ctx> EvalCtx<'envborrow, 'ctx> {
    /// do application
    ///
    /// # Panics
    ///
    /// Panics if the applicand argument does not evaluate to a function type
    ///
    pub(crate) fn evaluate_apply(
        &mut self,
        applicand: &HlirNode<'ctx>,
        arguments: &[HlirNode<'ctx>],
        source_region: &SourceRegion,
    ) -> EvalResult<'ctx> {
        // unpack everything
        let TypedValue { ty: _, kind } = self.evaluate_ast(applicand)?;
        match &kind {
            TypedValueKind::Val(obj) => match &**obj {
                StegoObject::FnFamily(fn_fam) => {
                    self.evaluate_apply_old(applicand, arguments, source_region, fn_fam, &[][..])
                }
                StegoObject::FnPartial(FnPartial {
                    family,
                    already_supplied_arguments,
                }) => self.evaluate_apply_old(
                    applicand,
                    arguments,
                    source_region,
                    family,
                    &already_supplied_arguments[..],
                ),
                _ => {
                    return err(
                        loc::err::internal::unhandled_case(obj, file!(), line!(), column!()),
                        source_region.clone(),
                    )
                }
            },
            TypedValueKind::StegoFn {
                debug_name,
                fn_impl,
            } => {
                let num_required_args = fn_impl.pattern_set.borrow().len();
                let num_supplied_args = arguments.len();
                match num_supplied_args.cmp(&num_required_args) {
                    Ordering::Less => {
                        // not enough args supplied. create partial.
                        todo!()
                    }
                    Ordering::Equal => {
                        // exactly right. evaluate function
                        let mut evalled_args = Vec::with_capacity(arguments.len());
                        for x in arguments {
                            evalled_args.push(self.evaluate_ast(x)?);
                        }
                        self.evaluate_fn_impl(fn_impl, evalled_args.as_slice(), source_region)
                    }
                    Ordering::Greater => err(
                        loc::err::too_many_args(
                            Some(debug_name),
                            num_required_args,
                            num_supplied_args,
                        ),
                        source_region.clone(),
                    ),
                }
            }
            x => todo!("unhandled case in evaluate_apply. {x:?}"),
        }
    }
    fn evaluate_apply_old(
        &mut self,
        applicand: &HlirNode<'ctx>,
        arguments: &[HlirNode<'ctx>],
        source_region: &SourceRegion,
        fn_fam: &Rc<FnFamily<'ctx>>,
        already_supplied_arguments: &[TypedValue<'ctx>],
    ) -> EvalResult<'ctx> {
        let TypedValue { ty, kind: _ } = self.evaluate_ast(applicand)?;
        // determine whether we can execute the function, or if this is partial application
        let num_combined_args = already_supplied_arguments.len() + arguments.len();
        let required_args = fn_fam.num_params;
        match num_combined_args.cmp(&required_args) {
            // too many args
            Ordering::Greater => err(
                loc::err::too_many_args(Some(&fn_fam.debug_name), required_args, num_combined_args),
                source_region.clone(),
            ),

            // partial function application
            Ordering::Less => {
                // evaluate arguments
                let mut evalled_args = Vec::with_capacity(arguments.len());
                for x in arguments {
                    evalled_args.push(self.evaluate_ast(x)?);
                }

                let partial = StegoObject::FnPartial(FnPartial {
                    family: Rc::clone(fn_fam),
                    already_supplied_arguments: already_supplied_arguments
                        .iter()
                        .map(TypedValue::clone)
                        .chain(evalled_args)
                        .collect_vec(),
                });

                if let Some(fn_data) = ty.get_fn_data() {
                    Ok(TypedValue::val(
                        self.env.ti_ctx.mk_fn_type_from_iter(
                            fn_data.params[arguments.len()..].iter().copied(),
                            fn_data.result,
                        ),
                        partial.into(),
                    ))
                } else {
                    err(
                        loc::err::not_function(&ty.to_string()),
                        applicand.source_region.clone(),
                    )
                }
            }

            // Exactly right. Now that we are definitely about to call it, we can
            // evaluate the functions arguments
            Ordering::Equal => {
                // evaluate arguments
                let mut evalled_args = Vec::with_capacity(arguments.len());
                for x in arguments {
                    evalled_args.push(self.evaluate_ast(x)?);
                }

                let all_evaluated_arguments = already_supplied_arguments
                    .iter()
                    .cloned()
                    .chain(evalled_args)
                    .collect_vec();

                // select an implementation
                let fn_impl =
                    fn_fam.find_implementation(&all_evaluated_arguments[..], source_region)?;
                self.evaluate_fn_impl(fn_impl, all_evaluated_arguments.as_slice(), source_region)
            }
        }
    }
}
