use crate::{
    ast::astnode::{AstNode, AstNodeKind},
    error::{Error, EvalResult},
    hlir::HlirNode,
    interpreter::EvalCtx,
    loc,
    parse::Parser,
    StringType, lang::AdvanceResult,
};

impl<'envborrow, 'ctx> Parser<'envborrow, 'ctx> {
    ///> *dot_expr* ← *apply_expr* ( `.` *name*)<sup>?</sup>
    pub(crate) fn read_dot_expr(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        let region_builder = self.begin_source_region();

        // left hand side
        let Some(lhs) = self.read_dotless_apply_expr()? else {
            return Ok(None);
        };

        // dot
        if !self.skip_exact(crate::config::DOT) {
            return Ok(Some(lhs));
        }

        // method name
        let method_name = match self.skip_comments_and_whitespace()? {
            AdvanceResult::Continue => match self.read_name() {
                None => {return self.expected_something(loc::err::member_name_expected_after_dot);},
                Some(x) => x,
            },
            _ => {
                return self.expected_something(loc::err::member_name_expected_after_dot);
            },
        };

        Ok(Some(AstNode::new(
            region_builder(self),
            AstNodeKind::DotExpr {
                lhs: Box::new(lhs),
                method_name: method_name.into(),
            },
        )))
    }
}

impl<'envborrow, 'ctx> EvalCtx<'envborrow, 'ctx> {
    pub(crate) fn eval_dot(&mut self, lhs: &HlirNode<'ctx>, name: &StringType) -> EvalResult<'ctx> {
        let value = self.evaluate_ast(lhs)?;
        Ok(value.member(name, &self.env.ti_ctx, &lhs.source_region)?)
    }
}
