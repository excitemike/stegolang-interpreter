use crate::{
    ast::astnode::{
        AstNode,
        AstNodeKind::{BinOpExpr, BitwiseNot, UnaryMinus, UnaryPlus},
    },
    ast::Operator::{Divide, Minus, Plus, Remainder, Times},
    config::{DIVIDE_OPERATOR, MINUS, MULTIPLY_OPERATOR, PLUS, REMAINDER_OPERATOR},
    error::Error,
    loc,
    parse::Parser,
    skip_inside_expr,
};

impl<'envborrow, 'ctx> Parser<'envborrow, 'ctx> {
    ///> *addsub_expr* ← *term_expr* ( ( `+` / `-` ) *term_expr* )<sup>\*</sup>
    ///
    /// addition and subtraction
    /// left-associative operators with the same precedence
    pub(crate) fn addsub_expr(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        self.read_binop(
            &[(PLUS, Plus), (MINUS, Minus)],
            Parser::read_term_expr,
            |_, source_region, first, rest| {
                let kind = BinOpExpr { first, rest };
                Ok(AstNode {
                    source_region,
                    kind,
                })
            },
        )
    }

    ///> *term_expr* ← *factor_expr* ( ( `*` &#124; `/` &#124; `%` ) *factor_expr* )<sup>\*</sup>
    ///
    /// multiplication, division, and modulus
    /// left-associative operators with the same precedence
    fn read_term_expr(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        self.read_binop(
            &[
                (MULTIPLY_OPERATOR, Times),
                (DIVIDE_OPERATOR, Divide),
                (REMAINDER_OPERATOR, Remainder),
            ],
            Parser::read_factor_expr,
            |_, source_region, first, rest| {
                let kind = BinOpExpr { first, rest };
                Ok(AstNode::new(source_region, kind))
            },
        )
    }

    ///> *factor_expr* ← ( ( `+` &#124; `-` &#124; `~`) *factor_expr* ) &#124; *power_expr*
    ///
    fn read_factor_expr(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        let end_source_region = self.begin_source_region();

        let ctor = if self.skip_exact(crate::config::PLUS) {
            UnaryPlus
        } else if self.skip_exact(crate::config::MINUS) {
            UnaryMinus
        } else if self.skip_exact(crate::config::BITWISE_NOT) {
            BitwiseNot
        } else {
            return self.read_power_expr();
        };

        skip_inside_expr!(self, {
            return self.expected_something(loc::err::expected_unary_subexpr);
        });

        match self.read_factor_expr()? {
            Some(astnode) => {
                let source_region = end_source_region(self);
                let kind = ctor(Box::new(astnode));
                Ok(Some(AstNode {
                    source_region,
                    kind,
                }))
            }
            None => self.expected_something(loc::err::expected_unary_subexpr),
        }
    }

    /// Parse exponentiation or higher-priority syntax
    ///
    ///> *power_expr*             ←     *power* / *await_expr*
    ///
    /// TODO implement
    fn read_power_expr(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        self.read_await_expr()
    }
}
