use crate::{
    ast::{astnode::AstNode, Operator},
    config,
    controlflow::ControlFlow,
    error::{Error, EvalResult},
    hlir::HlirNode,
    interpreter::EvalCtx,
    loc,
    parse::Parser,
    skip_inside_expr,
    sourceregion::SourceRegion,
    ti::IntoTiResult,
    ti::TiError,
    StringType,
};

impl<'envborrow, 'ctx> Parser<'envborrow, 'ctx> {
    /// helper for binary operation functions
    pub(crate) fn read_binop<SubExprFn, CtorFn>(
        &mut self,
        ops: &[(&str, Operator)],
        subexpr_fn: SubExprFn,
        ctor_fn: CtorFn,
    ) -> Result<Option<AstNode>, Vec<Error>>
    where
        SubExprFn: Fn(&mut Self) -> Result<Option<AstNode>, Vec<Error>>,
        CtorFn: FnOnce(
            &mut Parser<'_, 'ctx>,
            SourceRegion,
            Box<AstNode>,
            Vec<(Operator, AstNode)>,
        ) -> Result<AstNode, Vec<Error>>,
    {
        let region_builder = self.begin_source_region();

        // lhs
        let first = subexpr_fn(self)?;
        if first.is_none() {
            return Ok(None);
        }
        let first = first.unwrap();

        skip_inside_expr!(self, { return Ok(Some(first)) });

        let mut v: Vec<(Operator, AstNode)> = vec![];
        while let Some(op) = self.read_operator(ops) {
            skip_inside_expr!(self, {
                return self.expected_something(loc::err::expected_rhs);
            });
            match subexpr_fn(self)? {
                Some(factor) => v.push((op, factor)),
                None => return self.expected_something(loc::err::expected_rhs),
            }
            skip_inside_expr!(self, { break });
        }

        if v.is_empty() {
            Ok(Some(first))
        } else {
            let loc = region_builder(self);
            let first = Box::new(first);
            let rest = v;
            Ok(Some(ctor_fn(self, loc, first, rest)?))
        }
    }

    /// helper for binary operation functions
    fn read_operator(&mut self, ops: &[(&str, Operator)]) -> Option<Operator> {
        for (op_str, op_enum) in ops {
            if op_str
                .chars()
                .next()
                .is_some_and(|c| c.is_ascii_punctuation())
            {
                if self.skip_exact(op_str) {
                    return Some(*op_enum);
                }
            } else if self.skip_exact_word(op_str) {
                return Some(*op_enum);
            }
        }
        None
    }
}

impl<'envborrow, 'ctx> EvalCtx<'envborrow, 'ctx> {
    // TODO: this shouldn't exist - operators should have desugared into method calls before we got here
    pub(crate) fn evaluate_in_operator(
        &mut self,
        container: &HlirNode<'ctx>,
        invert: bool,
        item: &HlirNode<'ctx>,
        source_region: &SourceRegion,
    ) -> EvalResult<'ctx> {
        let container = self.evaluate_ast(container)?;
        let item = self.evaluate_ast(item)?;
        let trait_name = StringType::from(config::CONTAINS_TRAIT_NAME);
        let method_name = StringType::from(config::CONTAINS_METHOD_NAME);

        // find the trait the function comes from
        let trait_type = self
            .env
            .ti_ctx
            .scopes
            .borrow()
            .get(&trait_name)
            .ok_or_else(|| loc::err::missing_trait_for_op(&trait_name))
            .into_tiresult(source_region)?
            .expect_trait_scheme(source_region)?
            .instantiate_with(
                &self.env.ti_ctx,
                std::slice::from_ref(&item.ty),
                source_region,
            )?;

        let imp = self.env.ti_ctx.get_trait_implementation_for_eval(
            container.ty,
            trait_type,
            source_region,
        )?;

        let (_, fn_fam) = imp.find_member_function(&method_name).ok_or_else(|| {
            TiError::new(
                loc::err::missing_method_for_op(&method_name),
                source_region.clone(),
            )
        })?;

        let params = &[container, item];
        let fn_impl = fn_fam.find_implementation(params, source_region)?;
        let result = self.evaluate_fn_impl(fn_impl, &params[..], source_region)?;
        if invert {
            result.logical_not(&self.env.ti_ctx).ok_or_else(|| {
                ControlFlow::err(
                    loc::err::internal::op_should_return_bool(
                        config::IN_OPERATOR,
                        &result.to_string(),
                    ),
                    source_region.clone(),
                )
            })
        } else {
            Ok(result)
        }
    }
}
