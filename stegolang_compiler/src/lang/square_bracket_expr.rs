use crate::{ast::astnode::AstNode, error::Error, parse::Parser};

impl<'envborrow, 'ctx> Parser<'envborrow, 'ctx> {
    // TODO implement
    pub(crate) fn read_square_bracket_expr(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        self.read_curly_brace_expr()
    }
}
