use crate::{
    ast::literal::Literal,
    config::{
        CHARACTER_BEGIN, CHARACTER_END, FALSE, HEXADECIMAL_PREFIX, OCTAL_PREFIX, STRING_BEGIN, TRUE,
    },
    error::Error,
    loc,
    parse::{IntoParseResult, Parser},
    util::RcStr,
};
use num::{bigint::ToBigInt, ToPrimitive, Zero};

impl<'envborrow, 'ctx> Parser<'envborrow, 'ctx> {
    /// consumes and converts the escape sequence not including the initial backslash
    fn read_char_escape(&mut self) -> Result<char, Vec<Error>> {
        let start_line_number = self.line_number;
        let start_column_number = self.column_number;
        let c = match self.read_char() {
            None => {
                let (short_msg, help) = loc::err::char_literal_unexpected_end_of_input();
                return self.error(short_msg, help, self.line_number, self.column_number);
            }
            Some(x) => x,
        };

        match c {
            '\'' => Ok('\''),
            '"' => Ok('"'),
            '\\' => Ok('\\'),
            'n' => Ok('\n'),
            'r' => Ok('\r'),
            't' => Ok('\t'),
            '\0' => Ok('\0'),
            // ascii escape
            'x' => self.read_char_ascii_escape(start_line_number, start_column_number),
            // unicode escape
            'u' => self.read_unicode_escape(c, start_line_number, start_column_number),
            c => Ok(c),
        }
    }

    /// after already reading the `u` character, consume and convert the rest of the escape sequence
    fn read_unicode_escape(
        &mut self,
        c: char,
        start_line_number: usize,
        start_column_number: usize,
    ) -> Result<char, Vec<Error>> {
        if !self.skip_exact("{") {
            let (short_msg, help) = loc::err::invalid_character_escape(&c.to_string());
            return self.error(short_msg, help, self.line_number, self.column_number);
        }
        let mut value: u32;
        match self.read_char() {
            None => {
                let (short_msg, help) = loc::err::char_literal_unexpected_end_of_input();
                return self.error(short_msg, help, self.line_number, self.column_number);
            }
            Some(c) => match c.to_digit(16) {
                None => {
                    let (short_msg, help) = loc::err::invalid_character_escape(&format!("u{c}"));
                    return self.error(short_msg, help, start_line_number, start_column_number);
                }
                Some(digit) => {
                    value = digit;
                }
            },
        };
        let mut finished = false;
        for _ in 0..6 {
            match self.read_char() {
                None => {
                    let (short_msg, help) = loc::err::char_literal_unexpected_end_of_input();
                    return self.error(short_msg, help, self.line_number, self.column_number);
                }
                Some('}') => {
                    finished = true;
                    break;
                }
                Some(c) => match c.to_digit(16) {
                    None => {
                        let (short_msg, help) = loc::err::char_literal_unexpected_end_of_input();
                        return self.error(short_msg, help, self.line_number, self.column_number);
                    }
                    Some(digit) => {
                        value = value * 0x10 + (digit);
                    }
                },
            }
        }
        if !finished {
            let (short_msg, help) = loc::err::invalid_character_escape("u{...");
            return self.error(short_msg, help, start_line_number, start_column_number);
        }
        Ok(unsafe { std::char::from_u32_unchecked(value) })
    }

    /// after already reading the `x` character, consume and convert the rest of the escape sequence
    fn read_char_ascii_escape(
        &mut self,
        start_line_number: usize,
        start_column_number: usize,
    ) -> Result<char, Vec<Error>> {
        let digit_a: u8 = match self.read_char() {
            None => {
                let (short_msg, help) = loc::err::char_literal_unexpected_end_of_input();
                return self.error(short_msg, help, self.line_number, self.column_number);
            }
            Some(c) => match c.to_digit(8) {
                None => {
                    let (short_msg, help) = loc::err::invalid_character_escape(&c.to_string());
                    return self.error(short_msg, help, start_line_number, start_column_number);
                }
                Some(x) => u8::try_from(x).or_else(|_| {
                    let (short_msg, help) = loc::err::invalid_character_escape(&format!("x{x}"));
                    self.error(short_msg, help, start_line_number, start_column_number)
                })?,
            },
        };
        let digit_b: u8 = match self.read_char() {
            None => {
                let (short_msg, help) = loc::err::char_literal_unexpected_end_of_input();
                return self.error(short_msg, help, self.line_number, self.column_number);
            }
            Some(x) => match x.to_digit(16) {
                None => {
                    let (short_msg, help) = loc::err::invalid_character_escape(&x.to_string());
                    return self.error(short_msg, help, start_line_number, start_column_number);
                }
                Some(x) => u8::try_from(x).or_else(|_| {
                    let (short_msg, help) =
                        loc::err::invalid_character_escape(&format!("x{digit_a}{x}"));
                    self.error(short_msg, help, start_line_number, start_column_number)
                })?,
            },
        };
        Ok((digit_a * 0x10 + digit_b) as char)
    }

    /// read a character literal
    fn read_char_literal(&mut self) -> Result<Option<char>, Vec<Error>> {
        let region_builder = self.begin_source_region();

        // opening '
        if !self.skip_exact(CHARACTER_BEGIN) {
            return Ok(None);
        }

        // check for escape
        let c = if self.skip_exact("\\") {
            self.read_char_escape()?
        } else if self.skip_exact(CHARACTER_END) {
            return Err(loc::err::empty_character_literal())
                .into_parse_result(&region_builder(self));
        } else {
            // not an escape sequence, use the character directly
            match self.get_code_by_byte(self.byte..) {
                None => {
                    let (short_msg, help) = loc::err::char_literal_unexpected_end_of_input();
                    return self.error(short_msg, help, self.line_number, self.column_number);
                }
                Some(s) => {
                    // newlines not allowed in character literals
                    if s.starts_with('\n') || s.starts_with("\r\n") {
                        return Err(loc::err::char_literal_line_break())
                            .into_parse_result(&region_builder(self));
                    }
                    let c = s.chars().next().unwrap();
                    self.move_forward(0, 1, c.len_utf8());
                    c
                }
            }
        };

        // closing '
        if !self.skip_exact(CHARACTER_END) {
            return self.expected_something(loc::err::expected_character_literal_end)?;
        }

        Ok(Some(c))
    }

    /// convert a decimal literal (integer or float) to an `AstNode`
    pub(crate) fn read_decimal_literal(&mut self) -> Result<Option<Literal>, Vec<Error>> {
        use Literal::{BigInt, Float, Int64};

        let start_line_number = self.line_number;
        let start_column_number = self.column_number;
        let start_byte = self.byte;

        // must begin with digit
        match self
            .get_code_by_byte(self.byte..)
            .unwrap_or("")
            .chars()
            .next()
        {
            None => return Ok(None),
            Some(c) if !c.is_numeric() => return Ok(None),
            _ => {}
        }

        // integer part
        {
            let mut chars = self.get_code_by_byte(self.byte..).unwrap_or("").chars();
            let mut columns_read = 0;
            let mut bytes_read = 0;
            loop {
                match chars.next() {
                    None => break,
                    Some(c) => {
                        if c.is_numeric() {
                            columns_read += 1;
                            bytes_read += c.len_utf8();
                        } else if c == 'e' || c == 'E' {
                            break;
                        } else if c.is_alphanumeric() {
                            let (short_msg, help) = loc::err::invalid_digit(10, c);
                            return self.error(
                                short_msg,
                                help,
                                self.line_number,
                                self.column_number,
                            );
                        } else if c == '_' {
                            columns_read += 1;
                            bytes_read += c.len_utf8();
                        } else {
                            break;
                        }
                    }
                }
            }
            self.move_forward(0, columns_read, bytes_read);
        }

        // the integer part is required
        if self.byte <= start_byte {
            return Ok(None);
        }

        // optional fractional part
        let mut is_float = false;
        if self.skip_exact(".") {
            is_float = true;
            self.read_fractional_part()?;
        }

        // optional exponent
        if let Some(Some(true)) = self
            .get_code_by_byte(self.byte..)
            .map(|s| s.chars().next().map(|c| (c == 'e') || (c == 'E')))
        {
            self.read_exponent_part(&mut is_float)?;
        }

        let s = self.get_code_by_byte(start_byte..self.byte).unwrap();
        if is_float {
            match s.parse::<f64>() {
                Err(error) => {
                    let (short_msg, help) = loc::err::from_parse_float_error(&error);
                    self.error(short_msg, help, start_line_number, start_column_number)
                }
                Ok(f) => Ok(Some(Float(f))),
            }
        } else {
            match s.parse::<num::BigInt>() {
                Err(error) => {
                    let (short_msg, help) = loc::err::from_parse_bigint_error(&error);
                    self.error(short_msg, help, start_line_number, start_column_number)
                }
                Ok(bigint) => match bigint.to_i64() {
                    Some(as_i64) => Ok(Some(Int64(as_i64))),
                    None => Ok(Some(BigInt(bigint))),
                },
            }
        }
    }

    /// read exponent part of decimal number. assumes you already consumed the 'e''
    fn read_exponent_part(&mut self, is_float: &mut bool) -> Result<(), Vec<Error>> {
        self.move_forward(0, 1, 1);
        *is_float = true;
        if !self.skip_exact("+") {
            self.skip_exact("-");
        }
        // optional sign
        // exponent digits
        {
            let mut chars = self.get_code_by_byte(self.byte..).unwrap_or("").chars();
            let mut columns_read = 0;
            let mut bytes_read = 0;
            loop {
                match chars.next() {
                    None => break,
                    Some(c) => {
                        if c.is_numeric() {
                            columns_read += 1;
                            bytes_read += c.len_utf8();
                        } else if c.is_alphanumeric() {
                            let (short_msg, help) = loc::err::invalid_digit(10, c);
                            return self.error(
                                short_msg,
                                help,
                                self.line_number,
                                self.column_number,
                            );
                        } else {
                            break;
                        }
                    }
                }
            }
            if bytes_read == 0 {
                let (short_msg, help) = loc::err::missing_exponent();
                return self.error(short_msg, help, self.line_number, self.column_number);
            }
            self.move_forward(0, columns_read, bytes_read);
        }
        Ok(())
    }

    /// read fractional part of decimal. assumes you already consumed the decimal point
    fn read_fractional_part(&mut self) -> Result<(), Vec<Error>> {
        let mut chars = self.get_code_by_byte(self.byte..).unwrap_or("").chars();
        let mut columns_read = 0;
        let mut bytes_read = 0;
        loop {
            match chars.next() {
                None => break,
                Some(c) => {
                    if c.is_numeric() {
                        columns_read += 1;
                        bytes_read += c.len_utf8();
                    } else if c == 'e' || c == 'E' {
                        break;
                    } else if c.is_alphanumeric() {
                        let (short_msg, help) = loc::err::invalid_digit_fractional(10, c);
                        return self.error(short_msg, help, self.line_number, self.column_number);
                    } else {
                        break;
                    }
                }
            }
        }
        self.move_forward(0, columns_read, bytes_read);
        Ok(())
    }

    /// read a base-16 integer literal
    fn read_hexadecimal_integer_literal(&mut self) -> Result<Option<Literal>, Vec<Error>> {
        self.read_integer_literal(HEXADECIMAL_PREFIX, 16)
    }

    // read a base-n integer literal
    fn read_integer_literal(
        &mut self,
        prefix: &str,
        base: u32,
    ) -> Result<Option<Literal>, Vec<Error>> {
        use Literal::{BigInt, Int64};
        let start_line_number = self.line_number;
        let start_column_number = self.column_number;
        // must begin with 0x
        if !self.skip_exact(prefix) {
            return Ok(None);
        }
        let mut bigint_value = num::BigInt::zero();
        let chars = self.get_code_by_byte(self.byte..).unwrap_or("").chars();
        {
            let mut columns_read = 0;
            let mut bytes_read = 0;
            for c in chars {
                if c.is_alphanumeric() {
                    if let Some(digit) = c.to_digit(base) {
                        bigint_value = bigint_value * base + digit.to_bigint().unwrap();
                        columns_read += 1;
                        bytes_read += c.len_utf8();
                    } else {
                        let (short_msg, help) = loc::err::invalid_digit(base, c);
                        return self.error(short_msg, help, self.line_number, self.column_number);
                    }
                } else if bytes_read == 0 {
                    let (short_msg, help) = loc::err::parse_int_error(base);
                    return self.error(short_msg, help, start_line_number, start_column_number);
                } else if c == '_' {
                    columns_read += 1;
                    bytes_read += c.len_utf8();
                } else {
                    break;
                }
            }
            self.move_forward(0, columns_read, bytes_read);
        }
        match bigint_value.to_i64() {
            Some(as_i64) => Ok(Some(Int64(as_i64))),
            None => Ok(Some(BigInt(bigint_value))),
        }
    }

    /// read a base-8 integer literal
    pub(crate) fn read_octal_integer_literal(&mut self) -> Result<Option<Literal>, Vec<Error>> {
        self.read_integer_literal(OCTAL_PREFIX, 8)
    }

    ///> *literal* ← *boolean_literal* &#124; *char_literal* &#124; *string_literal* &#124; *number_literal*
    ///
    /// TODO: single-quoted strings
    ///     means `StringLiteral` is not a concrete type. 1-character strings default to character
    ///     similarly to how integers default when type inference can't be sure
    ///
    pub(crate) fn read_literal(&mut self) -> Result<Option<Literal>, Vec<Error>> {
        use Literal::{Bool, Character, String};
        if self.skip_exact_word(TRUE) {
            Ok(Some(Bool(true)))
        } else if self.skip_exact_word(FALSE) {
            Ok(Some(Bool(false)))
        } else if let Some(x) = self.read_char_literal()? {
            Ok(Some(Character(x)))
        } else if let Some(x) = self.read_string_literal()? {
            Ok(Some(String(x)))
        } else if let Some(x) = self.read_hexadecimal_integer_literal()? {
            Ok(Some(x))
        } else if let Some(x) = self.read_octal_integer_literal()? {
            Ok(Some(x))
        } else if let Some(x) = self.read_decimal_literal()? {
            Ok(Some(x))
        } else {
            Ok(None)
        }
    }

    /// consumes and converts a string escape sequence not including the initial backslash
    fn read_string_escape(&mut self, s: &mut String) -> Result<(), Vec<Error>> {
        match self.peek_grapheme() {
            None => {
                return self.expected_something(loc::err::string_literal_unexpected_end_of_input)
            }
            Some(grapheme) => {
                if grapheme.trim().is_empty() {
                    self.skip_whitespace();
                } else {
                    (*s).push(self.read_char_escape()?);
                }
            }
        }
        Ok(())
    }

    /// read a string literal
    fn read_string_literal(&mut self) -> Result<Option<RcStr>, Vec<Error>> {
        // opening "
        if !self.skip_exact(STRING_BEGIN) {
            return Ok(None);
        }

        let mut result = String::new();

        loop {
            match self.read_char() {
                None => {
                    return self
                        .expected_something(loc::err::string_literal_unexpected_end_of_input)
                }
                // closing "
                Some('"') => break,
                Some('\\') => self.read_string_escape(&mut result)?,
                Some(c) => result.push(c),
            }
        }
        Ok(Some(RcStr::from(result)))
    }
}
