//! code for parsing, interpreting, and compiling let statements
use crate::{
    ast::astnode::{AstNode, AstNodeKind::Let},
    config::{LET, LET_EQ},
    error::Error,
    error::EvalResult,
    hlir::HlirNode,
    interpreter::EvalCtx,
    loc,
    parse::Parser,
    pattern::{StegoPattern, StegoPatternKind},
    skip_read_and_require, skip_required,
    ti::Substitutions,
    typedvalue::{TypedValue, TypedValueKind},
    StringType,
};

impl<'envborrow, 'ctx> Parser<'envborrow, 'ctx> {
    ///> *let_stmt* ← `let` *pattern* `=` *named_expr*
    ///
    pub(crate) fn read_let_stmt(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        let region_builder = self.begin_source_region();

        // has to begin with "let"
        if !self.skip_exact_word(LET) {
            return Ok(None);
        }

        // then you need a pattern
        let pattern = skip_read_and_require!(self, read_pattern, {
            return self.expected_something(loc::err::let_pattern_expected);
        });

        // then the equals
        skip_required!(self, skip_exact, LET_EQ, {
            return self.expected_something(loc::err::let_eq_expected);
        });

        match self.skip_whitespace() {
            crate::lang::AdvanceResult::Continue => {}
            _ => return self.expected_something(loc::err::let_expr_expected),
        }

        // finally the value
        // TODO: add to scope even if value errors out
        let value_expr = skip_read_and_require!(self, read_named_expr, {
            return self.expected_something(loc::err::let_expr_expected);
        });

        // TODO: more general destructuring
        let StegoPatternKind::Capture(name) = &pattern.kind else {
            todo!()
        };
        self.env.scope.insert(
            StringType::from(name),
            TypedValue {
                ty: self.env.ti_ctx.mk_nothing(),
                kind: TypedValueKind::PlaceHolder,
            },
        );

        let source_region = region_builder(self);
        let kind = Let {
            pattern: Box::new(pattern),
            value_expr: Box::new(value_expr),
        };

        Ok(Some(AstNode {
            source_region,
            kind,
        }))
    }
}

impl<'envborrow, 'ctx> EvalCtx<'envborrow, 'ctx> {
    /// evaluate a let
    pub(crate) fn evaluate_let(
        &mut self,
        pattern: &StegoPattern,
        value_expr: &HlirNode<'ctx>,
        substitutions: &Substitutions<'ctx>,
    ) -> EvalResult<'ctx> {
        let mut x = self.evaluate_ast(value_expr)?;
        x.generalize(substitutions, value_expr.inferred_type, &self.env.ti_ctx)?;
        match &pattern.kind {
            StegoPatternKind::Capture(name) => self.insert_value(name, x),

            // TODO: literal patterns are useless in a let, so produce an error
            StegoPatternKind::Literal(_) => todo!(),

            // TODO: or patterns get complicated. You have to guarantee that
            // all subpatterns bind the same names to the same types
            StegoPatternKind::Or(_) => todo!(),

            StegoPatternKind::Named(..) => todo!(),
        };
        Ok(TypedValue::nothing(&self.env.ti_ctx))
    }
}
