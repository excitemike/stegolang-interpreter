use super::AdvanceResult;
use crate::{ast::astnode::AstNode, error::Error, loc, parse::Parser};
impl<'envborrow, 'ctx> Parser<'envborrow, 'ctx> {
    /// read a program
    ///> *program* ← *statement_block*<sup>\?</sup> EOF
    ///
    pub(crate) fn read_program(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        self.in_new_scope(|parser| {
            debug_assert!(parser.get_indentation_level() == &1);
            let make_source_region = parser.begin_source_region();

            match parser.skip_comments_and_whitespace()? {
                AdvanceResult::NewStatement | AdvanceResult::EndBlock => {}
                AdvanceResult::Continue => {
                    let (short_msg, help) = loc::err::unexpected_indent();
                    parser.record_warning(short_msg, help, make_source_region(parser));
                }
                AdvanceResult::EndProgram => return Ok(None),
            }

            let statements = parser.read_statement_block()?;

            match parser.skip_comments_and_whitespace()? {
                AdvanceResult::EndProgram => {}
                _ => return parser.expected_something(loc::err::not_statement),
            }

            Ok(statements)
        })
    }
}
