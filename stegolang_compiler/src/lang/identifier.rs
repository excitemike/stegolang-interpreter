use unicode_segmentation::UnicodeSegmentation;

use crate::{
    config::{is_reserved_word, IDENTIFIER_REGEX},
    controlflow::err,
    error::EvalResult,
    interpreter::EvalCtx,
    loc,
    parse::Parser,
    sourceregion::SourceRegion,
    ti::{ITy, Substitutions},
    typedvalue::TypedValue,
    StringType,
};

impl<'envborrow, 'ctx> Parser<'envborrow, 'ctx> {
    /// consume the next token as a name
    pub(crate) fn read_name(&mut self) -> Option<String> {
        let code = self.get_code();
        let s = code.get_by_byte(self.byte..);
        if let Some(s) = s {
            if let Some(m) = IDENTIFIER_REGEX.find(s) {
                debug_assert!(m.start() == 0);
                let s = &s[..m.end()];
                if is_reserved_word(s) {
                    None
                } else if IDENTIFIER_REGEX.is_match(s) {
                    let copy = s.to_string();
                    let columns = s.graphemes(true).count();
                    let bytes = s.len();
                    self.move_forward(0, columns, bytes);
                    Some(copy)
                } else {
                    None
                }
            } else {
                None
            }
        } else {
            None
        }
    }
}

impl<'envborrow, 'ctx> EvalCtx<'envborrow, 'ctx> {
    pub fn eval_identifier(
        &mut self,
        name: &StringType,
        expected_ty: ITy<'ctx>,
        substitutions: &Substitutions<'ctx>,
        source_region: &SourceRegion,
    ) -> EvalResult<'ctx> {
        let result = match self.get(name) {
            Some(x) => Ok(TypedValue::clone(&x)),
            None => err(
                loc::err::identifier_not_in_scope(name),
                source_region.clone(),
            ),
        }?;
        let result = result
            .instantiate(substitutions, &self.env.ti_ctx, source_region)?
            .unwrap_or(result);

        // `result.ty` needs to be a specialization of `expected_scheme`
        assert!(
            result.ty.could_be_instance(expected_ty),
            "{} should be a specialization of {expected_ty}",
            result.ty
        );
        Ok(result)
    }
}
