use crate::{
    ast::astnode::AstNode,
    ast::astnode::AstNodeKind::{self, Statements},
    config,
    controlflow::ControlFlow,
    error::{Error, EvalResult},
    fntypes::fncode::FnCode,
    hlir::HlirNode,
    interpreter::EvalCtx,
    loc::{self, err},
    parse::Parser,
    pattern::StegoPattern,
    skip_inside_expr,
    sourceregion::SourceRegion,
    stegoobject::FnImpl,
    ti::ITy,
    typedvalue::{TypedValue, TypedValueKind},
    StringType,
};
use std::{cell::RefCell, collections::BTreeSet, rc::Rc};

impl<'envborrow, 'ctx> Parser<'envborrow, 'ctx> {
    /// read a lambda body after scope and indentation has been pushed
    #[allow(clippy::only_used_in_recursion)] // unused is needed to match expected type signature
    fn read_indented_lambda_body<MakeSourceRegionFn>(
        &mut self,
        pushed_indent: bool,
        unused: MakeSourceRegionFn,
    ) -> Result<AstNode, Vec<Error>>
    where
        MakeSourceRegionFn: FnOnce(&Parser) -> SourceRegion,
    {
        if self.is_inside_function {
            if !pushed_indent {
                return self.expected_something(err::expected_fn_body);
            }

            // function body
            match self.read_statement_block()? {
                Some(node) => {
                    if let AstNode {
                        kind: Statements { .. },
                        ..
                    } = node
                    {
                        Ok(node)
                    } else {
                        self.expected_something(err::expected_fn_body)
                    }
                }
                _ => self.expected_something(err::expected_fn_body),
            }
        } else {
            self.is_inside_function = true;
            let result = self.read_indented_lambda_body(pushed_indent, unused);
            self.is_inside_function = false;
            result
        }
    }

    /// read in a function/lambda body
    pub(super) fn read_lambda_body(&mut self) -> Result<Rc<RefCell<AstNode>>, Vec<Error>> {
        // skip whitespace to push indent for block
        let body = self.try_push_indentation_level(
            Parser::read_indented_lambda_body,
            self.begin_source_region(),
        )??;
        let body = Rc::new(RefCell::new(body));
        Ok(body)
    }

    ///> *lambda_expr* ← `\` *param_list* `=` *statements*
    ///
    pub(crate) fn read_lambda_expr(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        let make_region = self.begin_source_region();

        // start of lambda
        if !self.skip_exact(config::LAMBDA_BEGIN) {
            return self.read_logical_or_expr();
        }

        skip_inside_expr!(self, {
            return self.expected_something(err::lambda_eq_expected);
        });

        // param_list
        let mut params = Vec::new();
        let Some(first_pat) = self.read_pattern()? else {
            return self.expected_something(err::lambda_pattern_expected);
        };
        params.push(first_pat);
        while let Some(param) = self.read_pattern()? {
            params.push(param);
        }
        let params = Rc::new(RefCell::new(params));

        skip_inside_expr!(self, {
            return self.expected_something(err::lambda_sep_expected);
        });

        // then the equals
        if !self.skip_exact(config::LAMBDA_SEP) {
            return self.expected_something(err::lambda_sep_expected);
        }
        let body = self.read_lambda_body()?;
        let source_region = make_region(self);

        Ok(Some(AstNode::new(
            source_region,
            AstNodeKind::Lambda { body, params },
        )))
    }
}
impl<'envborrow, 'ctx> EvalCtx<'envborrow, 'ctx> {
    /// evaluate lambda expression
    pub(crate) fn eval_lambda(
        &mut self,
        inferred_type: ITy<'ctx>,
        body: Rc<RefCell<HlirNode<'ctx>>>,
        captures: &BTreeSet<StringType>,
        params: Rc<RefCell<Vec<StegoPattern>>>,
        source_region: &SourceRegion,
        placeholder_name: &Option<StringType>,
    ) -> EvalResult<'ctx> {
        if let Some(name) = placeholder_name {
            self.env.scope.insert(
                StringType::from(name),
                TypedValue {
                    ty: inferred_type,
                    kind: TypedValueKind::PlaceHolder,
                },
            );
        }

        let captures = self
            .env
            .scope
            .get_upvalues(captures.iter().cloned())
            .map_err(|x| ControlFlow::err(x, source_region.clone()))?;

        let fn_impl = FnImpl {
            captures,
            code: FnCode::Statements(body),
            pattern_set: params,
        };

        let debug_name = loc::lambda_created_at(
            &source_region.file_label,
            source_region.start_line_number,
            source_region.start_column_number,
        )
        .into();

        Ok(TypedValue::func(inferred_type, debug_name, fn_impl))
    }
}
