use crate::{
    ast::astnode::{AstNode, AstNodeKind::ReturnStmt},
    config::RETURN,
    error::Error,
    loc,
    parse::Parser,
    skip_read_and_require,
};

impl<'envborrow, 'ctx> Parser<'envborrow, 'ctx> {
    ///> *return_stmt* ← `return` *named_expr*<sup>?</sup>
    ///
    pub(crate) fn read_return_stmt(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        let start_line_number = self.line_number;
        let start_column_number = self.column_number;

        // has to begin with "return"
        if !self.skip_exact_word(RETURN) {
            return Ok(None);
        }

        if !self.is_inside_function {
            let (short_msg, help) = loc::err::return_outside_function();
            return self.error(short_msg, help, start_line_number, start_column_number);
        }

        let value = skip_read_and_require!(self, read_named_expr, {
            let source_region = self.make_source_region(start_line_number, start_column_number);
            let kind = ReturnStmt(None);
            return Ok(Some(AstNode::new(source_region, kind)));
        });

        let source_region = self.make_source_region(start_line_number, start_column_number);
        let kind = ReturnStmt(Some(Box::new(value)));
        Ok(Some(AstNode::new(source_region, kind)))
    }
}
