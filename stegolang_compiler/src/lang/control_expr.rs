use crate::{ast::astnode::AstNode, error::Error, parse::Parser};

impl<'envborrow, 'ctx> Parser<'envborrow, 'ctx> {
    ///> *control_expr* ← *do_expr* / *if_expr* / *lambda_expr* / *match_expr* / *for_expr* / *while_expr* / *or_expr*
    ///
    /// TODO implement
    pub(crate) fn read_control_expr(&mut self) -> Result<Option<AstNode>, Vec<Error>> {
        if let x @ Some(..) = self.read_if_expr()? {
            return Ok(x);
        }
        if let x @ Some(..) = self.read_do_expr()? {
            return Ok(x);
        }
        if let x @ Some(..) = self.read_lambda_expr()? {
            return Ok(x);
        }

        self.read_logical_or_expr()
    }
}
