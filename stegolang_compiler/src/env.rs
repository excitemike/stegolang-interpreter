use crate::{
    builtin::insert_builtins,
    ti::{arenas::Arenas, Context, TiResult},
    typedvalue::TypedValue,
    util::{output_stream::OutputStream, NestedScope},
    StringType,
};

/// environment that is run though parser, type inference, and desugaring passes
/// to produce a something suitable for code generation or evaluation
pub struct Env<'ctx> {
    /// track what's in scope as we walk ASTs
    pub(crate) scope: NestedScope<StringType, TypedValue<'ctx>>,

    /// type inference context - stays part of the execution environment
    /// for now because the repl can continuously define new stuff
    pub(crate) ti_ctx: Context<'ctx>,

    /// where to output messages
    pub(crate) out_stream: &'ctx mut dyn OutputStream,

    /// where to output error messages
    pub(crate) err_stream: &'ctx mut dyn OutputStream,
}

impl<'ctx> Env<'ctx> {
    /// create a fresh environment, including built-ins
    pub fn new(
        arenas: &'ctx Arenas<'ctx>,
        out_stream: &'ctx mut dyn OutputStream,
        err_stream: &'ctx mut dyn OutputStream,
    ) -> TiResult<Env<'ctx>> {
        let mut scope = NestedScope::new();
        let ti_ctx = Context::new(arenas);
        insert_builtins(&ti_ctx, &mut scope)?;
        Ok(Env {
            scope,
            ti_ctx,
            out_stream,
            err_stream,
        })
    }
    /// enter a fresh environment
    #[allow(dead_code)] // only used in tests at the moment
    pub fn enter<F, R>(
        out_stream: &'ctx mut dyn OutputStream,
        err_stream: &'ctx mut dyn OutputStream,
        callback: F,
    ) -> TiResult<R>
    where
        F: FnOnce(&mut Env<'_>) -> R,
    {
        let arenas = Arenas::new();
        let mut scope = NestedScope::new();
        let ti_ctx = Context::new(&arenas);
        insert_builtins(&ti_ctx, &mut scope)?;
        let mut env = Env {
            scope,
            ti_ctx,
            out_stream,
            err_stream,
        };
        Ok(callback(&mut env))
    }
}
