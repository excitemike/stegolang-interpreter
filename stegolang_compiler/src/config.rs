use lazy_static::lazy_static;
use regex::Regex;
use unicode_segmentation::UnicodeSegmentation;

// TODO separate module for strings
// TODO if I keep using string_cache, I should do compile-time atoms (see https://docs.rs/string_cache/0.8.2/string_cache/index.html)

/// Name of the method used for "+"
pub const ADD_METHOD_NAME: &str = "add";

/// Name of trait used for "+"
pub const ADD_TRAIT_NAME: &str = "Add";

/// operator used for assignment
pub const ASSIGNMENT_OPERATOR: &str = r"=";

pub const BIGINT_TYPE_NAME: &str = "BigInt";

pub const BITWISE_NOT: &str = "~";
pub const BLOCK_COMMENT_END: &str = "*/";
pub const BLOCK_COMMENT_START: &str = "/*";
pub const BOM: &str = "\u{EF}\u{BB}\u{BF}";

/// name of the Boolean type
pub const BOOLEAN: &str = "Bool";

pub const CHAR_TYPE_NAME: &str = "Char";
pub const CLOSE_CURLY: &str = "}";
pub const CHARACTER_BEGIN: &str = "'";
pub const CHARACTER_END: &str = "'";

pub const CLOSE_PAREN: &str = ")";
pub const CLOSE_SQUARE: &str = "]";

pub const CONTAINS_METHOD_NAME: &str = "contains";
pub const CONTAINS_TRAIT_NAME: &str = "Contains";
pub const COMPARE_TRAIT_NAME: &str = "Cmp";

/// marks a type annotation (declaration) statement
pub const DEC: &str = "dec";

pub const DICT_TYPE_CONSTRUCTOR_NAME: &str = "Dict";
pub const DIVIDE_METHOD_NAME: &str = "div";
pub const DIVIDE_OPERATOR: &str = "/";
pub const DIVIDE_TRAIT_NAME: &str = "Div";

/// separate condition and guarded code in a while expression/statement
pub const DO: &str = r"do";

#[allow(dead_code)] // TODO: implement dot operator
pub const DOT: &str = ".";

/// separates if clauses
pub const ELSE: &str = r"else";

pub const EQUALS_METHOD_NAME: &str = "eq";
pub const EQ_OPERATOR: &str = "==";
pub const EQUALS_TRAIT_NAME: &str = "Eq";

pub const FALSE: &str = "false";

pub const FLOAT64_TYPE_NAME: &str = "Float64";

/// begins a function definition
pub const FN_KEYWORD: &str = "fn";

/// in a fn statement, separates the name and function body
pub const FN_EQ: &str = "=";

/// Type name for functions
pub const FN_TYPE_NAME: &str = "Fn";

pub const GREATERTHAN_METHOD_NAME: &str = "gt";

pub const GT_OPERATOR: &str = ">";
pub const GTE_OPERATOR: &str = ">=";

/// prefix that identifies hexadecimal numbers
pub const HEXADECIMAL_PREFIX: &str = "0x";

/// begins an if expression/statement
pub const IF: &str = r"if";

/// parameter name to use when we don't care what the parameter is named
pub const IGNORE_PARAM: &str = r"_";

pub const IN_OPERATOR: &str = "in";

pub const IS_KEYWORD: &str = "is";

/// how to display the built-in Int64 type
pub const INT64_TYPE_NAME: &str = "Int64";

/// what goes between key and value in dictionary literals
pub const KEY_VALUE_SEPARATOR: &str = r":";

/// used in destructuring to capture multiple key value pairs
///
/// For example:
///
/// ```stegolang
/// let {some_key: value_capture, **others} = some_dict
/// ```
pub const KWSPREAD_OPERATOR: &str = "**";

pub const LAMBDA_BEGIN: &str = "\\";

/// how we refer to lambda expressions in error messages
pub const LAMBDA_NAME: &str = "Lambda";

pub const LAMBDA_SEP: &str = "=";

/// The name of this programming language
pub const LANGUAGE_NAME: &str = "Stegolang";

pub const LESSTHAN_METHOD_NAME: &str = "lt";

/// begins a let statement
pub const LET: &str = r"let";

/// in a let statement, separates the name and expression to assign to it
pub const LET_EQ: &str = r"=";

pub const LINE_COMMENT_START: &str = "//";
pub const LINES_PER_FILE_ESTIMATE: usize = 400;
pub const LOGICAL_NOT: &str = "!";
pub const LT_OPERATOR: &str = "<";
pub const LTE_OPERATOR: &str = "<=";
pub const MAX_ERRORS_TO_DISPLAY: u8 = 20;
pub const MAX_WARNINGS_TO_DISPLAY: u8 = 20;
pub const MINUS: &str = "-";
pub const MULTIPLY_METHOD_NAME: &str = "mul";
pub const MULTIPLY_OPERATOR: &str = "*";
pub const MULTIPLY_TRAIT_NAME: &str = "Mul";

/// separates name from value in named expressions
pub const NAMED_EXPR_OP: &str = r"@";

/// separates name from subpattern in named patterns
pub const NAMED_PATTERN_OP: &str = r"@";

pub const NEQ_OPERATOR: &str = "!=";
pub const NOT_IN_OPERATOR: &str = "not in";

/// a way of referring to the type of a lack of a value
pub const NOTHING_TYPE_NAME: &str = "Nothing";

/// a way of referring to the lack of a value
pub const NOTHING_VALUE: &str = "nothing";

/// prefix that identifies octal numbers
pub const OCTAL_PREFIX: &str = "0o";

pub const OPEN_CURLY: &str = "{";
pub const OPEN_PAREN: &str = "(";
pub const OPEN_SQUARE: &str = "[";

pub const PATTERN_SEPARATOR: &str = "|";
pub const PLUS: &str = "+";

#[allow(dead_code)] // TODO: use print trait
pub const PRINT_TRAIT_NAME: &str = "Print";

pub const REMAINDER_METHOD_NAME: &str = "rem";
pub const REMAINDER_OPERATOR: &str = "%";
pub const REMAINDER_TRAIT_NAME: &str = "Rem";
pub const REPL_EXIT_COMMAND: &str = "quit";

/// "filename" of the repl
pub const REPL_FILE: &str = "repl";

pub const REPL_LINE_DEFAULT_CAPACITY: usize = 80;

/// begins return statement
pub const RETURN: &str = "return";

pub const SELF_TYPE_NAME: &str = "Self";
pub const SEQUENCE_DELIM: &str = ",";
pub const SET_TYPE_CONSTRUCTOR_NAME: &str = "Set";
pub const SHEBANG: &str = "#!";

/// In destructuring, packs many items into a sequence
///
/// For example:
///
/// ```stegolang
/// let (first, *others, last) = some_tuple
/// ```
///
/// As an expression, shorthand for unpacking a sequence into multiple items.
///
/// For example:
///
/// ```stegolang
/// let items = [first, *others, last]
/// ```
pub const SPREAD_OPERATOR: &str = r"*";

pub const STATEMENT_SEPARATOR: &str = ";";
pub const STRING_BEGIN: &str = r#"""#;
pub const STRING_END: &str = r#"""#;

/// how to display the built-in string type
pub const STRING_TYPE_NAME: &str = "String";

pub const SUBTRACT_METHOD_NAME: &str = "sub";
pub const SUBTRACT_TRAIT_NAME: &str = "Sub";
pub const TAB_STOP: usize = 4;

/// separate condition and guarded code in an if expression/statement
pub const THEN: &str = r"then";

pub const TRUE: &str = "true";

/// marks a statement that creates a type
pub const TY: &str = "ty";

/// marks a statement that creates a typeclass
pub const TYCL: &str = "tycl";

/// typeof operator
pub const TYPEOF: &str = "typeof";

/// use statement
pub const USE: &str = r"use";

pub const VEC_TYPE_CONSTRUCTOR_NAME: &str = "Vec";

const IDENTIFIER_START_CHAR_REGEX_STR: &str = r"[_\p{Alphabetic}\p{Emoji_Presentation}\p{Extended_Pictographic}\p{Join_Control}--\d]|[\p{Diacritic}--\p{Grapheme_Base}]";
const IDENTIFIER_CHAR_REGEX_STR: &str = r"[_\d\p{Alphabetic}\p{Emoji_Presentation}\p{Extended_Pictographic}\p{Join_Control}]|[\p{Diacritic}--\p{Grapheme_Base}]";

/// hexadecimal numbers
pub const HEXADECIMAL_NUMBER_REGEX_STR: &str = r"0x[0-9A-Fa-f]+";

/// octal numbers
const OCTAL_NUMBER_REGEX_STR: &str = r"0o[0-7]+";

/// decimal, possibly-floating-point numbers
const DECIMAL_NUMBER_REGEX_STR: &str = r"[0-9]*\.?[0-9]+(?:[eE][-+]?[0-9]+)?";

lazy_static! {
    pub static ref BLOCK_COMMENT_START_COLS: usize = BLOCK_COMMENT_START.graphemes(true).count();
    pub static ref BLOCK_COMMENT_START_BYTES: usize = BLOCK_COMMENT_START.len();
    pub static ref BLOCK_COMMENT_END_COLS: usize = BLOCK_COMMENT_END.graphemes(true).count();
    pub static ref BLOCK_COMMENT_END_BYTES: usize = BLOCK_COMMENT_END.len();

    /// reserved words in stegolang
    pub static ref KEYWORDS: Vec<&'static str> = {
        let mut kwords = vec![
        FN_TYPE_NAME,
        "and",
        "async",
        "await",
        "break",
        "continue",
        DEC,
        DO,
        "else",
        FALSE,
        "fn",
        "for",
        "get",
        IF,
        "imm",
        IN_OPERATOR,
        "inf",
        IS_KEYWORD,
        LET,
        "match",
        "matching",
        "mod",
        "mut",
        "namespace",
        "nan",
        "not",
        NOT_IN_OPERATOR,
        "or",
        "otherwise",
        "readonly",
        "return",
        "set",
        "struct",
        THEN,
        TRUE,
        TY,
        TYCL,
        TYPEOF,
        "where",
        "while",
        "xor",
        ];
        kwords.sort_unstable();
        kwords
    };

    /// number literal regex with groups inside for hexadecimal numbers,
    /// octal numbers, decimal numbers (floating point or integer)
    pub static ref NUMBER_REGEX_STR: String = format!(
        r"(?:({})|({})|({}))\b",
        HEXADECIMAL_NUMBER_REGEX_STR,
        OCTAL_NUMBER_REGEX_STR,
        DECIMAL_NUMBER_REGEX_STR,
    );

    /// regex that matches keywords
    pub static ref KEYWORDS_REGEX_STR: String = format!(r"(?:{})\b", KEYWORDS.join("|").replace(' ', r"\x20"));

    /// regex that matches runs of punctuation/math symbols used in stegolang
    pub static ref SYMBOL_REGEX_STR: String = format!("{}|{}|{}|{}|{}|{}|{}|{}|{}",
        // these characters: (){{}}[],:;~.?@
        r"[(){}\[\],:;~\.\?@]",

        // = or ==
        "={1,2}",

        // *,**,*=,**=
        r"\*{1,2}=?",

        // /,/=
        "/=?",

        // +,+=,-,-=,^,^=,%,%=
        r"[\-+\^%!]=?",

        // &,&=,&&=
        "&{1,2}=?",

        // |,|=,||=
        r"\|{1,2}=?",

        // >,>>,>>>,>=,>>=,>>>=
        ">{1,3}=?",

        // <,<<,<=,<<=
        "<<?=?",
    );

    /// This link describes more character class options if I need to revisit this:
    /// <https://github.com/rust-lang/regex/blob/master/UNICODE.md#rl12-properties>
    pub static ref IDENTIFIER_REGEX_STR: String = format!("(?:{})(?:{})*", IDENTIFIER_START_CHAR_REGEX_STR, IDENTIFIER_CHAR_REGEX_STR);

    /// regex for matching any token and categorizing it
    /// refer to the TOKEN_MATCH_GROUP_* constants for interpreting the results
    /// TODO: format strings, raw strings, byte strings, regular expressions
    pub static ref TOKEN_REGEX_STR: String = format!(
        r#"(?x)
        # not captured - run of space all the way to end of input
        (?:\s+$)
        |
        # group 1 - indentation
        (
            # runs of whitespace that include a newline
            (?:\s*\n\s*)
            |
            # beginning of file indent
            (?:^[\s--\n]+)
        )
        |
        # not captured - other whitespace
        (?:\s+)
        |
        # group 2 - keywords
        ({})
        |
        # group 3 - BOM
        ({})
        |
        # group 4 - identifiers
        ({})
        |
        # group 5-7 -- 5=hex, 6=octal, 9=decimal
        (?:{})
        |
        # group 8 - line comment
        ({}[^\n]*\n?)
        |
        # group 9 - block comment start
        ({})
        |
        # group 10 - block comment end
        ({})
        |
        # group 11 - character literal
        (
            '(?:
                [^\\\n\r\t']
                |(?:\\')
                |(?:\\")
                |(?:\\x[0-7][0-9A-Fa-f])
                |(?:\\n)
                |(?:\\r)
                |(?:\\t)
                |(?:\\\\)
                |(?:\\0)
                |(?:\\u\{{[0-9A-Fa-f]{{1,6}}\}})
            )'
        )
        |
        # group 12 - string literal
        (
            "(?:
                [^\\"]
                |(?:\\')
                |(?:\\")
                |(?:\\x[0-7][0-9A-Fa-f])
                |(?:\\n)
                |(?:\\r)
                |(?:\\t)
                |(?:\\\\)
                |(?:\\0)
                |(?:\\u\{{[0-9A-Fa-f]{{1,6}}\}})
                |(?:\\\n\s*)
            )*"
        )
        |
        # group 13 - shebang line
        ({}[^\n]*\n?)
        |
        # group 14 - symbols
        ({})
        |
        # group 15 - error case. used to force pattern to match at the search position
        (.)"#,
        *KEYWORDS_REGEX_STR,
        BOM,
        *IDENTIFIER_REGEX_STR,
        *NUMBER_REGEX_STR,
        regex::escape(LINE_COMMENT_START),
        regex::escape(BLOCK_COMMENT_START),
        regex::escape(BLOCK_COMMENT_END),
        regex::escape(SHEBANG),
        *SYMBOL_REGEX_STR,
    );

    /// token regex. see comments on `TOKEN_REGEX_STR`
    pub static ref TOKEN_REGEX: Regex = Regex::new(&TOKEN_REGEX_STR).unwrap();

    /// indentifier regex
    pub static ref IDENTIFIER_REGEX: Regex = Regex::new(&format!(r"^(?:{})", *crate::config::IDENTIFIER_REGEX_STR)).unwrap();

    /// identify valid identifier characters
    pub static ref IDENTIFIER_CHAR_REGEX: Regex = Regex::new(&format!("^{}", crate::config::IDENTIFIER_CHAR_REGEX_STR)).unwrap();

    /// string start up to a block comment begin or end, whichever comes first
    pub static ref BLOCK_COMMENT_BEGIN_OR_END_REGEX: Regex = Regex::new(r"/\*|\*/").unwrap();

    /// used to detect extra ".<hexdigit>" after a hexadecimal number
    pub static ref HEX_FRACTION_REGEX: Regex = Regex::new(r"^\.[\dA-Fa-f]").unwrap();

    /// used to detect extra ".<digit>" after an octal number
    pub static ref OCTAL_FRACTION_REGEX: Regex = Regex::new(r"^\.\d").unwrap();

    /// used to detect unneccesary leading zero on a decimal number
    pub static ref DECIMAL_UNNECESSARY_LEADING_ZERO_REGEX: Regex = Regex::new(r"^0\d").unwrap();
}

/// check whether something is one of the language's reserved words
pub fn is_reserved_word(word: &str) -> bool {
    KEYWORDS.binary_search(&word).is_ok()
}

// unit tests:
