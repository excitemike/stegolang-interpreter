use itertools::izip;

use crate::ast::Operator;
use crate::env::Env;
use crate::fntypes::fncode::FnCode;
use crate::hlir::hlirnodekind::{HlirLetData, HlirNodeKind};
use crate::hlir::HlirNode;
use crate::reporting::{print_errors, report_error};
use crate::sourceregion::SourceRegion;
use crate::stegoobject::StegoObject;
use crate::ti::arenas::Arenas;
use crate::ti::freetypevars::FreeTypeVars;
use crate::ti::TiError;
use crate::util::output_stream::OutputStream;
use crate::{ast::astnode::AstNode, parse};
use crate::{
    controlflow::{err, ControlFlow},
    loc,
};
use crate::{
    error::{
        Error, ErrorCode,
        ErrorCode::{EvaluationError, InitializationError, ParseError},
        EvalResult,
    },
    stegoobject::FnImpl,
};
use crate::{
    typedvalue::{OwnedValue, TypedValue, TypedValueKind},
    StringType,
};
use std::borrow::Borrow;
use std::rc::Rc;

/// do type inference and then interpret the syntax tree
fn infer_types_and_evaluate<'ctx>(
    ast: &AstNode,
    env: &mut Env<'ctx>,
) -> Result<TypedValue<'ctx>, ErrorCode> {
    let node = env.ti_ctx.make_hlir_from_ast(ast).map_err(|e| {
        report_error(&mut env.err_stream.as_write(), &Error::from(e));
        ErrorCode::TypeError
    })?;

    for warning in env.ti_ctx.warnings.take() {
        report_error(
            &mut env.err_stream.as_write(),
            &Error::warning_from(warning),
        );
    }

    let result = evaluate_ast(&node, env);
    match result {
        Ok(value) => Ok(value),
        Err(error) => {
            report_error(&mut env.err_stream.as_write(), &error);
            Err(EvaluationError)
        }
    }
}

/// both parse the code and evaluate the syntax tree
fn parse_and_evaluate<T, U>(
    code: T,
    file_label: U,
    out_stream: &mut dyn OutputStream,
    err_stream: &mut dyn OutputStream,
) -> Result<OwnedValue, ErrorCode>
where
    T: Into<String> + Clone + std::marker::Send + 'static,
    U: Into<StringType> + Clone + std::marker::Send + 'static,
{
    let arenas = Arenas::new();
    let mut env = {
        let env = Env::new(&arenas, out_stream, err_stream);
        if env.is_err() {
            let e = env.err().unwrap();
            writeln!(
                err_stream.as_write(),
                "{}\n{}",
                e.msg_tuple.0,
                e.msg_tuple.1
            )
            .unwrap();
            return Err(InitializationError);
        }
        env.unwrap()
    };
    let parse_result = parse::parse_with_env(file_label, code, &mut env);

    let err_stream = &mut env.err_stream.as_write();
    match parse_result {
        // error(s) in parsed code
        Err(errors) => {
            print_errors(errors.into_iter(), err_stream);
            Err(ParseError)
        }
        // successfully parsed
        Ok((ast, warnings)) => {
            print_errors(warnings.into_iter(), err_stream);
            if let Some(ast) = ast {
                let result = infer_types_and_evaluate(&ast, &mut env)?;
                Ok(result.to_owned())
            } else {
                Ok(OwnedValue::Nothing)
            }
        }
    }
}

/// both parse the code and evaluate the syntax tree
///
/// # Errors
///
/// If there are errors in parsing or evaluating the code, they will be printed
/// to `err_stream` and an error code returned
#[inline]
pub fn parse_and_evaluate_file<'a, T, U>(
    code: T,
    file_label: U,
    out_stream: &'a mut dyn OutputStream,
    err_stream: &'a mut dyn OutputStream,
) -> Result<(), ErrorCode>
where
    T: Into<String> + Clone + std::marker::Send + 'static,
    U: Into<String> + Clone + std::marker::Send + 'static,
{
    let file_label_string = file_label.into();
    let file_result = parse_and_evaluate(code, file_label_string.clone(), out_stream, err_stream)?;
    eprintln!("Result of file \"{file_label_string}\": {file_result:?}");
    Ok(())
}

/// both parse the code and evaluate the syntax tree
#[inline]
pub(crate) fn parse_and_evaluate_string<'ctx, T, U>(
    code: T,
    file_label: U,
    out_stream: &'ctx mut dyn OutputStream,
    err_stream: &'ctx mut dyn OutputStream,
) -> Result<OwnedValue, ErrorCode>
where
    T: Into<String> + Clone + std::marker::Send + 'static,
    U: Into<StringType> + Clone + std::marker::Send + 'static,
{
    parse_and_evaluate(code, file_label, out_stream, err_stream)
}

/// evaluate a syntax tree
/// TODO: type-erasure step before evaluating
pub(crate) fn evaluate_ast<'ctx>(
    ast: &HlirNode<'ctx>,
    env: &mut Env<'ctx>,
) -> Result<TypedValue<'ctx>, Error> {
    let mut eval: EvalCtx<'_, 'ctx> = EvalCtx::new(env);

    eval.env.scope.push();
    let result = match eval.evaluate_ast(ast) {
        Ok(x) | Err(ControlFlow::Exit(x)) => Ok(x),
        Err(ControlFlow::Return(_) | ControlFlow::Break(_) | ControlFlow::Continue) => {
            unreachable!()
        }
        Err(ControlFlow::Error(err)) => Err(err),
    };
    eval.env.scope.pop();
    result
}

/// evaluation context
pub(crate) struct EvalCtx<'envborrow, 'ctx>
where
    'ctx: 'envborrow,
{
    /// information about types in this program
    pub(crate) env: &'envborrow mut Env<'ctx>,
}

impl<'envborrow, 'ctx> EvalCtx<'envborrow, 'ctx> {
    /// create a new context
    pub fn new(env: &'envborrow mut Env<'ctx>) -> Self {
        EvalCtx { env }
    }

    /// evaluate the parsed syntax tree
    pub(crate) fn evaluate_ast(&mut self, ast_node: &HlirNode<'ctx>) -> EvalResult<'ctx> {
        let source_region = ast_node.get_source_region();
        let result = match &ast_node.kind {
            HlirNodeKind::ApplyExpr { func, args } => {
                self.evaluate_apply(func, args, source_region)
            }
            HlirNodeKind::AssignStmt { lhs, rhs } => self.do_assign_stmt(source_region, lhs, rhs),
            HlirNodeKind::BitwiseNot(ast) => self.do_bitwise_not(ast),
            HlirNodeKind::BinOpExpr { first, rest } => {
                self.eval_left_assoc_ast(first, &rest[..], source_region)
            }
            HlirNodeKind::ComparisonOpExpr { first, rest } => {
                self.eval_comp_op_ast(first, &rest[..], source_region)
            }
            HlirNodeKind::DictExpr(elems) => self.eval_dict_literal(elems, source_region),
            HlirNodeKind::DoExpr(statements) => match &statements.kind {
                HlirNodeKind::Statements { statements, .. } => self.evaluate_do(statements),
                _ => unreachable!(),
            },
            HlirNodeKind::DotExpr { lhs, method_name } => self.eval_dot(lhs, method_name),
            HlirNodeKind::FnStmt(fn_stmt) => self.evaluate_fn_stmt(fn_stmt, source_region),
            HlirNodeKind::Identifier {
                name,
                substitutions,
            } => {
                let substitutions = substitutions.iter().copied().collect();
                self.eval_identifier(name, ast_node.inferred_type, &substitutions, source_region)
            }
            HlirNodeKind::IfExpr {
                arms,
                catch_all_else,
            } => self.evaluate_if(arms, catch_all_else),
            HlirNodeKind::InExpr {
                container,
                invert,
                item,
            } => self.evaluate_in_operator(container, *invert, item, source_region),
            HlirNodeKind::Lambda {
                body,
                captures,
                params,
                placeholder_name,
            } => self.eval_lambda(
                ast_node.inferred_type,
                Rc::clone(body),
                captures,
                Rc::clone(params),
                source_region,
                placeholder_name,
            ),
            HlirNodeKind::Let(HlirLetData {
                pattern,
                value_expr,
                substitutions,
            }) => self.evaluate_let(pattern, value_expr, substitutions),
            HlirNodeKind::LiteralValue(literal_kind) => Ok(TypedValue {
                ty: self.env.ti_ctx.type_from_literal_kind(literal_kind),
                kind: TypedValueKind::Val(StegoObject::from(literal_kind.clone()).into()),
            }),
            HlirNodeKind::LogicalNot(node) => self.evaluate_logical_not(node),
            HlirNodeKind::NamedExpr { name, value } => {
                let x = self.evaluate_ast(value)?;
                self.insert_value(StringType::from(name), x.clone());
                Ok(x)
            }
            HlirNodeKind::ReturnStmt(None) => {
                Err(ControlFlow::Return(TypedValue::nothing(&self.env.ti_ctx)))
            }
            HlirNodeKind::ReturnStmt(Some(node)) => {
                Err(ControlFlow::Return(self.evaluate_ast(node)?))
            }
            HlirNodeKind::SetExpr(v) => self.eval_set_literal(v, source_region),
            HlirNodeKind::Statements { statements, .. } => self.evaluate_statements(statements),
            HlirNodeKind::Tuple(ast_v) => {
                let mut obj_v = Vec::new();
                for node in ast_v {
                    obj_v.push(self.evaluate_ast(node)?);
                }
                Ok(TypedValue::tuple(&mut self.env.ti_ctx, obj_v))
            }
            HlirNodeKind::TypeOf(operand) => self.eval_typeof(operand),
            HlirNodeKind::UnaryMinus(ast) => self.eval_unary_minus(ast),
            HlirNodeKind::UnaryPlus(ast) => self.evaluate_ast(ast),
            HlirNodeKind::UnitType => Ok(TypedValue::nothing(&self.env.ti_ctx)),
        }?;

        // sanity check
        if result.ty.could_be_instance(ast_node.inferred_type) {
            Ok(result)
        } else {
            err(
                loc::err::internal::type_error_eval_and_ti_disagree(
                    &result.ty.to_string(),
                    &ast_node.inferred_type.to_string(),
                ),
                source_region.clone(),
            )
        }
    }

    /// evaluate a logical not node
    fn evaluate_logical_not<'a>(
        &'a mut self,
        node: &HlirNode<'ctx>,
    ) -> Result<TypedValue<'ctx>, ControlFlow<'ctx>> {
        match self.evaluate_ast(node) {
            Ok(x) => match &x.kind {
                TypedValueKind::Val(obj) => {
                    let truthy: StegoObject = (!obj.is_truthy()).into();
                    Ok(TypedValue {
                        ty: self.env.ti_ctx.mk_bool(),
                        kind: TypedValueKind::Val(truthy.into()),
                    })
                }
                _ => todo!("handle error"),
            },
            Err(x) => Err(x),
        }
    }

    /// evaluate a chain of left-associative operators
    pub(crate) fn eval_left_assoc_ast(
        &mut self,
        first: &HlirNode<'ctx>,
        rest: &[(Operator, HlirNode<'ctx>)],
        source_region: &SourceRegion,
    ) -> EvalResult<'ctx> {
        let mut lhs = self.evaluate_ast(first)?;
        for (op, node) in rest {
            let rhs = self.evaluate_ast(node)?;
            let op_result = self.do_op(*op, &lhs, &rhs, source_region)?;
            lhs = op_result;
        }
        Ok(lhs)
    }

    /// evaluate a chain of comparison operators
    pub(crate) fn eval_comp_op_ast(
        &mut self,
        first: &HlirNode<'ctx>,
        rest: &[(Operator, HlirNode<'ctx>)],
        source_region: &SourceRegion,
    ) -> EvalResult<'ctx> {
        let mut lhs = self.evaluate_ast(first)?;
        let mut good = true;
        for (op, node) in rest {
            let rhs = self.evaluate_ast(node)?;
            let op_result = self.do_op(*op, &lhs, &rhs, source_region)?;
            if let TypedValue {
                ty: _,
                kind: TypedValueKind::Val(obj),
            } = &op_result
            {
                if let StegoObject::True = &**obj {
                } else {
                    good = false;
                    break;
                }
            } else {
                good = false;
                break;
            };
            lhs = rhs;
        }
        Ok(TypedValue::bool(&self.env.ti_ctx, good))
    }

    /// evaluate a binary operator
    fn do_op(
        &mut self,
        op: Operator,
        lhs: &TypedValue<'ctx>,
        rhs: &TypedValue<'ctx>,
        sr: &SourceRegion,
    ) -> EvalResult<'ctx> {
        let (_method_type, fn_fam) = self
            .env
            .ti_ctx
            .find_op_method_for_eval(lhs.ty, rhs.ty, op, sr)?;

        let params = &[lhs.clone(), rhs.clone()];
        let fn_impl = fn_fam.find_implementation(params, sr)?;
        let result = self.evaluate_fn_impl(fn_impl, &params[..], sr)?;
        if op.get_invert() {
            result.logical_not(&self.env.ti_ctx).ok_or_else(|| {
                ControlFlow::err(
                    loc::err::internal::op_should_return_bool(op.to_str(), &result.to_string()),
                    sr.clone(),
                )
            })
        } else {
            Ok(result)
        }
    }

    /// evaluate a set of statements in order, return value is return value of
    /// last statement
    pub(crate) fn evaluate_statements(
        &mut self,
        statements: &[HlirNode<'ctx>],
    ) -> EvalResult<'ctx> {
        let mut result = TypedValue::nothing(&self.env.ti_ctx);
        for node in statements {
            result = self.evaluate_ast(node)?;
        }
        Ok(result)
    }

    /// Pushes a new child scope, calls f, then pops that scope
    pub(crate) fn with_scope<F, R>(&mut self, f: F) -> R
    where
        F: FnOnce(&mut EvalCtx<'_, 'ctx>) -> R,
    {
        self.env.scope.push();
        let result = f(self);
        self.env.scope.pop();
        result
    }

    /// evaluate a particular function implementation
    pub(crate) fn evaluate_fn_impl<T: Borrow<FnImpl<'ctx>>>(
        &mut self,
        fn_impl: T,
        arguments: &[TypedValue<'ctx>],
        sr: &SourceRegion,
    ) -> EvalResult<'ctx> {
        let fn_impl: &FnImpl = fn_impl.borrow();
        let result = self.with_scope(|ctx| {
            // insert captured vars into environment
            for (name, value) in &fn_impl.captures {
                ctx.env.scope.insert_upvalue(name.clone(), Rc::clone(value));
            }

            // TODO: push traceback info

            // run the function
            let result = match &fn_impl.code {
                // evaluate implementation statements
                FnCode::Statements(ast_node) => {
                    // destructure arguments into ctx
                    let v = (*fn_impl.pattern_set).borrow();
                    for (arg, pattern) in izip!(arguments, v.iter()) {
                        let destructure_results = pattern.destructure(arg).or_else(
                            |TiError {
                                 msg_tuple,
                                 source_region,
                             }| err(msg_tuple, source_region),
                        )?;
                        let destructure_results = destructure_results.unwrap(); // TODO: produce error if it fails
                        for (name, value) in destructure_results {
                            ctx.env.scope.insert(name, value);
                        }
                    }

                    let node_ref = ast_node.as_ref().borrow();
                    ctx.evaluate_ast(&node_ref)
                }

                // call native implementation
                FnCode::Native(f) => f(ctx, sr, arguments),
            };
            // TODO: pop traceback

            // convert Return(x) to x as we exit the function
            match result {
                Err(ControlFlow::Return(x)) => Ok(x),
                x => x,
            }
        })?;

        if result.ty.has_any_free_unification_variables(&[]) {
            err(
                loc::err::internal::cant_monomorphize(
                    &result.ty.to_string(),
                    &sr.file_label,
                    sr.start_line_number,
                    sr.start_column_number,
                ),
                sr.clone(),
            )
        } else {
            Ok(result)
        }
    }

    /// get a mutable reference to an item in the nearest scope we find it in
    ///
    pub(crate) fn get_mut<Q>(&mut self, key: &Q) -> Option<std::cell::RefMut<'_, TypedValue<'ctx>>>
    where
        Q: ?Sized + Ord,
        StringType: Borrow<Q>,
    {
        self.env.scope.get_mut(key)
    }

    /// add an item to the current scope
    ///
    pub(crate) fn insert_value<Q>(&mut self, name: Q, value: TypedValue<'ctx>)
    where
        Q: Into<StringType> + Ord,
    {
        self.env.scope.insert(name, value);
    }

    /// retrieve an item, if possible
    ///
    pub(crate) fn get<Q>(&mut self, key: &Q) -> Option<std::cell::Ref<'_, TypedValue<'ctx>>>
    where
        Q: ?Sized + Ord,
        StringType: Borrow<Q>,
    {
        self.env.scope.get(key)
    }
}
