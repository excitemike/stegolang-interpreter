use crate::{
    error::{Error, Severity},
    sourceregion::SourceRegion,
    typedvalue::TypedValue,
};
use std::borrow::Cow;

/// the return statement breaks control flow like an error
pub(crate) enum ControlFlow<'ctx> {
    /// End the program, resolving to the provided value
    #[allow(dead_code)] // TODO: use this variant
    Exit(TypedValue<'ctx>),

    /// End the current function, resolving to the provided value
    Return(TypedValue<'ctx>),

    /// End the current loop, resolving to the provided value
    #[allow(dead_code)] // TODO: use this variant
    Break(TypedValue<'ctx>),

    /// Skip what remains of the current iteration through the loop
    #[allow(dead_code)] // TODO: use this variant
    Continue,

    /// End the program, producing the provided error
    Error(Error),
}

impl<'ctx> ControlFlow<'ctx> {
    pub(crate) fn err<S1, S2>(msg_tuple: (S1, S2), source_region: SourceRegion) -> Self
    where
        S1: Into<Cow<'static, str>>,
        S2: Into<Cow<'static, str>>,
    {
        ControlFlow::Error(Error {
            short_msg: msg_tuple.0.into(),
            help: msg_tuple.1.into(),
            source_region,
            severity: Severity::Error,
        })
    }
}

impl<'ctx> From<crate::ti::TiError> for ControlFlow<'ctx> {
    fn from(x: crate::ti::TiError) -> Self {
        let crate::ti::TiError {
            msg_tuple,
            source_region,
        } = x;
        ControlFlow::err(msg_tuple, source_region)
    }
}

/// create an `EvalResult` for an error
pub(crate) fn err<'ctx, S1, S2, T>(
    msg_tuple: (S1, S2),
    source_region: SourceRegion,
) -> Result<T, ControlFlow<'ctx>>
where
    S1: Into<Cow<'static, str>>,
    S2: Into<Cow<'static, str>>,
{
    Err(ControlFlow::err(msg_tuple, source_region))
}
