mod trie;
use proc_macro::TokenStream;
use syn::parse_macro_input;
use trie::Trie;

/// Macro to generate the nested match expressions to match strings to values
/// sort of like they were in a Trie structure.
/// Start with a `&str` expression, then a comma, then comma separated branches
/// that each consist of, a run of zero or more characters (not in quotes or
/// anything) and then either curly braces containing subbranches or a fat arrow
/// followed by the value.
///
/// Produces an `Option<T>`.
///
/// # EXAMPLE
///
/// ```
/// enum Symbol {
///     Aqualad,
///     Aquaman,
///     JustABat,
///     Batman,
///     Batmite,
///     WonderWoman
/// }
/// use Symbol::*;
/// fn identify(s:&str) -> Symbol {
///     trie!{s,
///         Aqua {
///             lad => Aqualad,
///             man => Aquaman,
///         },
///         Bat {
///             => JustABat
///             m {
///                 an => Batman,
///                 ite => Batmite
///             }
///         },
///         WonderWoman => WonderWoman
///     }
/// }
/// assert_eq!(Some(WonderWoman), identify("WonderWoman"));
/// assert_eq!(Some(JustABat), identify("Bat"));
/// assert_eq!(Some(Aquaman), identify("Aquaman"));
/// assert_eq!(None, identify("Ironman"));
/// ```
#[proc_macro]
pub fn trie(input: TokenStream) -> TokenStream {
    parse_macro_input!(input as Trie).0.into()
}
