use quote::{quote, ToTokens};
use syn::{
    braced,
    ext::IdentExt,
    parse::{Parse, ParseStream},
    Token,
};

pub(crate) struct Trie(pub(crate) proc_macro2::TokenStream);
struct TrieCase(proc_macro2::TokenStream);
struct TrieConsequent(proc_macro2::TokenStream);

impl Parse for Trie {
    fn parse(input: ParseStream<'_>) -> syn::Result<Self> {
        // `&str` expr
        let str_expr = input.parse::<syn::Expr>()?;
        // comma
        input.parse::<Token![,]>()?;
        // cases
        let cases = parse_trie_cases(input)?;
        let temp = quote! {{
            let mut i = #str_expr . chars();
            match i.next() {
                #cases
                _ => None
            }
        }};
        Ok(Self(temp))
    }
}

impl Parse for TrieCase {
    fn parse(input: ParseStream<'_>) -> syn::Result<Self> {
        let lookahead = input.lookahead1();
        let pattern = if lookahead.peek(syn::token::FatArrow) {
            String::new()
        } else {
            input.call(syn::Ident::parse_any)?.to_string()
        };
        // case 1: could start with a fat arrow. Example:
        // ```
        // trie!{s,
        //     => Empty
        // }
        // ````
        // case 2: Could map from end of string to value. Example:
        // ```
        // trie!{s,
        //     X => BigEcks,
        //     x => LittleEcks,
        // }
        // ````
        // case 3: Could have subtries. Like after 'bat' in this example:
        // ```
        // trie!{s,
        //     bat {
        //         => "animal",
        //         cave => "place",
        //         m {
        //             an => "person",
        //             obile => "vehicle",
        //         }
        //     }
        // }
        // ````
        let consequent = input.parse::<TrieConsequent>()?;
        Ok(Self(build_trie_case(&pattern, consequent)))
    }
}
impl quote::ToTokens for TrieCase {
    fn to_tokens(&self, tokens: &mut proc_macro2::TokenStream) {
        self.0.to_tokens(tokens);
    }
}

/// wrap the given subtrie in `match`es for the given pattern
fn build_trie_case(pattern: &str, consequent: TrieConsequent) -> proc_macro2::TokenStream {
    let mut output = consequent.0;
    let mut iter = pattern.chars();
    let first = iter.next();
    for c in iter.rev() {
        output = quote! {
            match i.next() {
                Some(#c) => {#output}
                _ => None
            }
        }
    }
    if let Some(c) = first {
        output = quote! {
            Some(#c) => {#output}
        }
    } else {
        output = quote! {
            None => {#output}
        }
    }
    output
}

/// Parse the cases of the trie definition.
/// Assumes you are already inside the curly braces.
fn parse_trie_cases(input: ParseStream<'_>) -> syn::Result<proc_macro2::TokenStream> {
    Ok(input
        .parse_terminated(TrieCase::parse, Token![,])?
        .to_token_stream())
}

impl Parse for TrieConsequent {
    /// Two cases handled by this
    /// 1) Could map from end of string to value. Example:
    ///     `=> Value`
    ///
    /// 2) Could be curly braces for a subtrie. Example:
    /// ```
    /// {
    ///     => "article",
    ///     ardvark => "animal",
    ///     rtist => "job"
    /// }
    /// ````
    /// both map to expression returning an `Option<T>`
    fn parse(input: ParseStream) -> syn::Result<Self> {
        // curly brace to branch further or fat arrow to mark the result value
        let lookahead = input.lookahead1();
        if lookahead.peek(syn::token::Brace) {
            let brace_content;
            let _brace_token = braced!(brace_content in input);
            let branches = parse_trie_cases(&brace_content)?;
            Ok(Self(quote! {
                match i.next() {
                    #branches
                    _ => None
                }
            }))
        } else if lookahead.peek(syn::token::FatArrow) {
            input.parse::<syn::token::FatArrow>()?;
            let expr = input.parse::<syn::Expr>()?;
            Ok(Self(quote!(
                if i.next().is_none() {
                    Some(#expr)
                } else {
                    None
                }
            )))
        } else {
            Err(lookahead.error())
        }
    }
}
