@echo off
cd /D "%~dp0"
cd ..
cargo clippy -- -W clippy::pedantic 2> pedantic-clippy-results.txt
echo Results written to pedantic-clippy-results.txt