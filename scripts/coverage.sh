#!/usr/bin/bash

# one-time setup
CARGOCMDS="$(cargo --list)"
if [ "$CARGOCMDS" != *"kcov"* ]; then
  cargo install cargo-kcov
  sudo apt-get install cmake g++ pkg-config jq
  sudo apt-get install libcurl4-openssl-dev libelf-dev libdw-dev binutils-dev libiberty-dev
  cargo kcov --print-install-kcov-sh | sh
fi

# get coverage
cargo kcov
echo DONE!
echo now open ./target/cov/index.html
echo