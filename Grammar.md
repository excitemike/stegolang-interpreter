# Grammar.md

> [!WARNING]
> **This is going to be incorrect most of the time until the project is much much further along!**

<!-- -->

> [!NOTE]
> TODO
>
> - [ ] generate this file from source code
> - [ ] traits and trait implementations

## Notation

I think this is something close to [PEG](https://en.wikipedia.org/wiki/Parsing_expression_grammar) notation:

| Syntax | Meaning |
|--------|---------|
| *a* *b* | sequence |
| *x* <sup>?</sup> | optional |
| *x* <sup>+</sup> | one or more repetitions |
| *x* <sup>*</sup> | zero or more repetitions |
| *a* / *b* | ordered choice |
| & | positive lookahead |
| ! | negative lookahead |
| ( *x* ) | grouping |
| `x` | terminal syntax |
| *x* | nonterminal symbol |
| *x* ← *y* | parsing rule |
| CURRENTINDENT | indented to the current indentation level |
| INDENT | increase required indentation level |
| DEDENT | decrease required indentation level |
| ANYTHING !*x* | anything at all up until the first occurrence of *x*
| **EOF** | end of input |

---

## Stegolang Grammar

This applies to code after it's been parsed out of the markdown.
And for the sake of the significant-whitespace constructs, instead of skipping all whitespace after a token, we skip *before*. This allows us to, for instance, specify the double newline in *repl_input*.

---

### Top Level

*file* ← *BOM* <sup>?</sup> *shebang* <sup>?</sup> *program*

*program* ← *statement_block* **EOF**

---

### Statements

*statement_block* ← *statement* ( *statement_sep* *statement* ) <sup>\*</sup>

*statement_sep* ← ( *newline* CURRENTINDENT ) / `;`

*statement* ←
    *fn_stmt*
    / *ty_stmt*
    / *async_stmt*
    / *loop_stmt*
    / *for_stmt*
    / *while_stmt*
    / *block*
    / *break_stmt*
    / *continue_stmt*
    / *return_stmt*
    / *assert_stmt*
    / *import_stmt*
    / *let_stmt*
    / *assign_stmt*

*fn_stmt* ← `fn` *named_pattern_expr*<sup>+</sup> `=` *statements*

*assign_stmt* ← ( *named_pattern_expr* `=` )<sup>?</sup> *named_expr*

---

 ### Comments

*comment* ← *line_comment* / *multiline_comment*

*line_comment* ← `//` ANYTHING !*newline*

*multiline_comment* ← `/*` ANYTHING !( `/*` / `*/` )  [*multiline_comment* ] `*/`

---

### Expression Precedence

*named_expr*             ←     ( *capture_pattern* `@` )<sup>?</sup> *typeof_expr*

*typeof_expr*            ←     `typeof`<sup>?</sup> *control_expr*

*control_expr*           ←     *do_expr* / *if_expr* / *lambda_expr* / *match_expr* / *for_expr* / *while_expr* / *logical_or_expr*

*lambda_expr* ← `\` *named_pattern_expr*<sup>+</sup> `=` *statements*

*logical_or_expr*        ←     *logical_or* / *logical_and_expr*

*logical_and_expr*       ←     *logical_and* / *logical_not_expr*

*logical_not_expr*       ←     *logical_not* / *comparison_expr*

*comparison_expr*        ←     *comparison* / *bitwise_or_expr*

*star_expr*              ←     `*` *bitwise_or_expr*

*bitwise_or_expr*        ←     *bitwise_or* / *bitwise_xor_expr*

*bitwise_xor_expr*       ←     *bitwise_xor* / *bitwise_and_expr*

*bitwise_and_expr*       ←     *bitwise_and* / *shift_expr*

*shift_expr*             ←     *shift* / *add_sub_expr*

*add_sub_expr*           ←     *add_sub* / *term_expr*

*term_expr*              ←     *term* / *factor_expr*

*factor_expr*            ←     *factor* / *power_expr*

*power_expr*             ←     *power* / *await_expr*

*await_expr*             ←     `await`<sup>?</sup> *apply_expr*

*dot_expr*               ←     *dotless_apply_expr* ( `.` *name*)<sup>?</sup>

*apply_expr*             ←     *dot_expr* ( *dot_expr* )<sup>*</sup>

*dotless_apply_expr*     ←     *paren_expr* ( *paren_expr* )<sup>*</sup>

*paren_expr*             ←     *paren* / *square_expr*

*square_expr*            ←     *square* / *curly_expr*

*curly_expr*             ←     *curly* / *literal*

---

### Expression Syntax

*expressions*            ←     ( *control_expr* / *star_expr* ) ( `,` ( *control_expr*  / *star_expr* ) )<sup>*</sup> `,`<sup>?</sup>

*logical_or*             ←     *logical_and_expr* ( ( `or` / `||` ) *logical_and_expr* )<sup>*</sup>

*logical_and*            ←     *logical_not_expr* ( ( `and` / `&&` ) *logical_not_expr* )<sup>*</sup>

*logical_not*            ←     ( `not` / `!` ) *term*

*comparison*             ←     *bitwise_or_expr* ( *comparison_op* *bitwise_or_expr* )<sup>*</sup>

*comparison_op*          ←     `<` / `>` / `==` / `>=` / `<=` / `<>` / `!=` / `in` / `not in`

*bitwise_or*             ←     *bitwise_xor_expr* ( `|` *bitwise_xor_expr* )<sup>*</sup>

*bitwise_xor*            ←     *bitwise_and_expr* ( `xor` *bitwise_and_expr* )<sup>*</sup>

*bitwise_and*            ←     *shift_expr* ( `&` *shift_expr* )<sup>*</sup>

*shift*                  ←     *add_sub_expr* ( ( `<<` / `>>` ) *add_sub_expr* )<sup>*</sup>

*add_sub*                ←     *term_expr* ( ( `+` / `-` ) *term_expr* )<sup>*</sup>

*term*                   ←     *factor_expr* ( ( `*` / `/` / `%` ) *factor_expr* )<sup>*</sup>

*factor*                 ←     ( `+` / `-` / `~` ) *power_expr*

*power*                  ←     *await_expr* `^` *factor_expr*

*paren*                  ←     `(` ( *control_expr* ( `,` *control_expr* )<sup>*</sup> `,`<sup>?</sup> )<sup>?</sup> `)`

*square*                 ←     `[` ( *list_comprehension* / *range* )<sup>?</sup> `]`

*curly*                  ←     `{` ( *curly_comprehension* )<sup>?</sup> `}`

*range*                  ←     *logical_or_expr* (`,` *logical_or_expr* )<sup>?</sup> .. ( *logical_or_expr* )<sup>?</sup>

*qualified_name*         ←     *name* ( `.` *name* )<sup>*</sup>

*name*                   ←     ( ( *letter* / `_` ) ( *letter* / *digit* / `_` / `'` )<sup>*</sup> !*reserved*

*literal*                ←     *boolean_literal* / *number_literal* / *string_literals*

*letter*                 ←     any ascii or Unicode letter

*digit*                  ←     any ascii or Unicode decimal digit

*reserved*               ←     `and` / `async` / `await` / `break` / `continue` / `data` / `else` / `for` / `if` / `in` / `match` / `matching` / `module` / `namespace` / `not` / `or` / `otherwise` / `struct` / `while` / `xor`

---

### Comprehensions

*list_comprehension*     ←     ( *named_expr* / *star_expr* ) ( *comprehension_clauses* / ( `,` ( *named_expr* / *star_expr* ) )<sup>*</sup> )

*comprehension_clauses*  ←     `for` *expressions* `in` *logical_or_expr* ( *comprehension_contd* )<sup>*</sup>

*comprehension_contd*    ←     `for` *expressions* `in` *logical_or_expr* / `if` *named_expr* / `matching` *match_pattern_exprs*

*curly_comprehension*    ←     *mapping_comprehension* / *set_comprehension*

*mapping_comprehension*  ←     *mapping_elem* ( *comprehension_clauses* / ( `,` *mapping_elem* )<sup>*</sup> `,`<sup>?</sup> )<sup>?</sup>

*mapping_elem*           ←     *kwspread_expr* / *keyvalue_pair*

*keyvalue_pair*          ←     *control_expr* `:` *control_expr*

*set_comprehension*      ←     ( *control_expr* / *star_expr* ) ( *comprehension_clauses* / ( `,` *expressions* ) )<sup>*</sup>

---

### Pattern Matching

*match_expr*             ←     `match` *named_expr* *match_cases*

*match_cases*            ←     *match_cases_sigws* / *match_cases_nosigws*

*match_cases_sigws*      ←     `:` INDENT *match_case_sigws* ( *newline* *match_case_sigws* )<sup>*</sup> DEDENT

*match_cases_nosigws*    ←     `[` *match_case_nosigws* ( `,` *match_case_nosigws* )<sup>*</sup> `]`

*match_case_sigws*       ←     *match_pattern_exprs* ( `if` *named_expr* )<sup>?</sup> `:` *block_sigws*

*match_case_nosigws*     ←     *match_pattern_exprs* ( `if` *named_expr* )<sup>?</sup> `:` *block_nosigws*

*match_pattern_exprs*    ←     *match_pattern_exprs* ( `,` *match_pattern_expr* )<sup>*</sup> ','<sup>?</sup>

*match_pattern_expr*     ←     *named_pattern_expr*

*named_pattern_expr*     ←     *name* `@` *or_pattern_expr*

*or_pattern_expr*        ←     *closed_pattern_expr* ( ( `or` / `|` ) *closed_pattern_expr* )<sup>*</sup>

*closed_pattern_expr*    ←     *rest_pattern* / *name* / *literal_pattern* / *qualified_name* / *paren_pattern* / *sequence_pattern* / *mapping_pattern* / *struct_pattern* / *adt_pattern*

*rest_pattern*           ←     `..` *name*

*literal_pattern*        ←     *literal*

*paren_pattern*          ←     `(` ( *match_pattern* ( `,` *match_pattern* )<sup>*</sup> `,`<sup>?</sup> )<sup>?</sup> `)`

*sequence_pattern*       ←     `[` ( *match_pattern* ( `,` *match_pattern* )<sup>*</sup> `,`<sup>?</sup> )<sup>?</sup> `]`

*mapping_pattern*        ←     `{` ( *keyvalue_pattern* ( `,` *keyvalue_pattern* )<sup>*</sup> `,`<sup>?</sup> )<sup>?</sup> `}`

*keyvalue_pattern*       ←     *rest_pattern* / *qualified_name* `:` *or_pattern_expr*

*struct_pattern*         ←     `{` *field_pattern* ( `,` *field_pattern* )<sup>*</sup> `}`

*field_pattern*          ←     *name* `=` *or_pattern_expr*

*adt_pattern*            ←     *qualified_name* ( *match_pattern_expr* )<sup>*</sup>

---

## Other

*BOM* ← `\u{EF}\u{BB}\u{BF}`

*shebang* ← `#!` ANYTHING !*newline*

*newline* ← `\r\n` / `\n`
